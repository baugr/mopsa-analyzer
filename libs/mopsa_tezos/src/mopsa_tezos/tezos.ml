open Common
open Sigs

module Make_tezos(MopsaTezos : TEZOS_PROTOCOL_VIEW): MOPSA_TEZOS =
struct
  include MopsaTezos

  (********************
   * Context handling *
   ********************)
  let context = ref None

  let get_context () =
      match !context with
      | None ->
        let ctx = MopsaTezos.memory_context () in
        context := Some (ctx);
        ctx
      | Some ctx ->
        ctx

  let set_context ctx =
    context := Some ctx

  let memory_context = MopsaTezos.memory_context

  let step_constants :
    source:string -> payer:string -> self:string -> amount:Int64.t -> chain_id:string ->
    step_constants =
    MopsaTezos.step_constants


  let parse_file ~context ~storage ~filename =
    let ast, mopsa_ast, parameter_ty, storage_ty, locations =
      MopsaTezos.parse_michelson_file ~context ~filename
    in
    (module struct
      type nonrec tezos_ast = tezos_ast
      type nonrec tezos_step_constants = step_constants
      type nonrec tezos_location = location
      type nonrec tezos_locations = locations

      type mopsa_ast = {
        statement: M_ast.statement;
        storage_ty : M_ast.typ;
        parameter_ty : M_ast.typ;
      }
      type nonrec log_element = log_element =
          Log of Mopsa_utils.Location.range * M_ast.stack
      let mopsa_ast = {
        statement = mopsa_ast;
        parameter_ty = parameter_ty;
        storage_ty = storage_ty;
      }
      let locations = locations
      let filename = filename
      let tezos_ast = ast
      let step_constants = step_constants

      let interpret_crowbar =
        MopsaTezos.interpret_crowbar ~filename ~locations (get_context())

      let interpret_crowbar_multiple =
        MopsaTezos.interpret_crowbar_multiple ~filename ~locations
          (get_context())
    end : TEZOS_PARSED_CONTRACT)
end

