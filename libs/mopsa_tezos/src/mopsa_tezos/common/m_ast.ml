open Mopsa_utils

type typ =
  | T_unit
  | T_bool
  | T_int
  | T_nat
  | T_mutez
  | T_string
  | T_bytes
  (* Domain specific *)
  | T_address
  | T_chain_id
  | T_key
  | T_key_hash
  | T_operation
  | T_signature
  | T_timestamp
  (* Containers *)
  | T_pair of typ * typ
  | T_union of typ * string option * typ * string option
  | T_option of typ
  | T_list of typ
  | T_set of typ
  | T_map of typ * typ
  | T_big_map of typ * typ
  (* Others *)
  | T_lambda of typ * typ
  | T_contract of typ

type annot = Var_annot of string

type stack_ty = ((annot option) * typ) list

type constant_kind =
  | C_unit
  | C_bool of bool
  | C_mutez of Z.t
  | C_nat of Z.t
  | C_int of Z.t
  | C_string of string
  | C_bytes of string
  | C_timestamp of Z.t
  (* TODO *)
  | C_signature of string
  | C_key of string
  | C_key_hash of string
  | C_address of string * string
  (* Containers *)
  | C_option of constant option
  | C_pair of constant * constant
  | C_union of union
  | C_list of constant list
  | C_set of constant list
  | C_map of (constant * constant) list
  | C_lambda of statement

and constant =
  {
    ctyp : typ;
    ckind: constant_kind
  }

and union =
  | L of constant
  | R of constant



and instruction =
  | Drop of typ
  | Dup of typ
  | Swap of typ * typ
  | Const of constant
  | Cons_pair of typ * typ
  | Car of typ
  | Cdr of typ
  | Unpair of typ
  | Cons_some of typ
  | Cons_none of typ
  | If_none of typ * statement * statement
  | Cons_left of typ
  | Cons_right of typ
  | If_left of typ * statement * statement
  | Cons_list of typ
  | Nil of typ
  | If_cons of typ * statement * statement
  | List_map of statement
  | List_iter of statement
  | List_size of typ
  | Empty_set of typ
  | Set_iter of statement
  | Set_mem of typ
  | Set_update of typ
  | Set_size
  | Empty_map of typ * typ
  | Map_map of statement
  | Map_iter of statement
  | Map_mem of typ * typ
  | Map_get of typ * typ
  | Map_update of typ * typ
  | Map_size of typ * typ
  | Empty_big_map
  | Big_map_mem
  | Big_map_get
  | Big_map_update
  | Concat_string
  | Concat_string_pair
  | Slice_string
  | String_size
  | Concat_bytes
  | Concat_bytes_pair
  | Slice_bytes
  | Bytes_size
  | Add_seconds_to_timestamp
  | Add_timestamp_to_seconds
  | Sub_timestamp_seconds
  | Diff_timestamps
  | Add_tez
  | Sub_tez
  | Mul_teznat
  | Mul_nattez
  | Ediv_teznat
  | Ediv_tez
  | Or
  | And
  | Xor
  | Not
  | Is_nat
  | Neg_nat
  | Neg_int
  | Abs_int
  | Int_nat
  | Add_intint
  | Add_intnat
  | Add_natint
  | Add_natnat
  | Sub_int
  | Mul_intint
  | Mul_intnat
  | Mul_natint
  | Mul_natnat
  | Ediv_intint
  | Ediv_intnat
  | Ediv_natint
  | Ediv_natnat
  | Lsl_nat
  | Lsr_nat
  | Or_nat
  | And_nat
  | And_int_nat
  | Xor_nat
  | Not_nat
  | Not_int
  | Seq of statement * statement
  | If of statement * statement
  | Loop of statement
  | Loop_left of statement
  | Dip of statement
  | Exec
  | Apply
  | Lambda of statement
  | Failwith of typ
  | Nop
  | Compare of typ
  | Eq
  | Neq
  | Lt
  | Gt
  | Le
  | Ge
  | Address
  | Contract of typ * string
  | Transfer_tokens
  | Create_account
  | Implicit_account
  | Create_contract of statement
  | Create_contract_2 of { parameter_ty: typ;
                           storage_ty: typ;
                           code: statement }
  | Set_delegate
  | Now
  | Balance
  | Check_signature
  | Hash_key
  | Pack
  | Unpack
  | Blake2b
  | Sha256
  | Sha512
  | Steps_to_quota
  | Source
  | Sender
  | Self of string
  | Amount
  | Dig of int
  | Dug of int
  | Dipn of int * statement
  | Dropn of int
  | ChainId
  | Level
  | Self_address
  | Voting_power
  | Total_voting_power
  | Keccak
  | Sha3

and statement =
  {
    instr: instruction;
    srange: Location.range;
    bef: stack_ty;
    aft: stack_ty;
  }

type stack = constant list

let mk_constant ~ty constant =
  { ctyp = ty; ckind = constant }

let rec pp_typ fmt = function
  | T_unit -> Format.fprintf fmt "unit"
  | T_bool -> Format.fprintf fmt "bool"
  | T_int -> Format.fprintf fmt "int"
  | T_nat -> Format.fprintf fmt "nat"
  | T_mutez -> Format.fprintf fmt "mutez"
  | T_string -> Format.fprintf fmt "string"
  | T_bytes -> Format.fprintf fmt "bytes"
  (* Domain specific *)
  | T_address -> Format.fprintf fmt "address"
  | T_chain_id -> Format.fprintf fmt "chain_id"
  | T_key -> Format.fprintf fmt "key"
  | T_key_hash -> Format.fprintf fmt "key_hash"
  | T_operation -> Format.fprintf fmt "operation"
  | T_signature -> Format.fprintf fmt "signature"
  | T_timestamp -> Format.fprintf fmt "timestamp"
  (* Containers *)
  | T_pair(t1, t2) -> Format.fprintf fmt "(%a,%a)" pp_typ t1 pp_typ t2
  | T_union(t1, _, t2, _) -> Format.fprintf fmt "(%a|%a)" pp_typ t1 pp_typ t2
  | T_option t -> Format.fprintf fmt "%a option" pp_typ t
  | T_list t -> Format.fprintf fmt "%a list" pp_typ t
  | T_set t -> Format.fprintf fmt "%a set" pp_typ t
  | T_map (t1, t2) -> Format.fprintf fmt "(%a,%a) map" pp_typ t1 pp_typ t2
  | T_big_map (t1, t2) -> Format.fprintf fmt "(%a,%a) bigmap" pp_typ t1 pp_typ t2
  (* Others *)
  | T_lambda (t1, t2) -> Format.fprintf fmt "(%a,%a) lambda" pp_typ t1 pp_typ t2
  | T_contract t -> Format.fprintf fmt "%a contract" pp_typ t

let pp_stack_ty fmt =
  (Format.pp_print_list (fun fmt (_, ty) -> pp_typ fmt ty)) fmt

let rec pp_constant fmt constant =
  match constant.ckind with
  | C_unit -> Format.fprintf fmt "unit"
  | C_bool b -> Format.fprintf fmt "%b" b
  | C_mutez z -> Format.fprintf fmt "(%a:mutez)" Z.pp_print z
  | C_int i -> Format.fprintf fmt "(%a:int)" Z.pp_print i
  | C_nat i -> Format.fprintf fmt "(%a:nat)" Z.pp_print i
  | C_pair (x, y) -> Format.fprintf fmt "(%a, %a)" pp_constant x pp_constant y
  | C_option (Some x) -> Format.fprintf fmt "Some (%a)" pp_constant x
  | C_option None -> Format.fprintf fmt "None"
  | C_list (l) ->
    Format.fprintf fmt "{ %a }"
      (fun fmt l ->
         Format.pp_print_list pp_constant ~pp_sep:(fun fmt () -> Format.fprintf fmt ";")
           fmt
           l
      ) l
  | C_set l ->
    Format.fprintf fmt "{ %a }"
      (fun fmt l ->
         Format.pp_print_list pp_constant ~pp_sep:(fun fmt () -> Format.fprintf fmt ";")
           fmt
           l
      ) l
  | C_map l ->
    Format.fprintf fmt "{ %a }"
      (fun fmt l ->
         Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ";")
           (fun fmt (k, v) -> Format.fprintf fmt "Elt %a %a"
               pp_constant k
               pp_constant v
           )
           fmt
           l
      ) l
  | C_string s -> Format.fprintf fmt "%s" s
  | C_bytes s -> Format.fprintf fmt "0x%s" s
  | C_address (s, entry) -> Format.fprintf fmt "\"%s%%%s\"" s entry
  | C_timestamp t -> Format.fprintf fmt "%a" Z.pp_print t
  | C_union (L v) -> Format.fprintf fmt "(Left %a)" pp_constant v
  | C_union (R v) -> Format.fprintf fmt "(Right %a)" pp_constant v
  | C_key_hash s -> Format.fprintf fmt "\"%s\"" s
  | C_lambda stm -> Format.fprintf fmt "{ %a }" pp_statement stm
  | _ -> assert false

and pp_instruction fmt instr =
  match instr with
  | Drop _ -> Format.fprintf fmt "DROP"
  | Dup _ -> Format.fprintf fmt "DUP"
  | Swap _ -> Format.fprintf fmt "SWAP"
  | Const(v) -> Format.fprintf fmt "PUSH %a" pp_constant v
  | Cons_pair _ -> Format.fprintf fmt "CONS_PAIR"
  | Car _ -> Format.fprintf fmt "CAR"
  | Cdr _ -> Format.fprintf fmt "CDR"
  | Unpair _ -> Format.fprintf fmt "UNPAIR"
  | Cons_some _ -> Format.fprintf fmt "CONS_SOME"
  | Cons_none _ -> Format.fprintf fmt "CONS_NONE"
  | If_none(ty, tstm, fstm) ->
    Format.fprintf fmt "IF_NONE { %a } { %a }" pp_statement tstm pp_statement fstm
  | Cons_left ty -> Format.fprintf fmt "LEFT %a" pp_typ ty
  | Cons_right ty -> Format.fprintf fmt "RIGHT %a" pp_typ ty
  | If_left (_, tstm, fstm) ->
    Format.fprintf fmt "IF_LEFT { %a } { %a }" pp_statement tstm pp_statement fstm
  | Cons_list _ -> Format.fprintf fmt "CONS_LIST"
  | Nil(ty) -> Format.fprintf fmt "NIL"
  | If_cons (ty, tstm, fstm) ->
    Format.fprintf fmt "IF_CONS { %a } { %a }" pp_statement tstm pp_statement fstm
  | List_map (stm) ->
    Format.fprintf fmt "LIST_MAP { %a }" pp_statement stm
  | List_iter (stm) ->
    Format.fprintf fmt "LIST_ITER { %a }" pp_statement stm
  | List_size _ -> Format.fprintf fmt "LIST_SIZE"
  | Empty_set _ -> Format.fprintf fmt "EMPTY_SET"
  | Set_iter (stm) -> Format.fprintf fmt "SET_ITER { %a }" pp_statement stm
  | Set_mem _ -> Format.fprintf fmt "SET_MEM"
  | Set_update _ -> Format.fprintf fmt "SET_UPDATE"
  | Set_size -> Format.fprintf fmt "SET_SIZE"
  | Empty_map _ -> Format.fprintf fmt "EMPTY_MAP"
  | Map_map (stm) -> Format.fprintf fmt "MAP_MAP { %a }" pp_statement stm
  | Map_iter (stm) -> Format.fprintf fmt "MAP_ITER { %a } " pp_statement stm
  | Map_mem _ -> Format.fprintf fmt "MAP_MEM"
  | Map_get _ -> Format.fprintf fmt "MAP_GET"
  | Map_update _ -> Format.fprintf fmt "MAP_UPDATE"
  | Map_size _ -> Format.fprintf fmt "MAP_SIZE"
  | Empty_big_map -> Format.fprintf fmt "EMPTY_BIG_MAP"
  | Big_map_mem -> Format.fprintf fmt "BIG_MAP_MEM"
  | Big_map_get -> Format.fprintf fmt "BIG_MAP_GET"
  | Big_map_update -> Format.fprintf fmt "BIG_MAP_UPDATE"
  | Concat_string -> Format.fprintf fmt "CONCAT_STRING"
  | Concat_string_pair -> Format.fprintf fmt "CONCAT_STRING_PAIR"
  | Slice_string -> Format.fprintf fmt "SLICE_STRING"
  | String_size -> Format.fprintf fmt "STRING_SIZE"
  | Concat_bytes -> Format.fprintf fmt "CONCAT_BYTES"
  | Concat_bytes_pair -> Format.fprintf fmt "CONCAT_BYTES_PAIR"
  | Slice_bytes -> Format.fprintf fmt "SLICE_BYTES"
  | Bytes_size -> Format.fprintf fmt "BYTES_SIZE"
  | Add_seconds_to_timestamp -> Format.fprintf fmt "ADD_SECONDS_TO_TIMESTAMP"
  | Add_timestamp_to_seconds -> Format.fprintf fmt "ADD_TIMESTAMP_TO_SECONDS"
  | Sub_timestamp_seconds -> Format.fprintf fmt "SUB_TIMESTAMP_SECONDS"
  | Diff_timestamps -> Format.fprintf fmt "DIFF_TIMESTAMPS"
  | Add_tez -> Format.fprintf fmt "ADD_TEZ"
  | Sub_tez -> Format.fprintf fmt "SUB_TEZ"
  | Mul_teznat -> Format.fprintf fmt "MUL_TEZNAT"
  | Mul_nattez -> Format.fprintf fmt "MUL_NATTEZ"
  | Ediv_teznat -> Format.fprintf fmt "EDIV_TEZNAT"
  | Ediv_tez -> Format.fprintf fmt "EDIV_TEZ"
  | Or -> Format.fprintf fmt "OR"
  | And -> Format.fprintf fmt "AND"
  | Xor -> Format.fprintf fmt "XOR"
  | Not -> Format.fprintf fmt "NOT"
  | Is_nat -> Format.fprintf fmt "IS_NAT"
  | Neg_nat -> Format.fprintf fmt "NEG_NAT"
  | Neg_int -> Format.fprintf fmt "NEG_INT"
  | Abs_int -> Format.fprintf fmt "ABS_INT"
  | Int_nat -> Format.fprintf fmt "INT_NAT"
  | Add_intint -> Format.fprintf fmt "ADD_INTINT"
  | Add_intnat -> Format.fprintf fmt "ADD_INTNAT"
  | Add_natint -> Format.fprintf fmt "ADD_NATINT"
  | Add_natnat -> Format.fprintf fmt "ADD_NATNAT"
  | Sub_int -> Format.fprintf fmt "SUB_INT"
  | Mul_intint -> Format.fprintf fmt "MUL_INTINT"
  | Mul_intnat -> Format.fprintf fmt "MUL_INTNAT"
  | Mul_natint -> Format.fprintf fmt "MUL_NATINT"
  | Mul_natnat -> Format.fprintf fmt "MUL_NATNAT"
  | Ediv_intint -> Format.fprintf fmt "EDIV_INTINT"
  | Ediv_intnat -> Format.fprintf fmt "EDIV_INTNAT"
  | Ediv_natint -> Format.fprintf fmt "EDIV_NATINT"
  | Ediv_natnat -> Format.fprintf fmt "EDIV_NATNAT"
  | Lsl_nat -> Format.fprintf fmt "LSL_NAT"
  | Lsr_nat -> Format.fprintf fmt "LSR_NAT"
  | Or_nat -> Format.fprintf fmt "OR_NAT"
  | And_nat -> Format.fprintf fmt "AND_NAT"
  | And_int_nat -> Format.fprintf fmt "AND_INT_NAT"
  | Xor_nat -> Format.fprintf fmt "XOR_NAT"
  | Not_nat -> Format.fprintf fmt "NOT_NAT"
  | Not_int -> Format.fprintf fmt "NOT_INT"
  | Seq(stm1, stm2) -> Format.fprintf fmt "%a@;%a" pp_statement stm1
                         pp_statement stm2
  | If(stm1, stm2) -> Format.fprintf fmt "IF { %a } { %a }" pp_statement stm1
                        pp_statement
                        stm2
  | Loop (stm) -> Format.fprintf fmt "LOOP { %a } " pp_statement stm
  | Loop_left (stm) -> Format.fprintf fmt "LOOP_LEFT { %a } " pp_statement stm
  | Dip (stm) -> Format.fprintf fmt "DIP { %a }" pp_statement stm
  | Exec -> Format.fprintf fmt "EXEC"
  | Apply -> Format.fprintf fmt "APPLY"
  | Lambda stm -> Format.fprintf fmt "LAMBDA { %a }" pp_statement stm
  | Failwith typ -> Format.fprintf fmt "FAILWITH"
  | Nop -> Format.fprintf fmt "NOP"
  | Compare _ -> Format.fprintf fmt "COMPARE"
  | Eq -> Format.fprintf fmt "EQ"
  | Neq -> Format.fprintf fmt "NEQ"
  | Lt -> Format.fprintf fmt "LT"
  | Gt -> Format.fprintf fmt "GT"
  | Le -> Format.fprintf fmt "LE"
  | Ge -> Format.fprintf fmt "GE"
  | Address -> Format.fprintf fmt "ADDRESS"
  | Contract (typ, entrypoint) -> Format.fprintf fmt "CONTRACT %s %a" entrypoint pp_typ typ
  | Transfer_tokens -> Format.fprintf fmt "TRANSFER_TOKENS"
  | Create_account -> Format.fprintf fmt "CREATE_ACCOUNT"
  | Implicit_account -> Format.fprintf fmt "IMPLICIT_ACCOUNT"
  | Create_contract(stm) -> Format.fprintf fmt "CREATE_CONTRACT { %a }"
                              pp_statement stm
  | Create_contract_2 { code; _ } ->
    Format.fprintf fmt "CREATE_CONTRACT { %a } " pp_statement code
  | Set_delegate -> Format.fprintf fmt "SET_DELEGATE"
  | Now -> Format.fprintf fmt "NOW"
  | Balance -> Format.fprintf fmt "BALANCE"
  | Check_signature -> Format.fprintf fmt "CHECK_SIGNATURE"
  | Hash_key -> Format.fprintf fmt "HASH_KEY"
  | Pack -> Format.fprintf fmt "PACK"
  | Unpack -> Format.fprintf fmt "UNPACK"
  | Blake2b -> Format.fprintf fmt "BLAKE2B"
  | Sha256 -> Format.fprintf fmt "SHA256"
  | Sha512 -> Format.fprintf fmt "SHA512"
  | Steps_to_quota -> Format.fprintf fmt "STEPS_TO_QUOTA"
  | Source -> Format.fprintf fmt "SOURCE"
  | Sender -> Format.fprintf fmt "SENDER"
  | Self entrypoint -> Format.fprintf fmt "SELF(%S)" entrypoint
  | Amount -> Format.fprintf fmt "AMOUNT"
  | Dig(n) -> Format.fprintf fmt "DIG(%d)" n
  | Dug(n) -> Format.fprintf fmt "DUG(%d)" n
  | Dipn(n, stm) -> Format.fprintf fmt "DIP(%d) { %a} " n pp_statement stm
  | Dropn(n) -> Format.fprintf fmt "DROP(%d)" n
  | ChainId -> Format.fprintf fmt "CHAIN_ID"
  | Level -> Format.fprintf fmt "LEVEL"
  | Self_address -> Format.fprintf fmt "SELF_ADDRESS"
  | Voting_power -> Format.fprintf fmt "VOTING_POWER"
  | Total_voting_power -> Format.fprintf fmt "TOTAL_VOTING_POWER"
  | Keccak -> Format.fprintf fmt "KECCAK"
  | Sha3 -> Format.fprintf fmt "SHA3"

and pp_statement fmt stmt =
  pp_instruction fmt stmt.instr

let pp_stack fmt stack =
  Format.fprintf fmt "@[<hv 2>";
  List.iter (pp_constant fmt) stack;
  Format.fprintf fmt "@]"
