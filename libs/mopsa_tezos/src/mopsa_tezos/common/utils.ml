let read_lines fd =
  let rec loop (lineno, acc) fd =
    try
      let line = Stdlib.input_line fd in
      loop (lineno+1,line::acc) fd
    with End_of_file ->
      close_in fd;
      String.concat "\n" (List.rev acc)
  in
  loop (1, []) fd


(* FIXME: move elsewhere *)
module RangeMap = Map.Make(struct
    type t = Mopsa_utils.Location.range
    let compare = Mopsa_utils.Location.compare_range
  end)

module StringMap = Stdlib.Map.Make(String)

