open Mopsa_utils

(*
 * Signature required for tezos protocol implementation
 * as provided in libs/tezos_007 for example
 *)
module type TEZOS_PROTOCOL_VIEW = sig

  (**********************************************************
   * abstract types for the required types from the protocol
   **********************************************************)
  type context
  type location

  type locations =
    (location *
     (Tezos_micheline.Micheline_parser.location * int list)) list

  type tezos_ast
  type step_constants

  type log_element =
      Log of Mopsa_utils.Location.range * M_ast.stack


  (**********************************************************
   * Exported functions from tezos
   **********************************************************)
  val memory_context : unit -> context

  val step_constants :
    source:string ->
    payer:string ->
    self:string ->
    amount:Int64.t ->
    chain_id:string ->
    step_constants

  val parse_michelson_file:
    context:context ->
    filename:string ->
    tezos_ast * M_ast.statement * M_ast.typ * M_ast.typ * locations

  val interpret_crowbar :
    filename:string ->
    locations:locations ->
    context ->
    step_constants ->
    tezos_ast ->
    (log_element list) Crowbar.gen

  val interpret_crowbar_multiple :
    filename:string ->
    locations:locations ->
    context ->
    step_constants ->
    tezos_ast ->
    int ->
    (log_element list) list Crowbar.gen
end

(*
 * Signature for a module which is the result of a parsing by the tezos
 * michelson parser.
 * It will be embedded in the mopsa ast
 *)
module type TEZOS_PARSED_CONTRACT = sig

  type tezos_location
  type tezos_locations
  type tezos_ast
  type tezos_step_constants

  type log_element = Log of Mopsa_utils.Location.range * M_ast.stack

  type mopsa_ast = {
    statement: M_ast.statement;
    storage_ty: M_ast.typ;
    parameter_ty : M_ast.typ;
  }

  val filename  : string

  val locations : tezos_locations
  val mopsa_ast : mopsa_ast
  val tezos_ast : tezos_ast


  val step_constants :
    source:string ->
    payer:string ->
    self:string ->
    amount:Int64.t ->
    chain_id:string ->
    tezos_step_constants

  val interpret_crowbar :
    tezos_step_constants ->
    tezos_ast ->
    (log_element list) Crowbar.gen

  val interpret_crowbar_multiple :
    tezos_step_constants ->
    tezos_ast ->
    int ->
    (log_element list) list Crowbar.gen
end

(*
 * Signature to allow mopsa accessing Tezos features in a controlled environment
 *)
module type MOPSA_TEZOS = sig

  (**********************************************************
   * abstract types for the required types from the protocol
   **********************************************************)
  type context

  val memory_context : unit -> context

  (* FIXME: remove storage *)
  val parse_file:
    context:context ->
    storage:string option ->
    filename:string ->
    (module TEZOS_PARSED_CONTRACT)

end

