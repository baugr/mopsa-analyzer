open Mopsa_utils
open Common
open Tezos_007_common

let debug args = Debug.debug ~channel:"michelson.parser.translate" args

let translate_location filename locations loc =
  let open Tezos_micheline.Micheline_parser in
  match List.assoc_opt loc locations with
  | None ->
    Exceptions.panic "cannot find location %d\n" loc;
  | Some ({start; stop; }, _) ->
    let start = Location.mk_pos filename start.line start.column in
    let stop = Location.mk_pos filename stop.line stop.column in
    Location.mk_orig_range start stop

let rec translate_ty:
  type a . a Script_typed_ir.ty -> M_ast.typ =
  function
  | Script_typed_ir.Unit_t _ -> T_unit
  | Int_t _ -> T_int
  | Nat_t _ -> T_nat
  | Signature_t _ -> T_signature
  | String_t _ -> T_string
  | Bytes_t _ -> T_bytes
  | Mutez_t _ -> T_mutez
  | Key_hash_t _ -> T_key_hash
  | Key_t _ -> T_key
  | Timestamp_t _ -> T_timestamp
  | Address_t _ -> T_address
  | Bool_t _ -> T_bool
  | Pair_t ((tyl, _, _), (tyr, _, _), _) ->
    T_pair (translate_ty tyl, translate_ty tyr)
  | Union_t ((tyl, fl), (tyr, fr), _) ->
    let fl = Option.map (fun (Script_typed_ir.Field_annot s) -> s) fl in
    let fr = Option.map (fun (Script_typed_ir.Field_annot s) -> s) fr in
    T_union (translate_ty tyl, fl, translate_ty tyr, fr)
  | Lambda_t (arg, ret, _) ->
    T_lambda (translate_ty arg, translate_ty ret)
  | Option_t (ty, _) ->
    T_option (translate_ty ty)
  | List_t(ty, _) ->
    T_list (translate_ty ty)
  | Set_t(ty, _) ->
    let ty = ty_of_comparable_ty ty in
    T_set (translate_ty ty)
  | Map_t(tyk, tyv, _) ->
    let tyk = ty_of_comparable_ty tyk in
    T_map (translate_ty tyk, translate_ty tyv)
  | Big_map_t(tyk, tyv, _) ->
    let tyk = ty_of_comparable_ty tyk in
    T_big_map(translate_ty tyk, translate_ty tyv)
  | Contract_t(ty, _) ->
    T_contract(translate_ty ty)
  | Operation_t _ ->
    T_operation
  | Chain_id_t _ ->
    T_chain_id

let translate_annot v =
  Option.map (fun (Script_typed_ir.Var_annot s) -> M_ast.Var_annot s) v

let find_greatest_loc locations =
  List.fold_left (fun acc (loc, _) ->
      if acc > loc then
        acc
      else
        loc
    ) (-1) locations

let fix_locations :
  type a b . string -> locations -> (a, b) Script_typed_ir.descr ->
  (a, b) Script_typed_ir.descr * locations =
  fun filename locations instr ->
  let open Tezos_micheline.Micheline_parser in
  let open Script_typed_ir in
  let loc_pair_of_loc loc =
    match List.assoc_opt loc locations with
    | None -> None, None
    | Some ({ start; stop }, _) ->
      let start = Location.mk_pos filename start.line start.column in
      let stop = Location.mk_pos filename stop.line stop.column in
      (Some start, Some stop)
  in
  let new_location locations loc_max loc start_loc stop_loc =
    if loc <> -1 then
      (* FIXME: should replace returned start_loc by location from loc *)
      locations, loc_max, loc, start_loc, stop_loc
    else
      let start_loc, stop_loc =
        match start_loc, stop_loc with
        | Some start_loc, Some stop_loc -> start_loc, stop_loc
        | _ -> assert false
      in
      let mloc = loc_max + 1 in
      let startp = Location.{ point=0;
                              line=start_loc.pos_line;
                              column=start_loc.pos_column ;
                              byte = 0 }
      in
      let stopp = Location.{ point=0;
                             line=stop_loc.pos_line;
                             column=stop_loc.pos_column;
                             byte = 0 }
      in
      let newloc = { start = startp; stop = stopp } in
      ((mloc,(newloc,[mloc]))::locations), mloc, mloc, Some start_loc, Some stop_loc
  in
  let rec loop : type a b .
    (a, b) descr ->
    locations ->
    int ->
    (Location.pos option * Location.pos option) ->
    (a, b) descr * Location.pos option * Location.pos option * int * locations
    =
    fun (Script_typed_ir.{instr; loc; aft; bef } as desc) locations loc_max (start, stop) ->
      let process1_inner stm loc loc_max start stop =
        if loc = -1 then
          let stm, istart, istop, loc_max, locations = loop stm locations loc_max (start, stop) in
          let locations, loc_max, loc, start, stop = new_location locations loc_max (-1) istart istop in
          stm, loc, start, stop, loc_max, locations
        else
          let start, stop = loc_pair_of_loc loc in
          let stm, _, _, loc_max, locations = loop stm locations loc_max (start, stop) in
          stm, loc, start, stop, loc_max, locations
      in
      let process2_inner tstm fstm loc loc_max start stop =
        if loc = -1 then
          let tstm, start1, _, loc_max, locations = loop tstm locations loc_max (start, None) in
          let fstm, _ , stop2, loc_max, locations = loop fstm locations loc_max (None, stop) in
          let locations, loc_max, loc, start, stop = new_location locations loc_max (-1) start1 stop2 in
          tstm, fstm, loc, start, stop, loc_max, locations
        else
          let start, stop = loc_pair_of_loc loc in
          let tstm, _, _, loc_max, locations = loop tstm locations loc_max (start, stop) in
          let fstm, _, _, loc_max, locations = loop fstm locations loc_max (start, stop) in
          tstm, fstm, loc, start, stop, loc_max, locations
      in
      let instr, loc, start, stop, loc_max, locations = match instr, loc with
        | Seq(stm1, stm2), -1 ->
          let stm1, start1, _, loc_max, locations = loop stm1 locations loc_max (start, None) in
          let stm2, _ , stop2, loc_max, locations = loop stm2 locations loc_max (None, stop) in
          let locations, loc_max, loc, start, stop = new_location locations loc_max (-1) start1 stop2 in
          Seq (stm1, stm2), loc, start, stop, loc_max, locations
        | Seq(stm1, stm2), loc ->
          let start, stop = loc_pair_of_loc loc in
          let stm1, _, _, loc_max, locations = loop stm1 locations loc_max (start, None) in
          let stm2, _, _, loc_max, locations = loop stm2 locations loc_max (None, stop) in
          Seq(stm1, stm2), loc, start, stop, loc_max, locations
        | If_cons(tstm, fstm), loc ->
          let tstm, fstm, loc, start, stop, loc_max, locations = process2_inner tstm fstm loc loc_max start stop in
          If_cons(tstm, fstm), loc, start, stop, loc_max, locations
        | If_none(tstm, fstm), loc ->
          let tstm, fstm, loc, start, stop, loc_max, locations = process2_inner tstm fstm loc loc_max start stop in
          If_none(tstm, fstm), loc, start, stop, loc_max, locations
        | If_left(tstm, fstm), loc ->
          let tstm, fstm, loc, start, stop, loc_max, locations = process2_inner tstm fstm loc loc_max start stop in
          If_left(tstm, fstm), loc, start, stop, loc_max, locations
        | List_map stm, loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          List_map stm, loc, start, stop, loc_max, locations
        | List_iter stm, loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          List_iter stm, loc, start, stop, loc_max, locations
        | Set_iter stm, loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          Set_iter stm, loc, start, stop, loc_max, locations
        | Map_map stm, loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          Map_map stm, loc, start, stop, loc_max, locations
        | Map_iter stm, loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          Map_iter stm, loc, start, stop, loc_max, locations
        | If(tstm, fstm), loc ->
          let tstm, fstm, loc, start, stop, loc_max, locations = process2_inner tstm fstm loc loc_max start stop in
          If(tstm, fstm), loc, start, stop, loc_max, locations
        | Loop stm, loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          Loop stm, loc, start, stop, loc_max, locations
        | Loop_left stm, loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          Loop_left stm, loc, start, stop, loc_max, locations
        | Dip stm, loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          Dip stm, loc, start, stop, loc_max, locations
        | Dipn(n, witness, stm), loc ->
          let stm, loc, start, stop, loc_max, locations = process1_inner stm loc loc_max start stop in
          Dipn(n, witness,stm), loc, start, stop, loc_max, locations
        | Nop, -1 ->
          begin match stop with
            | None -> Exceptions.panic "fix_locations: found a nop without location. No surrounding location available"
            | Some _loc ->
              let locations, loc_max, loc, start, stop = new_location locations loc_max (-1) stop stop in
              Nop, loc, start, stop, loc_max, locations
          end
        | Nop, loc ->
          (* Use stop location from loc, and use as start and stop location *)
          let _, stop = loc_pair_of_loc loc in
          let locations, loc_max, loc, start, stop = new_location locations loc_max (-1) stop stop in
          instr, loc, start, stop, loc_max, locations
        | _, -1 ->
          Exceptions.panic "No location:@.%a"
            (pp_michelson_loc locations ~printloc:false ~fullloc:false) desc
        | instr, loc ->
          begin let start, stop = loc_pair_of_loc loc in
            begin match start, stop with
              | Some start, Some stop -> debug "Location: %a -> %a"
                                           Location.pp_position start
                                           Location.pp_position stop
              | _ -> debug "Location: None, None"
            end;
            debug "%a" (pp_michelson_loc locations ~printloc:true ~fullloc:false) desc;
            instr, loc, start, stop, loc_max, locations
          end
      in
      { instr; loc; aft; bef }, start, stop, loc_max, locations
  in
  let start, stop = loc_pair_of_loc instr.loc in
  let start, stop = match start, stop with
    | None, None -> Exceptions.panic "Cannot find initial locations";
    | _ -> start, stop
  in
  let loc_max = find_greatest_loc locations in
  let instr, _, _, _ , locations = loop instr locations loc_max (start, stop) in
  debug "@[<v 0>fixed locations:@;%a@;@]---\n"
    pp_locations locations;
  debug "@[<v 0>fixed michelson:@;%a@;@]---\n"
    (pp_michelson_loc locations ~printloc:true ~fullloc:true) instr;
  instr, locations

let rec translate_value :
  type a . locations -> a Script_typed_ir.ty -> a -> M_ast.constant =
  fun locations ty value ->
  let open M_ast in
  match ty, value with
  | Script_typed_ir.Unit_t _, () ->
    mk_constant ~ty:T_unit C_unit
  | Bool_t _ , b ->
    mk_constant ~ty:T_bool (C_bool b)
  | Int_t _, i ->
    mk_constant ~ty:T_int (C_int (Script_int.to_zint i))
  | Nat_t _, z  ->
    mk_constant ~ty:T_nat (C_nat (Script_int.to_zint z))
  | Signature_t _, s ->
    mk_constant ~ty:T_signature (C_signature (Signature.to_string s))
  | String_t _, s ->
    mk_constant ~ty:T_string (C_string s)
  | Bytes_t _ , b ->
    let s =
      Format.asprintf "%s"
      ((Seq.fold_left
          (fun acc c ->
             Format.asprintf "%s%2x" acc (Char.code c)) "" (Bytes.to_seq b))
      )
    in
    mk_constant ~ty:T_bytes (C_bytes s)
  | Mutez_t _, m ->
    mk_constant ~ty:T_mutez (C_mutez (Z.of_int64 (Tez.to_mutez m)))
  | Key_hash_t _, v ->
    mk_constant ~ty:T_key_hash (C_key_hash (Key_hash.to_b58check v))
  | Key_t _ , v ->
    mk_constant ~ty:T_key (C_key (Key.to_b58check v))
  | Timestamp_t _, v ->
    mk_constant ~ty:T_timestamp (C_timestamp (Timestamp.to_zint v))
  | Address_t _ , (contract, entry) ->
    mk_constant ~ty:T_address (C_address (Contract.to_b58check contract, entry))
  | Pair_t ((t1, _, _), (t2, _, _), _), (a, b) ->
    let l = translate_value locations t1 a in
    let r = translate_value locations t2 b in
    mk_constant ~ty:(T_pair(l.ctyp, r.ctyp)) (C_pair (l, r))
  | Union_t ((ty_l, _), (_, _), _), L v ->
    let ty = translate_ty ty in
    let v = translate_value locations ty_l v in
    mk_constant ~ty (C_union (L v))
  | Union_t ((_, _), (ty_r, _), _), R v ->
    let ty = translate_ty ty in
    let v = translate_value locations ty_r v in
    mk_constant ~ty (C_union (R v))
  | Option_t (ty, _), None ->
    let ty = translate_ty ty in
    mk_constant ~ty:(T_option(ty)) (C_option None)
  | Option_t (t, _), Some x ->
    let v = translate_value locations t x in
    mk_constant ~ty:(T_option v.ctyp) (C_option (Some v))
  | List_t (ty, _), { elements; _ } ->
    let ty' = translate_ty ty in
    let items = List.map (fun item ->
        translate_value locations ty item
      ) elements
    in
    mk_constant ~ty:(T_list ty') (C_list items)
  | Set_t (cty, _), (module Boxed_set) ->
    let ty = ty_of_comparable_ty cty in
    let ty' = translate_ty ty in
    let items = Boxed_set.OPS.fold (fun item acc ->
        let item = translate_value locations ty item in
        item::acc
      ) Boxed_set.boxed []
    in
    mk_constant ~ty:(T_set ty') (C_set (List.rev items))
  | Map_t (ckty, vty, _),  (module Boxed_map) ->
    let kty = ty_of_comparable_ty ckty in
    let items = Boxed_map.OPS.fold (fun key value acc ->
        let key = translate_value locations kty key in
        let value = translate_value locations vty value in
        (key, value)::acc
      ) (fst Boxed_map.boxed) []
    in
    let ty = translate_ty ty in
    mk_constant ~ty (C_map (List.rev items))
  | Big_map_t _ , _ ->
    Exceptions.panic "not yet supported: big_map"
  | Contract_t _ , _ -> Exceptions.panic "not yet supported: contract"
  | Operation_t _ , _-> Exceptions.panic "not yet supported: operation"
  | Chain_id_t _ , _-> Exceptions.panic "not yet supported: chain_id"
  | Lambda_t (_lty, _, _), lambda ->
    let ty = translate_ty ty in
    let lambda = translate_lambda "fn" locations lambda in
    mk_constant ~ty (C_lambda lambda)

and translate_instr :
  type a b . string -> locations -> (a, b) Script_typed_ir.descr -> M_ast.statement =
  fun filename locations instr ->
  let mk_statement instr loc bef aft =
    let srange = translate_location filename locations loc in
    let bef = translate_stack_ty bef in
    let aft = translate_stack_ty aft in
    let result = M_ast.{ instr; srange; bef; aft } in
    result
  in
  let rec loop : type a b .
    (a, b) Script_typed_ir.descr -> M_ast.statement =
    fun ({instr; loc; aft; bef }) ->
      (* debug "looping on @[<v 0>@;[%a]@] loc = %d" (pp_michelson_loc locations true) desc loc; *)
      let instr = match instr with
        (* stack ops *)
        | Script_typed_ir.Drop ->
          let Item_t (ty, _, _) = bef in
          mk_statement (Drop (translate_ty ty)) loc bef aft
        | Dup ->
          let Item_t (ty, _, _) = bef in
          mk_statement (Dup (translate_ty ty)) loc bef aft
        | Swap ->
          let Item_t (ty1, Item_t(ty2, _, _), _) = bef in
          mk_statement (Swap (translate_ty ty1, translate_ty ty2)) loc bef aft
        | Const v ->
          let Item_t (ty, _, _) = aft in
          let value = translate_value locations ty v in
          mk_statement (Const value) loc bef aft
        | Cons_pair ->
          let ty1, ty2 = match aft with
            | Item_t (Pair_t ((ty1, _, _), (ty2, _, _), _), _, _) -> ty1, ty2
            | _ -> assert false
          in
          let ty1 = translate_ty ty1 in
          let ty2 = translate_ty ty2 in
          mk_statement (Cons_pair (ty1, ty2)) loc bef aft
        | Car ->
          let Item_t (ty, _, _) = bef in
          mk_statement (Car (translate_ty ty)) loc bef aft
        | Cdr ->
          let Item_t (ty, _, _) = bef in
          mk_statement (Cdr (translate_ty ty)) loc bef aft
        | Cons_some ->
          let Item_t (ty, _, _) = aft in
          mk_statement (Cons_some (translate_ty ty)) loc bef aft
        | Cons_none _ ->
          let Item_t (ty, _, _) = aft in
          mk_statement (Cons_none (translate_ty ty)) loc bef aft
        | If_none(tstm, fstm) ->
          let Item_t (ty, _, _) = bef in
          mk_statement (If_none (translate_ty ty,
                                 loop tstm,
                                 loop fstm)) loc bef aft
        | Cons_left ->
          let Item_t (ty, _, _) = aft in
          mk_statement (Cons_left (translate_ty ty)) loc bef aft
        | Cons_right ->
          let Item_t (ty, _, _) = aft in
          mk_statement (Cons_right (translate_ty ty)) loc bef aft
        | If_left(tstm, fstm) ->
          let Item_t (ty, _, _) = bef in
          mk_statement (If_left (translate_ty ty, loop tstm, loop fstm)) loc bef aft
        | Cons_list ->
          let Item_t (ty, _, _) = aft in
          mk_statement (Cons_list (translate_ty ty)) loc bef aft
        | Nil ->
          let Item_t (ty, _, _) = aft in
          mk_statement (Nil (translate_ty ty)) loc bef aft
        | If_cons(tstm, fstm) ->
          let Item_t (ty, _, _) = bef in
          mk_statement (If_cons (translate_ty ty,
                                 loop tstm,
                                 loop fstm)) loc bef aft
        | List_map(stm) -> mk_statement (List_map(loop stm)) loc bef aft
        | List_iter(stm) -> mk_statement (List_iter (loop stm)) loc bef aft
        | List_size ->
          let Item_t (ty, _, _) = bef in
          mk_statement (List_size (translate_ty ty)) loc bef aft
        | Empty_set _ ->
          let ty = match aft with
            | Item_t (Set_t (ty, _), _, _) -> ty_of_comparable_ty ty
            | _ -> assert false
          in
          mk_statement (Empty_set (translate_ty ty)) loc bef aft
        | Set_iter (stm) -> mk_statement (Set_iter (loop stm)) loc bef aft
        | Set_mem ->
          let ty = match bef with
            | Item_t (_, Item_t (Set_t (ty, _), _, _), _) ->
             ty_of_comparable_ty ty
            | _ -> assert false
          in
          mk_statement (Set_mem (translate_ty ty)) loc bef aft
        | Set_update ->
          let ty = match aft with
            | Item_t (Set_t(ty, _), _, _) -> ty_of_comparable_ty ty
            | _ -> assert false
          in
          mk_statement (Set_update (translate_ty ty)) loc bef aft
        | Set_size -> mk_statement Set_size loc bef aft
        | Empty_map _ ->
          let kty, vty = match aft with
            | Item_t (Map_t(kty, vty, _), _, _) ->
              translate_ty (ty_of_comparable_ty kty), translate_ty vty
            | _ -> assert false
          in
          mk_statement (Empty_map (kty, vty)) loc bef aft
        | Map_map (stm) -> mk_statement (Map_map (loop stm)) loc bef aft
        | Map_iter (stm) -> mk_statement (Map_iter (loop stm)) loc bef aft
        | Map_mem ->
          let kty, vty = match bef with
            | Item_t (_, Item_t (Map_t(kty, vty, _), _, _), _) ->
              translate_ty (ty_of_comparable_ty kty), translate_ty vty
            | _ -> assert false
          in
          mk_statement (Map_mem(kty, vty)) loc bef aft
        | Map_get ->
          let kty, vty = match bef with
            | Item_t (_, Item_t(Map_t(kty, vty, _), _, _), _) ->
              translate_ty (ty_of_comparable_ty kty), translate_ty vty
            | _ -> assert false
          in
          mk_statement (Map_get(kty, vty)) loc bef aft
        | Map_update ->
          let kty, vty = match aft with
            | Item_t (Map_t(kty, vty, _), _, _) ->
              translate_ty (ty_of_comparable_ty kty), translate_ty vty
            | _ -> assert false
          in
          mk_statement (Map_update(kty, vty)) loc bef aft
        | Map_size ->
          let kty, vty = match bef with
            | Item_t (Map_t(kty, vty, _), _, _) ->
              translate_ty (ty_of_comparable_ty kty), translate_ty vty
            | _ -> assert false
          in
          mk_statement (Map_size(kty, vty)) loc bef aft
        | Empty_big_map(_, _) -> mk_statement (Empty_big_map) loc bef aft
        | Big_map_mem -> mk_statement (Big_map_mem) loc bef aft
        | Big_map_get -> mk_statement (Big_map_get) loc bef aft
        | Big_map_update -> mk_statement (Big_map_update) loc bef aft
        | Concat_string -> mk_statement (Concat_string) loc bef aft
        | Concat_string_pair -> mk_statement (Concat_string_pair) loc bef aft
        | Slice_string -> mk_statement (Slice_string) loc bef aft
        | String_size -> mk_statement (String_size) loc bef aft
        | Concat_bytes -> mk_statement (Concat_bytes) loc bef aft
        | Concat_bytes_pair -> mk_statement (Concat_bytes_pair) loc bef aft
        | Slice_bytes -> mk_statement (Slice_bytes) loc bef aft
        | Bytes_size -> mk_statement (Bytes_size) loc bef aft
        | Add_seconds_to_timestamp -> mk_statement (Add_seconds_to_timestamp)
                                        loc bef aft
        | Add_timestamp_to_seconds -> mk_statement (Add_timestamp_to_seconds)
                                        loc bef aft
        | Sub_timestamp_seconds -> mk_statement (Sub_timestamp_seconds) loc
                                     bef aft
        | Diff_timestamps -> mk_statement (Diff_timestamps) loc bef aft
        | Add_tez -> mk_statement (Add_tez) loc bef aft
        | Sub_tez -> mk_statement (Sub_tez) loc bef aft
        | Mul_teznat -> mk_statement (Mul_teznat) loc bef aft
        | Mul_nattez -> mk_statement (Mul_nattez) loc bef aft
        | Ediv_teznat -> mk_statement (Ediv_teznat) loc bef aft
        | Ediv_tez -> mk_statement (Ediv_tez) loc bef aft
        | Or -> mk_statement (Or) loc bef aft
        | And -> mk_statement (And) loc bef aft
        | Xor -> mk_statement (Xor) loc bef aft
        | Not -> mk_statement (Not) loc bef aft
        | Is_nat -> mk_statement (Is_nat) loc bef aft
        | Neg_nat -> mk_statement (Neg_nat) loc bef aft
        | Neg_int -> mk_statement (Neg_int) loc bef aft
        | Abs_int -> mk_statement (Abs_int) loc bef aft
        | Int_nat -> mk_statement (Int_nat) loc bef aft
        | Add_intint -> mk_statement (Add_intint) loc bef aft
        | Add_intnat -> mk_statement (Add_intnat) loc bef aft
        | Add_natint -> mk_statement (Add_natint) loc bef aft
        | Add_natnat -> mk_statement (Add_natnat) loc bef aft
        | Sub_int -> mk_statement (Sub_int) loc bef aft
        | Mul_intint -> mk_statement (Mul_intint) loc bef aft
        | Mul_intnat -> mk_statement (Mul_intnat) loc bef aft
        | Mul_natint -> mk_statement (Mul_natint) loc bef aft
        | Mul_natnat -> mk_statement (Mul_natnat) loc bef aft
        | Ediv_intint -> mk_statement (Ediv_intint) loc bef aft
        | Ediv_intnat -> mk_statement (Ediv_intnat) loc bef aft
        | Ediv_natint -> mk_statement (Ediv_natint) loc bef aft
        | Ediv_natnat -> mk_statement (Ediv_natnat) loc bef aft
        | Lsl_nat -> mk_statement (Lsl_nat) loc bef aft
        | Lsr_nat -> mk_statement (Lsr_nat) loc bef aft
        | Or_nat -> mk_statement (Or_nat) loc bef aft
        | And_nat -> mk_statement (And_nat) loc bef aft
        | And_int_nat -> mk_statement (And_int_nat) loc bef aft
        | Xor_nat -> mk_statement (Xor_nat) loc bef aft
        | Not_nat -> mk_statement (Not_nat) loc bef aft
        | Not_int -> mk_statement (Not_int) loc bef aft
        | Seq (a, b) ->
          let a = loop a in
          let b = loop b in
          let srange =
            if loc = -1 then
              (
                Exceptions.warn "seq('%a', '%a') has no location@."
                  M_ast.pp_statement a M_ast.pp_statement b;
                (* FIXME translate to start/stop of left,right statements *)
                (* Location.mk_fresh_range () *)
                Location.mk_orig_range
                  (Location.mk_pos filename 0 0)
                  (Location.mk_pos filename 0 0)
              )
            else
              translate_location filename locations loc
          in
          let bef = translate_stack_ty bef in
          let aft = translate_stack_ty aft in
          { instr = (Seq (a, b)); srange; bef; aft  }
        | If (tstm, fstm) ->
          mk_statement (If (loop tstm, loop fstm)) loc bef aft
        | Loop (stm)-> mk_statement (Loop (loop stm)) loc bef aft
        | Loop_left (stm) -> mk_statement (Loop_left (loop stm)) loc bef aft
        | Dip (stm) -> mk_statement (Dip (loop stm)) loc bef aft
        | Exec -> mk_statement Exec loc bef aft
        | Apply _ -> mk_statement Apply loc bef aft
        | Lambda (Lam(descr, _)) -> mk_statement (Lambda (loop descr)) loc bef
                                      aft
        | Failwith _ ->
          let Item_t (ty, _, _) = bef in
          mk_statement (Failwith (translate_ty ty)) loc bef aft
        | Nop ->
          let srange =
            if loc = -1 then
              Location.mk_orig_range
                (Location.mk_pos filename 0 0)
                (Location.mk_pos filename 0 0)
            else
              translate_location filename locations loc
          in
          let bef = translate_stack_ty bef in
          let aft = translate_stack_ty aft in
          { instr = Nop; srange; bef; aft }
        | Compare _ ->
          let Item_t (ty, _, _) = bef in
          mk_statement (Compare (translate_ty ty)) loc bef aft
        | Eq -> mk_statement Eq loc bef aft
        | Neq -> mk_statement Neq loc bef aft
        | Lt -> mk_statement Lt loc bef aft
        | Gt -> mk_statement Gt loc bef aft
        | Le -> mk_statement Le loc bef aft
        | Ge -> mk_statement Ge loc bef aft
        | Address -> mk_statement Address loc bef aft
        | Contract(typ, entrypoint) ->
          let typ = translate_ty typ in
          mk_statement (Contract (typ, entrypoint)) loc bef aft
        | Transfer_tokens -> mk_statement Transfer_tokens loc bef aft
        | Create_account -> mk_statement Create_account loc bef aft
        | Implicit_account -> mk_statement Implicit_account loc bef aft
        | Create_contract(_, _, Lam (descr, _ ), _) ->
          mk_statement (Create_contract (loop descr)) loc bef aft
        | Create_contract_2(p, s, Lam(descr, _), _) ->
          let parameter_ty = translate_ty p in
          let storage_ty = translate_ty s in
          let code = loop descr in
          mk_statement (Create_contract_2{ parameter_ty; storage_ty; code }) loc bef aft
        | Set_delegate -> mk_statement Set_delegate loc bef aft
        | Now -> mk_statement Now loc bef aft
        | Balance -> mk_statement Balance loc bef aft
        | Check_signature -> mk_statement Check_signature loc bef aft
        | Hash_key -> mk_statement Hash_key loc bef aft
        | Pack _ -> mk_statement Pack loc bef aft
        | Unpack _ -> mk_statement Unpack loc bef aft
        | Blake2b -> mk_statement Blake2b loc bef aft
        | Sha256 -> mk_statement Sha256 loc bef aft
        | Sha512 -> mk_statement Sha512 loc bef aft
        | Steps_to_quota -> mk_statement Steps_to_quota loc bef aft
        | Source -> mk_statement Source loc bef aft
        | Sender -> mk_statement Sender loc bef aft
        | Self(_, entrypoint) -> mk_statement (Self(entrypoint)) loc bef aft
        | Amount -> mk_statement Amount loc bef aft
        | Dig (n, _)-> mk_statement (Dig n) loc bef aft
        | Dug (n, _) -> mk_statement (Dug n) loc bef aft
        | Dipn (n, _, stm) -> mk_statement (Dipn(n, loop stm)) loc bef aft
        | Dropn (n, _) -> mk_statement (Dropn n) loc bef aft
        | ChainId -> mk_statement ChainId loc bef aft
      in
      debug "translated instr = %a (pos = @<h>%a@]) (bef=%a) (aft=%a)" M_ast.pp_statement instr
        Location.pp_range instr.srange
        M_ast.pp_stack_ty instr.bef
        M_ast.pp_stack_ty instr.aft
      ;
      instr
  in
  loop instr

and translate_lambda :
  type a b . string -> locations -> (a, b) Script_typed_ir.lambda -> M_ast.statement =
  fun filename locations lambda ->
  let open Script_typed_ir in
  match lambda with
  | Lam(descr, _) ->
    let descr, locations = fix_locations filename locations descr in
    translate_instr filename locations descr

and translate_stack_ty:
  type a . a Script_typed_ir.stack_ty -> M_ast.stack_ty =
  function
  | Item_t(hd, tl, annot) ->
    (translate_annot annot, translate_ty hd)::
    (translate_stack_ty tl)
  | Empty_t -> []

let rec translate_stack : type a .
  locations -> a Script_typed_ir.stack_ty -> a -> M_ast.stack =
  fun locations stack_ty stack ->
  match stack_ty, stack with
  | Item_t(hd, tl, annot), (v, rest) ->
    (translate_value locations hd v)::
    (translate_stack locations tl rest)
  | Empty_t, _ ->
    []

let translate filename locations Script_ir_translator.(
    Ex_code {code; arg_type; storage_type; _}
  )
  =
  translate_lambda filename locations code,
  translate_ty arg_type,
  translate_ty storage_type

