open Mopsa_utils
open Tezos_007_common
open Interp

let rec random_constant :
  type a . a Script_typed_ir.ty -> a Crowbar.gen =
  fun typ ->
  let open Crowbar in
  match typ with
  | Unit_t _ -> const ()
  | Bool_t _ -> map [bool] (fun b -> b)
  | Int_t _ -> map [int] (fun i -> Script_int.of_int i)
  | Nat_t _ ->
    map [range ~min:0 max_int] (fun i -> Script_int.abs @@ Script_int.of_int i)
  | Pair_t ((lty, _, _), (rty, _, _), _) ->
    map [random_constant lty; random_constant rty] (fun x y -> (x, y))
  | Signature_t _ ->
    map [bytes] (fun s -> Signature.of_b58check_opt s |> nonetheless)
  | String_t _ ->
    map [bytes] (fun s -> s)
  | Bytes_t _ ->
    map [bytes] (fun s -> Bytes.of_string s)
  | Mutez_t _ ->
    map [int64] (fun i -> Tez.of_mutez i |> Crowbar.nonetheless)
  | Key_hash_t _ ->
    map [bytes] (fun b -> Key_hash.of_string_opt b |> nonetheless)
  | Key_t _ ->
    map [bytes] (fun b -> Key.of_b58check_opt b |> nonetheless)
  | Timestamp_t _ ->
    map [bytes] (fun ts -> Timestamp.of_string ts |> nonetheless)
  | Address_t _ ->
    map [bytes; bytes] (fun addr ep ->
        (match Contract.of_b58check addr with
         | Ok addr -> addr
         | Error _ -> bad_test ()
        ), ep)
  | Union_t ((lty, _), (rty, _), _) ->
    choose
      [
        map [random_constant lty] (fun s -> Script_typed_ir.L s);
        map [random_constant rty] (fun s -> Script_typed_ir.R s);
      ]
  | Option_t (ty, _) ->
    choose
      [
        const None;
        map [random_constant ty] (fun c -> Some c);
      ]
  | List_t (ty, _) ->
    let box xs = Script_typed_ir.{elements = xs; length = List.length xs} in
    map [list (random_constant ty)] (fun l -> box l)
  | Set_t (ty, _) ->
    let box : type elt . elt Script_typed_ir.comparable_ty -> elt list ->
      (module Script_typed_ir.Boxed_set with type elt = elt) =
      fun ty xs ->
        let module OPS = Protocol.Environment.Set.Make(struct
            type t = elt
            let compare = Script_ir_translator.compare_comparable ty
          end) in
        (module struct
          type nonrec elt = elt
          let elt_ty = ty
          module OPS = OPS
          let boxed = OPS.of_list xs
          let size = List.length xs
        end)
    in
    map [list (random_constant (ty_of_comparable_ty ty))] (fun l -> box ty l)
  | Map_t (kty, vty, _) ->
    let box : type key value .
      key Script_typed_ir.comparable_ty ->
      value Script_typed_ir.ty ->
      (key * value) list ->
      (module Script_typed_ir.Boxed_map with type key = key and type value = value) =
      fun kty vty xs ->
        let module OPS = Protocol.Environment.Map.Make(struct
            type t = key
            let compare = Script_ir_translator.compare_comparable kty
          end)
        in
        (module struct
          type nonrec key = key
          type nonrec value = value
          let key_ty = kty
          module OPS = OPS
          let boxed =
            (List.fold_left (fun map (k, v) ->
                 OPS.add k v map
               ) OPS.empty xs),
            List.length xs
        end)
    in
    map [list (pair
                 (random_constant (ty_of_comparable_ty kty))
                 (random_constant vty))]
      (fun l -> box kty vty l)
  | Contract_t (ty, _) ->
    map [bytes; bytes] (fun contract ep ->
        (match Contract.of_b58check contract with
         | Ok addr -> ty, (addr, ep)
         | Error _ -> bad_test ()
        )
      )
  | Chain_id_t _ ->
    map [bytes] (fun s -> Chain_id.of_b58check_opt s |> nonetheless)
  | Operation_t _ -> assert false (* should not happen in storage or parameter *)
  | Big_map_t _ ->
    (* NOTE: deliberatly delay exception raise in order the exception
     * to be catched outside the mopsa runner caller *)
    Exceptions.warn "%s: TODO: Crowbar gen for big_map" __LOC__;
    map [const ()] (fun () -> bad_test ())
  | Lambda_t _ ->
    (* NOTE: deliberatly delay exception raise in order the exception
     * to be catched outside the mopsa runner caller *)
    Exceptions.warn "%s: TODO: Crowbar gen for lambda" __LOC__;
    map [const ()] (fun () -> bad_test ())

let random_constant:
  type a . a Script_typed_ir.ty -> a Crowbar.gen =
  fun ty ->
  Crowbar.with_printer
    (fun fmt v -> pp_michelson_value fmt (ty, v))
    (random_constant ty)

let random_init_stack :
  type parameter storage .
  parameter Script_typed_ir.ty ->
  storage Script_typed_ir.ty ->
  ((parameter * storage) * unit) Crowbar.gen
  =
  fun parameter_ty storage_ty ->
  Crowbar.map [random_constant parameter_ty; random_constant storage_ty]
    (fun parameter storage ->
       (parameter, storage), () (* stack is empty rest *)
    )

let random_init_stack_list :
  type parameter storage .
  parameter Script_typed_ir.ty ->
  storage Script_typed_ir.ty ->
  ((parameter * storage) * unit) list Crowbar.gen
  =
  fun parameter_ty storage_ty ->
  Crowbar.list (random_init_stack parameter_ty storage_ty)

let interpret_crowbar ~filename ~locations context step_constants ex_code =
  let module Params : STATE_LOGGER_PARAM = struct
    type location = Script.location
    type locations =
      (Int.t * (Tezos_micheline.Micheline_parser.location * int list)) list
    let filename = filename
    let locations = locations
  end in
  let module Logger = State_logger (Params) in
  let logger = (module Logger : Script_interpreter.STEP_LOGGER) in
  let Script_ir_translator.Ex_code { code = Lam (descr, _);
                                     arg_type;
                                     storage_type; _ } = ex_code in
  Crowbar.map [random_init_stack arg_type storage_type]
    (fun stack ->
       Format.eprintf "Crowbar: testing with stack = %a@." pp_stack (descr.bef, stack);
       match Lwt_main.run @@
         Script_interpreter.step logger context step_constants descr stack
       with
       | Ok (end_of_stack, context) ->
         Logger.get_state ()
       | Error errs ->
         Crowbar.failf "Error executing step: %a"
           (Format.pp_print_list
              (fun fmt err ->
                 Error_monad.pp fmt
                   (Protocol.Environment.Ecoproto_error err))
           ) errs
    )

let interpret_crowbar_multiple ~filename ~locations context step_constants ex_code n =
  let module Params : STATE_LOGGER_PARAM = struct
    type location = Script.location
    type locations =
      (Int.t * (Tezos_micheline.Micheline_parser.location * int list)) list
    let filename = filename
    let locations = locations
  end in
  let module Logger = State_logger (Params) in
  let logger = (module Logger : Script_interpreter.STEP_LOGGER) in
  let Script_ir_translator.Ex_code { code = Lam (descr, _);
                                     arg_type;
                                     storage_type; _ } = ex_code in
  let open Crowbar in
  let log_elements_gen =
    Crowbar.map [random_init_stack arg_type storage_type]
      (fun stack ->
         match Lwt_main.run @@
           Script_interpreter.step logger context step_constants descr stack
         with
         | Ok (end_of_stack, context) -> Logger.get_state ()
         | Error errs ->
           Crowbar.failf "Error executing step: %a"
             (Format.pp_print_list
                (fun fmt err ->
                   Error_monad.pp fmt
                     (Protocol.Environment.Ecoproto_error err))
             ) errs
      )
  in
  Crowbar.list log_elements_gen

