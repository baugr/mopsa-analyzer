open Mopsa_utils
open Tezos_007_common

let debug args = Debug.debug ~channel:"michelson.parser.parser" args

let parse_code code =
  begin match Michelson_v1_parser.parse_toplevel code with
    | ast, [] -> Error_monad.return ast
    | _, errl -> Lwt.return @@ Error errl
  end

let parse_storage storage =
  begin match Michelson_v1_parser.parse_expression storage with
    | storage, [] -> Error_monad.return storage.expanded
    | _, errl -> Lwt.return @@ Error errl end

let parse_michelson context source_str =
  let open Tezos_error_monad.TzMonad in
  parse_code source_str >>=? fun pcode ->
  let code = Script.lazy_expr pcode.expanded in
  debug "@[<v 0>initial locations:@;%a@;@]---@\n"
    pp_locations pcode.expansion_table;
  debug "@[<v 0>unexpansion table:@;%a@;@]---@\n"
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt "@;")
       (fun fmt (loc,to_loc) -> Format.fprintf fmt "%d -> %d" loc to_loc))
    pcode.unexpansion_table;
  Script_ir_translator.parse_code
    context ~legacy:false ~code >>=?! fun (ex_code, _) ->
  let open Script_ir_translator in
  let (Ex_code { code = Lam (descr, _) ; _ }) = ex_code in
  debug "@[<v 0>initial michelson:@;%a@;@]---\n"
    (pp_michelson_loc pcode.expansion_table ~printloc:true ~fullloc:false) descr;
  let module M = Map.Make(Int) in
  let expansion_map = List.fold_left
      (fun acc (loc, (pos, lst)) ->
         M.add loc (pos,lst) acc
      )
      M.empty
      pcode.expansion_table
  in
  let unexpansion_map = List.fold_left
      (fun acc (loc, to_loc) ->
         M.add loc to_loc acc
      )
      M.empty
      pcode.unexpansion_table
  in
  let locations_map =
    M.fold (fun loc to_loc acc ->
        match M.find_opt to_loc expansion_map with
        | None -> assert false
        | Some dest -> M.add loc dest acc
      ) unexpansion_map M.empty
  in
  let locations = M.fold (fun loc (pos,lst) acc ->
      (loc, (pos,lst))::acc) locations_map []
  in
  return (ex_code, locations)
