open Mopsa_utils

module Tezos_protocol = Tezos_protocol_007_PsDELPH1
module Client = Tezos_client_007_PsDELPH1

module Protocol = Tezos_protocol.Protocol

module Time = Tezos_base.Time
module Operation_list_list_hash = Tezos_crypto.Operation_list_list_hash

module Context_hash = Tezos_crypto.Context_hash
module Block_hash = Tezos_crypto.Block_hash
module Signature = Tezos_crypto.Signature
module Key_hash = Tezos_crypto.Signature.Public_key_hash
module Key = Tezos_crypto.Signature.Public_key

module Script_ir_translator = Protocol.Script_ir_translator
module Script_interpreter = Protocol.Script_interpreter
module Script_typed_ir = Protocol.Script_typed_ir

module Script = Protocol.Alpha_context.Script
module Tez = Protocol.Alpha_context.Tez
module Timestamp = Protocol.Alpha_context.Script_timestamp
module Contract = Protocol.Alpha_context.Contract

module Parameters_repr = Protocol.Parameters_repr
module Script_int = Protocol.Script_int_repr
module Tez_repr = Protocol.Tez_repr
module Period_repr = Protocol.Period_repr
module Constants_repr = Protocol.Constants_repr
module Gas_limit_repr = Protocol.Gas_limit_repr
module Operation_repr = Protocol.Operation_repr
module Chain_id = Protocol.Environment.Chain_id

module Michelson_v1_parser = Client.Michelson_v1_parser
module Michelson_v1_error_reporter = Client.Michelson_v1_error_reporter
module Misc = Protocol.Misc

module Error_monad = Tezos_error_monad.Error_monad

let ( >>=? ) v f =
  let open Lwt in
  v >>= fun v ->
  Protocol.Environment.wrap_error v |> function
  | Error _ as err -> Lwt.return err
  | Ok v -> f v

let ( >>=?! ) v f =
  let open Lwt in
  v >>= fun v ->
  Protocol.Environment.wrap_error v |> function
  | Error _ as err -> Lwt.return err
  | Ok v -> f v

type locations =
  (Protocol.Alpha_context.Script.location *
   (Tezos_micheline.Micheline_parser.location * int list))
    list

let pp_loc locations ~fullloc fmt loc =
  if fullloc then
    match List.assoc_opt loc locations with
    | None -> Format.fprintf fmt "[ ?? ]             "
    | Some pos ->
      let (Tezos_micheline.Micheline_parser.{start; stop}, _) = pos in
      Format.fprintf fmt "[%4d] %4d:%-2d-%4d:%-2d --  "
        loc
        start.line
        start.column
        stop.line
        stop.column
  else
    Format.fprintf fmt "[%4d]  " loc

(* FIXME: factorize *)
let pp_locations fmt locations =
  let open Tezos_micheline.Micheline_parser in
  List.iter (fun (key, ({start; stop; _}, _)) ->
      Format.fprintf fmt "%-4d - %d:%d -- %d:%d@;"
        key
        start.line
        start.column
        stop.line
        stop.column
    ) locations

let rec ty_of_comparable_ty :
  type a s. (a, s) Script_typed_ir.comparable_struct -> a Script_typed_ir.ty =
  function
  | Int_key tname ->
    Int_t tname
  | Nat_key tname ->
    Nat_t tname
  | String_key tname ->
    String_t tname
  | Bytes_key tname ->
    Bytes_t tname
  | Mutez_key tname ->
    Mutez_t tname
  | Bool_key tname ->
    Bool_t tname
  | Key_hash_key tname ->
    Key_hash_t tname
  | Timestamp_key tname ->
    Timestamp_t tname
  | Address_key tname ->
    Address_t tname
  | Pair_key ((l, al), (r, ar), tname) ->
    Pair_t
      ( (ty_of_comparable_ty l, al, None),
        (ty_of_comparable_ty r, ar, None),
        tname
      )

let rec pp_michelson_value : type a .
  Format.formatter ->
  (a Script_typed_ir.ty * a) -> unit =
  fun fmt (ty, v) ->
  match ty, v with
  | Script_typed_ir.Unit_t _, () -> Format.fprintf fmt "()"
  | Int_t _, i -> Z.pp_print fmt (Script_int.to_zint i)
  | Nat_t _, z  -> Z.pp_print fmt (Script_int.to_zint z)
  | Signature_t _, _ -> Exceptions.panic "not yet supported: signature"
  | String_t _, s -> Format.fprintf fmt "\"%s\"" s
  | Bytes_t _ , s ->
    Format.fprintf fmt "0x%s"
      ((Seq.fold_left
          (fun acc c ->
             Format.asprintf "%s%2x" acc (Char.code c)) "" (Bytes.to_seq s))
      )
  | Mutez_t _ , v -> Format.fprintf fmt "%Ld" (Tez.to_mutez v)
  | Key_hash_t _, v -> Key_hash.pp fmt v
  | Key_t _ , _ -> Exceptions.panic "not yet supported: key"
  | Timestamp_t _ , v -> Format.fprintf fmt "%s" (Timestamp.to_string v)
  | Address_t _ , (contract, entrypoint) ->
    (match entrypoint with
     | "default" -> Protocol.Alpha_context.Contract.pp fmt contract
     | _ -> Format.fprintf fmt "%a%%%s"
              Protocol.Alpha_context.Contract.pp contract
              entrypoint
    )
  | Bool_t _ , b -> Format.fprintf fmt "%b" b
  | Pair_t (((ty_a, _, _), (ty_b, _, _), _)), (a, b) ->
    Format.fprintf fmt "(Pair %a %a)"
      pp_michelson_value (ty_a, a)
      pp_michelson_value (ty_b, b)
  | Union_t ((l_ty, fo), _, _), L v ->
    (match fo with
     | Some (Script_typed_ir.Field_annot s) ->
       Debug.warn "GOT A FIELD ANNOT: %s" s;
       Format.fprintf fmt "(Left %a)" pp_michelson_value (l_ty, v)
     | None ->
       Debug.warn "GOT NO FIELD ANNOT";
       Format.fprintf fmt "(Left %a)" pp_michelson_value (l_ty, v)
    )
  | Union_t (_, (r_ty, _), _), R v ->
    Format.fprintf fmt "(Right %a)" pp_michelson_value (r_ty, v)
  | Lambda_t _ , _-> Exceptions.panic "not yet supported: lambda"
  | Option_t (_, _), None -> Format.fprintf fmt "None"
  | Option_t (ty, _), Some v ->
    Format.fprintf fmt "Some %a" pp_michelson_value (ty, v)
  | List_t (ty, _), { elements } ->
    Format.fprintf fmt "{";
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt "; ")
       (fun fmt item -> pp_michelson_value fmt (ty, item))
    ) fmt elements;
    Format.fprintf fmt "}";
  | Set_t (ty, _), (module Boxed_set) ->
    Format.fprintf fmt "{ ";
    Boxed_set.OPS.iter (fun v ->
        pp_michelson_value fmt (ty_of_comparable_ty ty, v);
        Format.fprintf fmt "; ";
      ) Boxed_set.boxed;
    Format.fprintf fmt " }";
  | Map_t (kty, vty, _), (module Boxed_map) ->
    Format.fprintf fmt "{ ";
    Boxed_map.OPS.iter (fun k v ->
        Format.fprintf fmt "Elt %a %a"
          pp_michelson_value (ty_of_comparable_ty kty, k)
          pp_michelson_value (vty, v)
      ) (fst Boxed_map.boxed)
  | Big_map_t _ , _-> Exceptions.panic "not yet supported: big_map"
  | Contract_t _ , _-> Exceptions.panic "not yet supported: contract"
  | Operation_t _ , _-> Exceptions.panic "not yet supported: operation"
  | Chain_id_t _ , _-> Exceptions.panic "not yet supported: chain_id"

let rec pp_michelson_loc : type a b .
  locations ->
  ?printloc:bool ->
  ?fullloc:bool ->
  Format.formatter ->
  (a, b) Script_typed_ir.descr -> unit =
  fun locations ?(printloc=true) ?(fullloc=false) fmt ({instr; loc; aft; _ }) ->
  let pp_loc fmt loc = pp_loc locations ~fullloc fmt loc in
  let pp_block fmt stmt =
    Format.fprintf fmt
      "@[<v 0>{@[<v 2>@;%a@]@;}@]"
      (pp_michelson_loc locations ~printloc ~fullloc) stmt
  in
  match instr with
  (* stack ops *)
  | Script_typed_ir.Drop -> Format.fprintf fmt "%aDROP" pp_loc loc
  | Dup -> Format.fprintf fmt "%aDUP" pp_loc loc
  | Swap -> Format.fprintf fmt "%aSWAP" pp_loc loc
  | Const v ->
    let Item_t(ty, _, _) = aft in
    Format.fprintf fmt "%aPUSH %a" pp_loc loc pp_michelson_value (ty, v)
  | Cons_pair -> Format.fprintf fmt "%aPAIR" pp_loc loc
  | Car -> Format.fprintf fmt "%aCAR" pp_loc loc
  | Cdr -> Format.fprintf fmt "%aCDR" pp_loc loc
  | Cons_some -> Format.fprintf fmt "%aCONS" pp_loc loc
  | Cons_none _ -> Format.fprintf fmt "%aNONE" pp_loc loc
  | If_none(tstm, fstm) ->
    Format.fprintf fmt "%aIF_NONE @[<v 0>%a@;%a@]"
      pp_loc loc pp_block tstm  pp_block fstm
  | Cons_left -> Format.fprintf fmt "%aLEFT" pp_loc loc
  | Cons_right -> Format.fprintf fmt "%aRIGHT" pp_loc loc
  | Cons_list -> Format.fprintf fmt "%aCONS_LIST" pp_loc loc
  | Nil -> Format.fprintf fmt "%aNIL" pp_loc loc
  | If_cons(tstm, fstm) ->
    Format.fprintf fmt "%aIF_CONS @[<v 0>%a@;%a@]"
      pp_loc loc pp_block tstm pp_block fstm;
  | List_map(stm) ->
    Format.fprintf fmt "%aLIST_MAP %a" pp_loc loc pp_block stm
  | List_iter(stm) ->
    Format.fprintf fmt "%aLIST_ITER %a" pp_loc loc pp_block stm
  | List_size -> Format.fprintf fmt "%aLIST_SIZE" pp_loc loc
  | Empty_set _ -> Format.fprintf fmt "%aEMPTY_SET" pp_loc loc
  | Set_iter (stm) ->
    Format.fprintf fmt "%aSET_ITER %a" pp_loc loc pp_block stm
  | Set_mem -> Format.fprintf fmt "%aSET_MEM" pp_loc loc
  | Set_update -> Format.fprintf fmt "%aSET_UPDATE" pp_loc loc
  | Set_size -> Format.fprintf fmt "%aSET_SIZE" pp_loc loc
  | Empty_map _ -> Format.fprintf fmt "%aEMPTY_MAP" pp_loc loc
  | Map_map (stm) ->
    Format.fprintf fmt "%aMAP_MAP %a" pp_loc loc pp_block stm
  | Map_iter (stm) ->
    Format.fprintf fmt "%aMAP_ITER %a" pp_loc loc pp_block stm
  | Map_mem -> Format.fprintf fmt "%aMAP_MEM" pp_loc loc
  | Map_get -> Format.fprintf fmt "%aMAP_GET" pp_loc loc
  | Map_update -> Format.fprintf fmt "%aMAP_UPDATE" pp_loc loc
  | Map_size -> Format.fprintf fmt "%aMAP_SIZE" pp_loc loc
  | Empty_big_map(_, _) -> Format.fprintf fmt "%aEMPTY_BIG_MAP" pp_loc loc
  | Big_map_mem -> Format.fprintf fmt "%aBIG_MAP_MEM" pp_loc loc
  | Big_map_get -> Format.fprintf fmt "%aBIG_MAP_GET" pp_loc loc
  | Big_map_update -> Format.fprintf fmt "%aBIG_MAP_UPDATE" pp_loc loc
  | Concat_string -> Format.fprintf fmt "%aCONCAT_STRING" pp_loc loc
  | Concat_string_pair -> Format.fprintf fmt "%aCONCAT_STRING_PAIR" pp_loc loc
  | Slice_string -> Format.fprintf fmt "%aSLICE_STRING" pp_loc loc
  | String_size -> Format.fprintf fmt "%aSTRING_SIZE" pp_loc loc
  | Concat_bytes -> Format.fprintf fmt "%aCONCAT_BYTES" pp_loc loc
  | Concat_bytes_pair -> Format.fprintf fmt "%aCONCAT_BYTES_PAIR" pp_loc loc
  | Slice_bytes -> Format.fprintf fmt "%aSLICE_BYTES" pp_loc loc
  | Bytes_size -> Format.fprintf fmt "%aBYTES_SIZE" pp_loc loc
  | Add_seconds_to_timestamp ->
    Format.fprintf fmt "%aADD_SECONDS_TO_TIMESTAMP" pp_loc loc
  | Add_timestamp_to_seconds ->
    Format.fprintf fmt "%aADD_TIMESTAMP_TO_SECONDS" pp_loc loc
  | Sub_timestamp_seconds ->
    Format.fprintf fmt "%aSUB_TIMESTAMP_TO_SECONDS" pp_loc loc
  | Diff_timestamps ->
    Format.fprintf fmt "%aDIFF_TIMESTAMPS" pp_loc loc
  | Add_tez -> Format.fprintf fmt "%aADD_TEZ" pp_loc loc
  | Sub_tez -> Format.fprintf fmt "%aSUB_TEZ" pp_loc loc
  | Mul_teznat -> Format.fprintf fmt "%aMUL_TEZ_NAT" pp_loc loc
  | Mul_nattez -> Format.fprintf fmt "%aMUL_NATTEZ" pp_loc loc
  | Ediv_teznat -> Format.fprintf fmt "%aEDIV_TEZNAT" pp_loc loc
  | Ediv_tez -> Format.fprintf fmt "%aEDIV_TEZ" pp_loc loc
  | Or -> Format.fprintf fmt "%aOR" pp_loc loc
  | And -> Format.fprintf fmt "%aAND" pp_loc loc
  | Xor -> Format.fprintf fmt "%aXOR" pp_loc loc
  | Not -> Format.fprintf fmt "%aNOT" pp_loc loc
  | Is_nat -> Format.fprintf fmt "%aIS_NAT" pp_loc loc
  | Neg_nat -> Format.fprintf fmt "%aNEG_NAT" pp_loc loc
  | Neg_int -> Format.fprintf fmt "%aNEG_INT" pp_loc loc
  | Abs_int -> Format.fprintf fmt "%aABS_INT" pp_loc loc
  | Int_nat -> Format.fprintf fmt "%aINT_NAT" pp_loc loc
  | Add_intint -> Format.fprintf fmt "%aADD_INTINT" pp_loc loc
  | Add_intnat -> Format.fprintf fmt "%aADD_INTNAT" pp_loc loc
  | Add_natint -> Format.fprintf fmt "%aADD_NATINT" pp_loc loc
  | Add_natnat -> Format.fprintf fmt "%aADD_NATNAT" pp_loc loc
  | Sub_int -> Format.fprintf fmt "%aSUB_INT" pp_loc loc
  | Mul_intint -> Format.fprintf fmt "%aMUL_INTINT" pp_loc loc
  | Mul_intnat -> Format.fprintf fmt "%aMUL_INTNAT" pp_loc loc
  | Mul_natint -> Format.fprintf fmt "%aMUL_NATINT" pp_loc loc
  | Mul_natnat -> Format.fprintf fmt "%aMUL_NATNAT" pp_loc loc
  | Ediv_intint -> Format.fprintf fmt "%aEDIV_INTINT" pp_loc loc
  | Ediv_intnat -> Format.fprintf fmt "%aEDIV_INTNAT" pp_loc loc
  | Ediv_natint -> Format.fprintf fmt "%aEDIV_NATINT" pp_loc loc
  | Ediv_natnat -> Format.fprintf fmt "%aEDIV_NATNAT" pp_loc loc
  | Lsl_nat -> Format.fprintf fmt "%aLSL_NAT" pp_loc loc
  | Lsr_nat -> Format.fprintf fmt "%aLSR_NAT" pp_loc loc
  | Or_nat -> Format.fprintf fmt "%aOR_NAT" pp_loc loc
  | And_nat -> Format.fprintf fmt "%aAND_NAT" pp_loc loc
  | And_int_nat -> Format.fprintf fmt "%aAND_INT_NAT" pp_loc loc
  | Xor_nat -> Format.fprintf fmt "%aXOR_NAT" pp_loc loc
  | Not_nat -> Format.fprintf fmt "%aNOT_NAT" pp_loc loc
  | Not_int -> Format.fprintf fmt "%aNOT_INT" pp_loc loc
  | Seq (a, b) ->
    Format.fprintf fmt "@[<v 0>%a@;%a@]"
      (pp_michelson_loc locations ~printloc ~fullloc) a
      (pp_michelson_loc locations ~printloc ~fullloc) b
  (* DEBUG: In case of problem with fix_location: deactivate this : *)
  (* Format.fprintf fmt "%a@[<v 0>%a@;%a@]" *)
  (*   pp_loc loc *)
  (*   (pp_michelson_loc locations printloc) a *)
  (*   (pp_michelson_loc locations printloc) b *)
  | If (tstm, fstm) ->
    Format.fprintf fmt "%aIF @[<v 0>%a@;%a@]"
      pp_loc loc pp_block tstm pp_block fstm;
  | If_left(tstm, fstm) ->
    Format.fprintf fmt "%aIF_LEFT @[<v 0>%a@;%a@]"
      pp_loc loc pp_block tstm pp_block fstm;
  | Loop (stm) -> Format.fprintf fmt "%aLOOP %a" pp_loc loc pp_block stm
  | Loop_left (stm) ->
    Format.fprintf fmt "%aLOOP_LEFT %a" pp_loc loc pp_block stm
  | Dip (stm) ->
    Format.fprintf fmt "%aDIP %a" pp_loc loc pp_block stm
  | Exec ->
    Format.fprintf fmt "%aEXEC" pp_loc loc
  | Apply _ ->
    Format.fprintf fmt "%aAPPLY" pp_loc loc
  | Lambda (Lam(descr, _)) ->
    Format.fprintf fmt "%aLAMBDA %a" pp_loc loc pp_block descr
  | Failwith _ ->
    Format.fprintf fmt "%aFAILWITH" pp_loc loc
  | Nop ->
    Format.fprintf fmt "%aNOP" pp_loc loc
  | Compare _ ->
    Format.fprintf fmt "%aCOMPARE" pp_loc loc
  | Eq -> Format.fprintf fmt "%aEQ" pp_loc loc
  | Neq -> Format.fprintf fmt "%aNEQ" pp_loc loc
  | Lt -> Format.fprintf fmt "%aLT" pp_loc loc
  | Gt -> Format.fprintf fmt "%aGT" pp_loc loc
  | Le -> Format.fprintf fmt "%aLE" pp_loc loc
  | Ge -> Format.fprintf fmt "%aGE" pp_loc loc
  | Address -> Format.fprintf fmt "%aADDRESS" pp_loc loc
  | Contract(ty, entry) -> Format.fprintf fmt "%aCONTRACT %%%s" pp_loc loc entry
  | Transfer_tokens -> Format.fprintf fmt "%aTRANSFER_TOKENS" pp_loc loc
  | Create_account -> Format.fprintf fmt "%aCREATE_ACCOUNT" pp_loc loc
  | Implicit_account -> Format.fprintf fmt "%aIMPLICIT_ACCOUNT" pp_loc loc
  | Create_contract(_, _, Lam (descr, _), _) ->
    Format.fprintf fmt "%aCREATE_CONTRACT %a" pp_loc loc pp_block descr
  | Create_contract_2(_, _, Lam(descr, _ ), _) ->
    Format.fprintf fmt "%aCREATE_CONTRACT_2 %a" pp_loc loc pp_block descr
  | Set_delegate -> Format.fprintf fmt "%aSET_DELEGATE" pp_loc loc
  | Now -> Format.fprintf fmt "%aNOW" pp_loc loc
  | Balance -> Format.fprintf fmt "%aBALANCE" pp_loc loc
  | Check_signature -> Format.fprintf fmt "%aCHECK_SIGNATURE" pp_loc loc
  | Hash_key -> Format.fprintf fmt "%aHASH_KEY" pp_loc loc
  | Pack _ -> Format.fprintf fmt "%aPACK _" pp_loc loc
  | Unpack _ -> Format.fprintf fmt "%aUNPACK _" pp_loc loc
  | Blake2b -> Format.fprintf fmt "%aBLAKE2B" pp_loc loc
  | Sha256 -> Format.fprintf fmt "%aSHA256" pp_loc loc
  | Sha512 -> Format.fprintf fmt "%aSHA512" pp_loc loc
  | Steps_to_quota -> Format.fprintf fmt "%aSTEPS_TO_QUOTA" pp_loc loc
  | Source -> Format.fprintf fmt "%aSOURCE" pp_loc loc
  | Sender -> Format.fprintf fmt "%aSENDER" pp_loc loc
  | Self(_, _) -> Format.fprintf fmt "%aSELF" pp_loc loc
  | Amount -> Format.fprintf fmt "%aAMOUNT" pp_loc loc
  | Dig (n, _) -> Format.fprintf fmt "%aDIG %d" pp_loc loc n
  | Dug (n, _) -> Format.fprintf fmt "%aDUG %d" pp_loc loc n
  | Dipn (n, _, stm) ->
    Format.fprintf fmt "%aDIPN %d %a" pp_loc loc n pp_block stm
  | Dropn (n, _) -> Format.fprintf fmt "%aDROPN %d" pp_loc loc n
  | ChainId -> Format.fprintf fmt "%aCHAINID" pp_loc loc

let rec pp_stack :
  type a .  Format.formatter -> (a Script_typed_ir.stack_ty * a) -> unit =
  fun fmt (ty, stack) ->
  match ty, stack with
  | Script_typed_ir.Item_t (ity, rty, _), (v, rest) ->
    Format.fprintf fmt "%a::%a"
      pp_michelson_value (ity, v)
      pp_stack (rty, rest)
  | Script_typed_ir.Empty_t, () ->
    ()

