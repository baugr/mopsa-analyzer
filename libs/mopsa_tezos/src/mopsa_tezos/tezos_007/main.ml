open Mopsa_utils
open Common
open Tezos_007_common
open Sigs

module M : TEZOS_PROTOCOL_VIEW = struct
  type context = Protocol.Alpha_context.context

  type location = Script.location
  type locations =
    (location * (Tezos_micheline.Micheline_parser.location * int list))
      list

  type step_constants = Script_interpreter.step_constants
  type tezos_ast = Script_ir_translator.ex_code
  type log_element = Interp.log_element =
    | Log of Mopsa_utils.Location.range * M_ast.stack


  (* WARNING: this calls lwt_main.run *)
  let memory_context () =
    let memory_context () =
      let genesis_block =
        Block_hash.of_b58check_exn "BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2"
      in
      let protocol_param_key = [ "protocol_parameters" ] in
      let bootstrap_accounts =
        let open Parameters_repr in
        [ { public_key_hash =
              Signature.Public_key_hash.of_b58check_exn "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" ;
            public_key = Some
                (Signature.Public_key.of_b58check_exn "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav") ;
            amount = Tez_repr.of_mutez_exn 4000000000000L
          } ]
      in
      let constants =
        Constants_repr.
          {
            preserved_cycles = 5;
            blocks_per_cycle = 4096l;
            blocks_per_commitment = 32l;
            blocks_per_roll_snapshot = 256l;
            blocks_per_voting_period = 32768l;
            time_between_blocks = List.map Period_repr.of_seconds_exn [60L; 40L];
            endorsers_per_block = 32;
            hard_gas_limit_per_operation =
              Gas_limit_repr.Arith.(integral_of_int 1_040_000);
            hard_gas_limit_per_block =
              Gas_limit_repr.Arith.(integral_of_int 10_400_000);
            proof_of_work_threshold = Int64.(sub (shift_left 1L 46) 1L);
            tokens_per_roll = Tez_repr.(mul_exn one 8_000);
            michelson_maximum_type_size = 1000;
            seed_nonce_revelation_tip =
              (match Tez_repr.(one /? 8L) with Ok c -> c | Error _ -> assert false);
            origination_size = 257;
            block_security_deposit = Tez_repr.(mul_exn one 512);
            endorsement_security_deposit = Tez_repr.(mul_exn one 64);
            baking_reward_per_endorsement =
              Tez_repr.[of_mutez_exn 1_250_000L; of_mutez_exn 187_500L];
            endorsement_reward =
              Tez_repr.[of_mutez_exn 1_250_000L; of_mutez_exn 833_333L];
            hard_storage_limit_per_operation = Z.of_int 60_000;
            cost_per_byte = Tez_repr.of_mutez_exn 250L;
            test_chain_duration = Int64.mul 32768L 60L;
            quorum_min = 20_00l;
            (* quorum is in centile of a percentage *)
            quorum_max = 70_00l;
            min_proposal_quorum = 5_00l;
            initial_endorsers = 24;
            delay_per_missing_endorsement = Period_repr.of_seconds_exn 8L;
          }
      in
      let dummy_shell_header =
        { Tezos_base.Block_header.level = 0l ;
          predecessor = genesis_block ;
          timestamp = Time.Protocol.epoch ;
          fitness = (Protocol.Fitness_repr.from_int64 0L) ;
          operations_hash = Operation_list_list_hash.zero ;
          proto_level = 0 ;
          validation_passes = 0 ;
          context = Context_hash.zero ;
        } in
      let json =
        Data_encoding.Json.construct
          Protocol.Parameters_repr.encoding
          Protocol.Parameters_repr.{
            bootstrap_accounts ;
            bootstrap_contracts = [] ;
            commitments = [] ;
            constants ;
            security_deposit_ramp_up_cycles = None ;
            no_reward_cycles = None;
          } in
      let proto_params =
        Data_encoding.Binary.to_bytes_exn Data_encoding.json json
      in
      let open Tezos_error_monad.TzMonad in
      Tezos_protocol_environment.Context.(
        add Memory_context.empty ["version"] (Bytes.of_string "genesis")
      ) >>= fun ctxt ->
      Tezos_protocol_environment.Context.(
        add ctxt protocol_param_key proto_params
      ) >>= fun ctxt ->
      Protocol.Main.init ctxt dummy_shell_header >>=?! fun { context; _ } ->
      let level = 0l in
      let timestamp = Time.Protocol.epoch in
      let fitness = [] in
      Protocol.Alpha_context.prepare context ~level ~timestamp
        ~predecessor_timestamp:timestamp
        ~fitness >>=?! fun context ->
      Error_monad.return context
    in
    match Lwt_main.run @@
      memory_context ()
    with
    | Ok context -> context
    | Error _ -> raise (Failure "Memory context not initialized")


  let step_constants ~source ~payer ~self ~amount ~chain_id =
    let open Error_monad in
    let result =
      Contract.of_b58check source >>? fun source ->
      Contract.of_b58check payer >>? fun payer ->
      Contract.of_b58check self >>? fun self ->
      let chain_id = Chain_id.of_b58check_opt chain_id |> Option.value ~default:Chain_id.zero in
      let amount = Tez.of_mutez amount |> Option.value ~default:Tez.zero in
      ok (Script_interpreter.{ source; payer; self; amount; chain_id; })
    in
    match result with
    | Ok res -> res
    | Error _ -> assert false


  (* WARNING: Calls Lwt_main.run *)
  (* FIXME: returns context *)
  let parse_michelson_file ~context ~filename =
    let fd = open_in filename in
    let content = Common.Utils.read_lines fd in
    close_in fd;
    let ast, locs =
      match Lwt_main.run (Parser.parse_michelson context content) with
      | Ok (ex_code, locations) -> (ex_code, locations)
      | Error errs ->
        Exceptions.panic "%a"
          (fun fmt err -> Michelson_v1_error_reporter.report_errors ~details:true
              ~show_source:true fmt err) errs
    in
    let statement, parameter_ty, storage_ty =
      Translate.translate filename locs ast
    in
    ast, statement, parameter_ty, storage_ty, locs

  (* TODO: adds interpret *)
  let interpret_crowbar = Crowbar_generators.interpret_crowbar
  let interpret_crowbar_multiple = Crowbar_generators.interpret_crowbar_multiple

end
include M
