open Mopsa_utils
open Common
open Tezos_007_common

module type STATE_LOGGER_PARAM = sig
  type location
  type locations =
    (Int.t * (Tezos_micheline.Micheline_parser.location * int list)) list

  val filename : string
  val locations : locations
end

type log_element =
  | Log of Location.range * M_ast.stack

module State_logger (P : STATE_LOGGER_PARAM) = struct

  open Error_monad

  type context = Protocol.Alpha_context.context
  type ('bef, 'aft) descr = ('bef, 'aft) Protocol.Script_typed_ir.descr

  type t = log_element list

  let log : log_element list ref = ref []

  let log_interp ctxt descr stack =
    ()

  let log_entry _ctxt _descr _stack =
    ()

  let log_exit : context ->
    ('bef, 'aft) descr ->
    'aft ->
    unit =
    fun ctxt descr stack ->
    if descr.loc <> -1 then
      (
        let range = Translate.translate_location P.filename P.locations descr.loc in
        let stack = Translate.translate_stack P.locations descr.aft stack in
        (* Format.eprintf "at %a, aft stack = %a@." *)
        (*   Location.pp_range range *)
        (*   M_ast.pp_stack stack ; *)
        log := Log (range, stack) :: !log
      )

  let get_log () =
    return_none

  let get_state () =
    !log
end

