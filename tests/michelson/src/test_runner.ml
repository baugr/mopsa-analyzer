open Mopsa_utils
open Mopsa_analyzer.Framework
open Core.All

let analyze_files (files:string list) (args:string list option) : int =
  let t = Timing.start () in
  let config = Config.Paths.resolve_config_file !Config.Abstraction.Parser.opt_config in
  let abstraction = Config.Abstraction.Parser.parse config in
  let domain = Config.Abstraction.Builder.from_json abstraction.domain in

  let prog = Runner.parse_program abstraction.language files in

  (* Top layer analyzer *)
  let module Domain = (val domain) in
  let module Toplevel = Toplevel.Make(Domain) in
  let module Engine = Engines.Automatic.Make(Toplevel) in
  (* let module Engine = Engines.Interactive.Engine.Make(Toplevel) in *)

  let flow = Engine.init prog in
  let stmt = mk_stmt (S_program (prog, args)) prog.prog_range in
  let res = Engine.exec stmt flow |> post_to_flow Engine.man in
  let t = Timing.stop t in
  Hook.on_finish Engine.man res;
  Output.Factory.report Engine.man res ~time:t ~files

let find filter dir =
  let rec aux acc files =
    match files with
    | fn::fns when Sys.is_directory fn ->
      Sys.readdir fn |>
      Array.to_list |>
      List.map (Filename.concat fn) |>
      List.rev_append fns |>
      aux acc
    | fn::fns when filter fn ->
      aux (fn::acc) fns
    | fn::fns ->
      aux acc fns
    | [] -> List.sort String.compare acc
  in
  aux [] [dir]

let run () =
  let config = "../../share/mopsa/configs/michelson/crowbar.json" in
  let files =
    match Sys.getenv_opt "MOPSA_CROWBAR_FILE" with
    | None ->
      []
    | Some fn ->
      if Sys.file_exists fn && Sys.is_directory fn; then
        find (fun s -> Filename.check_suffix s ".tz") fn
      else
        String.split_on_char ':' fn
  in
  let files = List.(filter Sys.file_exists files) in
  if files = [] then
    Exceptions.warn "MOPSA_CROWBAR_FILE env variable must be set to a directory or a colon-separated list of files";
  Config.Abstraction.Parser.opt_config := config;
  Debug.print_warnings := false;
  List.iter (fun file ->
      Printf.eprintf "Analyzing %s\n%!" file;
      let ret = analyze_files [file] None in
      Printf.eprintf "ret : %d\n" ret
    ) files

let () =
  run ()
