#!/bin/sh

RUNNER="./_build/test_runner"
DEFAULT="-v"
CONTRACTS_DIR="../../analyzer/tests/michelson/smart-contracts/onchain/carthagenet/"
UNIT_TESTS_DIR="../../analyzer/tests/michelson/nreg"
MAX_NUM=3

usage() {
        printf "suite.sh [OPTIONS] [DIR|FILE...]\n"
        printf "suite.sh [OPTIONS]\n"
}

unsplit() {
        files=""
        for f in "$@"; do
                if [ -n "$files" ]; then
                        files="$f:$files"
                else
                        files="$f"
                fi
        done
        echo "$files"
}

###############################################################################
# Leaf helpers
###############################################################################
run_one_quickcheck() {
        export MOPSA_CROWBAR_FILE="$1"
        $RUNNER $DEFAULT
}

run_one_afl() {
        export MOPSA_CROWBAR_FILE="$1"
        afl-fuzz -t 1000000 -i input -o output $RUNNER @@
}


###############################################################################
# AFL mode
###############################################################################
run_afl() {
        # At this time, use afl on a subset of carthage smart-contracts
        files=$(find "$1" -iname "*.tz" | shuf | head -n $MAX_NUM)
        files="$(unsplit $files)"
        run_one_afl "$files"
}

run_afl_contracts() {
        run_afl "$CONTRACTS_DIR"
}

run_afl_units() {
        run_afl "$UNIT_TESTS_DIR"
}


###############################################################################
# Quickcheck mode
###############################################################################
run_qc() {
        # At this time, use afl on a subset of carthage smart-contracts
        files=$(find "$1" -iname "*.tz" | shuf | head -n $MAX_NUM)
        echo "$files"
        files="$(unsplit $files)"
        run_one_quickcheck "$files"
}

run_qc_contracts() {
        run_qc "$CONTRACTS_DIR"
}

run_qc_unit() {
        run_qc "$UNIT_TESTS_DIR"
}


###############################################################################
# Quickcheck: one file at a time
###############################################################################
run_file() {
        printf "run_file %s\n" "$1"
        export MOPSA_CROWBAR_FILE="$1"
        $RUNNER $DEFAULT
}

run_directory() {
        for fn in "$1"/*; do
                export MOPSA_CROWBAR_FILE="$fn"
                $RUNNER $DEFAULT
        done
}

debug_file() {
        printf "debugging program %s\n" "$1"
        export MOPSA_CROWBAR_FILE="$1"
        $RUNNER $DEFAULT "$2"
}


if [ $# -eq 0 ]; then
        usage
        exit 0
elif [ $# -eq 1 ]; then
        case $1 in
                afl-contracts)
                        run_afl_contracts
                        ;;
                afl-unit)
                        run_afl_units
                        ;;
                quickcheck-contracts)
                        run_qc_contracts
                        ;;
                quickcheck-unit)
                        run_qc_unit
                        ;;
                *)
                        if [ -d "$1" ]; then
                                run_directory "$1"
                        elif [ -f "$1" ]; then
                                run_file "$1"
                        else
                                "Error: $1 not a dir or file"
                                exit 1
                        fi
                        ;;
        esac
else
        case $1 in
                debug)
                        run_qc_contracts "$2" "$3"
                        ;;
                *)
                        files=$(unsplit "$files")
                        run_file "${files}"
                        ;;
        esac
fi
