#!/bin/sh

CONFIG=michelson/intra-unique-default.json

DEBUG=false
ONLYFAIL=false
KEEPSTD=false
PRINTOUT=""
PRINTSRC=""
NREG=false
ONLYDIFF=false

gre="\033[32;32m"
red="\033[31;1m"
yel="\033[33;33m"
whi="\033[37;1m"
rst="\033[0m"

panic () {
        echo "panic: $*"
        exit 1
}

log_debug () {
        if [ $DEBUG = "true" ]; then
                printf "${yel}DEBUG: %s${rst}\n" "$*"
        fi
}

cmd () {
        if [ $DEBUG = "true" ]; then
                log_debug "  ** run '$*'"
        fi
        $@
}

errpipe_cmd() {
        out=$1
        err=$2
        shift 2
        if [ $DEBUG = "true" ]; then
                log_debug "  ** run '$* 1>$out 2> $err'"
        fi
        "$@" 1>"$out" 2> "$err"
}

summary() {
        printf "\nSummary\n"
        printf "=======\n"
        if [ "$corrects" -eq 0 ]; then
                printf "corrects: ${gre}$corrects${rst}/$total\n"
        else
                printf "corrects: ${gre}$corrects${rst}/$total\n"
        fi
        if [ "$failures" -eq 0 ]; then
                printf "failures: ${gre}$failures${rst}/$total\n"
        else
                printf "failures: ${red}$failures${rst}/$total\n"
        fi
        if [ "$crashes" -eq 0 ]; then
                printf "crashes: ${gre}$crashes${rst}/$total\n"
        else
                printf "crashes: ${red}$crashes${rst}/$total\n"
        fi
}

run_tests() {
        total=0
        failures=0
        corrects=0
        crashes=0

        while read -r name
        do
                stdfile=$(mktemp -t mopsa-michelson-XXXXXXXX)
                outfile="$stdfile.stdout"
                errfile="$stdfile.stderr"
                total=$((total + 1))
                errpipe_cmd "$outfile" "$errfile" ./bin/mopsa-michelson -config $CONFIG -format json "$name,address=tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" -output "$stdfile"
                success=$(jq '.success' $stdfile 2>/dev/null)
                bname=$(basename "$name")
                case $success in
                        true)
                                corrects=$((corrects+ 1))
                                if [ "$ONLYFAIL" = "false" ]; then
                                        printf "%s ... ${gre}ok${rst}\n" "$bname"
                                fi
                                ;;
                        false)
                                failures=$((failures + 1))
                                printf "%s ... ${red}failure${rst}\n" "$bname"
                                if [ "$PRINTOUT" = "true" ]; then
                                        printf "${whi}panic: %s${rst}\n" "$(jq -r '.exception' $stdfile)"
                                        printf "%s\n" "$(jq -r '.backtrace' $stdfile | head)"
                                fi
                                ;;
                        *)
                                crashes=$((crashes + 1))
                                printf "%s ... ${red}crash${rst}\n" "$bname"
                                if [ "$PRINTOUT" = "true" ]; then
                                        echo "---stdout---"
                                        head "$outfile"
                                        echo "---stderr---"
                                        head "$errfile"
                                        echo "------------"
                                fi
                                ;;
                esac

                if [ $KEEPSTD = "true" ]; then
                        echo "output : $stdfile"
                        echo "stdout : $outfile"
                        echo "stderr : $errfile"
                else
                        rm -f "$stdfile"
                        rm -f "$outfile"
                        rm -f "$errfile"
                fi
        done << EOF
$@
EOF
        summary
}

chain_contracts() {
        failing=""
        for script in analyzer/tests/michelson/smart-contracts/onchain/mainnet/scripts/*;
        do
                if ./bin/mopsa-michelson-rel -config $CONFIG "$script,address=tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" -no-warning -silent > stderr 2>&1 ; then
                        printf "%s: ok\n" "$script";
                else
                        printf "%s: error\n" "$script";
                        failing="$failing\n$script"
                        sed -n -e '/^panic in /,/Called.*src\/mopsa_analyzer\/lang\/michelson/p' stderr
                fi
        done
        echo "FAILING TESTS:"
        echo "$failing"
}


summary_nr () {
        printf "\nSummary\n"
        printf "=======\n"
        if [ "$pass" -eq 0 ]; then
                printf "pass: ${gre}%s${rst}/$total\n" "$pass"
        else
                printf "pass: ${gre}%s${rst}/$total\n" "$pass"
        fi
        if [ "$fail" -eq 0 ]; then
                printf "fail: ${gre}%s${rst}/$total\n" "$fail"
        else
                printf "fail: ${red}%s${rst}/$total\n" "$fail"
        fi
        if [ "$crashes" -eq 0 ]; then
                printf "crashes: ${gre}%s${rst}/$total\n" "$crashes"
        else
                printf "crashes: ${red}%s${rst}/$total\n" "$crashes"
        fi
}

run_nonreg () {
        total=0
        pass=0
        fail=0
        crashes=0
        while read -r name
        do
                stdfile="$(mktemp -t mopsa-michelson-XXXXXXXX)"
                difffile="${stdfile}.diff"
                outfile="$stdfile.stdout"
                errfile="$stdfile.stderr"
                total=$((total + 1))
                errpipe_cmd "$outfile" "$errfile" ./bin/mopsa-michelson -config $CONFIG -silent -lflow "$name,address=tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
                sed -i "/^\s\+Time:.*$/d" "$outfile"
                ret="$?"
                bname=$(basename "$name")
                if [ "$PROMOTE" = "true" ]; then
                        cp "$outfile" "${name}.expected"
                fi
                case "$ret" in
                        0|1)
                                if diff --color=always "${name}.expected" "$outfile" > "$difffile" 2>&1; then
                                        if [ "$ONLYFAIL" = "false" ]; then
                                                printf "%s ... ${gre}ok${rst}\n" "$bname"
                                        fi
                                        pass=$((pass + 1))
                                else
                                        printf "%s ... ${red}failure${rst}\n" "$bname"
                                        fail=$((fail + 1))
                                        if [ "$PRINTOUT" = "true" ]; then
                                                if [ "$PRINTSRC" = "true" ]; then
                                                        cat "$name"
                                                fi
                                                echo "output:"
                                                cat "$outfile"
                                                echo "diff:"
                                                cat "$difffile"
                                        fi
                                        if [ "$ONLYDIFF" = "true" ]; then
                                                echo "${name}"
                                                cat "$difffile"
                                        fi
                                fi
                                ;;
                        *)
                                crashes=$((crashes + 1))
                                printf "%s ... ${red}crash${rst}\n" "$bname"
                                if [ "$PRINTOUT" = "true" ]; then
                                        echo "---stdout---"
                                        head "$outfile"
                                        echo "---stderr---"
                                        head "$errfile"
                                        echo "------------"
                                fi
                                ;;
                esac

                if [ $KEEPSTD = "true" ]; then
                        echo "output : $stdfile"
                        echo "stdout : $outfile"
                        echo "stderr : $errfile"
                else
                        rm -f "$stdfile"
                        rm -f "$outfile"
                        rm -f "$errfile"
                fi
        done << EOF
$@
EOF
        summary_nr
}

command -v jq > /dev/null || panic "need jq"

usage() {
        echo "$0 [OPTIONS] [CMD] FILES..."
        echo "Options:"
        echo "  --only-fail:    be silent about successful tests"
        echo "  --debug:        verbose debugging of this script"
        echo "  --keep:         keep mopsa output file, stdout and stderr"
        echo "  --print:        print mopsa stack traces"
        echo "  --nreg:         only non regression tests"
        echo "  --promote:      promote non regression output to expected output"
        exit 0
}

tests=""
CMD=nreg
while [ $# -gt 0 ]; do
        case $1 in
                --only-fail)
                        ONLYFAIL=true
                        shift
                        ;;
                --debug)
                        DEBUG=true
                        shift
                        ;;
                --keep)
                        KEEPSTD=true
                        shift
                        ;;
                --print)
                        PRINTOUT=true
                        shift
                        ;;
                --help)
                        usage;
                        exit 0
                        ;;
                nreg)
                        CMD="nreg"
                        shift
                        ;;
                --src)
                        PRINTSRC=true
                        shift
                        ;;
                --onlydiff)
                        ONLYDIFF=true
                        shift
                        ;;
                --promote)
                        PROMOTE=true
                        shift
                        ;;
                chain)
                        CMD="chain"
                        shift
                        ;;
                simple)
                        CMD="simple"
                        shift
                        ;;
                *)
                        [ -z "$tests" ] && tests="$1" || tests=$(printf "%s\n%s" "$tests" "$1")
                        PRINTOUT=true
                        shift
                        ;;
        esac
done

case $CMD in
        nreg)
                if [ -z "$tests" ]; then
                        [ -z "$PRINTOUT" ] && PRINTOUT=false
                        tests=$(find analyzer/tests/michelson/simple -name '*.tz'|sort)
                fi
                tests=$(find analyzer/tests/michelson/nreg/ -name '*.tz'|sort)
                run_nonreg "$tests"
                ;;
        chain)
                chain_contracts
                ;;
        simple)
                if [ -z "$tests" ]; then
                        [ -z "$PRINTOUT" ] && PRINTOUT=false
                        tests=$(find analyzer/tests/michelson/simple -name '*.tz'|sort)
                fi
                run_tests "$tests"
                ;;
esac
