# Building

```sh
sudo apt install opam llvm clang llvm-dev libclang-dev libclang-cpp-dev libgmp-dev libmpfr-dev
./setup-tezos.sh
eval $(opam env)
autoconf -f
./configure
make
```


# Usage

```sh
./bin/mopsa-michelson -lflow FILE
```


# Mopsa general readme

[README.Mopsa.md](README.Mopsa.md)
