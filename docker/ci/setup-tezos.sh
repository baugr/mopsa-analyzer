#!/bin/sh
# set -x -e
export OPAMCLI=2.0

tezos_url="https://gitlab.com/tezos/tezos"
tezos_branch="latest-release"
tezos_checkout="995be816652888459d865e14c52bb5ac09e7b324"

tezos_dependencies="\
        tezos-client-006-PsCARTHA \
        tezos-protocol-006-PsCARTHA \
        tezos-client-007-PsDELPH1 \
        tezos-protocol-007-PsDELPH1"

mopsa_dependencies="\
        zarith \
        menhir \
        apron \
        yojson \
        crowbar \
        "

dev_dependencies="\
        ocp-indent \
        merlin"

msg () {
        printf "\033[31;36m%s\033[0m\n" "$*"
}

panic () {
        printf "\033[31;36m%s\033[0m\n" "$*"
        exit 1
}

# Checks the tezos repository is checked out in the parsers/* michelson
# directory and is on the correct branch
check_tezos_repository() {
        tezos_root="$1"
        msg "Checking tezos checkout... tezos_root=$tezos_root"
        if [ ! -d "$tezos_root" ]; then
                msg "Cloning $tezos_url ..."
                git clone $tezos_url --branch $tezos_branch "$tezos_root"
                msg "Resetting to $tezos_checkout"
                git -C "$tezos_root" reset --hard "$tezos_checkout"
        else
                msg "Checking-out branch $tezos_branch ..."
                current_branch=$(git -C "$tezos_root" rev-parse --abbrev-ref HEAD)
                msg "Fetching updates"
                git -C "$tezos_root" fetch --all
                if [ "$current_branch" = "$tezos_branch" ]; then
                        msg "Resetting to $tezos_checkout"
                        if ! git -C "$tezos_root" reset --hard "$tezos_checkout"; then
                                echo "Error: Cannot find commit $tezos_checkout"
                                exit 1
                        else
                                msg "ok"
                        fi
                else
                        msg "Resetting to $tezos_checkout"
                        #git -C "$tezos_root" checkout "$tezos_branch"
                        #git -C "$tezos_root" reset --hard $tezos_checkout
                fi
        fi
}


check_opam_switch() {
        export OPAMYES=${OPAMYES:=true}
        wanted_switch="$1"
        echo "check_opam_switch (wanted_switch=$1)"
        if [ "$(opam switch show --short)" = "$wanted_switch" ]; then
                echo "Opam switch is present"
                if [ "$(opam show ocaml -f version)" = "$ocaml_version" ]; then
                        return
                else
                        echo "Unexpected ocaml version. You should consider deleting your local opam switch with"
                        echo "  rm -rf _opam"
                        echo "and running this script again"
                        exit 1
                fi
        else
                msg "Setup opam switch with configured repositories"
                #opam switch create --repositories="$opam_repository_name" "$wanted_switch" --empty
                opam switch create "$wanted_switch" --empty
                eval "$(opam env --shell=sh)"
                #msg "Adding opam default repository"
                #opam repository add default --rank=-1
                msg "Installing ocaml-base-compiler"

                # install from local version
                #opam install ~/Public/git/ocaml || exit 1
                #opam switch set-base ocaml-variants

                # install from repo
                opam install "ocaml-base-compiler.$ocaml_version"
                opam switch set-invariant "ocaml-base-compiler.$ocaml_version" || opam switch set-base "ocaml-base-compiler.$ocaml_version"
                eval "$(opam env)"
        fi
}

pin_tezos_packages() {
        tezos_root="$1"
        export OPAMYES=${OPAMYES:=true}
        msg "Looking for removed packages"
        pins=$(opam pin | grep "file://" | cut -f 1 -d '.')
        toremove=""
        for pin in $pins; do
                dir=$(opam show "$pin" -f pin | cut -f 3- -d '/')
                if [ ! -d "$dir" ] && [ "$pin" != "ocaml-variants" ]; then
                        opam unpin "$pin" -n
                        toremove="$toremove $pin"
                fi
        done
        if [ "x" != "x$toremove" ]; then
                opam remove "$toremove" -y
        fi
        msg "Pinning local tezos packages from tezos_root=$tezos_root"
        # look for packages
        packages=$(find "$tezos_root/vendors" "$tezos_root/src" -name \*.opam -exec dirname {} \; | sort -u)

        #opam install opam-depext
        #opam depext conf-gmp conf-libev conf-m4 conf-perl conf-pkg-config conf-hidapi

        # Statically pin some packages
        opam pin hacl-star 0.3.2

        # pin all tezos packages
        echo "Pinning all tezos packages"
        dirs=$(opam pin | grep "file://" | cut -f 3- -d '/')
        for package in $packages; do
                if echo "$dirs" | grep -q -E "$package(\$| )" ; then
                        echo "$package already pinned"
                else
                        echo "Pinning $package"
                        opam pin -q -n "$package"
                fi
        done
        eval "$(opam env)"
}

install_rust_dependencies() {

        echo "OPAM_SWITCH_PREFIX: $OPAM_SWITCH_PREFIX";
        ZCASH_PARAMS="${OPAM_SWITCH_PREFIX}/share/zcash-params"
        OUTPUT_PARAMETERS="${opam_repository_url}/-/raw/${opam_repository_tag}/zcash-params/sapling-output.params"
        SPEND_PARAMETERS="${opam_repository_url}/-/raw/${opam_repository_tag}/zcash-params/sapling-spend.params"

        echo "Installing Sapling parameters in ${ZCASH_PARAMS}"
        #rm -rf "${ZCASH_PARAMS}"
        mkdir -p "${ZCASH_PARAMS}"
        curl -s -o "${ZCASH_PARAMS}/sapling-output.params" "${OUTPUT_PARAMETERS}"
        curl -s -o "${ZCASH_PARAMS}/sapling-spend.params" "${SPEND_PARAMETERS}"

}

install_dependencies() {
        tezos_root="$1"
        export OPAMYES=${OPAMYES:=true}
        # install the minimum dependencies
        msg "Installing tezos dependencies"
        tezos_dependencies=$(echo "$tezos_dependencies" | tr '\t ' ',')
        msg "dependencies: $tezos_dependencies"

        tezos_dependencies=$(opam list --required "$tezos_dependencies" --rec --pinned -s --with-test)
        msg "full dependencies: $tezos_dependencies"

        # shellcheck disable=SC2086
        opam install $mopsa_dependencies $dev_dependencies || exit 1
        opam install --ignore-constraints-on=ocaml,ocaml-variants \
                tezos-protocol-006-PsCARTHA tezos-client-006-PsCARTHA \
                tezos-protocol-007-PsDELPH1 tezos-client-007-PsDELPH1 || exit 1
}

tezos_root="$1"
wanted_switch="$2"
eval "$(opam env)"
check_tezos_repository "$tezos_root"

#. "$tezos_root/scripts/version.sh"
# override ocaml version
ocaml_version=4.12.1+options
opam_repository_tag=28c81453b41d5414a0d32af8fe10c5197f43d360
opam_repository_url=https://gitlab.com/tezos/opam-repository.git
opam_repository=$opam_repository_url\#$opam_repository_tag

# check_global_opam_repository
check_opam_switch "$wanted_switch"
#install_rust_dependencies
#pin_tezos_packages "$tezos_root"
#install_dependencies "$tezos_root"

sed "s#%%CURDIR%%#file://$PWD/vendors/tezos#" docker/ci/switch-export.opam > switch-export.opam
opam switch import switch-export.opam

