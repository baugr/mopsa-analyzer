#!/bin/sh

# This script is expected to be run from the project root
EXECUTABLE=$PWD/bin/mopsa-michelson
OUTPUT=benchs.log

rm -f ${OUTPUT}

TEMP=$(mktemp -t benchXXXX)
COUNT=0
for file in analyzer/tests/michelson/smart-contracts/onchain/mainnet/scripts/*.tz
do
        COUNT=$(($COUNT + 1))
        /usr/bin/time -o ${TEMP} -f "%e" ${EXECUTABLE} $file -silent -no-warning 2>&1 > stderr
        if [ $? -eq 0 ]; then
                cat ${TEMP} >> $OUTPUT
        else
                echo "ERROR: $file"
        fi
done

sort -n ${OUTPUT} | awk '
  BEGIN {
    c = 0;
    sum = 0;
  }
  $1 ~ /^(\-)?[0-9]*(\.[0-9]*)?$/ {
    a[c++] = $1;
    sum += $1;
  }
  END {
    ave = sum / c;
    if( (c % 2) == 1 ) {
      median = a[ int(c/2) ];
    } else {
      median = ( a[c/2] + a[c/2-1] ) / 2;
    }
    OFS="\t";
    printf "success    : " c "\n";
    printf "errors     : " '$COUNT' - c "\n";
    printf "total time : " sum "\n";
    printf "avg        : " ave "\n";
    printf "median     : " median "\n";
    printf "min        : " a[0] "\n";
    printf "max        : " a[c-1] "\n";
  }
'
