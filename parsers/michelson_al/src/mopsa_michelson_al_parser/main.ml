open Mopsa_utils

let debug fmt = Debug.debug ~channel:"michelson.parser.michelson_al" fmt

let parse_file (filename: string) =
  let fd = open_in filename in
  let lexbuf = Lexing.from_channel fd in
  Lexer.enter_state lexbuf Lexer.Source_file;
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename; };
  let parse_assertions lexbuf =
    let rec loop acc n lexbuf =
      try
        match Parser.assertion Lexer.main lexbuf with
        | None ->
          loop acc n lexbuf
        | Some (stmt) ->
          (* FIXME *)
          debug "%a: found assertion : %a"
            Location.pp_range (Al_cst.srange stmt) Al_cst.pp_statement stmt;
          loop (stmt::acc) (n+1) lexbuf
      with
      | End_of_file -> acc
      | Parser.Error ->
        let range = Location.from_lexing_range
            (Lexing.lexeme_start_p lexbuf)
            (Lexing.lexeme_end_p lexbuf)
        in
        let str = Lexing.lexeme lexbuf in
        raise (Exceptions.syntax_error range "syntax error on token '%s'" str)
    in
    List.rev (loop [] 0 lexbuf)
  in
  let assertions = parse_assertions lexbuf in
  let assertions =
    List.map (fun stmt -> Al_ast.translate_stmt stmt) assertions
  in
  assertions

let parse_constant ty (value:string) =
  let lexbuf = Lexing.from_string value in
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = "-"; };
  Lexer.enter_state lexbuf Lexer.Cli_parameter;
  let cst =
    try
      Parser.constant Lexer.main lexbuf
    with
    | End_of_file
    | Parser.Error ->
      let range = Location.from_lexing_range
          (Lexing.lexeme_start_p lexbuf)
          (Lexing.lexeme_end_p lexbuf)
      in
      raise (Exceptions.unnamed_syntax_error range)
  in
  Al_ast.translate_constant ~ty cst
