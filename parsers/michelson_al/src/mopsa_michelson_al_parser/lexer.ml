# 1 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
 
  open Mopsa_utils
  open Parser

  type state =
    | Source_file
    | Cli_parameter
    | Expression

  let pp_state fmt = function
    | Source_file -> Format.pp_print_string fmt "source_file"
    | Cli_parameter -> Format.pp_print_string fmt "cli_parameter"
    | Expression -> Format.pp_print_string fmt "expression"

  let state = ref []

  let enter_state lexbuf s =
    let range = Location.from_lexing_range
        lexbuf.Lexing.lex_start_p
        lexbuf.Lexing.lex_curr_p
    in
    Debug.debug "%a: switching state to %a" Location.pp_range range pp_state s;
    state := s::!state

  let panic lexbuf msg =
    let loc = Location.from_lexing_pos
      lexbuf.Lexing.lex_curr_p
    in
    let loc = Format.asprintf "%a" Location.pp_position loc in
    Exceptions.panic ~loc msg

  let cur_state () =
    match !state with
    | [] -> Exceptions.panic "no current state"
    | hd::tl -> hd

  let restore_state lexbuf =
    let state =
      match !state with
      | [] -> panic lexbuf "Cannot restore more state"
      | hd::tl -> state := tl
    in
    let range = Location.from_lexing_range
        lexbuf.Lexing.lex_start_p
        lexbuf.Lexing.lex_curr_p
    in
    Debug.debug "%a: restore state to %a"
      Location.pp_range range
      pp_state (cur_state())

# 53 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"
let __ocaml_lex_tables = {
  Lexing.lex_base =
   "\000\000\250\255\251\255\001\000\252\255\253\255\000\000\255\255\
    \254\255\002\000\253\255\004\000\254\255\255\255\001\000\255\255\
    \007\000\253\255\008\000\255\255\009\000\252\255\253\255\011\000\
    \254\255\255\255\014\000\251\255\252\255\000\000\015\000\255\255\
    \000\000\000\000\000\000\000\000\254\255\019\000\251\255\002\000\
    \253\255\016\000\254\255\255\255\002\000\001\000\001\000\001\000\
    \252\255\092\000\235\255\001\000\237\255\012\000\022\000\035\000\
    \240\255\019\000\020\000\021\000\246\255\247\255\248\255\249\255\
    \250\255\251\255\252\255\252\000\020\000\254\255\255\255\245\255\
    \243\255\241\255\239\255\236\255";
  Lexing.lex_backtrk =
   "\255\255\255\255\255\255\003\000\255\255\255\255\005\000\255\255\
    \255\255\255\255\255\255\001\000\255\255\255\255\255\255\255\255\
    \255\255\255\255\000\000\255\255\255\255\255\255\255\255\001\000\
    \255\255\255\255\255\255\255\255\255\255\004\000\000\000\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\004\000\
    \255\255\001\000\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\020\000\255\255\017\000\020\000\017\000\
    \255\255\013\000\011\000\020\000\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\002\000\001\000\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255";
  Lexing.lex_default =
   "\001\000\000\000\000\000\255\255\000\000\000\000\255\255\000\000\
    \000\000\010\000\000\000\255\255\000\000\000\000\015\000\000\000\
    \017\000\000\000\255\255\000\000\021\000\000\000\000\000\255\255\
    \000\000\000\000\027\000\000\000\000\000\255\255\255\255\000\000\
    \255\255\255\255\255\255\255\255\000\000\038\000\000\000\255\255\
    \000\000\255\255\000\000\000\000\255\255\255\255\255\255\255\255\
    \000\000\050\000\000\000\255\255\000\000\255\255\255\255\255\255\
    \000\000\255\255\255\255\255\255\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\255\255\255\255\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000";
  Lexing.lex_trans =
   "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\004\000\004\000\012\000\003\000\012\000\011\000\
    \019\000\019\000\019\000\024\000\018\000\024\000\023\000\031\000\
    \031\000\031\000\042\000\030\000\043\000\042\000\069\000\000\000\
    \041\000\000\000\005\000\007\000\013\000\000\000\000\000\019\000\
    \000\000\000\000\008\000\025\000\000\000\000\000\031\000\006\000\
    \075\000\000\000\000\000\043\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\036\000\048\000\053\000\053\000\053\000\053\000\
    \053\000\053\000\053\000\053\000\053\000\053\000\053\000\053\000\
    \053\000\053\000\053\000\053\000\053\000\053\000\053\000\053\000\
    \073\000\072\000\071\000\053\000\053\000\053\000\053\000\053\000\
    \053\000\053\000\053\000\053\000\053\000\000\000\000\000\000\000\
    \000\000\035\000\047\000\000\000\000\000\070\000\069\000\000\000\
    \000\000\068\000\000\000\000\000\000\000\000\000\000\000\032\000\
    \033\000\044\000\045\000\034\000\046\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\029\000\070\000\059\000\056\000\000\000\
    \039\000\000\000\000\000\000\000\066\000\065\000\051\000\000\000\
    \000\000\054\000\000\000\000\000\055\000\053\000\053\000\053\000\
    \053\000\053\000\053\000\053\000\053\000\053\000\000\000\000\000\
    \057\000\060\000\058\000\074\000\000\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\064\000\
    \000\000\063\000\000\000\067\000\000\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\062\000\
    \000\000\061\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \002\000\255\255\255\255\000\000\000\000\000\000\000\000\255\255\
    \000\000\022\000\000\000\000\000\000\000\000\000\028\000\000\000\
    \000\000\000\000\000\000\040\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\067\000\000\000\000\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\000\000\
    \000\000\000\000\000\000\067\000\052\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
    \000\000\000\000\000\000\000\000\000\000";
  Lexing.lex_check =
   "\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\000\000\003\000\009\000\000\000\011\000\009\000\
    \016\000\016\000\018\000\020\000\016\000\023\000\020\000\026\000\
    \026\000\030\000\041\000\026\000\037\000\037\000\068\000\255\255\
    \037\000\255\255\000\000\000\000\009\000\255\255\255\255\016\000\
    \255\255\255\255\006\000\020\000\255\255\255\255\026\000\000\000\
    \051\000\255\255\255\255\037\000\255\255\255\255\255\255\255\255\
    \255\255\255\255\035\000\047\000\053\000\053\000\053\000\053\000\
    \053\000\053\000\053\000\053\000\053\000\053\000\054\000\054\000\
    \054\000\054\000\054\000\054\000\054\000\054\000\054\000\054\000\
    \057\000\058\000\059\000\055\000\055\000\055\000\055\000\055\000\
    \055\000\055\000\055\000\055\000\055\000\255\255\255\255\255\255\
    \255\255\034\000\046\000\255\255\255\255\049\000\049\000\255\255\
    \255\255\049\000\255\255\255\255\255\255\255\255\255\255\029\000\
    \032\000\039\000\044\000\033\000\045\000\255\255\255\255\255\255\
    \255\255\255\255\255\255\026\000\049\000\049\000\049\000\255\255\
    \037\000\255\255\255\255\255\255\049\000\049\000\049\000\255\255\
    \255\255\049\000\255\255\255\255\049\000\049\000\049\000\049\000\
    \049\000\049\000\049\000\049\000\049\000\049\000\255\255\255\255\
    \049\000\049\000\049\000\055\000\255\255\049\000\049\000\049\000\
    \049\000\049\000\049\000\049\000\049\000\049\000\049\000\049\000\
    \049\000\049\000\049\000\049\000\049\000\049\000\049\000\049\000\
    \049\000\049\000\049\000\049\000\049\000\049\000\049\000\049\000\
    \255\255\049\000\255\255\049\000\255\255\049\000\049\000\049\000\
    \049\000\049\000\049\000\049\000\049\000\049\000\049\000\049\000\
    \049\000\049\000\049\000\049\000\049\000\049\000\049\000\049\000\
    \049\000\049\000\049\000\049\000\049\000\049\000\049\000\049\000\
    \255\255\049\000\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \000\000\014\000\009\000\255\255\255\255\255\255\255\255\016\000\
    \255\255\020\000\255\255\255\255\255\255\255\255\026\000\255\255\
    \255\255\255\255\255\255\037\000\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\067\000\255\255\255\255\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\255\255\
    \255\255\255\255\255\255\067\000\049\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\067\000\
    \067\000\067\000\067\000\067\000\067\000\067\000\067\000\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
    \255\255\255\255\255\255\255\255\255\255";
  Lexing.lex_base_code =
   "";
  Lexing.lex_backtrk_code =
   "";
  Lexing.lex_default_code =
   "";
  Lexing.lex_trans_code =
   "";
  Lexing.lex_check_code =
   "";
  Lexing.lex_code =
   "";
}

let rec source_file lexbuf =
   __ocaml_lex_source_file_rec lexbuf 0
and __ocaml_lex_source_file_rec lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 68 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Debug.debug "dash";
                 lex_inline_comment lexbuf
               )
# 242 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 1 ->
# 72 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Debug.debug "enter comment";
                 lex_comment lexbuf
               )
# 250 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 2 ->
# 76 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Debug.debug "enter string";
                 lex_source_string lexbuf;
                 source_file lexbuf
               )
# 259 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 3 ->
# 81 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Lexing.new_line lexbuf;
                 source_file lexbuf
               )
# 267 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 4 ->
# 85 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( raise End_of_file                          )
# 272 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 5 ->
# 86 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( source_file lexbuf                         )
# 277 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_source_file_rec lexbuf __ocaml_lex_state

and lex_source_string lexbuf =
   __ocaml_lex_lex_source_string_rec lexbuf 9
and __ocaml_lex_lex_source_string_rec lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 89 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( ()                                          )
# 289 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 1 ->
# 90 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Lexing.new_line lexbuf;
                 lex_source_string lexbuf;
               )
# 297 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 2 ->
# 94 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( lex_source_string lexbuf                    )
# 302 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_lex_source_string_rec lexbuf __ocaml_lex_state

and lex_source_bytes lexbuf =
   __ocaml_lex_lex_source_bytes_rec lexbuf 14
and __ocaml_lex_lex_source_bytes_rec lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 97 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( lex_bytes (Buffer.create 64) lexbuf         )
# 314 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_lex_source_bytes_rec lexbuf __ocaml_lex_state

and lex_bytes buffer lexbuf =
   __ocaml_lex_lex_bytes_rec buffer lexbuf 16
and __ocaml_lex_lex_bytes_rec buffer lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 100 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Buffer.contents buffer                      )
# 326 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 1 ->
# 101 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Lexing.new_line lexbuf;
                 lex_bytes buffer lexbuf
               )
# 334 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 2 ->
let
# 105 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
       c
# 340 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"
= Lexing.sub_lexeme_char lexbuf lexbuf.Lexing.lex_start_pos in
# 105 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Buffer.add_char buffer c;
                 lex_bytes buffer lexbuf
               )
# 347 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_lex_bytes_rec buffer lexbuf __ocaml_lex_state

and lex_expression_string buffer lexbuf =
   __ocaml_lex_lex_expression_string_rec buffer lexbuf 20
and __ocaml_lex_lex_expression_string_rec buffer lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 112 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Buffer.contents buffer                      )
# 359 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 1 ->
# 113 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Lexing.new_line lexbuf;
                 lex_expression_string buffer lexbuf
               )
# 367 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 2 ->
# 117 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( raise End_of_file                           )
# 372 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 3 ->
let
# 118 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
       c
# 378 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"
= Lexing.sub_lexeme_char lexbuf lexbuf.Lexing.lex_start_pos in
# 118 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Buffer.add_char buffer c;
                 lex_expression_string buffer lexbuf
               )
# 385 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_lex_expression_string_rec buffer lexbuf __ocaml_lex_state

and lex_inline_comment lexbuf =
   __ocaml_lex_lex_inline_comment_rec lexbuf 26
and __ocaml_lex_lex_inline_comment_rec lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 124 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 lex_inline_comment lexbuf
               )
# 399 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 1 ->
# 127 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Debug.debug "expression";
                 enter_state lexbuf Expression;
                 expression lexbuf
               )
# 408 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 2 ->
# 132 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Lexing.new_line lexbuf; EOE                )
# 413 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 3 ->
# 133 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( raise End_of_file                          )
# 418 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 4 ->
# 134 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( lex_inline_comment lexbuf                  )
# 423 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_lex_inline_comment_rec lexbuf __ocaml_lex_state

and lex_comment lexbuf =
   __ocaml_lex_lex_comment_rec lexbuf 37
and __ocaml_lex_lex_comment_rec lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 137 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( lex_comment lexbuf                         )
# 435 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 1 ->
# 138 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Lexing.new_line lexbuf; lex_comment lexbuf )
# 440 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 2 ->
# 139 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( raise End_of_file                          )
# 445 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 3 ->
# 140 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                  enter_state lexbuf Expression;
                  expression lexbuf
               )
# 453 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 4 ->
# 144 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( lex_comment lexbuf                         )
# 458 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_lex_comment_rec lexbuf __ocaml_lex_state

and expression lexbuf =
   __ocaml_lex_expression_rec lexbuf 49
and __ocaml_lex_expression_rec lexbuf __ocaml_lex_state =
  match Lexing.engine __ocaml_lex_tables __ocaml_lex_state lexbuf with
      | 0 ->
# 147 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( expression lexbuf                                  )
# 470 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 1 ->
# 148 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "EOE"; EOE                             )
# 475 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 2 ->
let
# 149 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
           i
# 481 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"
= Lexing.sub_lexeme lexbuf lexbuf.Lexing.lex_start_pos lexbuf.Lexing.lex_curr_pos in
# 149 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Debug.debug "ident : %s" i;
                 T_IDENT(i)
               )
# 488 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 3 ->
# 153 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "LPAR"; LPAR                           )
# 493 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 4 ->
# 154 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "RPAR"; RPAR                           )
# 498 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 5 ->
# 155 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "LBRACKET"; LBRACKET                   )
# 503 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 6 ->
# 156 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "RBRACKET"; RBRACKET                   )
# 508 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 7 ->
# 157 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( LBRACE                                             )
# 513 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 8 ->
# 158 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( RBRACE                                             )
# 518 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 9 ->
# 159 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "T_EQ"; T_EQ                           )
# 523 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 10 ->
# 160 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "T_NE"; T_NE                           )
# 528 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 11 ->
# 161 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "T_GT"; T_GT                           )
# 533 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 12 ->
# 162 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "T_GE"; T_GE                           )
# 538 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 13 ->
# 163 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "T_LT"; T_LT                           )
# 543 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 14 ->
# 164 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "T_LE"; T_LE                           )
# 548 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 15 ->
# 165 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 let buf = Buffer.create 32 in
                 let s = lex_expression_string buf lexbuf in
                 T_STRING s
               )
# 557 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 16 ->
# 170 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                 Debug.debug "enter bytes";
                 T_BYTES (lex_source_bytes lexbuf)
               )
# 565 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 17 ->
let
# 174 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
             d
# 571 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"
= Lexing.sub_lexeme lexbuf lexbuf.Lexing.lex_start_pos lexbuf.Lexing.lex_curr_pos in
# 174 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( Debug.debug "T_INT(%s)" d; T_INT d                 )
# 575 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 18 ->
# 175 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                  if cur_state() = Source_file then
                    raise End_of_file
                  else
                    (Debug.debug "EOE"; EOE)
               )
# 585 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 19 ->
# 181 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               (
                  if cur_state() = Source_file then
                    (Debug.debug "EOE"; EOE)
                  else
                    panic lexbuf "Unexpected sequence: '*/'"
               )
# 595 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | 20 ->
let
# 187 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
       c
# 601 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"
= Lexing.sub_lexeme_char lexbuf lexbuf.Lexing.lex_start_pos in
# 187 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
               ( panic lexbuf "Unexpected char: %c" c       )
# 605 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"

  | __ocaml_lex_state -> lexbuf.Lexing.refill_buff lexbuf;
      __ocaml_lex_expression_rec lexbuf __ocaml_lex_state

;;

# 189 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.mll"
 
  let main lexbuf =
    let state = cur_state() in
    Debug.debug "Calling lexer with state : %a" pp_state (cur_state());
    match cur_state() with
    | Source_file -> source_file lexbuf
    | Cli_parameter -> expression lexbuf
    | Expression -> expression lexbuf

# 622 "/home/melanargia/Work/projects/mopsa/parsers/michelson_al/src/michelson_al_parser/lexer.ml"
