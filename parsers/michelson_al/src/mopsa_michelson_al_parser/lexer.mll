{
  open Mopsa_utils
  open Parser

  type state =
    | Source_file
    | Cli_parameter
    | Expression

  let pp_state fmt = function
    | Source_file -> Format.pp_print_string fmt "source_file"
    | Cli_parameter -> Format.pp_print_string fmt "cli_parameter"
    | Expression -> Format.pp_print_string fmt "expression"

  let state = ref []

  let panic lexbuf msg =
    let loc = Location.from_lexing_pos
      lexbuf.Lexing.lex_curr_p
    in
    let loc = Format.asprintf "%a" Location.pp_position loc in
    Exceptions.panic ~loc msg

  let cur_state () =
    match !state with
    | [] -> Exceptions.panic "no current state"
    | hd::tl -> hd

  let enter_state lexbuf s =
    let range = Location.from_lexing_range
        lexbuf.Lexing.lex_start_p
        lexbuf.Lexing.lex_curr_p
    in
    Debug.debug "%a: switching state to %a" Location.pp_range range pp_state s;
    state := s::!state

  let restore_state lexbuf =
    (match !state with
    | [] -> panic lexbuf "Cannot restore more state"
    | hd::tl -> state := tl);
    let range = Location.from_lexing_range
        lexbuf.Lexing.lex_start_p
        lexbuf.Lexing.lex_curr_p
    in
    Debug.debug "%a: restore state to %a"
      Location.pp_range range
      pp_state (cur_state())
}

let space    = [' ' '\t']
let newline  = "\n" | "\r" | "\r\n"
let alpha    = ['_' 'a'-'z' 'A'-'Z']

let digit    = ['0'-'9']
let hexit    = ['0'-'9' 'a'-'f' 'A'-'F']

let decimal  = '-'? digit+
let hexadecimal = "0x" hexit+

let integer  = decimal
let alphanum = alpha | digit
let ident    = alpha (alphanum | '-')*

rule source_file = parse
| "#"          {
                 Debug.debug "dash";
                 lex_inline_comment lexbuf
               }
| "/*"         {
                 Debug.debug "enter comment";
                 lex_comment lexbuf
               }
| '"'          {
                 Debug.debug "enter string";
                 lex_source_string lexbuf;
                 source_file lexbuf
               }
| newline      {
                 Lexing.new_line lexbuf;
                 source_file lexbuf
               }
| eof          { raise End_of_file                          }
| _            { source_file lexbuf                         }

and lex_source_string = parse
| '"'          { ()                                          }
| newline      {
                 Lexing.new_line lexbuf;
                 lex_source_string lexbuf;
               }
| _            { lex_source_string lexbuf                    }

and lex_expression_string buffer = parse
| '"'          { Buffer.contents buffer                      }
| newline      {
                 Lexing.new_line lexbuf;
                 lex_expression_string buffer lexbuf
               }
| eof          { raise End_of_file                           }
| _ as c       {
                 Buffer.add_char buffer c;
                 lex_expression_string buffer lexbuf
               }

and lex_inline_comment = parse
| space        {
                 lex_inline_comment lexbuf
               }
| "mopsa:"     {
                 Debug.debug "expression";
                 enter_state lexbuf Expression;
                 expression lexbuf
               }
| newline      { Lexing.new_line lexbuf; EOE                }
| eof          { raise End_of_file                          }
| _            { lex_inline_comment lexbuf                  }

and lex_comment = parse
| space        { lex_comment lexbuf                         }
| newline      { Lexing.new_line lexbuf; lex_comment lexbuf }
| eof          { raise End_of_file                          }
| "mopsa:"     {
                  enter_state lexbuf Expression;
                  expression lexbuf
               }
| _            { lex_comment lexbuf                         }

and expression = parse
| space        { expression lexbuf                                  }
| newline      { Lexing.new_line lexbuf;
                 Debug.debug "EOE";
                 restore_state lexbuf;
                 EOE
               }
| ident as i   {
                 Debug.debug "T_IDENT(%s)" i;
                 T_IDENT(i)
               }
| '('          { Debug.debug "LPAR"; LPAR                           }
| ')'          { Debug.debug "RPAR"; RPAR                           }
| '['          { Debug.debug "LBRACKET"; LBRACKET                   }
| ']'          { Debug.debug "RBRACKET"; RBRACKET                   }
| '{'          { Debug.debug "LBRACE"; LBRACE                       }
| '}'          { Debug.debug "RBRACE"; RBRACE                       }
| '='          { Debug.debug "T_EQ"; T_EQ                           }
| "!="         { Debug.debug "T_NE"; T_NE                           }
| ">"          { Debug.debug "T_GT"; T_GT                           }
| ">="         { Debug.debug "T_GE"; T_GE                           }
| "<"          { Debug.debug "T_LT"; T_LT                           }
| "<="         { Debug.debug "T_LE"; T_LE                           }
| ';'          { Debug.debug "SEMI"; SEMI                           }
| '"'          {
                 let buf = Buffer.create 32 in
                 let s = lex_expression_string buf lexbuf in
                 Debug.debug "T_STRING(%s)" s;
                 T_STRING s
               }
| hexadecimal as d
               {
                  Debug.debug "T_BYTES(%s)" d;
                  T_BYTES (d)
               }
| integer as d {
                 Debug.debug "T_INT(%s)" d;
                 T_INT d
               }
| eof          {
                  if cur_state() = Source_file then
                    raise End_of_file
                  else
                    (Debug.debug "EOE"; EOE)
               }
| "*/"         {
                  if cur_state() = Source_file then
                    (Debug.debug "EOE"; EOE)
                  else
                    panic lexbuf "Unexpected sequence: '*/'"
               }
| _ as c       { panic lexbuf "Unexpected char: %c" c       }

{
  let main lexbuf =
    (* Debug.debug "Calling lexer with state : %a" pp_state (cur_state()); *)
    match cur_state() with
    | Source_file -> source_file lexbuf
    | Cli_parameter -> expression lexbuf
    | Expression -> expression lexbuf
}
