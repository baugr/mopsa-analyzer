%{
  open Mopsa_utils
  open Al_cst
  let mk_pos s e =
    Location.from_lexing_range s e
%}

%start <Al_cst.constant> constant
%start <Al_cst.statement option> assertion
%token <string> T_IDENT
%token <string> T_INT
%token <string> T_STRING
%token <string> T_BYTES
%token EOE
%token LPAR RPAR LBRACE RBRACE LBRACKET RBRACKET
%token SEMI
%token T_EQ T_NE T_LT T_LE T_GT T_GE

%nonassoc T_EQ T_NE T_LT T_LE T_GT T_GE

%%

%inline binop:
| T_EQ                                       { O_eq                                           }
| T_NE                                       { O_ne                                           }
| T_LT                                       { O_lt                                           }
| T_LE                                       { O_le                                           }
| T_GT                                       { O_gt                                           }
| T_GE                                       { O_ge                                           }

ident:
| id = T_IDENT                               { mk_ident id (mk_pos $startpos $endpos)         }

node:
| v = T_INT                                  { mk_int v (mk_pos $startpos $endpos)            }
| v = T_STRING                               { mk_string v (mk_pos $startpos $endpos)         }
| v = T_BYTES                                { mk_bytes v (mk_pos $startpos $endpos)          }
| id = ident                                 { mk_prim id [] (mk_pos $startpos $endpos) }
| LPAR p = toplevel_node RPAR                { p                                              }
| LBRACE l = items RBRACE                    { mk_seq l (mk_pos $startpos $endpos)            }

items:
|                                            { []                                             }
| c = toplevel_node                          { [c]                                             }
| c = toplevel_node SEMI tl = items          { c::tl                                          }

toplevel_node:
| n = node                                   { n }
| id = ident args = node+                    { mk_prim id args (mk_pos $startpos $endpos)     }


(*
 * Constant entry point
 *)
constant:
| n = toplevel_node EOE                      { n }


(*
 * Assertion entry point
 *)
variable:
| v = ident LBRACKET i = T_INT RBRACKET      { mk_index v i (mk_pos $startpos $endpos)        }

expression:
| n = node                                   { mk_constant n (mk_pos $startpos $endpos)       }
| v = variable                               { v                                              }
| n1 = expression op = binop n2 = expression
{ mk_binop op (mk_pos $startpos(op) $endpos(op)) n1 n2 (mk_pos $startpos $endpos) }

assertion:
| id = ident LPAR e = expression RPAR EOE    { Some (mk_call id e (mk_pos $startpos $endpos)) }
| EOE                                        { None                                           }
