open Mopsa_utils
type range = Location.range


(******************************************************************************
 * Micheline subset
 ******************************************************************************)
type ident =
  { name : string; range : range }

type node =
  | N_int of Z.t * range
  | N_string of string * range
  | N_bytes of string * range
  | N_prim of ident * node list * range
  | N_seq of node list * range

let node_range = function
  | N_int (_, range)
  | N_string (_, range)
  | N_bytes (_, range)
  | N_prim (_, _, range)
  | N_seq (_, range) -> range

type constant = node

let pp_ident fmt { name; _ } =
  Format.fprintf fmt "%s" name

let rec pp_node fmt : node -> unit = function
  | N_int (s, _) -> Z.pp_print fmt s
  | N_string (s, _) -> Format.fprintf fmt "%s" s
  | N_bytes (b, _) ->
    Format.fprintf fmt "0x";
    String.iter (fun ch -> Format.fprintf fmt "%x" (Char.code ch)) b
  | N_seq (l, _) ->
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
      pp_node
      fmt
      l
  | N_prim (ident, nodes, _) ->
    Format.fprintf fmt "(%a %a)" pp_ident ident
    (Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt " ")
      pp_node
    )
    nodes

let mk_ident name range =
  { name; range }

let mk_int i range =
  N_int (Z.of_string i, range)

let mk_string s range =
  N_string (s, range)

(* let mk_bytes bytes range = *)
(*   let of_hex_string s = *)
(*     let to_char c = *)
(*       match c with *)
(*       | '0'..'9' -> Char.code c - 48 *)
(*       | 'A'..'F' -> Char.code c - 55 *)
(*       | 'a'..'f' -> Char.code c - 87 *)
(*       | _ -> Exceptions.panic "unexpected char for bytes: %c" c *)
(*     in *)
(*     let len = *)
(*       let slen = String.length s - 2 in *)
(*       if slen mod 2 = 0 then *)
(*         (String.length s - 2) / 2 *)
(*       else *)
(*         Exceptions.panic "unexpected odd length for bytes: %d" slen *)
(*     in *)
(*     Bytes.init len ((fun i -> *)
(*         let idx1 = i * 2 + 2 in *)
(*         let idx2 = i * 2 + 3 in *)
(*         let res = ((to_char (s.[idx1]) lsl 4)) + (to_char ((s.[idx2]))) in *)
(*         Char.chr res *)
(*       )) *)
(*   in *)
(*   N_bytes (of_hex_string (String.sub bytes 2 (String.length bytes - 2)), range) *)

let mk_bytes (bytes:string) range =
  N_bytes ((String.sub bytes 2 (String.length bytes - 2)), range)

let mk_prim ident args range =
  N_prim (ident, args, range)

let mk_seq l range =
  N_seq (l, range)


(******************************************************************************
 * Assertions subset
 ******************************************************************************)
type binop =
  | O_eq
  | O_ne
  | O_lt
  | O_le
  | O_gt
  | O_ge

type expression =
  | E_subscript of ident * string * range
  | E_constant of node * range
  | E_binop of binop * range * expression * expression * range

type statement =
  | A_call of ident * expression * range


let pp_binop fmt = function
  | O_eq -> Format.fprintf fmt "="
  | O_ne -> Format.fprintf fmt "<>"
  | O_lt -> Format.fprintf fmt "<"
  | O_le -> Format.fprintf fmt "<="
  | O_gt -> Format.fprintf fmt ">"
  | O_ge -> Format.fprintf fmt ">="

let rec pp_expression fmt = function
  | E_subscript(ident, i, _) ->
    Format.fprintf fmt "%a[%s]" pp_ident ident i
  | E_constant (node, _) ->
    Format.fprintf fmt "%a" pp_node node
  | E_binop (op, _, n1, n2, _) ->
    Format.fprintf fmt "(%a %a %a)" pp_expression n1 pp_binop op pp_expression n2

let pp_statement fmt = function
  | A_call(ident, expression, _) ->
    Format.fprintf fmt "%a(%a)" pp_ident ident pp_expression expression


let mk_constant c range =
  E_constant(c, range)

let mk_binop op orange e1 e2 range =
  E_binop (op, orange, e1, e2, range)

let mk_index id idx irange =
  E_subscript(id, idx, irange)

let mk_call id expr range =
  A_call (id, expr, range)

let srange stmt =
  match stmt with
  | A_call(_, _, range) -> range
