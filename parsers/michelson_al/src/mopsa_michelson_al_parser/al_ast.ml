open Mopsa_utils

(******************************************************************************
 * Types definitions
 ******************************************************************************)
type typ =
  | T_unknown
  | T_unit
  | T_bool
  | T_int
  | T_nat
  | T_mutez
  | T_string
  | T_bytes
  (* Domain specific *)
  | T_address
  | T_chain_id
  | T_key
  | T_key_hash
  | T_operation
  | T_signature
  | T_timestamp
  (* Containers *)
  | T_pair of typ * typ
  | T_union of typ * string option * typ * string option
  | T_option of typ
  | T_list of typ
  | T_set of typ
  | T_map of typ * typ
  | T_big_map of typ * typ
  (* Others *)
  | T_lambda of typ * typ
  | T_contract of typ

type constant_kind =
  | Unit
  | Bool of bool
  | Mutez of Z.t
  | Nat of Z.t
  | Int of Z.t
  | String of string
  | Bytes of string
  | Timestamp of Z.t
  (* TODO *)
  | Signature of string
  | Key of string
  | Key_hash of string
  | Address of string * string
  (* Containers *)
  | Option of constant option
  | Pair of constant * constant
  | Union of union
  | List of constant list
  | Set of constant list
  | Map of (constant * constant) list

and union =
  | L of constant
  | R of constant

and constant =
  {
    ctyp: typ;
    ckind : constant_kind;
  }

type expression_kind =
  | E_constant of constant
  | E_stackpos_var of int
  | E_binop of Al_cst.binop * expression * expression

and expression =
  {
    ekind : expression_kind;
    erange : Location.range;
    etyp : typ;
  }

type statement_kind =
  | S_assert of expression
  | S_assert_unreachable

and statement =
  {
    skind : statement_kind;
    srange : Location.range;
  }


(******************************************************************************
 * Pretty printing
 ******************************************************************************)
let pp_typ fmt typ =
  let is_atom = function
    | T_unknown
    | T_unit
    | T_bool
    | T_int
    | T_nat
    | T_mutez
    | T_string
    | T_bytes
    | T_address
    | T_chain_id
    | T_key
    | T_key_hash
    | T_operation
    | T_signature
    | T_timestamp -> true
    | T_pair _
    | T_list _
    | T_map _
    | T_set _
    | T_big_map _
    | T_union _
    | T_option _
    | T_lambda _
    | T_contract _ -> false
  in
  let rec pp_typ fmt typ =
    match typ with
    | T_unknown -> Format.pp_print_string fmt "'a"
    | T_unit -> Format.pp_print_string fmt "unit"
    | T_bool -> Format.pp_print_string fmt "bool"
    | T_int -> Format.pp_print_string fmt "int"
    | T_nat -> Format.pp_print_string fmt "nat"
    | T_mutez -> Format.pp_print_string fmt "mutez"
    | T_string -> Format.pp_print_string fmt "string"
    | T_bytes -> Format.pp_print_string fmt "bytes"
    (* Domain specific *)
    | T_address -> Format.pp_print_string fmt "address"
    | T_chain_id -> Format.pp_print_string fmt "chain_id"
    | T_key -> Format.pp_print_string fmt "key"
    | T_key_hash -> Format.pp_print_string fmt "key_hash"
    | T_operation -> Format.pp_print_string fmt "operation"
    | T_signature -> Format.pp_print_string fmt "signature"
    | T_timestamp -> Format.pp_print_string fmt "timestamp"
    (* Containers *)
    | T_pair(t1, t2) ->
      Format.fprintf fmt "pair %a %a" pp_typ_nest t1 pp_typ_nest t2
    | T_union(t1, _, t2, _) ->
      Format.fprintf fmt "(%a|%a)" pp_typ_nest t1 pp_typ_nest t2
    | T_option t ->
      Format.fprintf fmt "%a option" pp_typ_nest t
    | T_list t -> Format.fprintf fmt "%a list" pp_typ_nest t
    | T_set t -> Format.fprintf fmt "%a set" pp_typ_nest t
    | T_map (t1, t2) ->
      Format.fprintf fmt "(%a,%a) map" pp_typ_nest t1 pp_typ_nest t2
    | T_big_map (t1, t2) ->
      Format.fprintf fmt "(%a,%a) bigmap" pp_typ_nest t1 pp_typ_nest t2
    (* Others *)
    | T_lambda (t1, t2) ->
      Format.fprintf fmt "(%a,%a) lambda" pp_typ_nest t1 pp_typ_nest t2
    | T_contract t -> Format.fprintf fmt "%a contract" pp_typ_nest t
  and pp_typ_nest fmt typ =
    if is_atom typ then
      pp_typ fmt typ
    else
      Format.fprintf fmt "(%a)" pp_typ typ
  in
  pp_typ fmt typ

let rec pp_constant fmt constant =
  match constant.ckind with
  | Unit -> Format.fprintf fmt "unit"
  | Bool b -> Format.fprintf fmt "%b" b
  | Mutez z -> Format.fprintf fmt "(%a:mutez)" Z.pp_print z
  | Int i -> Format.fprintf fmt "(%a:int)" Z.pp_print i
  | Nat i -> Format.fprintf fmt "(%a:nat)" Z.pp_print i
  | Pair (x, y) -> Format.fprintf fmt "(%a, %a)" pp_constant x pp_constant y
  | Option (Some x) -> Format.fprintf fmt "Some (%a)" pp_constant x
  | Option None -> Format.fprintf fmt "None"
  | List (l) ->
    Format.fprintf fmt "{ %a }"
      (Format.pp_print_list pp_constant
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ";"))
      l
  | Set l ->
    Format.fprintf fmt "{ %a }"
      (Format.pp_print_list
         pp_constant ~pp_sep:(fun fmt () -> Format.fprintf fmt ";"))
      l
  | Map l ->
    Format.fprintf fmt "{ %a }"
      (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ";")
         (fun fmt (k, v) -> Format.fprintf fmt "Elt %a %a"
             pp_constant k
             pp_constant v
         ))
      l
  | String s -> Format.fprintf fmt "\"%s\"" s
  | Bytes s -> Format.fprintf fmt "0x%s" s
  | Address (s, e) -> Format.fprintf fmt "\"%s%%%s\"" s e
  | Timestamp t -> Format.fprintf fmt "%a" Z.pp_print t
  | Union (L v) -> Format.fprintf fmt "(Left %a)" pp_constant v
  | Union (R v) -> Format.fprintf fmt "(Right %a)" pp_constant v
  | Key_hash kh -> Format.fprintf fmt "\"%s\"" kh
  | _ -> Exceptions.panic "TODO"

let rec pp_expr fmt ?(typed=false) ?(range=false) expr =
  let pp_expr fmt expr = pp_expr fmt ~typed ~range expr in
  let pp_expr_kind fmt ekind =
    match ekind with
    | E_constant (constant) ->
      Format.fprintf fmt "%a" pp_constant constant
    | E_stackpos_var i ->
      Format.fprintf fmt "stackpos[%d]" i
    | E_binop (op, e1, e2) ->
      Format.fprintf fmt "%a %a %a" pp_expr e1 Al_cst.pp_binop op pp_expr e2
  in
  let pp_expr fmt expr =
    if typed then
      Format.fprintf fmt "%a : %a" pp_expr_kind expr.ekind pp_typ expr.etyp
    else
      pp_expr_kind fmt expr.ekind
  in
  let pp_expr fmt expr =
    if range then
      Format.fprintf fmt "%a : %a"
        Location.pp_range expr.erange pp_expr expr
    else
      pp_expr fmt expr
  in
  pp_expr fmt expr


let pp_stmt fmt ?(typed=false) ?(range=false) stmt =
  let pp_expr fmt expr = pp_expr fmt ~typed ~range expr in
  let pp_stmt_kind fmt skind =
    match skind with
    | S_assert (expr) -> Format.fprintf fmt "assert(%a)" pp_expr expr
    | S_assert_unreachable -> Format.fprintf fmt "assert_unreachable"
  in
  let pp_stmt fmt stmt =
    if range then
      Format.fprintf fmt "%a -- %a"
        Location.pp_range stmt.srange
        pp_stmt_kind stmt.skind
    else
      pp_stmt_kind fmt stmt.skind
  in
  pp_stmt fmt stmt


let rec ty_of_node n =
  match n with
  | Al_cst.N_prim ({name="unit"; _}, [], loc) -> T_unit
  | N_prim ({name="bool"; _}, [], loc) -> T_bool
  | N_prim ({name="nat"; _}, [], loc) -> T_nat
  | N_prim ({name="int"; _}, [], loc) -> T_int
  | N_prim ({name="mutez";_}, [], loc) -> T_mutez
  | N_prim ({name="timestamp"; _}, [], loc) -> T_timestamp
  | N_prim ({name="bytes"; _}, [], loc) -> T_bytes
  | N_prim ({name="string"; _}, [], loc) -> T_string
  | N_prim ({name="address"; _}, [], loc) -> T_address
  | N_prim ({name="chain_id"; _}, [], loc) -> T_chain_id
  | N_prim ({name="key"; _}, [], loc) -> T_key
  | N_prim ({name="key_hash"; _}, [], loc) -> T_key_hash
  | N_prim ({name="operation"; _}, [], loc) -> T_operation
  | N_prim ({name="signature"; _}, [], loc) -> T_signature
  | N_prim ({name="option"; _}, [ty], loc) ->
    T_option(ty_of_node ty)
  | N_prim ({name="pair"; _}, [ty1; ty2], loc) ->
    T_pair(ty_of_node ty1, ty_of_node ty2)
  | N_prim ({name="or"; _}, [ty1; ty2], loc) ->
    T_union (ty_of_node ty1, None, ty_of_node ty2, None)
  | N_prim ({name="list"; _}, [ty], loc) ->
    T_list (ty_of_node ty)
  | N_prim ({name="set"; _}, [ty], loc) ->
    T_set (ty_of_node ty)
  | N_prim ({name="map"; _}, [kty; vty], loc) ->
    T_map (ty_of_node kty, ty_of_node vty)
  | N_prim ({name="big_map"; _}, [kty; vty], loc) ->
    T_big_map(ty_of_node kty, ty_of_node vty)
  | N_prim ({name="contract"; _}, [ty], loc) ->
    T_contract (ty_of_node ty)
  | N_prim ({name="lambda"; _}, [pty; rty], loc) ->
    T_lambda (ty_of_node pty, ty_of_node rty)
  | N_prim ({name; _}, _, loc) ->
    Exceptions.panic_at loc "Unexpected primitive: %s" name
  | N_string (_, loc)
  | N_bytes (_, loc)
  | N_seq (_, loc)
  | N_int (_, loc) ->
    Exceptions.panic_at loc "Unexpected node: %a" Al_cst.pp_node n


(******************************************************************************
 * Constructors
 ******************************************************************************)
let mk_constant ~ty ckind =
  { ctyp = ty; ckind }

let mk_unit ~ty () =
  mk_constant ~ty Unit

let mk_int ~ty i =
  mk_constant ~ty (Int i)

let mk_mutez ~ty i =
  mk_constant ~ty (Mutez i)

let mk_nat ~ty i =
  mk_constant ~ty (Nat i)

let mk_bool ~ty b =
  mk_constant ~ty (Bool b)

let mk_string ~ty s =
  mk_constant ~ty (String s)

let mk_address ~ty address entry  =
  mk_constant ~ty (Address (address, entry))

let mk_bytes ~ty s =
  mk_constant ~ty (Bytes s)

let mk_pair ~ty c1 c2 =
  mk_constant ~ty (Pair (c1, c2))

let mk_none ~ty () =
  mk_constant ~ty (Option None)

let mk_some ~ty o =
  mk_constant ~ty (Option (Some o))

let mk_left ~ty l =
  mk_constant ~ty (Union (L l))

let mk_right ~ty r =
  mk_constant ~ty (Union (R r))

let mk_list ~ty items =
  mk_constant ~ty (List items)

let mk_set ~ty items =
  mk_constant ~ty (Set items)

let mk_map ~ty items =
  mk_constant ~ty (Map items)

(*
 * Expressions
 *)
let mk_index i erange =
  { ekind = E_stackpos_var i; etyp = T_unknown; erange }

let mk_binop op l r erange =
  let etyp =
    match op with
    | Al_cst.O_eq
    | O_ne
    | O_lt
    | O_gt
    | O_le
    | O_ge -> T_bool
  in
  { ekind = E_binop (op, l, r); etyp; erange }

(*
 * Statements
 *)
let mk_assert expr srange =
  { skind = S_assert (expr); srange }


(******************************************************************************
 * Second stage parsing, from Al_cst ( ~= micheline ) to constants
 ******************************************************************************)
let rec translate_constant ?(ty=T_unknown) n : constant =
  match ty, n with
  | (T_unknown|T_unit), Al_cst.N_prim({name="Unit"; _}, [], _) ->
    mk_unit ~ty ()
  | (T_unknown|T_int), Al_cst.N_int (i, _) -> mk_int ~ty i
  | T_nat, N_int(i, _) -> mk_nat ~ty i
  | T_mutez, N_int (i, _) -> mk_mutez ~ty i
  | (T_unknown|T_string), N_string (s, _) -> mk_string ~ty s
  | T_address, N_string(s, _) ->
    (match String.split_on_char '%' s with
     | [] -> Exceptions.panic "Should not happen"
     | [address] -> mk_address ~ty address "default"
     | [address; entrypoint] -> mk_address ~ty address entrypoint
     | _ -> Exceptions.panic "Should not happen"
    )
  | (T_unknown|T_bytes), N_bytes (s, _) -> mk_bytes ~ty s
  | (T_unknown|T_bool), N_prim ({name="True"; _}, [], _) -> mk_bool ~ty true
  | (T_unknown|T_bool), N_prim ({name="False"; _}, [], loc) -> mk_bool ~ty false
  | T_pair(t1, t2), N_prim ({name="Pair"; _}, [c1; c2], _) ->
    let c1 = translate_constant ~ty:t1 c1 in
    let c2 = translate_constant ~ty:t2 c2 in
    mk_pair ~ty c1 c2
  | (T_unknown), N_prim ({name="Pair"; _}, [c1; c2], loc) ->
    let c1 = translate_constant c1 in
    let c2 = translate_constant c2 in
    mk_pair ~ty c1 c2
  | T_list(vty), N_seq(nodes, _) ->
    let items = List.map (translate_constant ~ty:vty) nodes in
    mk_list ~ty items
  | T_set(vty), N_seq(nodes, _) ->
    let items = List.map (translate_constant ~ty:vty) nodes in
    mk_set ~ty items
  | T_map(kty, vty), N_seq(nodes, _) ->
    let items = List.map (translate_elt kty vty) nodes in
    mk_map ~ty items
  | T_big_map(kty, vty), N_seq(nodes, _) ->
    let items = List.map (translate_elt kty vty) nodes in
    mk_map ~ty items
  | _ , N_prim ({name="None"; _}, [], loc) ->
    (match ty with
     | T_unknown -> mk_none ~ty ()
     | T_option(ty) -> mk_none ~ty ()
     | _ -> Exceptions.panic_at loc "Unexpected type for None: %a" pp_typ ty
    )
  | _, N_prim ({name="Some"; _}, [node], loc) ->
    (match ty with
     | T_unknown -> mk_some ~ty (translate_constant node)
     (* TODO: assert types are compatible *)
     | T_option ctyp -> mk_some ~ty (translate_constant ~ty node)
     | _ -> Exceptions.panic_at loc "Unexpected type for Some: %a" pp_typ ty
    )
  | _, N_prim ({name="Left"; _}, [node], loc) ->
    (match ty with
     | T_unknown ->
       mk_left ~ty (translate_constant node)
     (* TODO: assert types are compatible *)
     | T_union (lty, _, _, _) ->
       mk_left ~ty (translate_constant ~ty:lty node)
     | _ -> Exceptions.panic_at loc "Unexpected type for Left: %a" pp_typ ty
    )
  | _, N_prim ({name="Right"; _}, [node], loc) ->
    (match ty with
     | T_unknown ->
       mk_right ~ty (translate_constant node)
     | T_union (_, _, rty, _) ->
       mk_right ~ty (translate_constant ~ty:rty node)
     | _ -> Exceptions.panic_at loc "Unexpected type for Right: %a" pp_typ ty
    )
  | _, N_int(_, loc)
  | _, N_string(_, loc)
  | _, N_bytes(_, loc) ->
    Exceptions.panic_at loc "Unexpected value for type %a: %a" pp_typ ty Al_cst.pp_node n
  | _, N_prim(_, _, loc) -> Exceptions.panic_at loc "Cannot parse %a" Al_cst.pp_node n
  | typ, N_seq (_, loc) ->
    Exceptions.panic_at loc "Unexpected seq of type %a while expecting a constant: %a" pp_typ typ Al_cst.pp_node n

and translate_elt kty vty elt =
  match elt with
  | N_prim ({name="Elt"; _}, (key::value::[]), range) ->
    let key = translate_constant ~ty:kty key in
    let value = translate_constant ~ty:vty value in
    key, value
  | _ ->
    Exceptions.panic_at (Al_cst.node_range elt)
      "Unable to parse map element"

let rec translate_expr n =
  match n with
  | Al_cst.E_subscript({name; range}, idx, loc) ->
    let to_int i =
      try
        Int32.to_int (Int32.of_string i)
      with _ ->
        Exceptions.panic "Cannot parse '%s' as an integer" i
    in
    (match name with
     | "stackpos" -> mk_index (to_int idx) loc
     | "stack_pos" ->
       Debug.warn_at loc "please use 'stackpos' instead of 'stack_pos'";
       mk_index (to_int idx) loc
     | _ ->
       Exceptions.panic "Unrecognized identifier: %s" name
    )
  | E_binop(op, _, l, r, loc) ->
    mk_binop op (translate_expr l) (translate_expr r) loc
  | E_constant(c, loc) ->
    let c = translate_constant c in
    {
      ekind = E_constant(c);
      erange = loc;
      etyp = T_unknown;
    }

let rec translate_stmt stmt =
  match stmt with
  | Al_cst.A_call({name="assert"; _}, expr, loc) ->
    mk_assert (translate_expr expr) loc
  | Al_cst.A_call({name; _},_, loc) ->
    Exceptions.panic_at loc "Unrecognized assertion: %s" name
