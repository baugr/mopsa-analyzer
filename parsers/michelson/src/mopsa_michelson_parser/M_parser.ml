open Mopsa_tezos.Common.Sigs

type michelson_version =
  | Tezos_006
  | Tezos_007

let tezos_module version =
  match version with
  (* | Tezos_006 -> (module Tezos_006) *)
  | Tezos_007 ->
    let module MopsaTezos =
      Mopsa_tezos.Tezos.Make_tezos(Mopsa_tezos.Tezos_007.Main)
    in
    (module MopsaTezos: MOPSA_TEZOS)
  | _ -> assert false

