open Mopsa
open Universal.Ast

type typ +=
  | T_mcs_unit
  | T_mcs_bool
  | T_mcs_mutez
  | T_mcs_int
  | T_mcs_timestamp
  | T_mcs_string
  | T_mcs_bytes
  | T_mcs_address
  | T_mcs_chain_id
  | T_mcs_key
  | T_mcs_key_hash
  | T_mcs_operation
  | T_mcs_signature
  (* Containers *)
  | T_mcs_pair of typ * typ
  | T_mcs_union of typ * string option * typ * string option
  | T_mcs_option of typ
  | T_mcs_list of typ
  | T_mcs_set of typ
  | T_mcs_map of typ * typ
  | T_mcs_lambda of typ * typ
  | T_mcs_alpha

type annot = Var_annot of string
type stack_ty = (annot option * typ) list

let pp_stack_item fmt v =
  match v with
  | Some (Var_annot s), typ -> Format.fprintf fmt "%a %%%s" pp_typ typ s
  | None, typ -> Format.fprintf fmt "%a" pp_typ typ

let pp_stack fmt v = (Format.pp_print_list
                        ~pp_sep:(fun fmt () -> Format.fprintf fmt "; ")
                        pp_stack_item) fmt v

let stack_get_type stack index =
  let len = List.length stack in
  if index < 0 || index >= len then
    panic "invalid access to item %d of stack: %a" index pp_stack stack
  else
    let (_, typ) = List.nth stack index in
    typ

(* get stack type from index relative to stack pointer *)
let stack_get_type_spi stack spi =
  let index = (List.length stack - 1) - spi in
  stack_get_type stack index

let () =
  register_typ {
    compare = (fun next t1 t2 ->
        match t1, t2 with
        | T_mcs_pair (t1, t2), T_mcs_pair (t1', t2') ->
          Compare.compose [
            (fun () -> compare_typ t1 t1');
            (fun () -> compare_typ t2 t2')
          ]
        | T_mcs_union(t1, _, t2, _), T_mcs_union (t1', _, t2', _) ->
          Compare.compose [
            (fun () -> compare_typ t1 t1');
            (fun () -> compare_typ t2 t2')
          ]
        | T_mcs_option(t1), T_mcs_option(t2) -> compare_typ t1 t2
        | T_mcs_list(t1), T_mcs_list(t2) -> compare_typ t1 t2
        | T_mcs_set (t1), T_mcs_set (t2) -> compare_typ t1 t2
        | T_mcs_map(tk1, tv1), T_mcs_map (tk2, tv2) ->
          Compare.compose [
            (fun () -> compare_typ tk1 tk2);
            (fun () -> compare_typ tv1 tv2)
          ]
        | T_mcs_lambda (arg1, ret1), T_mcs_lambda (arg2, ret2) ->
          Compare.compose [
            (fun () -> compare_typ arg1 arg2);
            (fun () -> compare_typ ret1 ret2)
          ]
        | _ -> next t1 t2
      );

    print = (fun default fmt typ ->
        let open Format in
        match typ with
        | T_mcs_unit -> pp_print_string fmt "m_unit"
        | T_mcs_bool -> pp_print_string fmt "m_bool"
        | T_mcs_mutez -> pp_print_string fmt "m_mutez"
        | T_mcs_int -> pp_print_string fmt "m_int"
        | T_mcs_timestamp -> pp_print_string fmt "m_timestamp"
        | T_mcs_string -> pp_print_string fmt "m_string"
        | T_mcs_bytes -> pp_print_string fmt "m_bytes"
        | T_mcs_address -> pp_print_string fmt "m_address"
        | T_mcs_chain_id -> pp_print_string fmt "m_chain_id"
        | T_mcs_key -> pp_print_string fmt "m_key"
        | T_mcs_key_hash -> pp_print_string fmt "m_key_hash"
        | T_mcs_operation -> pp_print_string fmt "m_operation"
        | T_mcs_signature -> pp_print_string fmt "m_signature"
        (* Containers *)
        | T_mcs_pair (t1, t2) -> Format.fprintf fmt "(pair %a %a)" pp_typ t1 pp_typ t2
        | T_mcs_union (t1, _, t2, _) -> Format.fprintf fmt "(or %a %a)" pp_typ t1 pp_typ t2
        | T_mcs_option t -> Format.fprintf fmt "(option %a)" pp_typ t
        | T_mcs_list t -> Format.fprintf fmt "(list %a)" pp_typ t
        | T_mcs_set t -> Format.fprintf fmt "(set %a)" pp_typ t
        | T_mcs_map (key, v) -> Format.fprintf fmt "(map %a %a)" pp_typ key pp_typ v
        | T_mcs_lambda (arg, ret) -> Format.fprintf fmt "(lambda %a %a)" pp_typ arg pp_typ ret
        | T_mcs_alpha -> Format.fprintf fmt "'a"
        | _ -> default fmt typ
      );
  }

type constant_union =
  | C_mcs_left of constant
  | C_mcs_right of constant

type address =
  | Address_caller of (string * string) option
  | Address_filename of string
  | Address_constant of string * string

let compare_address x y =
  match x, y with
  | Address_caller (Some (addr1, ep1)), Address_caller (Some (addr2, ep2)) ->
    Compare.compose [
      (fun () -> String.compare addr1 addr2);
      (fun () -> String.compare ep1 ep2);
    ]
  | Address_filename x, Address_filename y ->
    String.compare x y
  | Address_constant (addr1, ep1) , Address_constant (addr2, ep2) ->
    Compare.compose [
      (fun () -> String.compare addr1 addr2);
      (fun () -> String.compare ep1 ep2);
    ]
  | _ -> Stdlib.compare x y

type constant +=
  | C_mcs_pair of constant * constant
  | C_option of constant option
  | C_mcs_list of constant list
  | C_mcs_set of constant list
  | C_mcs_map of (constant * constant) list
  | C_mcs_union of constant_union
  | C_mcs_address of address
  | C_mcs_contract of string * string
  | C_mcs_lambda of stmt
  | C_mcs_type of typ

let () =
  register_constant {
    compare = (fun next c1 c2 ->
        match c1, c2 with
        | C_mcs_pair (v1, v2), C_mcs_pair (v1', v2') ->
          Compare.compose [
            (fun () -> compare_constant v1 v1');
            (fun () -> compare_constant v2 v2')
          ]
        | C_option (Some v1), C_option (Some v2) ->
          compare_constant v1 v2
        | C_mcs_list l1, C_mcs_list l2 ->
          Compare.list compare l1 l2
        (* XXX: what the importance of ordering in this case ? *)
        | C_mcs_set l1, C_mcs_set l2 ->
          Exceptions.warn "Comparing 2 sets as list: non-ordering is being ignored";
          Compare.list compare l1 l2
        | C_mcs_map l1, C_mcs_map l2 ->
          Compare.list
            (fun (k1, v1) (k2, v2) ->
               Compare.compose [
                 (fun () -> compare_constant k1 k2);
                 (fun () -> compare_constant v1 v2);
               ]) l1 l2
        | C_mcs_union (C_mcs_left v1), C_mcs_union (C_mcs_left v2)
        | C_mcs_union (C_mcs_right v1), C_mcs_union (C_mcs_right v2) ->
          compare_constant v1 v2
        | C_mcs_address addr1 , C_mcs_address addr2 ->
          compare_address addr1 addr2
        | C_mcs_contract (v1, e1), C_mcs_contract (v2, e2) ->
          Compare.compose [
            (fun () -> String.compare v1 v2);
            (fun () -> String.compare e1 e2);
          ]
        | C_mcs_lambda stm1, C_mcs_lambda stm2 ->
          compare_stmt stm1 stm2
        | C_mcs_type t1, C_mcs_type t2 ->
          compare_typ t1 t2
        | _ -> next c1 c2
      );

    print = (fun default fmt -> function
        | C_mcs_pair(v1, v2) -> Format.fprintf fmt "(%a, %a)" pp_constant v1
                              pp_constant v2
        | C_option None -> Format.fprintf fmt "None"
        | C_option (Some v) -> Format.fprintf fmt "Some (%a)" pp_constant v
        | C_mcs_list l
        | C_mcs_set l ->
          Format.fprintf fmt "{ %a }"
            (fun fmt l ->
               Format.pp_print_list
                 ~pp_sep:(fun fmt () -> Format.pp_print_string fmt "; ")
                 (fun fmt item -> pp_constant fmt item)
                 fmt
                 l)
            l
        | C_mcs_map l ->
          Format.fprintf fmt "{ %a }"
            (fun fmt l ->
               Format.pp_print_list
                 ~pp_sep:(fun fmt () -> Format.pp_print_string fmt "; ")
                 (fun fmt (k,v) -> Format.fprintf fmt "Elt %a %a"
                     pp_constant k
                     pp_constant v
                 ) fmt l)
            l
        | C_mcs_union (C_mcs_left v) ->
          Format.fprintf fmt "(Left %a)" pp_constant v
        | C_mcs_union (C_mcs_right v) ->
          Format.fprintf fmt "(Right %a)" pp_constant v
        | C_mcs_address addr ->
          (match addr with
           | Address_caller _ ->
             Format.fprintf fmt "caller"
           | Address_filename fn ->
             Format.fprintf fmt "%s" fn
           | Address_constant (addr, ep) ->
             Format.fprintf fmt "\"%s%%%s\"" addr ep
          )
        | C_mcs_contract (s, e) ->
          Format.fprintf fmt "\"%s%%%s\"" s e
        | C_mcs_lambda stm ->
          Format.fprintf fmt "lambda {%a}" pp_stmt stm
        | C_mcs_type t -> pp_typ fmt t
        | c -> default fmt c
      );
  }

type stack = Mopsa_michelson_parser.M_ast.stack

type var_kind +=
  | V_stackpos of int
  | V_ghost of var * string
  | V_mcs_named of string * string (* contract address and name *)
  | V_mcs_alpha of string

let rec pp_var_untyped next fmt v =
  match vkind v with
  | V_ghost (var, name) ->
    Format.fprintf fmt "%a.%s" (pp_var_untyped next) var name
  | V_stackpos i ->
    Format.fprintf fmt "stackpos[%d]" i;
  | V_mcs_named(address, name) ->
    Format.fprintf fmt "%s.%s" address name
  | _ -> next fmt v

let pp_var_typed next fmt v =
  match vkind v with
  | V_stackpos i ->
    Format.fprintf fmt "stackpos[%d] : %a" i pp_typ v.vtyp
  | V_ghost(vv, name) ->
    Format.fprintf fmt "%a.%s : %a" (pp_var_untyped next) vv name pp_typ v.vtyp
  | V_mcs_named(address, name) ->
    Format.fprintf fmt "%s.%s : %a" address name pp_typ v.vtyp
  | _ -> next fmt v

let () =
  register_var {
    compare = (fun next v1 v2 ->
        match vkind v1, vkind v2 with
        | V_stackpos i, V_stackpos j -> Int.compare i j
        | V_ghost (v1', name1), V_ghost(v2',name2) ->
          Compare.compose [
            (fun () -> compare_var v1' v2');
            (fun () -> String.compare name1 name2)
          ]
        | V_mcs_named(c1, v1), V_mcs_named(c2, v2) ->
          Compare.compose [
            (fun () -> String.compare c1 c2);
            (fun () -> String.compare v1 v2);
          ]
        | V_mcs_alpha(n1), V_mcs_alpha(n2) -> String.compare n1 n2
        | _ -> next v1 v2
      );
    print = (fun next fmt v -> pp_var_typed next fmt v)
  }

let mk_stackpos typ n =
  let vname = Format.asprintf "(sp[%d]:%a)" n pp_typ typ in
  mkv vname (V_stackpos n) typ

let mk_var_ghost ?(mode=STRONG) typ var name =
  let vname = Format.asprintf "(%s.%s):%a" var.vname name pp_typ typ in
  mkv vname ~mode (V_ghost(var, name)) typ

let mk_var_named ?(mode=STRONG) typ ?(address="") name =
  let vname = Format.asprintf "(%s.%s:%a)" address name pp_typ typ in
  mkv vname ~mode (V_mcs_named(address, name)) typ

let mk_var_alpha ?(mode=STRONG) name =
  let name = Format.asprintf "%s" name in
  mkv name ~mode (V_mcs_alpha(name)) T_mcs_alpha

type operator +=
  | O_isnat

let () = register_operator {
    compare = (fun next op1 op2 -> next op1 op2);
    print = (fun default fmt op ->
        match op with
        | O_isnat -> Format.fprintf fmt "isnat"
        | _ -> default fmt op
      )
  }

type instr =
  | S_mcs_drop of typ (* typ of the dropped variable *)
  | S_mcs_dup of typ (* typ of the created variable *)
  | S_mcs_swap of typ * typ (* typ of the created variables *)
  | S_mcs_const of typ * constant
  | S_mcs_cons_pair of typ * typ
  | S_mcs_car of typ
  | S_mcs_cdr of typ
  | S_mcs_unpair of typ
  | S_mcs_cons_some of typ
  | S_mcs_cons_none of typ
  | S_mcs_if_none of typ * stmt * stmt
  | S_mcs_cons_left of typ
  | S_mcs_cons_right of typ
  | S_mcs_if_left of typ * stmt * stmt
  | S_mcs_cons_list of typ
  | S_mcs_nil of typ
  | S_mcs_if_cons of typ * stmt * stmt
  | S_mcs_list_map of stmt
  | S_mcs_list_iter of stmt
  | S_mcs_list_size of typ
  | S_mcs_empty_set of typ
  | S_mcs_set_iter of stmt
  | S_mcs_set_mem of { ity: typ }
  | S_mcs_set_update of { item: typ }
  | S_mcs_set_size
  | S_mcs_empty_map of { kty: typ; vty: typ }
  | S_mcs_map_map of stmt
  | S_mcs_map_iter of stmt
  | S_mcs_map_mem of { kty: typ; vty: typ }
  | S_mcs_map_get of { kty: typ; vty: typ }
  | S_mcs_map_update of { kty: typ; vty: typ }
  | S_mcs_map_size of { kty: typ; vty: typ }
  | S_mcs_empty_big_map
  | S_mcs_big_map_mem
  | S_mcs_big_map_get
  | S_mcs_big_map_update
  | S_mcs_concat_string
  | S_mcs_concat_string_pair
  | S_mcs_slice_string
  | S_mcs_string_size
  | S_mcs_concat_bytes
  | S_mcs_concat_bytes_pair
  | S_mcs_slice_bytes
  | S_mcs_bytes_size
  | S_mcs_add_seconds_to_timestamp
  | S_mcs_add_timestamp_to_seconds
  | S_mcs_sub_timestamp_seconds
  | S_mcs_diff_timestamps
  | S_mcs_add_tez
  | S_mcs_sub_tez
  | S_mcs_mul_teznat
  | S_mcs_mul_nattez
  | S_mcs_ediv_teznat
  | S_mcs_ediv_tez
  | S_mcs_or
  | S_mcs_and
  | S_mcs_xor
  | S_mcs_not
  | S_mcs_is_nat
  | S_mcs_neg_nat
  | S_mcs_neg_int
  | S_mcs_abs_int
  | S_mcs_int_nat
  | S_mcs_add_intint
  | S_mcs_add_intnat
  | S_mcs_add_natint
  | S_mcs_add_natnat
  | S_mcs_sub_int
  | S_mcs_mul_intint
  | S_mcs_mul_intnat
  | S_mcs_mul_natint
  | S_mcs_mul_natnat
  | S_mcs_ediv_intint
  | S_mcs_ediv_intnat
  | S_mcs_ediv_natint
  | S_mcs_ediv_natnat
  | S_mcs_lsl_nat
  | S_mcs_lsr_nat
  | S_mcs_or_nat
  | S_mcs_and_nat
  | S_mcs_and_int_nat
  | S_mcs_xor_nat
  | S_mcs_not_nat
  | S_mcs_not_int
  | S_mcs_seq of stmt * stmt
  | S_mcs_if of stmt * stmt
  | S_mcs_loop of stmt
  | S_mcs_loop_left of stmt
  | S_mcs_dip of stmt
  | S_mcs_exec
  | S_mcs_apply
  | S_mcs_lambda of stmt
  | S_mcs_failwith of typ
  | S_mcs_nop
  | S_mcs_compare of typ
  | S_mcs_eq
  | S_mcs_neq
  | S_mcs_lt
  | S_mcs_gt
  | S_mcs_le
  | S_mcs_ge
  | S_mcs_address
  | S_mcs_contract of typ * string
  | S_mcs_transfer_tokens
  | S_mcs_create_account
  | S_mcs_implicit_account
  | S_mcs_create_contract of stmt
  | S_mcs_create_contract_2 of { parameter_ty: typ ; storage_ty: typ; code: stmt }
  | S_mcs_set_delegate
  | S_mcs_now
  | S_mcs_balance
  | S_mcs_check_signature
  | S_mcs_hash_key
  | S_mcs_pack
  | S_mcs_unpack
  | S_mcs_blake2b
  | S_mcs_sha256
  | S_mcs_sha512
  | S_mcs_sha3
  | S_mcs_keccak
  | S_mcs_steps_to_quota
  | S_mcs_source
  | S_mcs_sender
  | S_mcs_self of string
  | S_mcs_amount
  | S_mcs_dig of int
  | S_mcs_dug of int
  | S_mcs_dipn of int * stmt
  | S_mcs_dropn of int
  | S_mcs_chain_id
  (* Edo *)
  | S_mcs_level
  | S_mcs_self_address
  | S_mcs_voting_power
  | S_mcs_total_voting_power
  (* other *)
  | S_mcs_wrapped_before of { stm: stmt; assertion: stmt }
  | S_mcs_wrapped_after of { stm: stmt; assertion: stmt }

type contract =
  {
    (* FIXME: add filename ? ( only for error messages )  *)
    filename: string;
    statement: stmt;
    parameter_ty: typ;
    storage_ty: typ;
    tezos_ast: (module Mopsa_tezos.Common.Sigs.TEZOS_PARSED_CONTRACT);
    (* FIXME: remove that: *)
    fixme_storage_ty : Mopsa_michelson_al_parser.Al_ast.typ;
    fixme_parameter_ty : Mopsa_michelson_al_parser.Al_ast.typ;
  }

(* TODO: ideally, allow abstract values *)
type contract_context =
  {
    storage: constant option;
    parameter : constant option;
    amount: constant option;
    address: string option;
    entrypoint: string option;
    balance: Z.t option;
  }

type stmt_kind +=
  | S_michelson_instr of
      {
        instr: instr;
        bef: stack_ty;
        aft: stack_ty;
        sp: int;
        loc: Location.range;
      }
  (* Inter-contract *)
  | S_mcs_inter_exec of
      {
        contract: contract option;
        address: string option;
        entry: string option;
        amount: expr;
        parameter: expr
      }
  | S_mcs_inter_operation_list_iter of { operations: expr }
  | S_mcs_inter_exec_transfer of { transfer : expr }
  | S_mcs_inter_exec_transfer_address of {
      address: expr;
      parameter : expr;
      amount: expr;
    }
  | S_mcs_store_add_contracts of (contract * contract_context) list
  | S_mcs_store_exec_contract of {
      address: Common.Addresses.Address.t;
      parameter: expr;
      amount: expr;
    }

let compare_contract
    { statement = stm1;
      parameter_ty = param1;
      storage_ty = stor1; _}
    { statement = stm2;
      parameter_ty = param2;
      storage_ty = stor2; _} =
  Compare.compose [
    (fun () -> compare_stmt stm1 stm2);
    (fun () -> compare_typ param1 param2);
    (fun () -> compare_typ stor1 stor2);
  ]

let pp_contract fmt v =
  Format.fprintf fmt "@[<v 0>@[<v 2>{@;";
  Format.fprintf fmt "filename     = %a@;" Format.pp_print_string v.filename;
  Format.fprintf fmt "statement    = ...@;";
  Format.fprintf fmt "parameter_ty = %a@;" pp_typ v.parameter_ty;
  Format.fprintf fmt "storage_ty   = %a" pp_typ v.storage_ty;
  Format.fprintf fmt "@]@;}@]"

let pp_contract_short fmt v = Format.pp_print_string fmt v.filename

let pp_contract_context fmt v =
  let pp_opt pp fmt o =
    (Format.pp_print_option pp) fmt o
  in
  Format.fprintf fmt "@[<v 0>@[<v 2>{@;";
  Format.fprintf fmt "storage    = %a@;" (pp_opt pp_constant) v.storage;
  Format.fprintf fmt "parameter  = %a@;" (pp_opt pp_constant) v.parameter;
  Format.fprintf fmt "amount     = %a@;" (pp_opt pp_constant) v.amount;
  Format.fprintf fmt "address    = %a@;" (pp_opt Format.pp_print_string) v.address;
  Format.fprintf fmt "entrypoint = %a" (pp_opt Format.pp_print_string) v.entrypoint;
  Format.fprintf fmt "@]@;}@]"

module Contracts_map = Map.Make(String)

type prog_kind +=
  | P_michelson of {
      contract: contract;
      context: contract_context;
      externs: (contract * contract_context) list;
    }

module RangeMap = Mopsa_tezos.Common.Utils.RangeMap

module StateStoreKey = GenContextKey(struct
    type 'a t = 'a flow RangeMap.t
    let print pp fmt state_store = Format.fprintf fmt "Range store"
  end)

let init_state_store flow =
  let ctx = Flow.get_ctx flow in
  Flow.set_ctx (add_ctx StateStoreKey.key RangeMap.empty ctx) flow

let get_state_store flow =
  Flow.get_ctx flow |>
  find_ctx StateStoreKey.key

let add_state_store range flow =
  let ctx = Flow.get_ctx flow in
  let range_map = find_ctx StateStoreKey.key ctx in
  let range_map = RangeMap.add range flow range_map in
  Flow.set_ctx (add_ctx StateStoreKey.key range_map ctx) flow

(******************************************************************************
 * Current context of evaluation
 * Stores the current contract address and entry point
 *****************************************************************************)
type michelson_context =
  {
    address: string option;
    entry: string;
  }

module ContractContextKey = GenContextKey(struct
    type 'a t = michelson_context
    let print _ fmt { address; entry } =
      Format.fprintf fmt "%s%%%s"
        (match address with None -> "unknown address" | Some v -> v)
        entry
  end)

let current_contract_ctx = ContractContextKey.key

let set_current_contract_context mctx flow =
  Flow.set_ctx (Flow.get_ctx flow |> add_ctx current_contract_ctx mctx) flow

let get_current_contract_context flow =
  Flow.get_ctx flow |> find_ctx current_contract_ctx
