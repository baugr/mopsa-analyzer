open Mopsa_michelson_parser
open Mopsa
open Universal.Ast
open Mopsa_michelson_al_parser
open Ast
open Unstacked_ast

exception Typing_error

let rec translate_ty ty =
  match ty with
  | Al_ast.T_unit -> T_mcs_unit
  | T_bool -> T_mcs_bool
  | T_int -> T_mcs_int
  | T_nat -> T_mcs_int
  | T_mutez -> T_mcs_mutez
  | T_signature -> T_mcs_signature
  | T_string -> T_mcs_string
  | T_bytes -> T_mcs_bytes
  | T_key -> T_mcs_key
  | T_key_hash -> T_mcs_key_hash
  | T_timestamp -> T_mcs_timestamp
  | T_address -> T_mcs_address
  (* Containers *)
  | T_pair (tyl, tyr) -> T_mcs_pair (translate_ty tyl, translate_ty tyr)
  | T_union (tyl, fl, tyr, fr) ->
    T_mcs_union (translate_ty tyl, fl, translate_ty tyr, fr)
  | T_option ty -> T_mcs_option (translate_ty ty)
  | T_list ty -> T_mcs_list (translate_ty ty)
  | T_set ty -> T_mcs_set (translate_ty ty)
  | T_map (tyk, tyv) -> T_mcs_map (translate_ty tyk, translate_ty tyv)
  | T_big_map (tyk, tyv) -> T_mcs_map (translate_ty tyk, translate_ty tyv)
  | T_operation -> T_mcs_operation
  | T_chain_id -> T_mcs_chain_id
  | T_lambda (arg, ret) -> T_mcs_lambda (translate_ty arg, translate_ty ret)
  | T_contract arg -> T_mcs_address
  | T_unknown -> raise Typing_error

let translate_constant const =
  let open Al_ast in
  try
    let ty = translate_ty const.ctyp in
    let rec aux ckind =
      match ckind with
      | Unit -> C_unit
      | Bool b -> C_bool b
      | Int i -> C_int i
      | Nat i -> C_int i
      | Mutez m -> C_int m
      | String s -> C_string s
      | Pair (x, y) ->
        let c1 = aux x.ckind in
        let c2 = aux y.ckind in
        C_mcs_pair (c1, c2)
      | Option v ->
        let v =
          match v with
          | Some v ->
            let c = aux v.ckind in
            Some (c)
          | None ->
            None
        in
        C_option v
      | List l ->
        C_mcs_list (List.map (fun v -> aux v.ckind) l)
      | Set l ->
        C_mcs_set (List.map (fun v -> aux v.ckind) l)
      | Map l ->
        C_mcs_map (List.map (fun (k, v) -> (aux k.ckind, aux v.ckind)) l)
      | Union (L v) ->
        C_mcs_union (C_mcs_left (aux v.ckind))
      | Union (R v) ->
        C_mcs_union (C_mcs_right (aux v.ckind))
      | Timestamp v -> C_int v
      | Bytes s -> C_string s
      | Signature _ -> panic "unhandled constant: signature"
      | Key _ -> panic "unhandled constant: key"
      | Key_hash _ -> panic "unhandled constant: key_hash"
      | Address (v, e) -> C_mcs_address (Address_constant (v, e))
    in
    ty, aux const.ckind
  with
  Typing_error -> panic "Typing error on %a" pp_constant const

let translate_stack stack =
  List.map translate_constant stack


let translate_op (op: Al_cst.binop) : operator =
  match op with
  | O_eq -> O_eq
  | O_ne -> O_ne
  | O_gt -> O_gt
  | O_lt -> O_lt
  | O_ge -> O_ge
  | O_le -> O_le

let rec translate_expression (expression : Al_ast.expression) : Mopsa.expr =
  let expression : Mopsa.expr =
    let range = expression.erange in
    let etyp = translate_ty expression.etyp in
    match expression with
    | {ekind = E_constant c} ->
      let _, c = translate_constant c in
      mk_constant ~etyp c range
    | { ekind = E_stackpos_var(i); _} ->
      let var = mk_stackpos etyp i in
      mk_var var range
    | {ekind = E_binop (op, e1, e2); _} ->
      let e1 = translate_expression e1 in
      let e2 = translate_expression e2 in
      mk_binop ~etyp:T_mcs_bool
        (mk_mcs_compare e1 e2 range)
        (translate_op op)
        (mk_zero ~typ:T_mcs_int range)
        range
  in
  Debug.debug "%a : %a" pp_expr expression pp_typ (etyp expression);
  expression

let translate_stmt stmt =
  let range = stmt.Al_ast.srange in
  match stmt.skind with
  | Al_ast.S_assert(expr) ->
    Universal.Ast.mk_assert (translate_expression expr) range
  | _ ->
    Mopsa.panic_at range "Unrecognized assertion : %a"
      (Al_ast.pp_stmt ~typed:false ~range:false) stmt
