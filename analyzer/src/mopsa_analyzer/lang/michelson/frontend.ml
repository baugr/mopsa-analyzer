open Mopsa_michelson_parser
open Mopsa
open Universal.Ast
open Ast
open Mopsa_michelson_al_parser

type contract_cli_arg =
  {
    filename: string;
    storage: string option;
    parameter: string option;
    address: string option;
  }

let debug fmt = Debug.debug ~channel:"michelson.frontend.merge" fmt

let debug_ty fmt = Debug.debug ~channel:"michelson.frontend.type" fmt

let debug_arg fmt = Debug.debug ~channel:"michelson.frontend.arguments" fmt

let debug fmt = Debug.debug ~channel:"michelson.frontend" fmt

let opt_parameter = ref ""

let opt_storage = ref ""

let opt_proto_version = ref "007"

let opt_entry_point = ref ""

let opt_contract_cli_args : contract_cli_arg list ref = ref []

let opt_failure_alarm = ref false

let pp_contract fmt contract =
  let pp_option opt =
    (Format.pp_print_option
       ~none:(fun fmt () -> Format.pp_print_string fmt "None")
       Format.pp_print_string
    )
      opt
  in
  Format.fprintf fmt "@[<v 0>@[<v 2>Contract {@;";
  Format.fprintf fmt "filename  = '%s'@;" contract.filename;
  Format.fprintf fmt "storage   = '%a'@;" pp_option contract.storage;
  Format.fprintf fmt "parameter = '%a'@;" pp_option contract.parameter;
  Format.fprintf fmt "address   = '%a'" pp_option contract.address;
  Format.fprintf fmt "@]@;}@]"

let parse_contract_cli_arg str =
  let start_with str prefix offset =
    let rec loop max offset i =
      if i < max then
        str.[offset + i] = prefix.[i] && loop max offset (i+1)
      else
        true
    in
    if String.length prefix > String.length str - offset + 1 then
      false
    else
      loop (String.length prefix) offset 0 in
  (* First parsing part : filename *)
  let rec parse_main str i : contract_cli_arg =
    let i, filename = parse_item (Buffer.create 16) str (String.length str) i in
    let contract =
      { filename;
        storage = None;
        parameter = None;
        address = None
      }
    in
    parse_tail contract str (String.length str) i
  (* Tail: optional parts *)
  and parse_tail contract str max i =
    if i >= max then
      contract
    else
      match str with
      | str when start_with str "address=" i ->
        let i, address = parse_optional str max (i + String.length "address=") in
        parse_tail { contract with address } str max i
      | str when start_with str "storage=" i ->
        let i, storage = parse_optional str max (i + String.length "storage=") in
        parse_tail { contract with storage } str max i
      | str when start_with str "parameter=" i ->
        let i, parameter = parse_optional str max (i + String.length "parameter=") in
        parse_tail { contract with parameter } str max i
      | _ -> assert false
  and parse_optional str max i =
    let i, n = parse_item (Buffer.create 16) str max i in
    i, Some n
  and parse_item buf str max i =
    if i = max then
      i, Buffer.contents buf
    else
      match str.[i] with
      | ',' ->
        (i+1), Buffer.contents buf
      | c ->
        Buffer.add_char buf c;
        parse_item buf str max (i+1)
  in
  parse_main str 0

let register_external_contract contract_spec_arg =
  let contract_cli_arg = parse_contract_cli_arg contract_spec_arg in
  opt_contract_cli_args := contract_cli_arg :: (!opt_contract_cli_args)

let () =
  register_language_option
    "michelson"
    {
      key = "-parameter";
      category = "Michelson";
      doc = " parameter passed to the smart-contract";
      spec = ArgExt.Set_string opt_parameter;
      default = "";
    } ;
  register_language_option
    "michelson"
    {
      key = "-storage";
      category = "Michelson";
      doc = " initial storage of the smart-contract";
      spec = ArgExt.Set_string opt_storage;
      default = "";
    } ;
  (* register_language_option *)
  (*   "michelson" *)
  (*   { *)
  (*     key = "-protocol-version"; *)
  (*     category = "Michelson"; *)
  (*     doc = " specify the tezos protocol version to use"; *)
  (*     spec = ArgExt.Set_string opt_proto_version; *)
  (*     default = "7"; *)
  (*   } ; *)
  register_language_option
    "michelson"
    {
      key = "-entrypoint";
      category = "Michelson";
      doc = " specify an entrypoint";
      spec = ArgExt.Set_string opt_entry_point;
      default = "";
    } ;
  register_language_option
    "michelson"
    {
      key = "-external-contract";
      category = "Michelson";
      doc = " specify an external contract which can be called, as address:file.tz";
      spec = ArgExt.String register_external_contract;
      default = "";
    };
  register_language_option
    "michelson" {
    key = "-failure-alarms";
    category = "Michelson";
    doc = " enable failure for alarms";
    spec = ArgExt.Set opt_failure_alarm;
    default = "false";
  }


let rec of_ty ty =
  match ty with
  | Al_ast.T_unit -> T_mcs_unit
  | T_bool -> T_mcs_bool
  | T_int -> T_mcs_int
  | T_nat -> T_mcs_int
  | T_mutez -> T_mcs_mutez
  | T_signature -> T_mcs_signature
  | T_string -> T_mcs_string
  | T_bytes -> T_mcs_bytes
  | T_key -> T_mcs_key
  | T_key_hash -> T_mcs_key_hash
  | T_timestamp -> T_mcs_timestamp
  | T_address -> T_mcs_address
  (* Containers *)
  | T_pair (tyl, tyr) -> T_mcs_pair (of_ty tyl, of_ty tyr)
  | T_union (tyl, fl, tyr, fr) -> T_mcs_union (of_ty tyl, fl, of_ty tyr, fr)
  | T_option ty -> T_mcs_option (of_ty ty)
  | T_list ty -> T_mcs_list (of_ty ty)
  | T_set ty -> T_mcs_set (of_ty ty)
  | T_map (tyk, tyv) -> T_mcs_map (of_ty tyk, of_ty tyv)
  | T_big_map (tyk, tyv) -> T_mcs_map (of_ty tyk, of_ty tyv)
  | T_operation -> T_mcs_operation
  | T_chain_id -> T_mcs_chain_id
  | T_lambda (arg, ret) -> T_mcs_lambda (of_ty arg, of_ty ret)
  | T_contract _ -> T_mcs_address
  | T_unknown -> assert false

let translate_annot v =
  Option.map (fun (Pre_ast.Var_annot s) -> Ast.Var_annot s) v

let translate_stack stack =
  List.map (fun (annot, ty) ->
      translate_annot annot, of_ty ty
    ) stack

let rec of_const const =
  let open Pre_ast in
  let ty = of_ty const.ctyp in
  let rec aux ckind =
    match ckind with
    | Unit -> C_unit
    | Bool b -> C_bool b
    | Int i -> C_int i
    | Nat i -> C_int i
    | Mutez m -> C_int m
    | String s -> C_string s
    | Pair (x, y) ->
      let c1 = aux x.ckind in
      let c2 = aux y.ckind in
      C_mcs_pair (c1, c2)
    | Option v ->
      let v =
        match v with
        | Some v ->
          let c = aux v.ckind in
          Some (c)
        | None ->
          None
      in
      C_option v
    | List l ->
      C_mcs_list (List.map (fun v -> aux v.ckind) l)
    | Set l ->
      C_mcs_set (List.map (fun v -> aux v.ckind) l)
    | Map l ->
      C_mcs_map (List.map (fun (k, v) -> (aux k.ckind, aux v.ckind)) l)
    | Union (L v) ->
      C_mcs_union (C_mcs_left (aux v.ckind))
    | Union (R v) ->
      C_mcs_union (C_mcs_right (aux v.ckind))
    | Timestamp v -> C_int v
    | Bytes s -> C_string s
    | Signature s -> C_string s
    | Key k -> C_string k
    | Key_hash kh -> C_string kh
    | Address (v, e) -> C_mcs_address (Address_constant(v, e))
    | Lambda stm -> C_mcs_lambda (from_instruction stm)
  in
  ty, aux const.ckind

and from_instruction stmt =
  let open Pre_ast in
  let stm_kind_of_instruction stmt =
    let loc = stmt.srange in
    let stmt_kind instr bef aft =
      S_michelson_instr {instr; bef; sp = List.length bef; aft; loc}
    in
    let bef = translate_stack stmt.bef in
    let aft = translate_stack stmt.aft in
    match stmt.skind with
    (* Stack operations *)
    | S_drop ty -> stmt_kind (S_mcs_drop (of_ty ty)) bef aft
    | S_dropn n -> stmt_kind (S_mcs_dropn n) bef aft
    | S_dig n -> stmt_kind (S_mcs_dig n) bef aft
    | S_dug n -> stmt_kind (S_mcs_dug n) bef aft
    | S_dip code -> stmt_kind (S_mcs_dip (from_instruction code)) bef aft
    | S_dipn (n, code) ->
      stmt_kind (S_mcs_dipn (n, from_instruction code)) bef aft
    | S_const c ->
      let ty, v = of_const c in
      stmt_kind (S_mcs_const(ty, v)) bef aft
    | S_dup ty -> stmt_kind (S_mcs_dup (of_ty ty)) bef aft
    | S_swap (ty1, ty2) ->
      stmt_kind (S_mcs_swap (of_ty ty1, of_ty ty2)) bef aft
    (* Pairs *)
    | S_cons_pair (ty1, ty2) ->
      stmt_kind (S_mcs_cons_pair (of_ty ty1, of_ty ty2)) bef aft
    | S_car ty -> stmt_kind (S_mcs_car (of_ty ty)) bef aft
    | S_cdr ty -> stmt_kind (S_mcs_cdr (of_ty ty)) bef aft
    | S_unpair ty -> stmt_kind (S_mcs_unpair (of_ty ty)) bef aft
    (* Options *)
    | S_cons_none ty -> stmt_kind (S_mcs_cons_none (of_ty ty)) bef aft
    | S_cons_some ty -> stmt_kind (S_mcs_cons_some (of_ty ty)) bef aft
    | S_if_none (ty, tstm, fstm) ->
      let tstm = from_instruction tstm in
      let fstm = from_instruction fstm in
      stmt_kind (S_mcs_if_none (of_ty ty, tstm, fstm)) bef aft
    (* Lists *)
    | S_nil ty -> stmt_kind (S_mcs_nil (of_ty ty)) bef aft
    | S_cons_list ty -> stmt_kind (S_mcs_cons_list (of_ty ty)) bef aft
    | S_list_size ty -> stmt_kind (S_mcs_list_size (of_ty ty)) bef aft
    | S_if_cons (ty, tstm, fstm) ->
      let tstm = from_instruction tstm in
      let fstm = from_instruction fstm in
      stmt_kind (S_mcs_if_cons (of_ty ty, tstm, fstm)) bef aft
    | S_list_iter stm ->
      let stm = from_instruction stm in
      stmt_kind (S_mcs_list_iter stm) bef aft
    | S_list_map stm ->
      let stm = from_instruction stm in
      stmt_kind (S_mcs_list_map stm) bef aft
    (* Arithmetic *)
    | S_abs_int -> stmt_kind S_mcs_abs_int bef aft
    | S_neg_int -> stmt_kind S_mcs_neg_int bef aft
    | S_neg_nat -> stmt_kind S_mcs_neg_nat bef aft
    | S_add_intint -> stmt_kind S_mcs_add_intint bef aft
    | S_add_intnat -> stmt_kind S_mcs_add_intnat bef aft
    | S_add_natint -> stmt_kind S_mcs_add_natint bef aft
    | S_add_natnat -> stmt_kind S_mcs_add_natnat bef aft
    | S_sub_int -> stmt_kind S_mcs_sub_int bef aft
    | S_mul_intint -> stmt_kind S_mcs_mul_intint bef aft
    | S_mul_intnat -> stmt_kind S_mcs_mul_intnat bef aft
    | S_mul_natint -> stmt_kind S_mcs_mul_natint bef aft
    | S_mul_natnat -> stmt_kind S_mcs_mul_natnat bef aft
    | S_ediv_intint -> stmt_kind S_mcs_ediv_intint bef aft
    | S_ediv_intnat -> stmt_kind S_mcs_ediv_intnat bef aft
    | S_ediv_natint -> stmt_kind S_mcs_ediv_natint bef aft
    | S_ediv_natnat -> stmt_kind S_mcs_ediv_natnat bef aft
    (* Integer comparison *)
    | S_eq -> stmt_kind S_mcs_eq bef aft
    | S_neq -> stmt_kind S_mcs_neq bef aft
    | S_gt -> stmt_kind S_mcs_gt bef aft
    | S_ge -> stmt_kind S_mcs_ge bef aft
    | S_lt -> stmt_kind S_mcs_lt bef aft
    | S_le -> stmt_kind S_mcs_le bef aft
    (* Integer casts *)
    | S_is_nat -> stmt_kind S_mcs_is_nat bef aft
    | S_int_nat -> stmt_kind S_mcs_int_nat bef aft
    (* Bitwise and shift *)
    | S_not_int -> stmt_kind S_mcs_not_int bef aft
    | S_not_nat -> stmt_kind S_mcs_not_nat bef aft
    | S_and_nat -> stmt_kind S_mcs_and_nat bef aft
    | S_and_int_nat -> stmt_kind S_mcs_and_int_nat bef aft
    | S_or_nat -> stmt_kind S_mcs_or_nat bef aft
    | S_lsl_nat -> stmt_kind S_mcs_lsl_nat bef aft
    | S_lsr_nat -> stmt_kind S_mcs_lsr_nat bef aft
    (* Booleans *)
    | S_not -> stmt_kind S_mcs_not bef aft
    | S_and -> stmt_kind S_mcs_and bef aft
    | S_or -> stmt_kind S_mcs_or bef aft
    | S_xor -> stmt_kind S_mcs_xor bef aft
    | S_xor_nat -> stmt_kind S_mcs_xor_nat bef aft
    (* Mutez *)
    | S_add_tez -> stmt_kind S_mcs_add_tez bef aft
    | S_sub_tez -> stmt_kind S_mcs_sub_tez bef aft
    | S_mul_nattez -> stmt_kind S_mcs_mul_nattez bef aft
    | S_mul_teznat -> stmt_kind S_mcs_mul_teznat bef aft
    | S_ediv_tez -> stmt_kind S_mcs_ediv_tez bef aft
    | S_ediv_teznat -> stmt_kind S_mcs_ediv_teznat bef aft
    | S_amount -> stmt_kind S_mcs_amount bef aft
    | S_balance -> stmt_kind S_mcs_balance bef aft
    (* Timestamps *)
    | S_add_timestamp_to_seconds ->
      stmt_kind S_mcs_add_timestamp_to_seconds bef aft
    | S_add_seconds_to_timestamp ->
      stmt_kind S_mcs_add_seconds_to_timestamp bef aft
    | S_sub_timestamp_seconds -> stmt_kind S_mcs_sub_timestamp_seconds bef aft
    | S_diff_timestamps -> stmt_kind S_mcs_diff_timestamps bef aft
    | S_now -> stmt_kind S_mcs_now bef aft
    (* Strings *)
    | S_concat_string -> stmt_kind S_mcs_concat_string bef aft
    | S_concat_string_pair -> stmt_kind S_mcs_concat_string_pair bef aft
    | S_string_size -> stmt_kind S_mcs_string_size bef aft
    | S_slice_string -> stmt_kind S_mcs_slice_string bef aft
    (* Bytes *)
    | S_concat_bytes -> stmt_kind S_mcs_concat_bytes bef aft
    | S_concat_bytes_pair -> stmt_kind S_mcs_concat_bytes_pair bef aft
    | S_bytes_size -> stmt_kind S_mcs_bytes_size bef aft
    | S_slice_bytes -> stmt_kind S_mcs_slice_bytes bef aft
    | S_blake2b -> stmt_kind S_mcs_blake2b bef aft
    | S_sha256 -> stmt_kind S_mcs_sha256 bef aft
    | S_sha512 -> stmt_kind S_mcs_sha512 bef aft
    | S_sha3 -> stmt_kind S_mcs_sha3 bef aft
    | S_keccak -> stmt_kind S_mcs_keccak bef aft
    (* Polymorphic comparison *)
    | S_compare ty -> stmt_kind (S_mcs_compare (of_ty ty)) bef aft
    (* Control flow *)
    | S_if (tstm, fstm) ->
      let tstm = from_instruction tstm in
      let fstm = from_instruction fstm in
      stmt_kind (S_mcs_if (tstm, fstm)) bef aft
    | S_loop(stm) ->
      let stm = from_instruction stm in
      stmt_kind (S_mcs_loop stm) bef aft
    | S_loop_left (stm) ->
      let stm = from_instruction stm in
      stmt_kind (S_mcs_loop_left stm) bef aft
    (* Others *)
    | S_failwith ty -> stmt_kind (S_mcs_failwith (of_ty ty)) bef aft
    | S_nop -> stmt_kind S_mcs_nop bef aft
    | S_seq (stm1, stm2) ->
      let stm1 = from_instruction stm1 in
      let stm2 = from_instruction stm2 in
      stmt_kind (S_mcs_seq (stm1, stm2)) bef aft
    (* Unions *)
    | S_cons_left ty -> stmt_kind (S_mcs_cons_left (of_ty ty)) bef aft
    | S_cons_right ty -> stmt_kind (S_mcs_cons_right (of_ty ty)) bef aft
    | S_if_left (ty, tstm, fstm) ->
      let tstm = from_instruction tstm in
      let fstm = from_instruction fstm in
      stmt_kind (S_mcs_if_left (of_ty ty, tstm, fstm)) bef aft
    (* Sets *)
    | S_empty_set ty -> stmt_kind (S_mcs_empty_set (of_ty ty)) bef aft
    | S_set_update item ->
      stmt_kind (S_mcs_set_update {item = of_ty item }) bef aft
    | S_set_mem ity -> stmt_kind (S_mcs_set_mem { ity = of_ty ity}) bef aft
    | S_set_size -> stmt_kind S_mcs_set_size bef aft
    | S_set_iter lambda ->
      let lambda = from_instruction lambda in
      stmt_kind (S_mcs_set_iter (lambda)) bef aft
    (* Maps *)
    | S_empty_map (kty, vty) ->
      stmt_kind (S_mcs_empty_map {kty = of_ty kty; vty = of_ty vty}) bef aft
    | S_map_update (kty, vty) ->
      stmt_kind (S_mcs_map_update {kty = of_ty kty; vty=of_ty vty}) bef aft
    | S_map_get (kty, vty) ->
      stmt_kind (S_mcs_map_get {kty = of_ty kty; vty=of_ty vty}) bef aft
    | S_map_mem (kty, vty) ->
      stmt_kind (S_mcs_map_mem {kty = of_ty kty; vty=of_ty vty}) bef aft
    | S_map_size (kty, vty) ->
      stmt_kind (S_mcs_map_size {kty = of_ty kty; vty=of_ty vty}) bef aft
    | S_map_iter lambda ->
      let lambda = from_instruction lambda in
      stmt_kind (S_mcs_map_iter (lambda)) bef aft
    | S_map_map lambda ->
      let lambda = from_instruction lambda in
      stmt_kind (S_mcs_map_map (lambda)) bef aft
    (* Big Maps *)
    | S_empty_big_map ->
      stmt_kind (S_mcs_empty_big_map) bef aft
    | S_big_map_update ->
      stmt_kind (S_mcs_big_map_update ) bef aft
    | S_big_map_get ->
      stmt_kind (S_mcs_big_map_get) bef aft
    | S_big_map_mem ->
      stmt_kind (S_mcs_big_map_mem) bef aft
    (* Lambda *)
    | S_lambda stm ->
      let stm = from_instruction stm in
      stmt_kind (S_mcs_lambda stm) bef aft
    | S_apply ->
      stmt_kind S_mcs_apply bef aft
    | S_exec ->
      stmt_kind S_mcs_exec bef aft
    (* Contracts and addresses *)
    | S_contract (ty, entrypoint) ->
      stmt_kind (S_mcs_contract (of_ty ty, entrypoint)) bef aft
    | S_transfer_tokens ->
      stmt_kind (S_mcs_transfer_tokens) bef aft
    | S_self entrypoint ->
      stmt_kind (S_mcs_self entrypoint) bef aft
    | S_source ->
      stmt_kind S_mcs_source bef aft
    | S_sender ->
      stmt_kind S_mcs_sender bef aft
    | S_address ->
      stmt_kind S_mcs_address bef aft
    | S_implicit_account ->
      stmt_kind S_mcs_implicit_account bef aft
    | S_set_delegate ->
      stmt_kind S_mcs_set_delegate bef aft
    | S_create_account ->
      stmt_kind S_mcs_create_account bef aft
    | S_check_signature ->
      stmt_kind S_mcs_check_signature bef aft
    | S_hash_key ->
      stmt_kind S_mcs_hash_key bef aft
    | S_steps_to_quota ->
      stmt_kind S_mcs_steps_to_quota bef aft
    | S_chain_id ->
      stmt_kind S_mcs_chain_id bef aft
    | S_level ->
      stmt_kind S_mcs_level bef aft
    | S_self_address ->
      stmt_kind S_mcs_self_address bef aft
    | S_voting_power ->
      stmt_kind S_mcs_voting_power bef aft
    | S_total_voting_power ->
      stmt_kind S_mcs_total_voting_power bef aft
    | S_create_contract stmt ->
      let stmt = from_instruction stmt in
      stmt_kind (S_mcs_create_contract stmt) bef aft
    | S_create_contract_2 { parameter_ty; storage_ty; code } ->
      let parameter_ty = of_ty parameter_ty in
      let storage_ty = of_ty storage_ty in
      let code = from_instruction code in
      stmt_kind (S_mcs_create_contract_2 { parameter_ty; storage_ty; code }) bef aft
    (* serialisation *)
    | S_pack ->
      stmt_kind S_mcs_pack bef aft
    | S_unpack ->
      stmt_kind S_mcs_unpack bef aft
    (* other *)
    | S_wrapped_before { stmt; assertion } ->
      let stm = from_instruction stmt in
      let assertion = Al_ast_to_mopsa.translate_stmt assertion in
      stmt_kind (S_mcs_wrapped_before { stm; assertion }) bef aft
    | S_wrapped_after _ -> panic "TO DO : %a" pp_statement stmt
  in
  let skind = stm_kind_of_instruction stmt in
  {skind; srange = stmt.srange }

let stmt_of_ast ast = from_instruction ast

let tezos_version version =
  let version = String.lowercase_ascii version in
  match version with
  | "6" | "006" | "carthage" -> M_parser.Tezos_006
  | "7" | "007" | "delphi" -> M_parser.Tezos_007
  | _ -> panic "Unexpected protocol version: %s" version

(* FIXME: Hackish function allowing parsing of storage via the 006 protocol *)
let parse_constant constant_ty constant_str =
  match constant_str with
  | "" -> None, None, None
  | constant ->
    let ty, constant =
      debug_arg "Parsing expression: %s" constant ;
      let c = Mopsa_michelson_al_parser.Main.parse_constant constant_ty constant in
      let c = Al_ast_to_mopsa.translate_constant c in
      c
    in
    debug_arg "Parsed constant: %a" pp_constant constant;
    Some constant, Some constant, Some ty

let parse_constant ty c =
  let c = Mopsa_michelson_al_parser.Main.parse_constant ty c in
  snd (Al_ast_to_mopsa.translate_constant c)

let parse_contract tezos filename : Ast.contract =
  let (module Tezos : Mopsa_tezos.Common.Sigs.MOPSA_TEZOS) = tezos in
  let context = Tezos.memory_context () in
  let (module Parse_result) = Tezos.parse_file ~context ~storage:None ~filename in
  (* Parse assertions *)
  let assertions = Mopsa_michelson_al_parser.Main.parse_file filename in
  debug "@[<v 0>--- ASSERTIONS ---@;" ;
  List.iter
    (debug "%a"
       (Mopsa_michelson_al_parser.Al_ast.pp_stmt ~range:false ~typed:false))
    assertions ;
  debug "@]--- END ---@;" ;
  (* Translate ast *)
  let ast = Mast_to_preast.translate Parse_result.mopsa_ast.statement in
  let statement = Pass_merge_assertions.merge_ast_al ast assertions in
  let statement = from_instruction statement in
  (* Translate parameter / storage types *)
  let al_ast_parameter_ty = Mast_to_preast.translate_ty Parse_result.mopsa_ast.parameter_ty in
  let al_ast_storage_ty = Mast_to_preast.translate_ty Parse_result.mopsa_ast.storage_ty in
  (* Translate parameter / storage if specified *)
  let parameter_ty = of_ty al_ast_parameter_ty in
  let storage_ty = of_ty al_ast_storage_ty in
  {
    filename;
    statement ;
    parameter_ty;
    storage_ty;
    tezos_ast = (module Parse_result);
    fixme_storage_ty = al_ast_storage_ty;
    fixme_parameter_ty = al_ast_parameter_ty;
  }

let parse_contract_context tezos contract_arg : Ast.contract * Ast.contract_context =
  let contract = parse_contract tezos contract_arg.filename in
  debug "parse_contract_context";
  debug " * contract_arg : %a" pp_contract contract_arg;
  let parameter = Option.map (parse_constant contract.fixme_parameter_ty) contract_arg.parameter in
  let storage = Option.map (parse_constant contract.fixme_storage_ty) contract_arg.storage in
  let address = contract_arg.address in
  let context = { storage;
                  address;
                  parameter;
                  balance = None;
                  amount = None;
                  entrypoint = None;
                } in
  debug "parse_context:";
  debug "contract: %a" Ast.pp_contract contract;
  debug "context: %a" Ast.pp_contract_context context;
  contract, context

let parse_program (files : string list) : Framework.Core.Ast.Program.program =
  match files with
  | [contract_arg] ->
    let module Tezos =
      (val !opt_proto_version |>
           tezos_version |>
           M_parser.tezos_module
      )
    in
    let main_contract_cli_arg = parse_contract_cli_arg contract_arg in
    let contract, contract_context = parse_contract_context (module Tezos) main_contract_cli_arg in

    (* FIXME: force entrypoint *)
    let contract_context =
      match !opt_entry_point with
      | "" -> contract_context
      | entrypoint -> { contract_context with entrypoint = Some entrypoint }
    in

    (* Add main contract to list of externs in case of recursive calls *)
    let externs = [contract, contract_context] in

    (* Add external contracts to contracts map *)
    let externs = List.fold_left (fun cs contract_cli_arg ->
        parse_contract_context (module Tezos) contract_cli_arg::cs
      ) externs !opt_contract_cli_args
    in

    let prog_kind = P_michelson {
        contract;
        context = contract_context;
        externs;
      }
    in
    { prog_kind; prog_range = mk_program_range files }

  | [] -> panic "no input file"
  | _ -> panic "analysis of multiple files not supported"

(* Front-end registration *)
let () =
  register_frontend
    { lang = "michelson"; parse = parse_program }
