open Mopsa

module Address =
struct

  type t =
    | Address of string * string (* address and entrypoint *)
    | Contract of string * string (* address and entrypoint *)

  let compare x y =
    match x, y with
    | Address (x, e1), Address (y, e2) ->
      Compare.compose
        [
          (fun () -> String.compare x y);
          (fun () -> String.compare e1 e2)
        ]
    | Contract (x, e1), Contract (y, e2) ->
      Compare.compose
        [
          (fun () -> String.compare x y);
          (fun () -> String.compare e1 e2)
        ]
    | _ -> assert false

  let print = unformat (fun fmt x ->
      match x with
      | Address (x, e) -> Format.fprintf fmt "address: %s%%%s" x e
      | Contract (x, e) -> Format.fprintf fmt "contract %s%%%s" x e
    )

end
