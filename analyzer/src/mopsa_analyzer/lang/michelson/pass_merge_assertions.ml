open Mopsa
open Pre_ast
module Al_ast = Mopsa_michelson_al_parser.Al_ast

let debug args = Debug.debug ~channel:"michelson.parser.merge_assertions" args
let debug_ty args = Debug.debug ~channel:"michelson.parser.type_assertions" args

let rec type_constant typ constant =
  assert (Al_ast.T_unknown <> typ);
  match constant.Al_ast.ctyp with
  | Al_ast.T_unknown -> { constant with ctyp = typ }
  | _ -> constant

let rec type_expression
    ?(hint=Al_ast.T_unknown)
    (stack:Pre_ast.stack)
    (expression:Al_ast.expression) : Al_ast.expression
  =
  let open Al_ast in
  let pp_expr args = pp_expr ~typed:true ~range:false args in
  let expression =
    match expression with
    | { ekind = E_constant ({ ctyp = T_unknown; _} as c); etyp; _ } ->
      (match etyp with
      | T_unknown ->
        (match hint with
         | T_unknown ->
           Mopsa.panic "All constants should be correctly typed"
         | ty ->
           let c = type_constant ty c in
           { ekind = E_constant c; etyp = ty; erange = expression.erange }
        )
      | etyp ->
        let c = type_constant etyp c in
        { ekind = E_constant c; etyp; erange = expression.erange }
      )
    | { ekind = E_constant(c) } ->
      { expression with etyp = c.ctyp }
    | { ekind = E_stackpos_var i; etyp = T_unknown; _ } ->
      debug_ty "Stack :";
      List.iter (fun (_, ty) -> debug_ty "%a" pp_typ ty) stack;
      let etyp = stack_get_type_spi stack (i) in
      { expression with ekind = E_stackpos_var (i); etyp }
    (* FIXME: Assume binop always operate on 'a -> 'a -> 'b *)
    | {ekind = E_binop (op, e1, e2); _} ->
      (* Try typing e1 *)
      let e1 = type_expression stack e1 in
      (match e1.etyp with
       | T_unknown ->
         (* If we can't type e1, try typing with the help of e2 *)
         let e2 = type_expression stack e2 in
         (match e2.etyp with
          | T_unknown ->
            panic "Sorry, cannot infer type of %a" pp_expr expression
          | hint ->
            (* Type e1 again with the help of the e2 type *)
            let e1 = type_expression ~hint stack e1 in
            { expression with ekind = E_binop (op, e1, e2) }
         )
       (* type e2 with the infered type of e1 *)
       | hint ->
         let e2 = type_expression ~hint stack e2 in
         { expression with ekind = E_binop (op, e1, e2) }
      )
    | _ -> panic "Unhandled expression: %a" pp_expr expression
  in
  debug_ty "TYPED EXPRESSION: %a : %a" pp_expr expression pp_typ (expression.etyp) ;
  expression


let type_assertion stack (assertion : Al_ast.statement) : Al_ast.statement =
  let open Al_ast in
  let pp_stmt args = pp_stmt ~typed:false ~range:false args in
  let skind =
    match assertion.skind with
    | S_assert e -> S_assert (type_expression stack e)
    | _ -> panic "Unhandled statement: %a" pp_stmt assertion
  in
  {assertion with skind}

let merge_ast_al (ast: Pre_ast.statement) (assertions : Al_ast.statement list) =
  let pp_stmt args = Al_ast.pp_stmt ~typed:false ~range:false args in
  let wrap_before ~assertion ~stmt bef =
    let assertion = type_assertion bef assertion in
    debug "WRAPPED BEFORE: assertion = %a; stmt = %a; stack = %a"
      (Al_ast.pp_stmt ~typed:true ~range:false) assertion
      Pre_ast.pp_statement stmt
      pp_stack_ty bef
    ;
    S_wrapped_before { assertion; stmt }
  in
  let rec insert_assertions
      ({ skind; bef; aft; srange } as stmt)
      (assertions : Al_ast.statement list) nextloc
    =
    debug "insert_assertions: %a" pp_statement stmt;
    List.iter (fun assertion -> debug "  >assertion: %a" pp_stmt assertion) assertions ;
    match assertions with
    | [] -> stmt, []
    | assertion :: tail ->
      debug
        "comparing locations: '%a' and '%a' = %d"
        Location.pp_range
        srange
        Location.pp_range
        (assertion.srange)
        (Location.compare_range (assertion.srange) srange) ;
      (* debug "comparingl: @[<h>%a@]" pp_stmt assertion ; *)
      (* debug "comparingr: @[<h>%a@]" Ast.pp_instr (0, instr) ; *)
      let start_assertion =
        try Location.get_range_start (assertion.srange)
        with _ -> panic "Cannot get locations of %a" pp_stmt assertion
      in
      let start_statement =
        try Location.get_range_start srange
        with _ ->
          panic "Cannot get start location for %a" Location.pp_range srange
      in
      (match skind with
       | S_seq _ -> panic "Should not happen"
       | _ -> ()
      );
      if Location.compare_pos start_assertion start_statement < 0 then
        (
          let stmt, tail = insert_assertions stmt tail nextloc in
          debug "Inserting assertion : %a before %a"
            pp_stmt assertion Pre_ast.pp_statement stmt;
          let skind = wrap_before ~stmt ~assertion bef in
          { stmt with skind }, tail
        )
      else
        (
          debug "Not inserting assertion : %a before %a"
            pp_stmt assertion Pre_ast.pp_statement stmt;
          stmt, assertions
        )
  in
  let rec loop
      (stmt : Pre_ast.statement)
      (assertions : Al_ast.statement list)
      nextloc
    =
    debug "loop: %a (bef = %a)" pp_statement stmt pp_stack_ty stmt.bef;
    let stmt, assertions =
      match stmt.skind with
      | S_seq (stmt1, stmt2) ->
        (*
         * we do not want to insert before a seq, before bef/aft would not
         * correspond to the instruction before or after
         *)
        let stmt1, assertions = loop stmt1 assertions nextloc in
        let stmt2, assertions = loop stmt2 assertions nextloc in
        { stmt with skind = S_seq(stmt1, stmt2) }, assertions
      | _ ->
        let stmt, assertions = insert_assertions stmt assertions nextloc in
        inductive_insert stmt assertions nextloc
    in
    stmt, assertions
  and inductive_insert
      (stmt : Pre_ast.statement)
      (assertions : Al_ast.statement list)
      nextloc
    : Pre_ast.statement * Al_ast.statement list =
    debug "inductive_insert: %a (bef = %a)" pp_statement stmt pp_stack_ty stmt.bef;
    let skind, assertions =
      match stmt.skind with
      | S_wrapped_before {assertion; stmt} ->
        let stmt, assertions =
          inductive_insert stmt assertions nextloc
        in
        S_wrapped_before {assertion; stmt}, assertions
      | S_wrapped_after {assertion; stmt} ->
        let stmt, assertions =
          inductive_insert stmt assertions nextloc
        in
        S_wrapped_after {assertion; stmt}, assertions
      | S_seq (stm1, stm2) ->
        let stm1, assertions = loop stm1 assertions nextloc in
        let stm2, assertions = loop stm2 assertions nextloc in
        S_seq (stm1, stm2), assertions
      | S_if_none (ty, stm1, stm2) ->
        let stm1, assertions = loop stm1 assertions nextloc in
        let stm2, assertions = loop stm2 assertions nextloc in
        S_if_none (ty, stm1, stm2), assertions
      | S_if_left (ty, stm1, stm2) ->
        let stm1, assertions = loop stm1 assertions nextloc in
        let stm2, assertions = loop stm2 assertions nextloc in
        S_if_left (ty, stm1, stm2), assertions
      | S_if_cons (ty, stm1, stm2) ->
        let stm1, assertions = loop stm1 assertions nextloc in
        let stm2, assertions = loop stm2 assertions nextloc in
        S_if_cons (ty, stm1, stm2), assertions
      | S_list_map stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_list_map stmt, assertions
      | S_list_iter stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_list_iter stmt, assertions
      | S_set_iter stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_set_iter stmt, assertions
      | S_map_iter stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_map_iter stmt, assertions
      | S_map_map stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_map_map stmt, assertions
      | S_if (stm1, stm2) ->
        let stm1, assertions = loop stm1 assertions nextloc in
        let stm2, assertions = loop stm2 assertions nextloc in
        S_if (stm1, stm2), assertions
      | S_loop stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_loop stmt, assertions
      | S_loop_left stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_loop_left stmt, assertions
      | S_dip stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_dip stmt, assertions
      | S_lambda stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_lambda stmt, assertions
      | S_create_contract stmt ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_create_contract stmt, assertions
      | S_create_contract_2 { code; parameter_ty; storage_ty }->
        let code, assertions = loop code assertions nextloc in
        S_create_contract_2 { code; parameter_ty; storage_ty}, assertions
      | S_dipn (i, stmt) ->
        let stmt, assertions = loop stmt assertions nextloc in
        S_dipn (i, stmt), assertions
      | S_const _
      | S_nop
      | S_nil _
      | S_cons_pair _
      | S_drop _
      | S_dup _
      | S_swap _
      | S_car _
      | S_cdr _
      | S_unpair _
      | S_cons_some _
      | S_cons_none _
      | S_cons_left _
      | S_cons_right _
      | S_cons_list _
      | S_list_size _
      | S_empty_set _
      | S_set_mem _
      | S_set_update _
      | S_set_size
      | S_empty_map _
      | S_map_mem _
      | S_map_get _
      | S_map_update _
      | S_map_size _
      | S_empty_big_map
      | S_big_map_mem
      | S_big_map_get
      | S_big_map_update
      | S_concat_string
      | S_concat_string_pair
      | S_slice_string
      | S_string_size
      | S_concat_bytes
      | S_concat_bytes_pair
      | S_slice_bytes
      | S_bytes_size
      | S_add_seconds_to_timestamp
      | S_add_timestamp_to_seconds
      | S_sub_timestamp_seconds
      | S_diff_timestamps
      | S_add_tez
      | S_sub_tez
      | S_mul_teznat
      | S_mul_nattez
      | S_ediv_teznat
      | S_ediv_tez
      | S_or
      | S_and
      | S_xor
      | S_not
      | S_is_nat
      | S_neg_nat
      | S_neg_int
      | S_abs_int
      | S_int_nat
      | S_add_intint
      | S_add_intnat
      | S_add_natint
      | S_add_natnat
      | S_sub_int
      | S_mul_intint
      | S_mul_intnat
      | S_mul_natint
      | S_mul_natnat
      | S_ediv_intint
      | S_ediv_intnat
      | S_ediv_natint
      | S_ediv_natnat
      | S_lsl_nat
      | S_lsr_nat
      | S_or_nat
      | S_and_nat
      | S_and_int_nat
      | S_xor_nat
      | S_not_nat
      | S_not_int
      | S_exec
      | S_apply
      | S_failwith _
      | S_compare _
      | S_eq
      | S_neq
      | S_lt
      | S_gt
      | S_le
      | S_ge
      | S_address
      | S_contract _
      | S_transfer_tokens
      | S_create_account
      | S_implicit_account
      | S_set_delegate
      | S_now
      | S_balance
      | S_check_signature
      | S_hash_key
      | S_pack
      | S_unpack
      | S_blake2b
      | S_sha256
      | S_sha512
      | S_sha3
      | S_keccak
      | S_steps_to_quota
      | S_source
      | S_sender
      | S_self _
      | S_amount
      | S_dig _
      | S_dug _
      | S_dropn _
      | S_level
      | S_self_address
      | S_voting_power
      | S_total_voting_power
      | S_chain_id ->
        let stmt, assertions = insert_assertions stmt assertions nextloc in
        stmt.skind, assertions
    in
    { stmt with skind }, assertions
  in
  let ast, _ = loop ast assertions ast.srange in
  ast
