open Mopsa
open Unstacked_ast
open Ast

let () =
  register_expr_visitor (fun default expr ->
      match ekind expr with
      (* Strings *)
      | E_mcs_string_concat_pair(e1, e2) ->
        {exprs=[e1;e2]; stmts=[]},
        (function
          | {exprs=[e1;e2]; _} -> {expr with ekind=E_mcs_string_concat_pair(e1,e2)}
          | _ -> assert false
        )
      | E_mcs_string_concat e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_string_concat(e)}
          | _ -> assert false
        )
      | E_mcs_string_size e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_string_size(e)}
          | _ -> assert false
        )
      | E_mcs_string_slice { start; stop; string } ->
        {exprs=[start; stop; string]; stmts=[]},
        (function
          | {exprs=[start; stop; string]; _} ->
            {expr with ekind=E_mcs_string_slice{start; stop; string}}
          | _ -> assert false
        )
      (* Bytes *)
      | E_mcs_bytes_concat_pair(e1, e2) ->
        {exprs=[e1;e2]; stmts=[]},
        (function
          | {exprs=[e1;e2]; _} -> {expr with ekind=E_mcs_bytes_concat_pair(e1,e2)}
          | _ -> assert false
        )
      | E_mcs_bytes_concat e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_bytes_concat(e)}
          | _ -> assert false
        )
      | E_mcs_bytes_size e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_bytes_size(e)}
          | _ -> assert false
        )
      | E_mcs_bytes_slice { start; stop; bytes } ->
        {exprs=[start; stop; bytes]; stmts=[]},
        (function
          | {exprs=[start; stop; bytes]; _} ->
            {expr with ekind=E_mcs_bytes_slice{start; stop; bytes}}
          | _ -> assert false
        )
      | E_mcs_blake2b e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_blake2b(e)}
          | _ -> assert false
        )
      | E_mcs_sha256 e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_sha256(e)}
          | _ -> assert false
        )
      | E_mcs_sha512 e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_sha512(e)}
          | _ -> assert false
        )
      | E_mcs_sha3 e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_sha3(e)}
          | _ -> assert false
        )
      | E_mcs_keccak e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_keccak(e)}
          | _ -> assert false
        )
      (* Pairs *)
      | E_mcs_pair (e1, e2) ->
        {exprs=[e1;e2]; stmts=[]},
        (function
          | {exprs=[e1;e2]; _} -> {expr with ekind=E_mcs_pair(e1,e2)}
          | _ -> assert false
        )
      | E_mcs_car (e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_car(e)}
          | _ -> assert false
        )
      | E_mcs_cdr (e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_cdr(e)}
          | _ -> assert false
        )
      (* Options *)
      | E_mcs_some (e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_some(e)}
          | _ -> assert false
        )
      | E_mcs_none ->
        {exprs=[]; stmts=[]},
        (function
          | {exprs=[]; _} -> {expr with ekind=E_mcs_none}
          | _ -> assert false
        )
      | E_mcs_is_none(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_is_none(e)}
          | _ -> assert false
        )
      | E_mcs_unsome(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_unsome(e)}
          | _ -> assert false
        )
      (* Lists *)
      | E_mcs_list_is_nil(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind=E_mcs_list_is_nil(e)}
          | _ -> assert false
        )
      | E_mcs_list_cons (item, list) ->
        {exprs=[item; list]; stmts=[]},
        (function
          | {exprs=[item; list]; _} -> {expr with
                                        ekind=E_mcs_list_cons(item,list)}
          | _ -> assert false
        )
      | E_mcs_list_size (e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_list_size(e)}
          | _ -> assert false
        )
      | E_mcs_list_head (e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_list_head(e)}
          | _ -> assert false
        )
      | E_mcs_list_tail(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_list_tail(e)}
          | _ -> assert false
        )
      (* Sets *)
      | E_mcs_set_add { item; set } ->
        {exprs=[item;set]; stmts=[]},
        (function
          | {exprs=[item; set]; _} ->
            {expr with ekind = E_mcs_set_add{item; set}}
          | _ -> assert false
        )
      | E_mcs_set_remove {item; set} ->
        {exprs=[item; set]; stmts=[]},
        (function
          | {exprs=[item; set]; _} ->
            {expr with ekind = E_mcs_set_remove{item; set}}
          | _ -> assert false
        )
      | E_mcs_set_mem {item; set} ->
        {exprs=[item; set]; stmts=[]},
        (function
          | {exprs=[item; set]; _} ->
            {expr with ekind = E_mcs_set_mem{item; set}}
          | _ -> assert false
        )
      | E_mcs_set_size e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_set_size(e)}
          | _ -> assert false
        )
      (* Maps *)
      | E_mcs_map_add { key; value; map } ->
        {exprs=[key; value; map]; stmts=[]},
        (function
          | {exprs=[key;value;map]; _} ->
            {expr with ekind=E_mcs_map_add{key; value; map}}
          | _ -> assert false
        )
      | E_mcs_map_remove { key; map } ->
        {exprs=[key; map]; stmts=[]},
        (function
          | {exprs=[key;map]; _} ->
            {expr with ekind=E_mcs_map_remove{key;map}}
          | _ -> assert false
        )
      | E_mcs_map_mem {key; map} ->
        {exprs=[key; map]; stmts=[]},
        (function
          | {exprs=[key;map]; _} ->
            {expr with ekind=E_mcs_map_mem{key;map}}
          | _ -> assert false
        )
      | E_mcs_map_get{key; map} ->
        {exprs=[key; map]; stmts=[]},
        (function
          | {exprs=[key;map]; _} ->
            {expr with ekind=E_mcs_map_get{key;map}}
          | _ -> assert false
        )
      | E_mcs_map_size{map} ->
        {exprs=[map]; stmts=[]},
        (function
          | {exprs=[map]; _} ->
            {expr with ekind=E_mcs_map_size{map}}
          | _ -> assert false
        )
      (* Unions *)
      | E_mcs_cons_left(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_cons_left(e)}
          | _ -> assert false
        )
      | E_mcs_cons_right(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_cons_right(e)}
          | _ -> assert false
        )
      | E_mcs_is_left(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_is_left(e)}
          | _ -> assert false
        )
      | E_mcs_unleft(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_unleft(e)}
          | _ -> assert false
        )
      | E_mcs_unright(e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_unright(e)}
          | _ -> assert false
        )
      (* Lambdas *)
      | E_mcs_lambda_apply(lambda, e) ->
        {exprs=[lambda; e]; stmts=[]},
        (function
          | {exprs=[lambda;e]; _} ->
            {expr with ekind = E_mcs_lambda_apply(lambda, e)}
          | _ -> assert false
        )

      (* Compare and operators *)
      | E_mcs_compare(e1, e2) ->
        {exprs=[e1; e2]; stmts=[]},
        (function
          | {exprs=[e1; e2]; _} -> {expr with ekind = E_mcs_compare(e1, e2)}
          | _ -> assert false
        )
      | E_mcs_unop(op, e) ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_unop(op, e)}
          | _ -> assert false
        )
      | E_mcs_binop(op, e1, e2) ->
        {exprs=[e1; e2]; stmts=[]},
        (function
          | {exprs=[e1; e2]; _} -> {expr with ekind = E_mcs_binop(op, e1, e2)}
          | _ -> assert false
        )
      (* Domain specific *)
      | E_mcs_amount
      | E_mcs_balance
      | E_mcs_now -> leaf expr
      | E_mcs_transfer { parameter; amount; contract } ->
        {exprs=[parameter; amount; contract]; stmts=[]},
        (function
          | {exprs=[parameter; amount; contract]; _} ->
            {expr with ekind = E_mcs_transfer{parameter; amount; contract}}
          | _ -> assert false
        )
      | E_mcs_contract { address; entry; typ } ->
        {exprs=[address]; stmts=[]},
        (function
          | {exprs=[address]; _} -> {expr with ekind = E_mcs_contract{address; entry; typ }}
          | _ -> assert false
        )
      | E_mcs_address contract ->
        {exprs=[contract]; stmts=[]},
        (function
          | {exprs=[contract]; _} -> {expr with ekind = E_mcs_address contract}
          | _ -> assert false
        )
      | E_mcs_add_delegate e->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_add_delegate e}
          | _ -> assert false
        )
      | E_mcs_remove_delegate -> leaf expr
      | E_mcs_implicit_account e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_implicit_account e}
          | _ -> assert false
        )
      | E_mcs_source -> leaf expr
      | E_mcs_sender -> leaf expr
      | E_mcs_self _ -> leaf expr
      | E_mcs_create_contract { amount;
                                delegate;
                                code;
                                parameter_ty;
                                storage_ty;
                                storage;
                              } ->
        {exprs=[amount; delegate; storage]; stmts=[code]},
        (function
          | {exprs=[amount; delegate; storage]; stmts=[code]} ->
            {expr with ekind = E_mcs_create_contract{amount;
                                                     delegate;
                                                     code;
                                                     parameter_ty;
                                                     storage_ty;
                                                     storage }}
          | _ -> assert false
        )
      | E_mcs_universal_expression e ->
        {exprs=[e]; stmts=[]},
        (function
          | {exprs=[e]; _} -> {expr with ekind = E_mcs_universal_expression e}
          | _ -> assert false
        )
      | _ -> default expr
    );

  register_stmt_visitor (fun default stmt ->
      match skind stmt with
      | S_michelson_instr { instr; sp; bef; aft; loc } ->
        begin match instr with
          (* Stack manipulation *)
          | S_mcs_drop _
          | S_mcs_dropn _
          | S_mcs_dup _
          | S_mcs_swap _
          | S_mcs_dig _
          | S_mcs_dug _
          | S_mcs_const _ -> leaf stmt
          | S_mcs_dip (s) ->
            {exprs=[]; stmts=[s]},
            (function
              | {stmts=[s]; _} ->
                {stmt with
                 skind = S_michelson_instr { instr = S_mcs_dip (s);
                                             sp; bef; aft; loc }
                }
              | _ -> assert false
            )
          | S_mcs_dipn (n, s) ->
            {exprs=[]; stmts=[s]},
            (function
              | {stmts=[s]; _} ->
                {stmt with
                 skind = S_michelson_instr { instr = S_mcs_dipn (n, s);
                                             sp; bef; aft; loc }
                }
              | _ -> assert false
            )
          (* Arithmetic *)
          | S_mcs_abs_int
          | S_mcs_neg_int
          | S_mcs_neg_nat
          | S_mcs_add_intint
          | S_mcs_add_intnat
          | S_mcs_add_natint
          | S_mcs_add_natnat
          | S_mcs_sub_int
          | S_mcs_mul_intint
          | S_mcs_mul_intnat
          | S_mcs_mul_natint
          | S_mcs_mul_natnat
          | S_mcs_ediv_intint
          | S_mcs_ediv_intnat
          | S_mcs_ediv_natint
          | S_mcs_ediv_natnat -> leaf stmt
          (* Integer comparison *)
          | S_mcs_eq
          | S_mcs_neq
          | S_mcs_gt
          | S_mcs_ge
          | S_mcs_lt
          | S_mcs_le -> leaf stmt
          (* Polymorphic comparison *)
          | S_mcs_compare _ -> leaf stmt
          (* Integer casts *)
          | S_mcs_is_nat
          | S_mcs_int_nat -> leaf stmt
          (* Booleans *)
          | S_mcs_not
          | S_mcs_and
          | S_mcs_or
          | S_mcs_xor
          | S_mcs_xor_nat -> leaf stmt
          (* Bitwise and shift *)
          | S_mcs_not_int
          | S_mcs_not_nat
          | S_mcs_and_nat
          | S_mcs_and_int_nat
          | S_mcs_or_nat
          | S_mcs_lsl_nat
          | S_mcs_lsr_nat -> leaf stmt
          (* Mutez *)
          | S_mcs_add_tez
          | S_mcs_sub_tez
          | S_mcs_mul_nattez
          | S_mcs_mul_teznat
          | S_mcs_ediv_tez
          | S_mcs_ediv_teznat
          | S_mcs_amount
          | S_mcs_balance -> leaf stmt
          (* Timestamps *)
          | S_mcs_add_seconds_to_timestamp
          | S_mcs_add_timestamp_to_seconds
          | S_mcs_sub_timestamp_seconds
          | S_mcs_diff_timestamps
          | S_mcs_now -> leaf stmt
          (* Strings *)
          | S_mcs_concat_string
          | S_mcs_concat_string_pair
          | S_mcs_string_size
          | S_mcs_slice_string -> leaf stmt
          (* Bytes *)
          | S_mcs_concat_bytes
          | S_mcs_concat_bytes_pair
          | S_mcs_bytes_size
          | S_mcs_slice_bytes
          | S_mcs_blake2b
          | S_mcs_sha256
          | S_mcs_sha512
          | S_mcs_sha3
          | S_mcs_keccak -> leaf stmt
          (* pairs *)
          | S_mcs_cons_pair _
          | S_mcs_car _
          | S_mcs_cdr _
          | S_mcs_unpair _ -> leaf stmt
          (* options *)
          | S_mcs_cons_some _
          | S_mcs_cons_none _ -> leaf stmt
          | S_mcs_if_none(ty, tstm, fstm) ->
            {exprs=[]; stmts=[tstm; fstm]},
            (function
              | {stmts=[tstm; fstm]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_if_none (ty, tstm, fstm);
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          (* Lists *)
          | S_mcs_nil _ -> leaf stmt
          | S_mcs_cons_list _ -> leaf stmt
          | S_mcs_list_size _ -> leaf stmt
          | S_mcs_if_cons(ty, tstm, fstm) ->
            {exprs=[]; stmts=[tstm; fstm]},
            (function
              | {stmts=[tstm; fstm]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_if_cons (ty, tstm, fstm);
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          | S_mcs_list_iter stm ->
            {exprs=[]; stmts=[stm]},
            (function
              | {stmts=[stm]; _} ->
                {stmt with skind =
                             S_michelson_instr
                               {
                                 instr = S_mcs_list_iter (stm);
                                 sp; bef; aft; loc
                               }
                }
              | _ -> assert false
            )
          | S_mcs_list_map stm ->
            {exprs=[]; stmts=[stm]},
            (function
              | {stmts=[stm]; _} ->
                {stmt with skind =
                             S_michelson_instr
                               {
                                 instr = S_mcs_list_map (stm);
                                 sp; bef; aft; loc
                               }
                }
              | _ -> assert false
            )
          (* Sets *)
          | S_mcs_empty_set _
          | S_mcs_set_update _
          | S_mcs_set_mem _
          | S_mcs_set_size -> leaf stmt
          | S_mcs_set_iter (s) ->
            {exprs=[]; stmts=[s]},
            (function
              | {stmts=[s]; _} ->
                {stmt with
                 skind = S_michelson_instr { instr = S_mcs_set_iter (s);
                                             sp; bef; aft; loc }
                }
              | _ -> assert false
            )
          (* Maps *)
          | S_mcs_empty_map _
          | S_mcs_map_update _
          | S_mcs_map_mem _
          | S_mcs_map_get _
          | S_mcs_map_size _ -> leaf stmt
          | S_mcs_map_iter s ->
            {exprs=[]; stmts=[s]},
            (function
              | {stmts=[s]; _} ->
                {stmt with
                 skind = S_michelson_instr { instr = S_mcs_map_iter (s);
                                             sp; bef; aft; loc }
                }
              | _ -> assert false
            )
          | S_mcs_map_map s ->
            {exprs=[]; stmts=[s]},
            (function
              | {stmts=[s]; _} ->
                {stmt with
                 skind = S_michelson_instr { instr = S_mcs_map_map (s);
                                             sp; bef; aft; loc }
                }
              | _ -> assert false
            )

          (* Big maps *)
          | S_mcs_empty_big_map
          | S_mcs_big_map_mem
          | S_mcs_big_map_get
          | S_mcs_big_map_update -> leaf stmt

          (* Unions *)
          | S_mcs_cons_left _
          | S_mcs_cons_right _ -> leaf stmt
          | S_mcs_if_left (ty, tstm, fstm) ->
            {exprs=[]; stmts=[tstm; fstm]},
            (function
              | {stmts=[tstm; fstm]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_if_left(ty, tstm, fstm);
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          (* Control flow *)
          | S_mcs_if(stm1, stm2) ->
            {exprs=[]; stmts=[stm1; stm2]},
            (function
              | {stmts=[stm1; stm2]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_if(stm1, stm2);
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          | S_mcs_loop(s) ->
            {exprs=[]; stmts=[s]},
            (function
              | {stmts=[s]; _} ->
                {stmt with
                 skind = S_michelson_instr { instr = S_mcs_loop (s);
                                             sp; bef; aft; loc }
                }
              | _ -> assert false
            )
          | S_mcs_loop_left(s) ->
            {exprs=[]; stmts=[s]},
            (function
              | {stmts=[s]; _} ->
                {stmt with
                 skind = S_michelson_instr { instr = S_mcs_loop_left (s);
                                             sp; bef; aft; loc }
                }
              | _ -> assert false
            )
          (* Lambda *)
          | S_mcs_lambda (s) ->
            {exprs=[]; stmts=[s]},
            (function
              | {stmts=[s]; _} ->
                {stmt with
                 skind = S_michelson_instr { instr = S_mcs_lambda (s);
                                             sp; bef; aft; loc }
                }
              | _ -> assert false
            )
          | S_mcs_exec -> leaf stmt
          (* Domain specific *)
          | S_mcs_contract _
          | S_mcs_transfer_tokens
          | S_mcs_source
          | S_mcs_sender
          | S_mcs_self _ -> leaf stmt
          (* Specials *)
          | S_mcs_seq(stm1, stm2) ->
            {exprs=[]; stmts=[stm1; stm2]},
            (function
              | {stmts=[stm1; stm2]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_seq(stm1, stm2);
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          | S_mcs_wrapped_before { assertion; stm } ->
            {exprs=[]; stmts=[assertion; stm]},
            (function
              | {stmts=[assertion; stm]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_wrapped_before{ assertion; stm };
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          | S_mcs_wrapped_after { assertion; stm } ->
            {exprs=[]; stmts=[assertion; stm]},
            (function
              | {stmts=[assertion; stm]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_wrapped_after{ assertion; stm };
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          | S_mcs_nop -> leaf stmt
          | S_mcs_failwith _ -> leaf stmt
          | S_mcs_apply
          | S_mcs_address
          | S_mcs_create_account
          | S_mcs_implicit_account
          | S_mcs_set_delegate
          | S_mcs_check_signature
          | S_mcs_hash_key
          | S_mcs_pack
          | S_mcs_unpack
          | S_mcs_steps_to_quota
          | S_mcs_chain_id
          | S_mcs_level -> leaf stmt
          | S_mcs_create_contract stm ->
            {exprs=[]; stmts=[stm]},
            (function
              | {stmts=[stm]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_create_contract(stm);
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          | S_mcs_create_contract_2 { parameter_ty;
                                      storage_ty;
                                      code } ->
            {exprs=[]; stmts=[code]},
            (function
              | {stmts=[code]; _} ->
                { stmt with skind =
                              S_michelson_instr
                                {
                                  instr = S_mcs_create_contract_2{parameter_ty;
                                                                  storage_ty;
                                                                  code};
                                  sp; bef; aft; loc
                                }
                }
              | _ -> assert false
            )
          | S_mcs_self_address
          | S_mcs_voting_power
          | S_mcs_total_voting_power -> leaf stmt
          (* Inter contract *)
        end
      | S_mcs_inter_exec _ -> leaf stmt
      | S_mcs_inter_operation_list_iter { operations } ->
        {exprs=[operations]; stmts=[]},
        (function
          | {stmts=[]; exprs=[operations]} ->
            { stmt with skind = S_mcs_inter_operation_list_iter { operations }; }
          | _ -> assert false
        )
      | S_mcs_inter_exec_transfer { transfer } ->
        {exprs=[transfer]; stmts=[]},
        (function
          | {stmts=[]; exprs=[transfer]} ->
            { stmt with skind = S_mcs_inter_exec_transfer { transfer }; }
          | _ -> assert false
        )
      | S_mcs_inter_exec_transfer_address { address; parameter; amount } ->
        {exprs=[address; parameter; amount]; stmts=[]},
        (function
          | {stmts=[]; exprs=[address; parameter; amount]} ->
            {stmt with skind = S_mcs_inter_exec_transfer_address
                           {address;
                            parameter;
                            amount }}
          | _ -> assert false
        )
      | S_mcs_store_add_contracts _ -> leaf stmt
      | S_mcs_store_exec_contract { address; parameter; amount } ->
        {exprs=[parameter; amount]; stmts=[]},
        (function
          | {stmts=[]; exprs=[parameter; amount]} ->
            {stmt with skind = S_mcs_store_exec_contract
                           {address;
                            parameter;
                            amount }}
          | _ -> assert false
        )
      | _ -> default stmt
    );
  ()
