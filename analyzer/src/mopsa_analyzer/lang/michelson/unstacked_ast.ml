open Mopsa
open Ast
open Universal.Ast

type expr_kind +=
  (* Strings *)
  | E_mcs_string_concat_pair of expr * expr
  | E_mcs_string_concat of expr
  | E_mcs_string_size of expr
  | E_mcs_string_slice of { start: expr; stop: expr; string: expr }
  (* Bytes *)
  | E_mcs_bytes_concat_pair of expr * expr
  | E_mcs_bytes_concat of expr
  | E_mcs_bytes_size of expr
  | E_mcs_bytes_slice of { start: expr; stop: expr; bytes: expr }
  | E_mcs_blake2b of expr
  | E_mcs_sha256 of expr
  | E_mcs_sha512 of expr
  | E_mcs_sha3 of expr
  | E_mcs_keccak of expr
  (* Pairs *)
  | E_mcs_pair of expr * expr
  | E_mcs_car of expr
  | E_mcs_cdr of expr
  (* Options *)
  | E_mcs_none
  | E_mcs_some of expr
  | E_mcs_is_none of expr
  | E_mcs_unsome of expr
  (* Lists *)
  | E_mcs_list_cons of expr * expr
  | E_mcs_list_size of expr
  | E_mcs_list_is_nil of expr
  | E_mcs_list_head of expr
  | E_mcs_list_tail of expr
  (* Sets *)
  | E_mcs_set_add of { item: expr; set: expr }
  | E_mcs_set_remove of { item: expr; set: expr }
  | E_mcs_set_mem of { item: expr; set: expr }
  | E_mcs_set_size of expr
  (* Maps *)
  | E_mcs_map_add of { key: expr; value: expr; map: expr }
  | E_mcs_map_remove of { key: expr; map: expr }
  | E_mcs_map_mem of { key: expr; map: expr }
  | E_mcs_map_get of { key: expr; map: expr }
  | E_mcs_map_size of { map: expr }
  (* Unions *)
  | E_mcs_cons_left of expr
  | E_mcs_cons_right of expr
  | E_mcs_compare of expr * expr
  | E_mcs_is_left of expr
  | E_mcs_unleft of expr
  | E_mcs_unright of expr
  (* Lambdas *)
  | E_mcs_lambda_apply of expr * expr
  (* Operators *)
  | E_mcs_unop of operator * expr
  | E_mcs_binop of operator * expr * expr
  (* Domain specific *)
  | E_mcs_transfer of { parameter: expr; amount: expr; contract: expr }
  | E_mcs_contract of { address: expr; typ: typ; entry: string }
  | E_mcs_implicit_account of expr
  | E_mcs_create_contract of { parameter_ty: typ;
                               storage_ty: typ;
                               storage: expr;
                               code: stmt;
                               delegate: expr;
                               amount: expr;
                             }
  | E_mcs_address of expr
  | E_mcs_sender
  | E_mcs_self of string
  | E_mcs_source
  | E_mcs_add_delegate of expr
  | E_mcs_remove_delegate
  | E_mcs_is_transfer of expr
  (* Specials *)
  | E_mcs_amount
  | E_mcs_balance
  | E_mcs_now
  (* Wrapper for symbolic expressions *)
  | E_mcs_universal_expression of expr

let () = register_expr {
    compare = (fun next e1 e2 ->
        match ekind e1, ekind e2 with
        (* Strings *)
        | E_mcs_string_concat_pair(e1, e2), E_mcs_string_concat_pair(e1', e2') ->
            Compare.compose [
            (fun () -> compare_expr e1 e1');
            (fun () -> compare_expr e2 e2');
          ]
        | E_mcs_string_concat e, E_mcs_string_concat e' ->
          compare_expr e e'
        | E_mcs_string_size e, E_mcs_string_size e' ->
          compare_expr e e'
        | E_mcs_string_slice { start; stop; string },
          E_mcs_string_slice { start = start'; stop = stop'; string=string' } ->
            Compare.compose [
            (fun () -> compare_expr start start');
            (fun () -> compare_expr stop stop');
            (fun () -> compare_expr string string');
          ]
        (* Bytes *)
        | E_mcs_bytes_concat_pair(e1, e2), E_mcs_bytes_concat_pair(e1', e2') ->
            Compare.compose [
            (fun () -> compare_expr e1 e1');
            (fun () -> compare_expr e2 e2');
          ]
        | E_mcs_bytes_concat e, E_mcs_bytes_concat e' ->
          compare_expr e e'
        | E_mcs_bytes_size e, E_mcs_bytes_size e' ->
          compare_expr e e'
        | E_mcs_bytes_slice { start; stop; bytes },
          E_mcs_bytes_slice { start = start'; stop = stop'; bytes=bytes' } ->
            Compare.compose [
            (fun () -> compare_expr start start');
            (fun () -> compare_expr stop stop');
            (fun () -> compare_expr bytes bytes');
          ]
        | E_mcs_blake2b e, E_mcs_blake2b e'
        | E_mcs_sha256 e, E_mcs_sha256 e'
        | E_mcs_sha512 e, E_mcs_sha512 e'
        | E_mcs_sha3 e, E_mcs_sha3 e'
        | E_mcs_keccak e, E_mcs_keccak e' ->
          compare_expr e e'
        (* Pairs *)
        | E_mcs_pair(e1, e2), E_mcs_pair(e1', e2') ->
          Compare.compose [
            (fun () -> compare_expr e1 e1');
            (fun () -> compare_expr e2 e2');
          ]
        | E_mcs_car(e1), E_mcs_car(e2) ->
          compare_expr e1 e2
        | E_mcs_cdr(e1), E_mcs_cdr(e2) ->
          compare_expr e1 e2
        (* Options *)
        | E_mcs_some (e1), E_mcs_some(e2) ->
          compare_expr e1 e2
        | E_mcs_is_none (e1), E_mcs_is_none(e2) ->
          compare_expr e1 e2
        | E_mcs_unsome (e1), E_mcs_unsome (e2) ->
          compare_expr e1 e2
        (* Lists *)
        | E_mcs_list_cons (e1, e2), E_mcs_list_cons(e1', e2') ->
          Compare.compose [
            (fun () -> compare_expr e1 e1');
            (fun () -> compare_expr e2 e2');
          ]
        | E_mcs_list_size (e1), E_mcs_list_size (e2) ->
          compare_expr e1 e2
        | E_mcs_list_is_nil e1, E_mcs_list_is_nil e2 ->
          compare_expr e1 e2
        | E_mcs_list_head e1, E_mcs_list_head e2 ->
          compare_expr e1 e2
        | E_mcs_list_tail e1, E_mcs_list_tail e2 ->
          compare_expr e1 e2
        (* Unions *)
        | E_mcs_cons_left e1, E_mcs_cons_left e2 ->
          compare_expr e1 e2
        | E_mcs_cons_right e1, E_mcs_cons_right e2 ->
          compare_expr e1 e2
        | E_mcs_is_left e1, E_mcs_is_left e2 ->
          compare_expr e1 e2
        | E_mcs_unleft e1, E_mcs_unleft e2 ->
          compare_expr e1 e2
        | E_mcs_unright e1, E_mcs_unright e2 ->
          compare_expr e1 e2
        (* Sets *)
        | E_mcs_set_add {item=i1;set=s1}, E_mcs_set_add {item=i2; set=s2} ->
          Compare.compose [
            (fun () -> compare_expr i1 i2);
            (fun () -> compare_expr s1 s2);
          ]
        | E_mcs_set_remove {item=i1;set=s1}, E_mcs_set_remove {item=i2; set=s2} ->
          Compare.compose [
            (fun () -> compare_expr i1 i2);
            (fun () -> compare_expr s1 s2);
          ]
        | E_mcs_set_mem {item=i1;set=s1}, E_mcs_set_mem {item=i2; set=s2} ->
          Compare.compose [
            (fun () -> compare_expr i1 i2);
            (fun () -> compare_expr s1 s2);
          ]
        | E_mcs_set_size e1, E_mcs_set_size e2 -> compare_expr e1 e2
        (* Lambdas *)
        | E_mcs_lambda_apply(l1, a1), E_mcs_lambda_apply(l2, a2) ->
          Compare.compose [
            (fun () -> compare_expr l1 l2);
            (fun () -> compare_expr a1 a2);
          ]
        (* Compare *)
        | E_mcs_compare(e1, e2), E_mcs_compare(e1', e2') ->
          Compare.compose [
            (fun () -> compare_expr e1 e1');
            (fun () -> compare_expr e2 e2');
          ]
        (* Operators *)
        | E_mcs_unop(op, e), E_mcs_unop(op', e') ->
          Compare.compose [
            (fun () -> compare op op');
            (fun () -> compare_expr e e');
          ]
        | E_mcs_binop(op, e1, e2), E_mcs_binop(op', e1', e2') ->
          Compare.compose [
            (fun () -> compare op op');
            (fun () -> compare_expr e1 e1');
            (fun () -> compare_expr e2 e2');
          ]
        (* Operations *)
        | E_mcs_transfer { parameter=p1; amount=a1; contract=c1 },
          E_mcs_transfer { parameter=p2; amount=a2; contract=c2 } ->
          Compare.compose [
            (fun () -> compare_expr p1 p2);
            (fun () -> compare_expr a1 a2);
            (fun () -> compare_expr c1 c2);
          ]
        | E_mcs_contract { address = addr1;
                           entry = entry1;
                           typ = typ1 },
          E_mcs_contract { address = addr2;
                           entry = entry2;
                           typ = typ2 } ->
          Compare.compose [
            (fun () -> compare_typ typ1 typ2);
            (fun () -> String.compare entry1 entry2);
            (fun () -> compare_expr addr1 addr2);
          ]
        | E_mcs_add_delegate e1, E_mcs_add_delegate e2 ->
          compare_expr e1 e2
        | E_mcs_address e1, E_mcs_address e2 ->
          compare_expr e1 e2
        | E_mcs_implicit_account e1, E_mcs_implicit_account e2 ->
          compare_expr e1 e2
        | E_mcs_create_contract { parameter_ty = p1;
                                  storage_ty = s1;
                                  code = stm1;
                                  delegate = d1; amount = a1 },
          E_mcs_create_contract { parameter_ty = p2;
                                  storage_ty = s2; code = stm2;
                                  delegate = d2; amount = a2 } ->
          Compare.compose [
            (fun () -> compare_typ p1 p2);
            (fun () -> compare_typ s1 s2);
            (fun () -> compare_expr d1 d2);
            (fun () -> compare_expr a1 a2);
            (fun () -> compare_stmt stm1 stm2);
          ]
        | E_mcs_is_transfer e1, E_mcs_is_transfer e2 ->
          compare_expr e1 e2
        | E_mcs_universal_expression e1, E_mcs_universal_expression e2 ->
          compare_expr e1 e2
        | _ -> next e1 e2
      );
    print = (fun next fmt e ->
        match ekind e with
        (* Strings *)
        | E_mcs_string_concat_pair(e1, e2) ->
          Format.fprintf fmt "string_concat (%a, %a)" pp_expr e1 pp_expr e2
        | E_mcs_string_concat e ->
          Format.fprintf fmt "string_concat (%a)" pp_expr e
        | E_mcs_string_size e ->
          Format.fprintf fmt "string_size (%a)" pp_expr e
        | E_mcs_string_slice { start; stop; string } ->
          Format.fprintf fmt "string_slice (%a, %a, %a)"
            pp_expr start pp_expr stop pp_expr string
        (* Bytes *)
        | E_mcs_bytes_concat_pair (e1, e2) ->
          Format.fprintf fmt "bytes_concat (%a, %a)" pp_expr e1 pp_expr e2
        | E_mcs_bytes_concat e ->
          Format.fprintf fmt "bytes_concat (%a)" pp_expr e
        | E_mcs_bytes_size e ->
          Format.fprintf fmt "bytes_size (%a)" pp_expr e
        | E_mcs_bytes_slice { start; stop; bytes } ->
          Format.fprintf fmt "bytes_slice (%a, %a, %a)"
            pp_expr start pp_expr stop pp_expr bytes
        | E_mcs_blake2b e ->
          Format.fprintf fmt "blake2b(%a)" pp_expr e
        | E_mcs_sha256 e ->
          Format.fprintf fmt "sha256(%a)" pp_expr e
        | E_mcs_sha512 e ->
          Format.fprintf fmt "sha512(%a)" pp_expr e
        | E_mcs_sha3  e ->
          Format.fprintf fmt "sha3(%a)" pp_expr e
        | E_mcs_keccak e ->
          Format.fprintf fmt "keccak(%a)" pp_expr e
        (* Pairs *)
        | E_mcs_pair (e1, e2) ->
          Format.fprintf fmt "pair(%a, %a)" pp_expr e1 pp_expr e2
        | E_mcs_car e ->
          Format.fprintf fmt "fst(%a)" pp_expr e
        | E_mcs_cdr e ->
          Format.fprintf fmt "snd(%a)" pp_expr e
        (* Options *)
        | E_mcs_none ->
          Format.fprintf fmt "None"
        | E_mcs_some (e) ->
          Format.fprintf fmt "Some(%a)" pp_expr e
        | E_mcs_is_none (e) ->
          Format.fprintf fmt "is_none(%a)" pp_expr e
        | E_mcs_unsome(e) ->
          Format.fprintf fmt "unsome(%a)" pp_expr e
        (* Lists *)
        | E_mcs_list_cons (e1, e2) ->
          Format.fprintf fmt "%a::%a" pp_expr e1 pp_expr e2
        | E_mcs_list_size e ->
          Format.fprintf fmt "list_size(%a)" pp_expr e
        | E_mcs_list_is_nil e ->
          Format.fprintf fmt "list_is_nil(%a)" pp_expr e
        | E_mcs_list_head e ->
          Format.fprintf fmt "list_head(%a)" pp_expr e
        | E_mcs_list_tail e ->
          Format.fprintf fmt "list_tail(%a)" pp_expr e
        (* Sets *)
        | E_mcs_set_add { item; set } ->
          Format.fprintf fmt "set_add(%a, %a)" pp_expr set pp_expr item
        | E_mcs_set_remove { item; set } ->
          Format.fprintf fmt "set_remove(%a, %a)" pp_expr set pp_expr item
        | E_mcs_set_mem { item; set } ->
          Format.fprintf fmt "set_mem(%a, %a)" pp_expr set pp_expr item
        | E_mcs_set_size expr ->
          Format.fprintf fmt "set_size(%a)" pp_expr expr
        (* Maps *)
        | E_mcs_map_add { key; value; map } ->
          Format.fprintf fmt "map_add(%a, %a, %a)"
            pp_expr key pp_expr value pp_expr map
        | E_mcs_map_mem { key; map } ->
          Format.fprintf fmt "map_mem(%a, %a)" pp_expr key pp_expr map
        | E_mcs_map_get { key; map } ->
          Format.fprintf fmt "map_get(%a, %a)" pp_expr key pp_expr map
        | E_mcs_map_remove { key; map } ->
          Format.fprintf fmt "map_remove(%a, %a)" pp_expr key pp_expr map
        | E_mcs_map_size { map } ->
          Format.fprintf fmt "map_size(%a)" pp_expr map
        (* Unions *)
        | E_mcs_cons_left e ->
          Format.fprintf fmt "left(%a)" pp_expr e
        | E_mcs_cons_right e ->
          Format.fprintf fmt "right(%a)" pp_expr e
        | E_mcs_is_left e ->
          Format.fprintf fmt "is_left(%a)" pp_expr e
        | E_mcs_unleft e ->
          Format.fprintf fmt "unleft(%a)" pp_expr e
        | E_mcs_unright e ->
          Format.fprintf fmt "unright(%a)" pp_expr e
        (* Lambda *)
        | E_mcs_lambda_apply(lambda, arg) ->
          Format.fprintf fmt "apply(%a, %a)" pp_expr lambda pp_expr arg
        (* Comparison *)
        | E_mcs_compare (e1, e2) ->
          Format.fprintf fmt "compare(%a, %a)" pp_expr e1 pp_expr e2
        (* Operators *)
        | E_mcs_unop(op, e) ->
          Format.fprintf fmt "(%a %a)" pp_operator op pp_expr e
        | E_mcs_binop(op, e1, e2) ->
          Format.fprintf fmt "(%a %a_m %a)" pp_expr e1 pp_operator op pp_expr e2
        (* Domain specific *)
        | E_mcs_transfer { parameter; amount; contract } ->
          Format.fprintf fmt "transfer(%a, %a, %a)"
            pp_expr parameter pp_expr amount pp_expr contract
        | E_mcs_contract { address; entry; typ } ->
          Format.fprintf fmt "(contract((%a)%%%s) : contract %a)"
            pp_expr address
            entry
            pp_typ typ
        | E_mcs_implicit_account key_hash ->
          Format.fprintf fmt "implicit_account(%a)" pp_expr key_hash
        | E_mcs_add_delegate key_hash ->
          Format.fprintf fmt "add_delegate(%a)" pp_expr key_hash
        | E_mcs_remove_delegate ->
          Format.fprintf fmt "remove_delegate"
        | E_mcs_address contract ->
          Format.fprintf fmt "address (%a)" pp_expr contract
        | E_mcs_sender ->
          Format.fprintf fmt "#sender"
        | E_mcs_self entrypoint ->
          Format.fprintf fmt "#self%%%s" entrypoint
        | E_mcs_source ->
          Format.fprintf fmt "#source"
        | E_mcs_amount ->
          Format.fprintf fmt "#amount"
        | E_mcs_balance ->
          Format.fprintf fmt "#balance"
        | E_mcs_now ->
          Format.fprintf fmt "#now"
        | E_mcs_create_contract { parameter_ty = p1;
                                  storage_ty = s1;
                                  amount;
                                  code;
                                  delegate;
                                } ->
          Format.fprintf fmt "create_contract({amount=%a; delegate=%a; code=%a}"
            pp_expr amount
            pp_expr delegate
            pp_stmt code
        | E_mcs_is_transfer e ->
          Format.fprintf fmt "is_transfer(%a)" pp_expr e
        | E_mcs_universal_expression e ->
          Format.fprintf fmt "univ{|%a|}" pp_expr e
        | _ -> next fmt e
      )
  }

(* Strings *)
let mk_string_size e range =
  assert (compare_typ (etyp e) T_mcs_string = 0);
  mk_expr ~etyp:T_mcs_int (E_mcs_string_size e) range

let mk_string_concat_pair e1 e2 range =
  assert (compare_typ (etyp e1) T_mcs_string = 0);
  assert (compare_typ (etyp e2) T_mcs_string = 0);
  mk_expr ~etyp:T_mcs_string (E_mcs_string_concat_pair(e1, e2)) range

let mk_string_concat e range =
  assert (compare_typ (etyp e) (T_mcs_list T_mcs_string) = 0);
  mk_expr ~etyp:T_mcs_string (E_mcs_string_concat e) range

let mk_string_slice start stop string range =
  assert (compare_typ (etyp start) T_mcs_int = 0);
  assert (compare_typ (etyp stop) T_mcs_int = 0);
  assert (compare_typ (etyp string) T_mcs_string = 0);
  mk_expr
    ~etyp:(T_mcs_option T_mcs_string)
    (E_mcs_string_slice { start; stop; string }) range

(* Bytes *)
let mk_bytes_size e range =
  assert (compare_typ (etyp e) T_mcs_bytes = 0);
  mk_expr ~etyp:T_mcs_int (E_mcs_bytes_size e) range

let mk_bytes_concat_pair e1 e2 range =
  assert (compare_typ (etyp e1) T_mcs_bytes = 0);
  assert (compare_typ (etyp e2) T_mcs_bytes = 0);
  mk_expr ~etyp:T_mcs_bytes (E_mcs_bytes_concat_pair(e1, e2)) range

let mk_bytes_concat e range =
  assert (compare_typ (etyp e) (T_mcs_list T_mcs_bytes) = 0);
  mk_expr ~etyp:T_mcs_bytes (E_mcs_bytes_concat e) range

let mk_bytes_slice start stop bytes range =
  assert (compare_typ (etyp start) T_mcs_int = 0);
  assert (compare_typ (etyp stop) T_mcs_int = 0);
  assert (compare_typ (etyp bytes) T_mcs_bytes = 0);
  mk_expr
    ~etyp:(T_mcs_option T_mcs_bytes)
    (E_mcs_bytes_slice { start; stop; bytes }) range

let mk_blake2b e range =
  assert (compare_typ (etyp e) T_mcs_bytes = 0);
  mk_expr ~etyp:T_mcs_bytes (E_mcs_blake2b e) range

let mk_sha256 e range =
  assert (compare_typ (etyp e) T_mcs_bytes = 0);
  mk_expr ~etyp:T_mcs_bytes (E_mcs_sha256 e) range

let mk_sha512 e range =
  assert (compare_typ (etyp e) T_mcs_bytes = 0);
  mk_expr ~etyp:T_mcs_bytes (E_mcs_sha512 e) range

let mk_sha3 e range =
  assert (compare_typ (etyp e) T_mcs_bytes = 0);
  mk_expr ~etyp:T_mcs_bytes (E_mcs_sha3 e) range

let mk_keccak e range =
  assert (compare_typ (etyp e) T_mcs_bytes = 0);
  mk_expr ~etyp:T_mcs_bytes (E_mcs_keccak e) range

(* Pairs *)
let mk_pair e1 e2 loc =
  let t1 = etyp e1 in
  let t2 = etyp e2 in
  let etyp = T_mcs_pair (t1, t2) in
  mk_expr ~etyp (E_mcs_pair(e1,e2)) loc

let mk_car e loc =
  let etyp = match etyp e with
    | T_mcs_pair (t, _) -> t
    | ty -> panic "Unexpected type for car: %a is not a pair" pp_typ ty
  in
  mk_expr ~etyp (E_mcs_car e) loc

let mk_cdr e loc =
  let etyp = match etyp e with
    | T_mcs_pair (_, t) -> t
    | ty -> panic "Unexpected type for cdr: %a is not a pair" pp_typ ty
  in
  mk_expr ~etyp (E_mcs_cdr e) loc

let mk_none etyp loc =
  (match etyp with
   | T_mcs_option t -> ()
   | _ -> panic "Expecting a option type, got : %a" pp_typ etyp
  );
  mk_expr ~etyp (E_mcs_none) loc

let mk_some e loc =
  let etyp = T_mcs_option (etyp e) in
  mk_expr ~etyp (E_mcs_some e) loc

let mk_is_none e loc =
  let () = match etyp e with
    | T_mcs_option _ -> ()
    | t -> panic "Unexpected type for is_none: %a is not an option" pp_typ t
  in
  mk_expr ~etyp:T_mcs_bool (E_mcs_is_none e) loc

let mk_unsome e loc =
  let etyp = match etyp e with
    | T_mcs_option t -> t
    | t -> panic "Unexpected type for unsome: %a is not an option" pp_typ t
  in
  mk_expr ~etyp (E_mcs_unsome e) loc

let mk_list_nil etyp range =
  mk_constant ~etyp (C_mcs_list []) range

let mk_list_cons ~item ~list loc =
  let etyp =
    match etyp list, etyp item with
    | (T_mcs_list ty1) as etyp, ty2 when compare_typ ty1 ty2 = 0 -> etyp
    | lty, ity ->
      panic "Unexpected types for list_cons: %a %a" pp_typ ity pp_typ lty
  in
  mk_expr ~etyp (E_mcs_list_cons (item, list)) loc

let mk_list_size expr_list loc =
  (* XXX: T_mcs_int or T_nat ? *)
  mk_expr ~etyp:T_mcs_int (E_mcs_list_size (expr_list)) loc

let mk_list_head expr loc =
  let etyp = match etyp expr with
    | T_mcs_list t -> t
    | typ -> panic "Unexpected type for list_head: %a is not a list" pp_typ typ
  in
  mk_expr ~etyp (E_mcs_list_head expr) loc

let mk_list_tail expr loc =
  let etyp = match etyp expr with
    | (T_mcs_list _) as ty -> ty
    | typ -> panic "Unexpected type for list_tail: %a is not a list" pp_typ typ
  in
  mk_expr ~etyp (E_mcs_list_tail expr) loc

let mk_list_is_nil expr loc =
  let () = match etyp expr with
    | T_mcs_list t -> ()
    | typ -> panic "Unexpected type for list_is_nil: %a is not a list" pp_typ typ
  in
  mk_expr ~etyp:T_mcs_bool (E_mcs_list_is_nil expr) loc


(******************************************************************************
 * Sets
 ******************************************************************************)
let mk_set_empty etyp range =
  let () = match etyp with
    | T_mcs_set _ -> ()
    | _ -> panic "Expected set type"
  in
  mk_constant ~etyp (C_mcs_set []) range

let mk_set_add ~item ~set range =
  mk_expr ~etyp:(etyp set) (E_mcs_set_add { item; set }) range

let mk_set_remove ~item ~set range =
  mk_expr ~etyp:(etyp set) (E_mcs_set_remove { item; set}) range

let mk_set_mem ~item ~set range =
  mk_expr ~etyp:T_mcs_bool (E_mcs_set_mem { item; set }) range

let mk_set_size ~set range =
  mk_expr ~etyp:T_mcs_int (E_mcs_set_size set) range

(******************************************************************************
 * Maps
 ******************************************************************************)
let mk_map_empty ~etyp range =
  let () = match etyp with
    | T_mcs_map _ -> ()
    | _ -> panic "Expected map type, got : %a" pp_typ etyp
  in
  mk_constant ~etyp (C_mcs_map []) range

let mk_map_add ~key ~value ~map range =
  mk_expr ~etyp:(etyp map) (E_mcs_map_add { key; value; map }) range

let mk_map_remove ~key ~map range =
  mk_expr ~etyp:(etyp map) (E_mcs_map_remove { key; map }) range

let mk_map_mem ~key ~map range =
  mk_expr ~etyp:T_mcs_bool (E_mcs_map_mem { key; map }) range

let mk_map_get ~key ~map range =
  let etyp = match etyp map with
    | T_mcs_map(_, vty) -> T_mcs_option vty
    | _ -> assert false
  in
  mk_expr ~etyp (E_mcs_map_get { key; map }) range

let mk_map_size ~map range =
  mk_expr ~etyp:T_mcs_int (E_mcs_map_size {map}) range

(******************************************************************************
 * Unions
 ******************************************************************************)
let mk_mcs_cons_left ~etyp expr loc =
  (match etyp with
   | T_mcs_union (ty_l, _, _, _) when compare_typ ty_l expr.etyp = 0 -> ()
   | _ -> panic "Unexpected type for cons_left: %a" pp_typ etyp);
  mk_expr ~etyp (E_mcs_cons_left expr) loc

let mk_mcs_cons_right ~etyp expr loc =
  (match etyp with
   | T_mcs_union(_, _, ty_r, _) when compare_typ ty_r expr.etyp = 0 -> ()
   | _ -> panic "Unexpected type for cons_right: %a" pp_typ etyp);
  mk_expr ~etyp (E_mcs_cons_right expr) loc

let mk_is_left expr range =
  (match expr.etyp with
   | T_mcs_union _ -> ()
   | typ -> panic "Unexpected type for is_left: %a" pp_typ typ);
  mk_expr ~etyp:T_mcs_bool (E_mcs_is_left expr) range

let mk_unleft expr range =
  let etyp = match expr.etyp with
    | T_mcs_union (tyl, _, _, _) -> tyl
    | typ -> panic "Unexpected type for unleft: %a" pp_typ typ
  in
  mk_expr ~etyp (E_mcs_unleft expr) range

let mk_unright expr range =
  let etyp = match expr.etyp with
    | T_mcs_union (_, _, tyr, _) -> tyr
    | typ -> panic "Unexpected type for unright: %a" pp_typ typ
  in
  mk_expr ~etyp (E_mcs_unright expr) range

(******************************************************************************
 * Compare and operators
 ******************************************************************************)
let mk_mcs_compare e1 e2 loc =
  mk_expr ~etyp:T_mcs_int (E_mcs_compare(e1, e2)) loc

let mk_mcs_unop ~etyp op e loc =
  mk_expr ~etyp (E_mcs_unop(op, e)) loc

let mk_mcs_binop ~etyp e1 op e2 loc =
  mk_expr ~etyp (E_mcs_binop(op, e1, e2)) loc

(* Booleans *)
let mk_mcs_not e range =
  assert (compare_typ (etyp e) T_mcs_bool = 0);
  mk_mcs_unop ~etyp:T_mcs_bool O_log_not e range

(******************************************************************************
 * Domain specific
 ******************************************************************************)
let mk_transfer contract parameter amount range =
  mk_expr ~etyp:T_mcs_operation (E_mcs_transfer{parameter; contract; amount}) range

let mk_contract typ entry address range =
  mk_expr ~etyp:(T_mcs_option (T_mcs_address)) (E_mcs_contract { address; entry; typ }) range

let mk_amount loc =
  mk_expr ~etyp:T_mcs_mutez E_mcs_amount loc

let mk_balance loc =
  mk_expr ~etyp:T_mcs_mutez E_mcs_balance loc

let mk_now loc =
  mk_expr ~etyp:T_mcs_timestamp E_mcs_now loc

let mk_source loc =
  mk_expr ~etyp:T_mcs_address E_mcs_source loc

let mk_sender loc =
  mk_expr ~etyp:T_mcs_address E_mcs_sender loc

let mk_self entrypoint loc =
  mk_expr ~etyp:T_mcs_address (E_mcs_self entrypoint) loc

let mk_address contract loc =
  mk_expr ~etyp:T_mcs_address (E_mcs_address contract) loc

let mk_implicit_account e range =
  mk_expr ~etyp:(T_mcs_address) (E_mcs_implicit_account e) range

let mk_add_delegate e range =
  (match etyp e with
  | T_mcs_key_hash -> ()
  | ty -> panic "Expected type: key_hash, got : %a" pp_typ ty
  );
  mk_expr ~etyp:(T_mcs_operation) (E_mcs_add_delegate e) range

let mk_remove_delegate range =
  mk_expr ~etyp:(T_mcs_operation) E_mcs_remove_delegate range

let mk_is_transfer e range =
  (match etyp e with
   | T_mcs_operation -> ()
   | ty -> panic "Expected type: operation, got : %a" pp_typ ty
  );
  mk_expr ~etyp:T_mcs_bool (E_mcs_is_transfer e) range

let mk_lambda ~etyp stm range =
  (match etyp with
   | T_mcs_lambda _ -> ()
   | _ -> panic "Expected lambda type, got : %a" pp_typ etyp
  );
  mk_expr ~etyp (E_constant (C_mcs_lambda stm)) range

let mk_lambda_apply lambda arg range =
  let etyp =
    match lambda.etyp with
    | T_mcs_lambda(T_mcs_pair(t1, t2), t3) ->
      if compare_typ t1 arg.etyp <> 0 then
        panic "Unexpected type for lambda and apply argument: %a and %a"
          pp_typ lambda.etyp pp_typ arg.etyp
      else
        T_mcs_lambda(t2, t3)
    | _ -> panic "Unexpected type for lambda and apply argument: %a and %a"
             pp_typ lambda.etyp pp_typ arg.etyp
  in
  mk_expr ~etyp (E_mcs_lambda_apply(lambda, arg)) range

let mk_create_contract ~delegate ~amount ~storage ~parameter_ty ~storage_ty ~code range =
  mk_expr ~etyp:T_mcs_operation
    (E_mcs_create_contract { delegate; amount; storage; parameter_ty; storage_ty; code })
    range

let mk_universal_expression ~etyp e range =
  mk_expr ~etyp (E_mcs_universal_expression e) range

let mcs_not e range =
  (match e.etyp with
   | T_mcs_bool -> ()
   | _ -> panic "Expecting m_bool type"
  );
  mk_expr ~etyp:T_mcs_bool (E_mcs_unop(O_log_not, e)) range

let mcs_eq e1 e2 range =
  if compare_typ e1.etyp e2.etyp <> 0 then
    panic "Expecting same types for %a and %a" pp_typ e1.etyp pp_typ e2.etyp
  ;
  mk_expr ~etyp:T_mcs_bool (E_mcs_binop(O_eq, e1, e2)) range

let mcs_ne e1 e2 range =
  if compare_typ e1.etyp e2.etyp <> 0 then
    panic "Expecting same types for %a and %a" pp_typ e1.etyp pp_typ e2.etyp
  ;
  mk_expr ~etyp:T_mcs_bool (E_mcs_binop(O_ne, e1, e2)) range

let mcs_lt e1 e2 range =
  if compare_typ e1.etyp e2.etyp <> 0 then
    panic "Expecting same types for %a and %a" pp_typ e1.etyp pp_typ e2.etyp
  ;
  mk_expr ~etyp:T_mcs_bool (E_mcs_binop(O_lt, e1, e2)) range

let mcs_le e1 e2 range =
  if compare_typ e1.etyp e2.etyp <> 0 then
    panic "Expecting same types for %a and %a" pp_typ e1.etyp pp_typ e2.etyp
  ;
  mk_expr ~etyp:T_mcs_bool (E_mcs_binop(O_le, e1, e2)) range

let mcs_gt e1 e2 range =
  if compare_typ e1.etyp e2.etyp <> 0 then
    panic "Expecting same types for %a and %a" pp_typ e1.etyp pp_typ e2.etyp
  ;
  mk_expr ~etyp:T_mcs_bool (E_mcs_binop(O_gt, e1, e2)) range

let mcs_ge e1 e2 range =
  if compare_typ e1.etyp e2.etyp <> 0 then
    panic "Expecting same types for %a and %a" pp_typ e1.etyp pp_typ e2.etyp
  ;
  mk_expr ~etyp:T_mcs_bool (E_mcs_binop(O_ge, e1, e2)) range
