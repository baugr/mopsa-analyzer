open Mopsa

(* Checks for Michelson alarms *)
type check +=
  | CHK_MCL_SHIFT_OVERFLOW
  | CHK_MCL_ALWAYS_FAIL
  | CHK_MCL_MUTEZ_OVERFLOW
  | CHK_MCL_FAILURE

let () =
  Mopsa.register_check (fun next fmt -> function
      | CHK_MCL_SHIFT_OVERFLOW -> Format.fprintf fmt "shift overflow"
      | CHK_MCL_ALWAYS_FAIL -> Format.fprintf fmt "no non-failure flow"
      | CHK_MCL_MUTEZ_OVERFLOW -> Format.fprintf fmt "mutez overflow"
      | CHK_MCL_FAILURE -> Format.fprintf fmt "failure"
      | a -> next fmt a
    )

let unreachable_mcl_overflow_check range _man flow =
  Flow.add_unreachable_check CHK_MCL_SHIFT_OVERFLOW range flow

let safe_mcl_overflow_check range _man flow =
  Flow.add_safe_check CHK_MCL_SHIFT_OVERFLOW range flow

let safe_mcl_always_fail range _man flow =
  Flow.add_safe_check CHK_MCL_ALWAYS_FAIL range flow

let safe_mcl_mutez_overflow range _man flow =
  Flow.add_safe_check CHK_MCL_MUTEZ_OVERFLOW range flow

let safe_mcl_failure range _man flow =
  Flow.add_safe_check CHK_MCL_FAILURE range flow

(* Alarms for Michelson *)
type alarm_kind +=
  | A_mcl_shift_overflow of expr
  | A_mcl_always_fail
  | A_mcl_mutez_overflow of expr
  | A_mcl_failure of expr

let () =
  register_alarm {
    check = (fun next -> function
        | A_mcl_shift_overflow _ -> CHK_MCL_SHIFT_OVERFLOW
        | A_mcl_always_fail -> CHK_MCL_ALWAYS_FAIL
        | A_mcl_mutez_overflow _ -> CHK_MCL_MUTEZ_OVERFLOW
        | A_mcl_failure _ -> CHK_MCL_FAILURE
        | a -> next a
      );
    compare = (fun next a1 a2 ->
        match a1, a2 with
        | A_mcl_shift_overflow e1, A_mcl_shift_overflow e2 ->
          compare_expr e1 e2
        | A_mcl_mutez_overflow e1, A_mcl_mutez_overflow e2 ->
          compare_expr e1 e2
        | A_mcl_failure e1, A_mcl_failure e2 ->
          compare_expr e1 e2
        | _ -> next a1 a2
      );
    print = (fun next fmt -> function
        | A_mcl_shift_overflow expr ->
          Format.fprintf fmt "%a overflow" (Debug.bold pp_expr) expr
        | A_mcl_always_fail ->
          Format.fprintf fmt "This contract always fail"
        | A_mcl_mutez_overflow expr ->
          Format.fprintf fmt "%a overflow" (Debug.bold pp_expr) expr
        | A_mcl_failure expr ->
          Format.fprintf fmt "failwith \"%a\"" (Debug.bold pp_expr) expr
        | a -> next fmt a
      );
    join = (fun next a1 a2 -> next a1 a2)
  }

let raise_mcl_shift_overflow_alarm ?(bottom=true) expr range man flow =
  let cs = Flow.get_callstack flow in
  let alarm = mk_alarm (A_mcl_shift_overflow expr) cs range in
  Flow.raise_alarm alarm ~bottom man.lattice flow

let raise_mcl_always_fail ?(force=false) ?(bottom=true) range man flow =
  let cs = Flow.get_callstack flow in
  let alarm = mk_alarm (A_mcl_always_fail) cs range in
  Flow.raise_alarm alarm ~bottom ~force man.lattice flow

let raise_mcl_mutez_overflow ?(force=false) ?(bottom=true) expr range man flow =
  let cs = Flow.get_callstack flow in
  let alarm = mk_alarm (A_mcl_mutez_overflow expr) cs range in
  Flow.raise_alarm alarm ~bottom ~force man.lattice flow

let raise_mcl_failure ?(force=false) ?(bottom=false) expr range man flow =
  let cs = Flow.get_callstack flow in
  let alarm = mk_alarm (A_mcl_failure expr) cs range in
  Flow.raise_alarm alarm ~bottom ~force man.lattice flow
