open Mopsa_michelson_parser
open Ast
open Mopsa
open Universal.Ast

open M_ast

(* let mk_int_interval a b ?(typ=T_mcs_int) range = *)
(*   mk_constant ~etyp:typ (C_int_interval (Z.of_string a, Z.of_string b)) range *)

(* let translation_error loc msg = *)
(*   let loc = Format.asprintf "%a" Location.pp_range loc in *)
(*   Mopsa.panic ~loc msg *)
