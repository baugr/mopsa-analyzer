open Mopsa_michelson_parser
open M_ast
open Pre_ast

let debug args = Mopsa_utils.Debug.debug ~channel:"mast_to_preast" args

let rec translate_ty = function
  | M_ast.T_unit -> Al_ast.T_unit
  | T_bool -> T_bool
  | T_int -> T_int
  | T_nat -> T_nat
  | T_mutez -> T_mutez
  | T_string -> T_string
  | T_bytes -> T_bytes
  (* Domain specific *)
  | T_address -> T_address
  | T_chain_id -> T_chain_id
  | T_key -> T_key
  | T_key_hash -> T_key_hash
  | T_operation -> T_operation
  | T_signature -> T_signature
  | T_timestamp -> T_timestamp
  (* Containers *)
  | T_pair(t1, t2) -> T_pair(translate_ty t1, translate_ty t2)
  | T_union(t1,fl,t2,fr) -> T_union(translate_ty t1,fl,translate_ty t2,fr)
  | T_option typ -> T_option (translate_ty typ)
  | T_list typ -> T_list (translate_ty typ)
  | T_set typ -> T_set(translate_ty typ)
  | T_map (t1, t2) -> T_map (translate_ty t1, translate_ty t2)
  | T_big_map (t1, t2) -> T_big_map (translate_ty t1, translate_ty t2)
  (* Others *)
  | T_lambda (t1, t2) -> T_lambda (translate_ty t1, translate_ty t2)
  | T_contract typ -> T_contract (translate_ty typ)

let translate_annot v =
  Option.map (fun (M_ast.Var_annot s) -> Pre_ast.Var_annot s) v

let translate_stack_ty stack =
  List.map (fun (annot, ty) ->
      translate_annot annot, translate_ty ty
    ) stack

let rec translate_constant constant =
  let open M_ast in
  let rec aux { ckind; ctyp } =
    let ckind = match ckind with
      | C_unit -> Pre_ast.Unit
      | C_bool b -> Bool b
      | C_mutez m -> Mutez m
      | C_nat n -> Nat n
      | C_int i -> Int i
      | C_string s -> String s
      | C_bytes b -> Bytes b
      | C_timestamp ts -> Timestamp ts
      | C_signature b -> Signature b
      | C_key k -> Key k
      | C_key_hash kh -> Key_hash kh
      | C_address (s, e) -> Address (s, e)
      (* Containers *)
      | C_option o -> Option (Option.map aux o)
      | C_pair (c1, c2) -> Pair (aux c1, aux c2)
      | C_union (L c) -> Union(L (aux c))
      | C_union (R c) -> Union(R (aux c))
      | C_list l -> List (List.map aux l)
      | C_set l -> Set (List.map aux l)
      | C_map m ->
        Map (List.map (fun (k, v) -> aux k, aux v) m)
      | C_lambda stm -> Lambda (translate stm)
    in
    Al_ast.{ ckind; ctyp = translate_ty ctyp }
  in
  aux constant

and translate (statement : M_ast.statement): Pre_ast.statement =
  let srange = statement.srange in
  let skind =
    match statement.instr with
    | Drop ty -> S_drop (translate_ty ty)
    | Dup ty -> S_dup (translate_ty ty)
    | Swap (t1, t2) -> S_swap (translate_ty t1, translate_ty t2)
    | Const const -> S_const (translate_constant const)
    | Cons_pair(t1, t2) -> S_cons_pair (translate_ty t1, translate_ty t2)
    | Car ty -> S_car (translate_ty ty)
    | Cdr ty -> S_cdr (translate_ty ty)
    | Unpair ty -> S_unpair (translate_ty ty)
    | Cons_some ty -> S_cons_some (translate_ty ty)
    | Cons_none ty -> S_cons_none (translate_ty ty)
    | If_none (ty, tstm, fstm) ->
      let tstm = translate tstm in
      let fstm = translate fstm in
      S_if_none(translate_ty ty, tstm, fstm)
    | Cons_left ty -> S_cons_left (translate_ty ty)
    | Cons_right ty -> S_cons_right (translate_ty ty)
    | If_left (ty, tstm, fstm) ->
      let tstm = translate tstm in
      let fstm = translate fstm in
      S_if_left(translate_ty ty, tstm, fstm)
    | Cons_list ty ->
      S_cons_list (translate_ty ty)
    | Nil ty ->
      S_nil (translate_ty ty)
    | If_cons(ty, tstm, fstm) ->
      let tstm = translate tstm in
      let fstm = translate fstm in
      S_if_cons(translate_ty ty, tstm, fstm)
    | List_map (stmt) ->
      let stmt = translate stmt in
      S_list_map stmt
    | List_iter stmt ->
      let stmt = translate stmt in
      S_list_iter stmt
    | List_size ty ->
      S_list_size (translate_ty ty)
    | Empty_set ty ->
      S_empty_set (translate_ty ty)
    | Set_iter stmt ->
      let stmt = translate stmt in
      S_set_iter stmt
    | Set_mem ity ->
      S_set_mem (translate_ty ity)
    | Set_update item ->
      S_set_update (translate_ty item)
    | Set_size ->
      S_set_size
    | Empty_map (kty, vty) ->
      S_empty_map (translate_ty kty, translate_ty vty)
    | Map_map stmt ->
      let stmt = translate stmt in
      S_map_map stmt
    | Map_iter stmt ->
      let stmt = translate stmt in
      S_map_iter stmt
    | Map_mem (kty, vty) ->
      S_map_mem(translate_ty kty, translate_ty vty)
    | Map_get (kty, vty) ->
      S_map_get(translate_ty kty, translate_ty vty)
    | Map_update (kty, vty) ->
      S_map_update(translate_ty kty, translate_ty vty)
    | Map_size (kty, vty) ->
      S_map_size(translate_ty kty, translate_ty vty)
    | Empty_big_map -> S_empty_big_map
    | Big_map_mem -> S_big_map_mem
    | Big_map_get -> S_big_map_get
    | Big_map_update -> S_big_map_update
    | Concat_string -> S_concat_string
    | Concat_string_pair -> S_concat_string_pair
    | Slice_string -> S_slice_string
    | String_size -> S_string_size
    | Concat_bytes -> S_concat_bytes
    | Concat_bytes_pair -> S_concat_bytes_pair
    | Slice_bytes -> S_slice_bytes
    | Bytes_size -> S_bytes_size
    | Add_seconds_to_timestamp -> S_add_seconds_to_timestamp
    | Add_timestamp_to_seconds -> S_add_timestamp_to_seconds
    | Sub_timestamp_seconds -> S_sub_timestamp_seconds
    | Diff_timestamps -> S_diff_timestamps
    | Add_tez -> S_add_tez
    | Sub_tez -> S_sub_tez
    | Mul_teznat -> S_mul_teznat
    | Mul_nattez -> S_mul_nattez
    | Ediv_teznat -> S_ediv_teznat
    | Ediv_tez -> S_ediv_tez
    | Or -> S_or
    | And -> S_and
    | Xor -> S_xor
    | Not -> S_not
    | Is_nat -> S_is_nat
    | Neg_nat -> S_neg_nat
    | Neg_int -> S_neg_int
    | Abs_int -> S_abs_int
    | Int_nat -> S_int_nat
    | Add_intint -> S_add_intint
    | Add_intnat -> S_add_intnat
    | Add_natint -> S_add_natint
    | Add_natnat -> S_add_natnat
    | Sub_int -> S_sub_int
    | Mul_intint -> S_mul_intint
    | Mul_intnat -> S_mul_intnat
    | Mul_natint -> S_mul_natint
    | Mul_natnat -> S_mul_natnat
    | Ediv_intint -> S_ediv_intint
    | Ediv_intnat -> S_ediv_intnat
    | Ediv_natint -> S_ediv_natint
    | Ediv_natnat -> S_ediv_natnat
    | Lsl_nat -> S_lsl_nat
    | Lsr_nat -> S_lsr_nat
    | Or_nat -> S_or_nat
    | And_nat -> S_and_nat
    | And_int_nat -> S_and_int_nat
    | Xor_nat -> S_xor_nat
    | Not_nat -> S_not_nat
    | Not_int -> S_not_int
    | Seq (stm1, stm2) ->
      let stm1 = translate stm1 in
      let stm2 = translate stm2 in
      S_seq (stm1, stm2)
    | If (tstm, fstm) ->
      let tstm = translate tstm in
      let fstm = translate fstm in
      S_if (tstm, fstm)
    | Loop stmt ->
      let stmt = translate stmt in
      S_loop stmt
    | Loop_left stmt ->
      let stmt = translate stmt in
      S_loop_left stmt
    | Dip stmt ->
      let stmt = translate stmt in
      S_dip stmt
    | Exec -> S_exec
    | Apply -> S_apply
    | Lambda stmt ->
      let stmt = translate stmt in
      S_lambda stmt
    | Failwith ty -> S_failwith (translate_ty ty)
    | Nop -> S_nop
    | Compare ty -> S_compare (translate_ty ty)
    | Eq -> S_eq
    | Neq -> S_neq
    | Lt -> S_lt
    | Gt -> S_gt
    | Le -> S_le
    | Ge -> S_ge
    | Address -> S_address
    | Contract (ty, entry) -> S_contract (translate_ty ty, entry)
    | Transfer_tokens -> S_transfer_tokens
    | Create_account -> S_create_account
    | Implicit_account -> S_implicit_account
    | Create_contract stmt ->
      let stmt = translate stmt in
      S_create_contract stmt
    | Create_contract_2 { parameter_ty; storage_ty; code } ->
      let parameter_ty = translate_ty parameter_ty in
      let storage_ty = translate_ty storage_ty in
      let code = translate code in
      S_create_contract_2 { parameter_ty; storage_ty; code }
    | Set_delegate -> S_set_delegate
    | Now -> S_now
    | Balance -> S_balance
    | Check_signature -> S_check_signature
    | Hash_key -> S_hash_key
    | Pack -> S_pack
    | Unpack -> S_unpack
    | Blake2b -> S_blake2b
    | Sha256 -> S_sha256
    | Sha512 -> S_sha512
    | Sha3 -> S_sha3
    | Keccak -> S_keccak
    | Steps_to_quota -> S_steps_to_quota
    | Source -> S_source
    | Sender -> S_sender
    | Self entry -> S_self entry
    | Amount -> S_amount
    | Dig n -> S_dig n
    | Dug n -> S_dug n
    | Dipn (n, stmt) ->
      let stmt = translate stmt in
      S_dipn (n, stmt)
    | Dropn n -> S_dropn n
    | ChainId -> S_chain_id
    | _ -> assert false
  in
  let bef = translate_stack_ty statement.bef in
  let aft = translate_stack_ty statement.aft in
  let stmt = { skind; srange; bef; aft } in
  debug "translating to %a (bef = %a) (aft=%a)"
    pp_statement stmt
    pp_stack_ty bef
    pp_stack_ty aft
  ;
  stmt

let translate_stack stack =
  List.map translate_constant stack

