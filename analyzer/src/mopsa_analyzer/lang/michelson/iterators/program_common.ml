open Mopsa
open Ast
open Unstacked_ast

let debug args = Debug.debug ~channel:"intercontract" args

let default_entrypoint entry =
  match entry with
  | None -> "default"
  | Some e -> e

let address_of_program_address address =
  match address with
  | Some address -> address
  | None -> panic_at (Location.mk_orig_range
                        (Location.mk_pos "" 0 0)
                        (Location.mk_pos "" 0 0))
              "Specifying address is mandatory at this time"

let parameter_from_entry_parameter parameter_ty entry (parameter:Mopsa.expr option) range =
  let rec aux etyp =
    match etyp with
    | T_mcs_union (lty, Some lent, _, _) when lent = entry ->
      let parameter = Option.value ~default:(mk_top lty range) parameter in
      Some (Unstacked_ast.mk_mcs_cons_left ~etyp parameter range)
    | T_mcs_union (_, _, rty, Some rent) when rent = entry ->
      let parameter = Option.value ~default:(mk_top rty range) parameter in
      Some (Unstacked_ast.mk_mcs_cons_right ~etyp parameter range)
    | T_mcs_union (lty, _, rty, _) ->
      (match aux lty with
       | Some l ->
         Some (Unstacked_ast.mk_mcs_cons_left ~etyp l range)
       | None ->
         (match aux rty with
          | Some r ->
            Some (Unstacked_ast.mk_mcs_cons_right ~etyp r range)
          | None -> None
         )
      )
    | _ -> None
  in
  match aux parameter_ty with
  | None ->
    if String.compare entry "default" = 0 then
      (Option.value ~default:(mk_top parameter_ty range) parameter)
    else
      Exceptions.panic "Cannot find entry point : %s" entry
  | Some l ->
    debug "parameter is now : %a" pp_expr l;
    l

let get_entrypoints parameter_ty =
  let rec loop acc ty =
    match ty with
    | T_mcs_union (lty, l, rty, r) ->
      (match l with
       | Some entry -> loop (entry::acc) lty
       | None -> loop acc lty
      ) @
      (match r with
       | Some entry -> loop (entry::acc) rty
       | None -> loop acc rty
      )
    | _ -> acc
  in
  loop [] parameter_ty

let store_setup_contract_variables _i address range man flow =
  debug ">>store_setup_contract_variables";
  let lbound, rbound = Z.(zero, (one lsl 63) - one) in
  let self = mk_var (mk_var_named T_mcs_address "self") range in
  let sender = mk_var (mk_var_named T_mcs_address "sender") range in
  let source = mk_var (mk_var_named T_mcs_address "source") range in
  let balance = mk_var (mk_var_named T_mcs_mutez "balance") range in
  let amount = mk_var (mk_var_named T_mcs_mutez "amount") range in
  let self_value =
    (match address with
     | None  -> mk_top T_mcs_address range
     | Some address ->
       mk_constant ~etyp:T_mcs_address (C_mcs_address (Address_constant (address, "default"))) range
    )
  in
  let sender_value =
    mk_constant ~etyp:T_mcs_address (C_mcs_address (Address_caller None)) range
  in
  let source_value =
    mk_constant ~etyp:T_mcs_address (C_mcs_address (Address_caller None)) range
  in
  man.exec (mk_add self range) flow >>%
  man.exec (mk_add sender range) >>%
  man.exec (mk_add source range) >>%
  man.exec (mk_add balance range) >>%
  man.exec (mk_add amount range) >>% fun flow ->
  (match address with
   | None  ->
     man.exec (mk_assign self (mk_top T_mcs_address range) range) flow
   | Some address ->
     let value =
       mk_constant ~etyp:T_mcs_address (C_mcs_contract (address, "default")) range
     in
     man.exec (mk_assign self value range) flow
  ) >>%
  man.exec (mk_assign balance (Universal.Ast.mk_z_interval ~typ:T_mcs_mutez lbound rbound range) range) >>%
  man.exec (mk_assign amount (Universal.Ast.mk_z_interval ~typ:T_mcs_mutez lbound rbound range) range) >>% fun flow ->
  man.exec (mk_assign self self_value range) flow >>%
  man.exec (mk_assign sender sender_value range) >>%
  man.exec (mk_assign source source_value range)

let store_teardown_contract_variables range man flow =
  debug ">>store_teardown_contract_variables";
  let self = mk_var (mk_var_named T_mcs_address "self") range in
  let sender = mk_var (mk_var_named T_mcs_address "sender") range in
  let source = mk_var (mk_var_named T_mcs_address "source") range in
  let balance = mk_var (mk_var_named T_mcs_mutez "balance") range in
  let amount = mk_var (mk_var_named T_mcs_mutez "amount") range in
  man.exec (mk_remove self range) flow >>%
  man.exec (mk_remove sender range) >>%
  man.exec (mk_remove source range) >>%
  man.exec (mk_remove balance range) >>%
  man.exec (mk_remove amount range)

let store_setup_stack contract address entrypoint parameter range man flow =
  debug ">>store_setup_stack@\n%a" (format (Flow.print man.lattice.print)) flow;
  let parameter = parameter_from_entry_parameter contract.parameter_ty entrypoint parameter range in
  let vparameter = mk_var (mk_var_named contract.parameter_ty "parameter") range in
  let vstorage = mk_var (mk_var_named ~address contract.storage_ty "storage") range in
  let initial = Unstacked_ast.mk_pair parameter vstorage range in
  let v0 = mk_var (mk_stackpos (T_mcs_pair(contract.parameter_ty, contract.storage_ty)) 0) range in
  man.exec (mk_add vparameter range) flow >>%
  man.exec (mk_add v0 range) >>%
  man.exec (mk_assign vparameter parameter range) >>%
  man.exec (mk_assign v0 initial range)

let store_teardown_stack contract address range man flow =
  let v0 = mk_var (mk_stackpos
                     (T_mcs_pair(T_mcs_list T_mcs_operation,
                                 contract.storage_ty)
                     )
                     0) range
  in
  let vstorage = mk_var (mk_var_named ~address contract.storage_ty "storage") range in
  let voperations = mk_var (mk_var_named ~address (T_mcs_list T_mcs_operation) "operations") range in
  man.exec (mk_assign voperations (mk_car v0 range) range) flow >>%
  man.exec (mk_assign vstorage (mk_cdr v0 range) range) >>%
  man.exec (mk_remove v0 range)

(* FIXME: address is mandatory at this time *)
(* FIXME: entry is mandatory at this time, build the traversal and join of all allowed
 * entrypoints *)
let exec_contract (contract:contract) range man flow =
  if Flow.is_top man.lattice flow then
    let () = warn "top flow, cannot continue analysis" in
    Post.return flow
  else
    man.exec contract.statement flow >>% fun flow ->
    let tokens = Flow.get_token_map flow in
    (if TokenMap.mem T_cur tokens then
       flow
     else
       Alarms.raise_mcl_always_fail ~force:true range man flow
    ) |>
    Post.return

let lfp func range man flow =
  debug "%a: ENTERING LFP" pp_range range;
  let rec aux init_flow flow =
    debug "lfp: %a" (format (Flow.print man.lattice.print)) flow;
    func flow >>% fun flow' ->
    let flow' = Flow.join man.lattice init_flow flow' in
    let cur = Flow.get T_cur man.lattice flow in
    let cur' = Flow.get T_cur man.lattice flow' in
    debug "lfp: old = %a" (format (Flow.print man.lattice.print)) flow;
    (* warn "lfp: new = %a" (format (Flow.print man.lattice.print)) flow'; *)
    let is_sub = man.lattice.subset (Flow.get_ctx flow') cur' cur in
    (* warn "is_sub = %b" is_sub; *)
    if is_sub then
      (
        debug "%a: LEAVING WITH FLOW" pp_range range;
        Post.return flow'
      )
    else
      (
        debug "%a: LEAVING WITH WIDENING" pp_range range;
        let flow = Flow.widen man.lattice flow flow' in
        aux init_flow flow
      )
  in
  let res = aux flow flow in
  debug "%a: LEAVING LFP" pp_range range;
  res

let lfpi func range man flow =
  let rec loop i init_flow flow =
    debug "%a: lfp iteration %d" pp_range range i;
    debug "%a" (format (Flow.print man.lattice.print)) flow;
    func i flow >>% fun flow' ->
    let flow' = Flow.join man.lattice init_flow flow' in
    let cur = Flow.get T_cur man.lattice flow in
    let cur' = Flow.get T_cur man.lattice flow' in
    (* debug "lfp: old = %a" (format (Flow.print man.lattice.print)) flow; *)
    (* debug "lfp: new = %a" (format (Flow.print man.lattice.print)) flow'; *)
    let is_sub = man.lattice.subset (Flow.get_ctx flow') cur' cur in
    (* debug "is_sub = %b" is_sub; *)
    if is_sub then
      (
        debug "%a: " pp_range range;
        Post.return flow'
      )
    else
      (
        (* debug "%a: LEAVING WITH WIDENING" pp_range range; *)
        let flow = Flow.widen man.lattice flow flow' in
        loop (i+1) init_flow flow
      )
  in
  debug "%a: entering lfp iteration" pp_range range;
  debug "%a" (format (Flow.print man.lattice.print)) flow;
  let res = loop 0 flow flow in
  debug "%a: leaving lfp" pp_range range;
  debug "%a" (format (Flow.print man.lattice.print)) flow;
  res

let lfpN func max man flow =
  let cur = Flow.get T_cur man.lattice flow in
  let rec aux n flow =
    func flow >>% fun flow' ->
    let cur' = Flow.get T_cur man.lattice flow' in
    let is_sub = man.lattice.subset (Flow.get_ctx flow) cur' cur in
    if is_sub then
      Post.return flow'
    else
      (if n < max then
         let flow = Flow.join man.lattice flow flow' in
         aux (n + 1) flow
       else
         let flow = Flow.widen man.lattice flow flow' in
         aux (n + 1) flow
      )
  in
  aux 0 flow

let exec_contract_simple contract range man flow =
  debug ">>exec_contract_simple@\n%a" (format (Flow.print man.lattice.print)) flow;
  let stack_ty = T_mcs_pair(contract.parameter_ty, contract.storage_ty) in
  let initial = mk_top stack_ty range in
  let v0 = mk_var (mk_stackpos stack_ty 0) range in
  man.exec (mk_add v0 range) flow >>%
  man.exec (mk_assign v0 initial range) >>%
  exec_contract contract range man


let exec_contract1 contract address entrypoint parameter range man flow =
  let entrypoint = default_entrypoint entrypoint in

  (* Adds local variables: self, sender, source *)
  store_setup_contract_variables 0 (Some address) range man flow >>%

  (* prepare initial stack with parameter and storage *)
  store_setup_stack contract address entrypoint parameter range man >>%

  (* Then: executes the contract *)
  exec_contract contract range man >>% fun flow ->

  (* After execution, create a global variable to store operations *)
  let voperations = mk_var (mk_var_named ~address (T_mcs_list T_mcs_operation) "operations") range in
  man.exec (mk_add voperations range) flow >>%

  (* Unwraps the stack to keep only the store *)
  store_teardown_stack contract address range man >>% fun flow ->

  let vparameter = mk_var (mk_var_named contract.parameter_ty "parameter") range in
  man.exec (mk_remove vparameter range) flow >>%

  (* Remove local variables *)
  store_teardown_contract_variables range man

(* FIXME: ugly *)
let has_variables = ref false
let exec_add_vars_on_first_iteration address range man flow =
  if not !has_variables then
    (
      has_variables := true;
      let voperations = mk_var (mk_var_named ~address (T_mcs_list T_mcs_operation) "operations") range in
      man.exec (mk_add voperations range) flow
    )
  else
    Post.return flow

(* FIXME: support no entrypoint *)
let exec_contractN contract address entrypoint parameter range man flow =
  let entrypoints = get_entrypoints contract.parameter_ty in
  (* Executes in a fixpoint iteration *)
  lfpi (fun i lfp_flow ->
      let init_flow = lfp_flow in
      let flow =
        List.fold_left (fun joined_flow entrypoint ->
            let flow =
              (
                debug "ENTERING FOLD ON ENTRYPOINT %s" entrypoint;
                debug "%a" (format (Flow.print man.lattice.print)) init_flow;
                (* Adds local variables: self, sender, source *)
                store_setup_contract_variables i (Some address) range man init_flow >>% fun flow ->

                (* Prepare stack from the store *)
                store_setup_stack contract address entrypoint parameter range man flow >>% fun flow ->

                (* FIXME: change default to multiple entrypoints *)
                exec_contract contract range man flow >>% fun flow ->

                exec_add_vars_on_first_iteration address range man flow >>% fun flow ->

                (* Unwraps the stack to keep only the store *)
                store_teardown_stack contract address range man flow >>% fun flow ->

                let vparameter = mk_var (mk_var_named contract.parameter_ty "parameter") range in
                man.exec (mk_remove vparameter range) flow >>% fun flow ->

                (* Remove local variables *)
                store_teardown_contract_variables range man flow >>% fun flow ->
                Post.return flow
              ) |> post_to_flow man
            in
            let joined_flow = Flow.join man.lattice joined_flow flow in
            debug "EXITING FOLD ON ENTRYPOINT %s" entrypoint;
            debug "%a" (format (Flow.print man.lattice.print)) joined_flow;
            joined_flow
          ) (Flow.bottom_from flow) entrypoints
      in
      Post.return flow
    ) range man flow

let exec_contract_ops contract address entrypoint parameter range man flow =
  let entrypoint = default_entrypoint entrypoint in
  lfpi (fun i flow ->
      (* prepare the stack from parameter and storage *)
      store_setup_stack contract address entrypoint parameter range man flow >>%

      (* setup local variables *)
      store_setup_contract_variables i (Some address) range man >>%

      (* execute the contract *)
      exec_contract contract range man >>% fun flow ->

      (if i = 0 then
         let voperations = mk_var (mk_var_named ~address (T_mcs_list T_mcs_operation) "operations") range in
         man.exec (mk_add voperations range) flow
       else
         Post.return flow
      ) >>%

      (* Unwraps the stack to keep only the store *)
      store_teardown_stack contract address range man >>%

      (* Remove local variables *)
      store_teardown_contract_variables range man
    ) range man flow >>% fun flow ->

  (* Iterate over all contracts from the operation list
   * to execute the transfers *)
  let operations = mk_var (mk_var_named (T_mcs_list(T_mcs_operation)) ~address "operations") range in
  man.exec (mk_stmt (S_mcs_inter_operation_list_iter { operations }) range) flow >>% fun flow ->
  if Flow.is_top man.lattice flow then
    store_teardown_stack contract address range man flow
  else
    Post.return flow
