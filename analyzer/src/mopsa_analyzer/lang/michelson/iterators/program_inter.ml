open Mopsa
open Ast

module Domain = struct
  include GenStatelessDomainId (struct
    let name = "michelson.program-inter"
  end)

  open Program_common

  let init _ _ flow = flow

  let alarms = []

  let dependencies = []

  let print_expr _man _flow _printer _expr = ()
  let checks = [ Alarms.CHK_MCL_ALWAYS_FAIL ]

  let pp_constant_option fmt opt =
    Format.fprintf
      fmt
      "%a"
      (Format.pp_print_option
         ~none:(fun fmt () -> Format.pp_print_string fmt "None")
         pp_constant)
      opt

  let exec stmt man flow =
    match skind stmt with
    | S_program ({prog_kind =
                    Ast.P_michelson {
                      contract;
                      context;
                      externs;
                    };
                  _ }, _) ->
      debug "Launching evaluation with parameter=(%a) storage=(%a)"
        pp_constant_option context.parameter
        pp_constant_option context.storage
      ;
      let range_start = Location.get_range_start (srange stmt) in
      let range = Location.mk_orig_range range_start range_start in
      let address = address_of_program_address context.address in
      let parameter =
        Option.map
          (fun o -> mk_constant ~etyp:contract.parameter_ty o range)
          context.parameter
      in
      man.exec (mk_stmt (S_mcs_store_add_contracts externs) range) flow >>%
      exec_contract_ops contract address context.entrypoint parameter range man |>
      OptionExt.return

    | S_mcs_inter_exec { contract = None; _ } ->
      warn "possible call to an unknown contract. Results cannot be correct";
      debug "inter_exec>> @\n%a" (format (Flow.print man.lattice.print)) flow;
      Post.return (Flow.top (Flow.get_ctx flow)) |>
      OptionExt.return

    | S_mcs_inter_exec { contract = Some contract;
                         address = Some address;
                         entry;
                         parameter; _} ->
      let range_start = Location.get_range_start (srange contract.statement) in
      let range = Location.mk_orig_range range_start range_start in
      exec_contract_ops contract address entry (Some parameter) range man flow |>
      OptionExt.return
    | _ ->
      None

  let eval _ _ _ = None
  let ask _ _ _ = None

end

let () =
  Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
