open Mopsa
open Universal.Ast
open Unstacked_ast
open Ast

let pp_expr fmt expr =
  match ekind expr with
  | E_var (v, _) -> Format.fprintf fmt "(%a{vtyp:%a} : %a)" pp_var v pp_typ v.vtyp pp_typ expr.etyp
  |  _ -> Format.fprintf fmt "%a : %a" pp_expr expr pp_typ expr.etyp

module Domain = struct
  include GenStatelessDomainId (struct
      let name = "michelson.stack_statements"
    end)

  let dependencies = []

  let init _ _ flow = flow

  type token +=
    | T_mcs_failwith of Location.range * var

  let () =
    register_token
      {
        print = (fun next fmt -> function
            | T_mcs_failwith (range, var) ->
              let shortloc fmt range =
                let open Location in
                match range with
                | R_orig(p1, _) ->
                  Format.fprintf fmt "%d.%d" p1.pos_line p1.pos_column
                | _ -> pp_range fmt range
              in
              Format.fprintf fmt "FAILWITH-%a(%a)" shortloc range pp_var var
            | tok -> next fmt tok
          );
        compare = (fun next a b ->
            match a, b with
            | T_mcs_failwith (r1, _), T_mcs_failwith(r2, _) ->
              compare_range r1 r2
            | _ -> next a b
          )
      }

  let print_expr _man _flow _printer _expr = ()
  let checks = [Alarms.CHK_MCL_FAILURE]

  let check_type e1 e2 =
    if compare_typ (etyp e1) (etyp e2) = 0 then ()
    else panic "Expected same types for@\n\t%a and\n\t%a" pp_expr e1 pp_expr e2

  let is_type_michelson_comparable t =
    match t with
    | T_mcs_pair (_, _) -> true
    | T_mcs_option _ -> true
    | _ -> false

  let exec_unop etyp op ty_v v man flow loc =
    let v0 = mk_stackpos ty_v v in
    let vr = mk_stackpos etyp v in
    let e0 = mk_var v0 loc in
    let er = mk_var vr loc in
    (if compare_typ etyp ty_v = 0 then
       Post.return flow
     else
       man.exec (mk_add_var vr loc) flow
    ) >>$? fun () flow ->
    let expr = Unstacked_ast.mk_mcs_unop ~etyp op e0 loc in
    let stmt = mk_assign er expr loc in
    man.exec stmt flow >>$? fun () flow ->
    (if compare_typ etyp ty_v = 0 then
       Post.return flow
     else
       man.exec (mk_remove_var v0 loc) flow
    ) |> OptionExt.return

  let exec_binop etyp ty_left left op ty_right right man flow range =
    let e0 = mk_var (mk_stackpos ty_left left) range in
    let e1 = mk_var (mk_stackpos ty_right right) range in
    let er = mk_var (mk_stackpos etyp right) range in
    debug "exec_binop: %a <- %a op %a" pp_expr er pp_expr e0 pp_expr e1;
    (if compare_typ etyp ty_right = 0 then
       (
         debug "exec_binop: same type. doing nothing";
         Post.return flow
       )
     else
       (
         debug "exec_binop: different type. adding %a" pp_expr er;
         man.exec (mk_add er range) flow
       )
    ) >>$? fun () flow ->
    let expr = Unstacked_ast.mk_mcs_binop ~etyp e0 op e1 range in
    let stmt = mk_assign er expr range in
    man.exec stmt flow >>$? fun () flow ->
    (if compare_typ etyp ty_right = 0 then
       (
         debug "exec_binop: same type. doing nothing";
         Post.return flow
       )
     else
       (
         debug "exec_binop: different type. removing %a" pp_expr er;
         man.exec (mk_remove e1 range) flow
       )
    ) >>$? fun () flow ->
    man.exec (mk_remove e0 range) flow |>
    OptionExt.return

  let exec_binop0 op ty_left left man flow range =
    let e0 = mk_constant ~etyp:T_mcs_int (C_int (Z.of_int 0)) range in
    let v1 = mk_stackpos ty_left left in
    let vr = mk_stackpos T_mcs_bool left in
    let e1 = mk_var v1 range in
    let er = mk_var vr range in
    let expr = Unstacked_ast.mk_mcs_binop ~etyp:T_mcs_bool e1 op e0 range in
    man.exec (mk_add er range) flow >>%
    man.exec (mk_assign er expr range) >>%
    man.exec (mk_remove e1 range) |>
    OptionExt.return

  let exec (stmt : stmt) man (flow : 'a flow) : 'a post option =
    let range = srange stmt in
    if not (Flow.mem T_cur flow) then
      Post.return flow |> OptionExt.return
    else
    match skind stmt with
    | S_michelson_instr {instr; sp; loc; bef; aft; _} -> (
        match instr with
        | S_mcs_drop ty ->
          let v1 = mk_stackpos ty (sp - 1) in
          man.exec (mk_remove_var v1 loc) flow |>
          OptionExt.return
        | S_mcs_dropn n ->
          let rec loop n flow =
            if n = 0 then
              Post.return flow
            else
              let vn = mk_stackpos (stack_get_type_spi bef (sp-n)) (sp-n) in
              man.exec (mk_remove_var vn range) flow >>$ fun () flow ->
              loop (n-1) flow
          in
          loop n flow |> OptionExt.return
        | S_mcs_dup ty ->
          let v0 = mk_var (mk_stackpos ty (sp - 1)) range in
          let vr = mk_var (mk_stackpos ty sp) range in
          man.exec (mk_add vr range) flow >>$? fun () flow ->
          man.exec (mk_assign vr v0 range) flow |>
          OptionExt.return
        | S_mcs_swap _ ->
          let ty0 = stack_get_type_spi bef (sp-1) in
          let ty1 = stack_get_type_spi bef (sp-2) in
          let tmp0 = mk_var (mk_range_attr_var range "swap1" ty0) range in
          let tmp1 = mk_var (mk_range_attr_var range "swap2" ty1) range in
          let v0 = mk_var (mk_stackpos ty0 (sp-1)) range in
          let v1 = mk_var (mk_stackpos ty1 (sp-2)) range in
          let v0' = mk_var (mk_stackpos ty1 (sp-1)) range in
          let v1' = mk_var (mk_stackpos ty0 (sp-2)) range in
          man.exec (mk_rename v0 tmp0 range) flow >>%
          man.exec (mk_rename v1 tmp1 range) >>%
          man.exec (mk_rename tmp0 v1' range) >>%
          man.exec (mk_rename tmp1 v0' range) |>
          OptionExt.return
        | S_mcs_dig count ->
          let rec loop n flow =
            if n = 0 then Post.return flow
            else
              let xi = sp - n in
              let yi = sp - n - 1 in
              let ty = stack_get_type_spi bef xi in
              let vx = mk_stackpos ty xi in
              let vy = mk_stackpos ty yi in
              man.exec (mk_rename_var vx vy range) flow >>$ fun () flow ->
              loop (n-1) flow
          in
          let v0ty = stack_get_type_spi bef (sp-1-count) in
          let v0 = mk_stackpos v0ty (sp-1-count) in
          let tmp = mk_range_attr_var range "dig" v0ty in
          (* move (sp-1-count) item outside the stack *)
          man.exec (mk_rename_var v0 tmp range) flow >>$? fun () flow ->
          (* shift the stack *)
          loop count flow >>$? fun () flow ->
          (* move old (sp-1-count) item on top of the stack *)
          let vn = mk_stackpos v0ty (sp-1) in
          man.exec (mk_rename_var tmp vn range) flow |>
          OptionExt.return
        | S_mcs_dug count ->
          let shift_stack count flow =
            let rec loop i count flow =
              if i <= count then
                let ty = stack_get_type_spi bef (sp-1-i) in
                let src = mk_var (mk_stackpos ty (sp-1-i)) range in
                let dst = mk_var (mk_stackpos ty (sp-i)) range in
                man.exec (mk_rename src dst range) flow >>%
                loop (i+1) count
              else
                Post.return flow
            in
            loop 1 count flow
          in
          let v0ty = stack_get_type_spi bef (sp - 1) in
          let v0 = mk_var (mk_stackpos v0ty (sp-1)) range in
          let tmp = mk_var (mk_range_attr_var range "dug" v0ty) range in
          (* move top element outside the stack namespace *)
          man.exec (mk_rename v0 tmp range) flow >>$? fun () flow ->
          (* shift elements from sp-count to sp-1 *)
          shift_stack count flow >>$? fun () flow ->
          (* restore the top element into the stack, at sp-1-count *)
          let dst = mk_var (mk_stackpos v0ty (sp-1-count)) range in
          man.exec (mk_rename tmp dst range) flow |>
          OptionExt.return
        | S_mcs_dip stmt->
          let v0 = mk_stackpos (stack_get_type_spi bef (sp-1)) (sp-1) in
          let tmpty = stack_get_type_spi bef (sp-1) in
          let tmp = mk_range_attr_var range "dip" tmpty in
          man.exec (mk_rename_var v0 tmp range) flow >>$? fun () flow ->
          man.exec stmt flow >>$? fun () flow ->
          let sp = List.length aft in
          let vr = mk_stackpos (stack_get_type_spi aft (sp-1)) (sp-1) in
          man.exec (mk_rename_var tmp vr range) flow |> OptionExt.return
        | S_mcs_dipn (count, stmt) ->
          let vars =
            let rec loop acc n =
              if n = 0 then List.rev acc
              else
                let xi = sp - n in
                let ty = stack_get_type_spi bef xi in
                let name = Format.asprintf "dip%d" n in
                let tmp = mk_range_attr_var range name ty in
                loop (tmp::acc) (n-1)
            in
            loop [] count
          in
          let rec loop_pre vars n flow =
            match vars with
            | [] -> Post.return flow
            | tmp :: tl ->
              let xi = sp - n in
              let vx = mk_stackpos (stack_get_type_spi bef xi) xi in
              man.exec (mk_rename_var vx tmp range) flow >>$ fun () flow ->
              loop_pre tl (n-1) flow
          in
          let rec loop_post vars n flow =
            match vars with
            | [] -> Post.return flow
            | tmp :: tl ->
              let sp = List.length aft in
              let vr = mk_stackpos (stack_get_type_spi aft (sp-n)) (sp-n) in
              man.exec (mk_rename_var tmp vr range) flow >>$ fun () flow ->
              loop_post tl (n-1) flow
          in
          loop_pre vars count flow >>$? fun () flow ->
          man.exec stmt flow >>$? fun () flow ->
          loop_post vars count flow |>
          OptionExt.return
        | S_mcs_const (etyp, value) ->
          let () = match etyp with
            | T_unit
            | T_bool
            | T_int
            | T_string ->
              panic_at range "PUSH %a : %a" pp_constant value pp_typ etyp
            | _ -> ()
          in
          let var = mk_stackpos etyp sp in
          man.exec (mk_add_var var loc) flow >>$? fun () flow ->
          let stmt =
            mk_assign (mk_var var loc) (mk_constant ~etyp value loc) loc
          in
          man.exec stmt flow |> OptionExt.return
        (* Arithmetic *)
        | S_mcs_abs_int -> exec_unop T_mcs_int O_abs T_mcs_int (sp - 1) man flow loc
        | S_mcs_neg_int -> exec_unop T_mcs_int O_minus T_mcs_int (sp - 1) man flow loc
        | S_mcs_neg_nat -> exec_unop T_mcs_int O_minus T_mcs_int (sp - 1) man flow loc
        | S_mcs_add_intint ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_plus T_mcs_int (sp - 2) man flow loc
        | S_mcs_add_intnat ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_plus T_mcs_int (sp - 2) man flow loc
        | S_mcs_add_natint ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_plus T_mcs_int (sp - 2) man flow loc
        | S_mcs_add_natnat ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_plus T_mcs_int (sp - 2) man flow loc
        | S_mcs_sub_int ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_minus T_mcs_int (sp - 2) man flow loc
        | S_mcs_mul_intint ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_mult T_mcs_int (sp - 2) man flow loc
        | S_mcs_mul_intnat ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_mult T_mcs_int (sp - 2) man flow loc
        | S_mcs_mul_natint ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_mult T_mcs_int (sp - 2) man flow loc
        | S_mcs_mul_natnat ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_mult T_mcs_int (sp - 2) man flow loc
        | S_mcs_ediv_intint ->
          let rtyp = T_mcs_option (T_mcs_pair (T_mcs_int, T_mcs_int)) in
          exec_binop rtyp T_mcs_int (sp - 1) O_div T_mcs_int (sp - 2) man flow loc
        | S_mcs_ediv_intnat ->
          let rtyp = T_mcs_option (T_mcs_pair (T_mcs_int, T_mcs_int)) in
          exec_binop rtyp T_mcs_int (sp - 1) O_div T_mcs_int (sp - 2) man flow loc
        | S_mcs_ediv_natint ->
          let rtyp = T_mcs_option (T_mcs_pair (T_mcs_int, T_mcs_int)) in
          exec_binop rtyp T_mcs_int (sp - 1) O_div T_mcs_int (sp - 2) man flow loc
        | S_mcs_ediv_natnat ->
          let rtyp = T_mcs_option (T_mcs_pair (T_mcs_int, T_mcs_int)) in
          exec_binop rtyp T_mcs_int (sp - 1) O_div T_mcs_int (sp - 2) man flow loc
        (* Integer comparisons *)
        | S_mcs_eq -> exec_binop0 O_eq T_mcs_int (sp - 1) man flow loc
        | S_mcs_neq -> exec_binop0 O_ne T_mcs_int (sp - 1) man flow loc
        | S_mcs_gt -> exec_binop0 O_gt T_mcs_int (sp - 1) man flow loc
        | S_mcs_ge -> exec_binop0 O_ge T_mcs_int (sp - 1) man flow loc
        | S_mcs_lt -> exec_binop0 O_lt T_mcs_int (sp - 1) man flow loc
        | S_mcs_le -> exec_binop0 O_le T_mcs_int (sp - 1) man flow loc
        (* Polymorphic comparison *)
        | S_mcs_compare ty ->
          let v0 = mk_stackpos ty (sp - 1) in
          let v1 = mk_stackpos ty (sp - 2) in
          let e0 = mk_var v0 loc in
          let e1 = mk_var v1 loc in
          let vr = mk_stackpos T_mcs_int (sp-2) in
          let er = mk_var vr loc in
          (if compare_typ ty T_mcs_int = 0 then
             Post.return flow
           else
             man.exec (mk_add_var vr loc) flow
          ) >>$? fun () flow ->
          let expr = Unstacked_ast.mk_mcs_compare e0 e1 loc in
          man.eval expr flow >>$? fun expr flow ->
          man.exec (mk_assign er expr loc) flow >>$? fun () flow ->
          (if compare_typ ty T_mcs_int = 0 then
             Post.return flow
           else
             man.exec (mk_remove_var v1 loc) flow
          ) >>$? fun () flow ->
          man.exec (mk_remove_var v0 loc) flow |>
          OptionExt.return
        (* Integer casts *)
        | S_mcs_int_nat ->
          (* This is a no-op: for typing only *)
          Post.return flow |> OptionExt.return
        | S_mcs_is_nat ->
          exec_unop (T_mcs_option T_mcs_int) O_isnat T_mcs_int (sp - 1) man flow loc
        (* Bitwise and shifts *)
        | S_mcs_not_int ->
          exec_unop T_mcs_int O_bit_invert T_mcs_int (sp - 1) man flow loc
        | S_mcs_not_nat ->
          exec_unop T_mcs_int O_bit_invert T_mcs_int (sp - 1) man flow loc
        | S_mcs_and_nat ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_bit_and T_mcs_int (sp - 2) man flow loc
        | S_mcs_and_int_nat ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_bit_and T_mcs_int (sp - 2) man flow loc
        | S_mcs_or_nat ->
          exec_binop T_mcs_int T_mcs_int (sp - 1) O_bit_or T_mcs_int (sp - 2) man flow loc
        | S_mcs_lsl_nat ->
          exec_binop
            T_mcs_int
            T_mcs_int
            (sp - 1)
            O_bit_lshift
            T_mcs_int
            (sp - 2)
            man
            flow
            loc
        | S_mcs_lsr_nat ->
          exec_binop
            T_mcs_int
            T_mcs_int
            (sp - 1)
            O_bit_rshift
            T_mcs_int
            (sp - 2)
            man
            flow
            loc
        (* Booleans *)
        | S_mcs_not -> exec_unop T_mcs_bool O_log_not T_mcs_bool (sp - 1) man flow loc
        | S_mcs_and ->
          exec_binop
            T_mcs_bool
            T_mcs_bool
            (sp - 1)
            O_log_and
            T_mcs_bool
            (sp - 2)
            man
            flow
            loc
        | S_mcs_or ->
          exec_binop
            T_mcs_bool
            T_mcs_bool
            (sp - 1)
            O_log_or
            T_mcs_bool
            (sp - 2)
            man
            flow
            loc
        | S_mcs_xor ->
          exec_binop
            T_mcs_bool
            T_mcs_bool
            (sp - 1)
            O_log_xor
            T_mcs_bool
            (sp - 2)
            man
            flow
            loc
        | S_mcs_xor_nat ->
          exec_binop T_mcs_int T_mcs_int (sp-1) O_bit_xor T_mcs_int (sp-2) man flow loc
        (* Mutez *)
        | S_mcs_add_tez ->
          exec_binop
            T_mcs_mutez
            T_mcs_mutez
            (sp - 1)
            O_plus
            T_mcs_mutez
            (sp - 2)
            man
            flow
            loc
        | S_mcs_sub_tez ->
          exec_binop
            T_mcs_mutez
            T_mcs_mutez
            (sp - 1)
            O_minus
            T_mcs_mutez
            (sp - 2)
            man
            flow
            loc
        | S_mcs_mul_nattez ->
          debug "HERE : nattez";
          exec_binop
            T_mcs_mutez
            T_mcs_int
            (sp - 1)
            O_mult
            T_mcs_mutez
            (sp - 2)
            man
            flow
            loc
        | S_mcs_mul_teznat ->
          debug "HERE teznat";
          exec_binop
            T_mcs_mutez
            T_mcs_mutez
            (sp - 1)
            O_mult
            T_mcs_int
            (sp - 2)
            man
            flow
            loc
        | S_mcs_ediv_tez ->
          let rtyp = T_mcs_option (T_mcs_pair (T_mcs_int, T_mcs_mutez)) in
          exec_binop rtyp T_mcs_mutez (sp - 1) O_div T_mcs_mutez (sp - 2) man flow loc
        | S_mcs_ediv_teznat ->
          let rtyp = T_mcs_option (T_mcs_pair (T_mcs_mutez, T_mcs_mutez)) in
          exec_binop rtyp T_mcs_mutez (sp - 1) O_div T_mcs_int (sp - 2) man flow loc
        | S_mcs_amount ->
          let er = mk_var (mk_stackpos T_mcs_mutez sp) range in
          man.exec (mk_add er range) flow >>%
          man.exec (mk_assign er (Unstacked_ast.mk_amount range) range) |>
          OptionExt.return
        | S_mcs_balance ->
          let er = mk_var (mk_stackpos T_mcs_mutez sp) range in
          man.exec (mk_add er range) flow >>%
          man.exec (mk_assign er (Unstacked_ast.mk_balance range) range) |>
          OptionExt.return
        (* Timestamps *)
        | S_mcs_add_seconds_to_timestamp ->
          debug "S_mcs_add_seconds_to_timestamp";
          exec_binop
            T_mcs_timestamp
            T_mcs_int
            (sp - 1)
            O_plus
            T_mcs_timestamp
            (sp - 2)
            man
            flow
            loc
        | S_mcs_add_timestamp_to_seconds ->
          debug "S_mcs_add_timestamp_to_seconds";
          exec_binop
            T_mcs_timestamp
            T_mcs_timestamp
            (sp - 1)
            O_plus
            T_mcs_int
            (sp - 2)
            man
            flow
            loc
        | S_mcs_sub_timestamp_seconds ->
          debug "S_mcs_sub_timestamp_seconds";
          exec_binop
            T_mcs_timestamp
            T_mcs_timestamp
            (sp - 1)
            O_minus
            T_mcs_int
            (sp - 2)
            man
            flow
            loc
        | S_mcs_diff_timestamps ->
          debug "S_mcs_diff_timestamps";
          exec_binop
            T_mcs_int
            T_mcs_timestamp
            (sp - 1)
            O_minus
            T_mcs_timestamp
            (sp - 2)
            man
            flow
            loc
        | S_mcs_now ->
          let vr = mk_var (mk_stackpos T_mcs_timestamp sp) range in
          let expr = Unstacked_ast.mk_now range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr loc) |>
          OptionExt.return
        (* Strings *)
        | S_mcs_string_size ->
          let v0 = mk_var (mk_stackpos T_mcs_string (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_int (sp-1)) range in
          let expr = Unstacked_ast.mk_string_size v0 range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        | S_mcs_concat_string_pair ->
          let v0 = mk_var (mk_stackpos T_mcs_string (sp-1)) range in
          let v1 = mk_var (mk_stackpos T_mcs_string (sp-2)) range in
          let expr = Unstacked_ast.mk_string_concat_pair v0 v1 range in
          man.exec (mk_assign v1 expr range) flow >>$? fun () flow ->
          debug "trying remove var";
          man.exec (mk_remove v0 range) flow |>
          OptionExt.return
        | S_mcs_concat_string ->
          let v0 = mk_var (mk_stackpos (T_mcs_list T_mcs_string) (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_string (sp-1)) range in
          let expr = Unstacked_ast.mk_string_concat v0 range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        | S_mcs_slice_string ->
          let v0 = mk_var (mk_stackpos T_mcs_int (sp-1)) range in
          let v1 = mk_var (mk_stackpos T_mcs_int (sp-2)) range in
          let v2 = mk_var (mk_stackpos T_mcs_string (sp-3)) range in
          let vr = mk_var (mk_stackpos (T_mcs_option T_mcs_string) (sp-3)) range in
          let expr = Unstacked_ast.mk_string_slice v0 v1 v2 range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove v0 range) >>%
          man.exec (mk_remove v1 range) |>
          OptionExt.return
        (* Bytes *)
        | S_mcs_bytes_size ->
          let v0 = mk_var (mk_stackpos T_mcs_bytes (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_int (sp-1)) range in
          let expr = Unstacked_ast.mk_bytes_size v0 range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        | S_mcs_concat_bytes_pair ->
          let v0 = mk_var (mk_stackpos T_mcs_bytes (sp-1)) range in
          let v1 = mk_var (mk_stackpos T_mcs_bytes (sp-2)) range in
          let expr = Unstacked_ast.mk_bytes_concat_pair v0 v1 range in
          man.exec (mk_assign v1 expr range) flow >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        | S_mcs_concat_bytes ->
          let v0 = mk_var (mk_stackpos (T_mcs_list T_mcs_bytes) (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_bytes (sp-1)) range in
          let expr = Unstacked_ast.mk_bytes_concat v0 range in
          man.exec (mk_assign vr expr range) flow >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        | S_mcs_slice_bytes ->
          let v0 = mk_var (mk_stackpos T_mcs_int (sp-1)) range in
          let v1 = mk_var (mk_stackpos T_mcs_int (sp-2)) range in
          let v2 = mk_var (mk_stackpos T_mcs_bytes (sp-3)) range in
          let vr = mk_var (mk_stackpos (T_mcs_option T_mcs_bytes) (sp-3)) range in
          let expr = Unstacked_ast.mk_bytes_slice v0 v1 v2 range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove v0 range) >>%
          man.exec (mk_remove v1 range) |>
          OptionExt.return
        | S_mcs_blake2b ->
          let v = mk_var (mk_stackpos T_mcs_bytes (sp-1)) range in
          let expr = Unstacked_ast.mk_blake2b v range in
          man.exec (mk_assign v expr range) flow |>
          OptionExt.return
        | S_mcs_sha256 ->
          let v = mk_var (mk_stackpos T_mcs_bytes (sp-1)) range in
          let expr = Unstacked_ast.mk_sha256 v range in
          man.exec (mk_assign v expr range) flow |>
          OptionExt.return
        | S_mcs_sha512 ->
          let v = mk_var (mk_stackpos T_mcs_bytes (sp-1)) range in
          let expr = Unstacked_ast.mk_sha512 v range in
          man.exec (mk_assign v expr range) flow |>
          OptionExt.return
        | S_mcs_sha3 ->
          let v = mk_var (mk_stackpos T_mcs_bytes (sp-1)) range in
          let expr = Unstacked_ast.mk_sha3 v range in
          man.exec (mk_assign v expr range) flow |>
          OptionExt.return
        | S_mcs_keccak ->
          let v = mk_var (mk_stackpos T_mcs_bytes (sp-1)) range in
          let expr = Unstacked_ast.mk_keccak v range in
          man.exec (mk_assign v expr range) flow |>
          OptionExt.return
        | S_mcs_pack ->
          let t0 = stack_get_type_spi bef (sp-1) in
          let v0 = mk_var (mk_stackpos t0 (sp-1)) range in
          let tr = T_mcs_bytes in
          let vr = mk_var (mk_stackpos tr (sp-1)) range in
          (if compare_typ tr t0 = 0 then
             Post.return flow
           else
             man.exec (mk_add vr range) flow
          ) >>%? fun flow ->
          let expr = mk_top T_mcs_bytes range in
          man.exec (mk_assign vr expr range) flow >>%? fun flow ->
          (if compare_typ tr t0 = 0 then
             Post.return flow
           else
             man.exec (mk_remove v0 range) flow
          ) |>
          OptionExt.return
        | S_mcs_unpack ->
          let t0 = T_mcs_bytes in
          let v0 = mk_var (mk_stackpos t0 (sp-1)) range in
          let tr = stack_get_type_spi aft (sp-1) in
          let vr = mk_var (mk_stackpos tr (sp-1)) range in
          (if compare_typ tr t0 = 0 then
             Post.return flow
           else
             man.exec (mk_add vr range) flow
          ) >>%? fun flow ->
          let expr = mk_top tr range in
          man.exec (mk_assign vr expr range) flow >>%? fun flow ->
          (if compare_typ tr t0 = 0 then
             Post.return flow
           else
             man.exec (mk_remove v0 range) flow
          ) |>
          OptionExt.return
        (* Lists *)
        | S_mcs_nil ty ->
          let vr = mk_stackpos ty sp in
          let er = mk_var vr loc in
          man.exec (mk_add_var vr loc) flow >>$? fun () flow ->
          let expr = Unstacked_ast.mk_list_nil ty loc in
          man.exec (mk_assign er expr loc) flow |> OptionExt.return
        | S_mcs_cons_list _ ->
          let t_item = stack_get_type_spi bef (sp-1) in
          let t_list = stack_get_type_spi bef (sp-2) in
          let v0 = mk_stackpos t_item (sp - 1) in
          let v1 = mk_stackpos t_list (sp - 2) in
          let vr = mk_stackpos t_list (sp - 2) in
          let item = mk_var v0 range in
          let list = mk_var v1 range in
          let er = mk_var vr range in
          let expr = Unstacked_ast.mk_list_cons ~item ~list range in
          man.exec (mk_assign er expr range) flow >>$? fun () flow ->
          man.exec (mk_remove item range) flow |> OptionExt.return
        | S_mcs_list_size _ ->
          let t_list = stack_get_type_spi bef (sp-1) in
          let v0 = mk_var (mk_stackpos t_list (sp - 1)) range in
          (* XXX: T_mcs_int or T_nat ? *)
          let vr = mk_var (mk_stackpos T_mcs_int (sp - 1)) range in
          let expr = Unstacked_ast.mk_list_size v0 range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        | S_mcs_if_cons (_, tstm, fstm) ->
          let t_list = stack_get_type_spi bef (sp-1) in
          let t_item = match t_list with
            | T_mcs_list t -> t
            | t -> panic "Expecting type list, got : %a" pp_typ t
          in
          let v0 = mk_var (mk_stackpos t_list (sp - 1)) range in
          switch [
            [mk_list_is_nil v0 range],
            (fun flow ->
               man.exec (mk_remove v0 range) flow >>%
               man.exec fstm
            );
            [mk_not (mk_list_is_nil v0 range) range],
            (fun flow ->
               let er_item = mk_var (mk_stackpos t_item sp) range in
               let er_list = mk_var (mk_stackpos t_list (sp - 1)) range in
               man.exec (mk_add er_item range) flow >>% fun flow ->
               man.exec (mk_assign er_item (Unstacked_ast.mk_list_head v0 range) range) flow >>% fun flow ->
               man.exec (mk_assign er_list (Unstacked_ast.mk_list_tail v0 range) range) flow >>% fun flow ->
               man.exec tstm flow
            )
          ] man flow |>
          OptionExt.return
        (* Sets *)
        | S_mcs_empty_set _ ->
          let t_set = stack_get_type_spi aft sp in
          let vr = mk_var (mk_stackpos t_set sp) range in
          man.exec (mk_add vr range) flow >>%? fun flow ->
          let expr = Unstacked_ast.mk_set_empty t_set range in
          man.exec (mk_assign vr expr range) flow |>
          OptionExt.return
        | S_mcs_set_update {item = ty} ->
          let item = mk_var (mk_stackpos ty (sp - 1)) range in
          let flag = mk_var (mk_stackpos T_mcs_bool (sp - 2)) range in
          let set = mk_var (mk_stackpos (T_mcs_set ty) (sp - 3)) range in
          assume
            flag
            ~fthen:(fun flow ->
                let expr = Unstacked_ast.mk_set_add ~item ~set range in
                man.exec (mk_assign set expr range) flow)
            ~felse:(fun flow ->
                let expr = Unstacked_ast.mk_set_remove ~item ~set range in
                man.exec (mk_assign set expr range) flow
              ) man flow >>$? fun () flow ->
          man.exec (mk_remove item range) flow >>$? fun () flow ->
          man.exec (mk_remove flag range) flow |> OptionExt.return
        | S_mcs_set_mem { ity } ->
          let item = mk_var (mk_stackpos ity (sp-1)) range in
          let set = mk_var (mk_stackpos (T_mcs_set ity) (sp-2)) range in
          let er = mk_var (mk_stackpos (T_mcs_bool) (sp-2)) range in
          man.exec (mk_add er range) flow >>$? fun () flow ->
          let expr = Unstacked_ast.mk_set_mem ~set ~item range in
          man.exec (mk_assign er expr range) flow >>$? fun () flow ->
          man.exec (mk_remove item range) flow >>$? fun () flow ->
          man.exec (mk_remove set range) flow |> OptionExt.return
        | S_mcs_set_size ->
          let t_set = stack_get_type_spi bef (sp-1) in
          let set = mk_var (mk_stackpos t_set (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_int (sp-1)) range in
          let expr = Unstacked_ast.mk_set_size ~set range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove set range) |>
          OptionExt.return
        (* Maps *)
        | S_mcs_empty_map { kty; vty } ->
          let etyp = stack_get_type_spi aft sp in
          let er = mk_var (mk_stackpos etyp sp) range in
          man.exec (mk_add er range) flow >>$? fun () flow ->
          let expr = Unstacked_ast.mk_map_empty ~etyp range in
          man.exec (mk_assign er expr range) flow |> OptionExt.return
        | S_mcs_map_update { kty; vty } ->
          let key = mk_var (mk_stackpos kty (sp-1)) range in
          let opt_value = mk_var (mk_stackpos (T_mcs_option vty) (sp-2)) range in
          let map = mk_var (mk_stackpos (T_mcs_map (kty, vty)) (sp-3)) range in
          assume (Unstacked_ast.mk_is_none opt_value range)
            ~fthen:(fun flow ->
                let expr = Unstacked_ast.mk_map_remove ~key ~map range in
                man.exec (mk_assign map expr range) flow
              )
            ~felse:(fun flow ->
                let value = Unstacked_ast.mk_unsome opt_value range in
                let expr = Unstacked_ast.mk_map_add ~key ~value ~map range in
                man.exec (mk_assign map expr range) flow
              ) man flow >>$? fun () flow ->
          man.exec (mk_remove opt_value range) flow >>$? fun () flow ->
          man.exec (mk_remove key range) flow|> OptionExt.return
        | S_mcs_map_mem { kty; vty } ->
          let key = mk_var (mk_stackpos kty (sp-1)) range in
          let map = mk_var (mk_stackpos (T_mcs_map(kty, vty)) (sp-2)) range in
          let er = mk_var (mk_stackpos T_mcs_bool (sp-2)) range in
          let expr = Unstacked_ast.mk_map_mem ~key ~map range in
          (* FIXME should I eval here ? *)
          man.eval expr flow >>$? fun expr flow ->
          man.exec (mk_add er range) flow >>%
          man.exec (mk_assign er expr range) >>%
          man.exec (mk_remove key range) >>%
          man.exec (mk_remove map range) |>
          OptionExt.return
        | S_mcs_map_get _ ->
          let kty = stack_get_type_spi bef (sp-1) in
          let mty = stack_get_type_spi bef (sp-2) in
          let vty = match mty with
            | T_mcs_map(t1, t2) when compare_typ t1 kty = 0 -> t2
            | _ -> panic "incompatible types: update(%a, %a)"
                     pp_typ mty pp_typ kty
          in
          let key = mk_var (mk_stackpos kty (sp-1)) range in
          let map = mk_var (mk_stackpos mty (sp-2)) range in
          let er = mk_var (mk_stackpos (T_mcs_option vty) (sp-2)) range in
          man.exec (mk_add er range) flow >>$? fun () flow ->
          let expr = Unstacked_ast.mk_map_get ~key ~map range in
          man.exec (mk_assign er expr range) flow >>$? fun () flow ->
          man.exec (mk_remove key range) flow >>$? fun () flow ->
          man.exec (mk_remove map range) flow |> OptionExt.return
        | S_mcs_map_size { kty; vty } ->
          let map = mk_var (mk_stackpos (T_mcs_map(kty, vty)) (sp-1)) range in
          let er = mk_var (mk_stackpos T_mcs_int (sp-1)) range in
          man.exec (mk_add er range) flow >>$? fun () flow ->
          let expr = Unstacked_ast.mk_map_size ~map range in
          (* FIXME should I eval here ? *)
          man.eval expr flow >>$? fun expr flow ->
          man.exec (mk_assign er expr range) flow >>$? fun () flow ->
          man.exec (mk_remove map range) flow |> OptionExt.return

        (* Big Maps *)
        | S_mcs_empty_big_map ->
          let etyp = stack_get_type_spi aft sp in
          let er = mk_var (mk_stackpos etyp sp) range in
          man.exec (mk_add er range) flow >>$? fun () flow ->
          let expr = Unstacked_ast.mk_map_empty ~etyp range in
          man.exec (mk_assign er expr range) flow |> OptionExt.return
        | S_mcs_big_map_update ->
          let kty = stack_get_type_spi bef (sp-1) in
          let voptty = stack_get_type_spi bef (sp-2) in
          let mty = stack_get_type_spi bef (sp-3) in
          let () = match mty with
            | T_mcs_map(t1, t2)
              when compare_typ t1 kty = 0 &&
                   compare_typ (T_mcs_option t2) voptty = 0 -> ()
            | _ -> panic "incompatible types: update(%a, %a, %a)"
                     pp_typ mty pp_typ voptty pp_typ kty
          in
          let key = mk_var (mk_stackpos kty (sp-1)) range in
          let opt_value = mk_var (mk_stackpos voptty (sp-2)) range in
          let map = mk_var (mk_stackpos mty (sp-3)) range in
          assume (Unstacked_ast.mk_is_none opt_value range)
            ~fthen:(fun flow ->
                let expr = Unstacked_ast.mk_map_remove ~key ~map range in
                man.exec (mk_assign map expr range) flow
              )
            ~felse:(fun flow ->
                let value = Unstacked_ast.mk_unsome opt_value range in
                let expr = Unstacked_ast.mk_map_add ~key ~value ~map range in
                man.exec (mk_assign map expr range) flow
              ) man flow >>$? fun () flow ->
          man.exec (mk_remove opt_value range) flow >>$? fun () flow ->
          man.exec (mk_remove key range) flow|> OptionExt.return
        | S_mcs_big_map_mem ->
          let kty = stack_get_type_spi bef (sp-1) in
          let mty = stack_get_type_spi bef (sp-2) in
          let () = match mty with
            | T_mcs_map (t1, _) when compare_typ t1 kty = 0 -> ()
            | _ -> panic "incompatible types: update(%a, %a)"
                     pp_typ mty pp_typ kty
          in
          let key = mk_var (mk_stackpos kty (sp-1)) range in
          let map = mk_var (mk_stackpos mty (sp-2)) range in
          let er = mk_var (mk_stackpos T_mcs_bool (sp-2)) range in
          let expr = Unstacked_ast.mk_map_mem ~key ~map range in
          man.exec (mk_add er range) flow >>%
          man.exec (mk_assign er expr range) >>%
          man.exec (mk_remove key range) >>%
          man.exec (mk_remove map range) |>
          OptionExt.return
        | S_mcs_big_map_get ->
          let kty = stack_get_type_spi bef (sp-1) in
          let mty = stack_get_type_spi bef (sp-2) in
          let vty = match mty with
            | T_mcs_map(t1, t2) when compare_typ t1 kty = 0 -> t2
            | _ -> panic "incompatible types: update(%a, %a)"
                     pp_typ mty pp_typ kty
          in
          let key = mk_var (mk_stackpos kty (sp-1)) range in
          let map = mk_var (mk_stackpos mty (sp-2)) range in
          let er = mk_var (mk_stackpos (T_mcs_option vty) (sp-2)) range in
          man.exec (mk_add er range) flow >>$? fun () flow ->
          let expr = Unstacked_ast.mk_map_get ~key ~map range in
          man.exec (mk_assign er expr range) flow >>$? fun () flow ->
          man.exec (mk_remove key range) flow >>$? fun () flow ->
          man.exec (mk_remove map range) flow |> OptionExt.return

        (* Pairs *)
        | S_mcs_cons_pair (ty1, ty2) ->
          let vr = mk_stackpos (T_mcs_pair (ty1, ty2)) (sp - 2) in
          let v0 = mk_stackpos ty1 (sp - 1) in
          let v1 = mk_stackpos ty2 (sp - 2) in
          let er = mk_var vr loc in
          let e0 = mk_var v0 loc in
          let e1 = mk_var v1 loc in
          man.exec (mk_add_var vr loc) flow >>$? fun () flow ->
          let e = Unstacked_ast.mk_pair e0 e1 loc in
          man.eval e flow >>$? fun e flow ->
          let stmt = mk_assign er e loc in
          man.exec stmt flow >>$? fun () flow ->
          man.exec (mk_remove_var v0 loc) flow >>$? fun () flow ->
          man.exec (mk_remove_var v1 loc) flow |>
          OptionExt.return
        | S_mcs_car _ ->
          let pty = stack_get_type_spi bef (sp-1) in
          let fty = match pty with
            | T_mcs_pair(t, _) -> t
            | _ -> panic "Expecting a pair type, got: %a" pp_typ pty
          in
          let v0 = mk_var (mk_stackpos pty (sp-1)) range in
          let vr = mk_var (mk_stackpos fty (sp-1)) range in
          man.exec (mk_add vr loc) flow >>%? fun flow ->
          man.exec (mk_assign vr (Unstacked_ast.mk_car v0 range) range) flow >>%
          man.exec (mk_remove v0 loc) |>
          OptionExt.return
        | S_mcs_cdr _ ->
          let pty = stack_get_type_spi bef (sp-1) in
          let sty = match pty with
            | T_mcs_pair(_, t) -> t
            | _ -> panic "Expecting a pair type, got: %a" pp_typ pty
          in
          let v0 = mk_var (mk_stackpos pty (sp-1)) range in
          let vr = mk_var (mk_stackpos sty (sp-1)) range in
          man.exec (mk_add vr loc) flow >>%? fun flow ->
          man.exec (mk_assign vr (Unstacked_ast.mk_cdr v0 range) range) flow >>%
          man.exec (mk_remove v0 loc) |>
          OptionExt.return
        | S_mcs_unpair _ ->
          let pty = stack_get_type_spi bef (sp-1) in
          let fty, sty = match pty with
            | T_mcs_pair(fty, sty) -> fty, sty
            | _ -> panic "Expecting a pair type, got: %a" pp_typ pty
          in
          let v0 = mk_stackpos pty (sp - 1) in
          let vr0 = mk_stackpos fty (sp - 1) in
          let vr1 = mk_stackpos sty sp in
          let e0 = mk_var v0 loc in
          let er0 = mk_var vr0 loc in
          let er1 = mk_var vr1 loc in
          man.exec (mk_add_var vr0 loc) flow >>$? fun () flow ->
          man.exec (mk_add_var vr1 loc) flow >>$? fun () flow ->
          man.eval (Unstacked_ast.mk_car e0 loc) flow >>$? fun car flow ->
          man.eval (Unstacked_ast.mk_cdr e0 loc) flow >>$? fun cdr flow ->
          man.exec (mk_assign er0 cdr range) flow >>$? fun () flow ->
          man.exec (mk_assign er1 car range) flow >>$? fun () flow ->
          man.exec (mk_remove_var v0 loc) flow |>
          OptionExt.return
        (* option *)
        | S_mcs_cons_some _ ->
          let oty = stack_get_type_spi aft (sp-1) in
          let ity = match oty with
            | T_mcs_option t -> t
            | _ -> panic "Expecting a option type, got: %a" pp_typ oty
          in
          let v0 = mk_stackpos ity (sp - 1) in
          let vr = mk_stackpos oty (sp - 1) in
          let e0 = mk_var v0 loc in
          let er = mk_var vr loc in
          man.exec (mk_add_var vr loc) flow >>$? fun () flow ->
          let e = Unstacked_ast.mk_some e0 loc in
          man.eval e flow >>$? fun e flow ->
          let stmt = mk_assign er e loc in
          man.exec stmt flow >>$? fun () flow ->
          man.exec (mk_remove e0 loc) flow |>
          OptionExt.return
        | S_mcs_cons_none t_opt ->
          let vr = mk_stackpos t_opt sp in
          let er = mk_var vr loc in
          man.exec (mk_add_var vr loc) flow >>$? fun () flow ->
          let e = Unstacked_ast.mk_none t_opt loc in
          man.eval e flow >>$? fun e flow ->
          let stmt = mk_assign er e loc in
          man.exec stmt flow |>
          OptionExt.return
        | S_mcs_if_none (_, tstm, fstm) ->
          let t_opt = stack_get_type_spi bef (sp-1) in
          let t_v = match t_opt with
            | T_mcs_option t -> t
            | t -> panic "Expected option type, got %a" pp_typ t
          in
          let v_opt = mk_var (mk_stackpos t_opt (sp-1)) range in
          assume (Unstacked_ast.mk_is_none v_opt range)
            ~fthen:(fun flow ->
                man.exec (mk_remove v_opt range) flow >>%
                man.exec tstm
              )
            ~felse:(fun flow ->
                let v0 = mk_var (mk_stackpos t_v (sp-1)) range in
                let expr = Unstacked_ast.mk_unsome v_opt range in
                man.exec (mk_add v0 range) flow >>%
                man.exec (mk_assign v0 expr range) >>%
                man.exec (mk_remove v_opt range) >>%
                man.exec fstm
              ) man flow |>
          OptionExt.return
        (* Unions *)
        | S_mcs_cons_left _ ->
          let uty = stack_get_type_spi aft (sp-1) in
          let ity = match uty with
            | T_mcs_union(t, _, _, _) -> t
            | _ -> panic "Expected union type, got: %a" pp_typ uty
          in
          let v0 = mk_var (mk_stackpos ity (sp - 1)) range in
          let vr = mk_var (mk_stackpos uty (sp - 1)) range in
          let expr = Unstacked_ast.mk_mcs_cons_left ~etyp:uty v0 range in
          man.exec (mk_add vr range) flow >>$? fun () flow ->
          man.exec (mk_assign vr expr range) flow >>$? fun () flow ->
          man.exec (mk_remove v0 range) flow |> OptionExt.return
        | S_mcs_cons_right _ ->
          let uty = stack_get_type_spi aft (sp-1) in
          let ity = match uty with
            | T_mcs_union(_, _, t, _) -> t
            | _ -> panic "Expected union type, got: %a" pp_typ uty
          in
          let v0 = mk_var (mk_stackpos ity (sp - 1)) range in
          let vr = mk_var (mk_stackpos uty (sp - 1)) range in
          let expr = Unstacked_ast.mk_mcs_cons_right ~etyp:uty v0 range in
          man.exec (mk_add vr range) flow >>$? fun () flow ->
          man.exec (mk_assign vr expr range) flow >>$? fun () flow ->
          man.exec (mk_remove v0 range) flow |>
          OptionExt.return
        | S_mcs_if_left(_, tstm, fstm) ->
          let uty = stack_get_type_spi bef (sp-1) in
          let lty, _, rty, _ = match uty with
            | T_mcs_union(lty, epl, rty, epr) -> lty, epl, rty, epr
            | _ -> panic "Expected union type, got: %a" pp_typ uty
          in
          let v0 = mk_var (mk_stackpos uty (sp-1)) range in
          assume (Unstacked_ast.mk_is_left v0 range)
            ~fthen:(fun flow ->
                let left = mk_var (mk_stackpos lty (sp-1)) range in
                let expr = Unstacked_ast.mk_unleft v0 range in
                man.exec (mk_add left range) flow >>%
                man.exec (mk_assign left expr range) >>%
                man.exec (mk_remove v0 range) >>%
                man.exec tstm
              )
            ~felse:(fun flow ->
                let right = mk_var (mk_stackpos rty (sp-1)) range in
                let expr = Unstacked_ast.mk_unright v0 range in
                man.exec (mk_add right range) flow >>%
                man.exec (mk_assign right expr range) >>%
                man.exec (mk_remove v0 range) >>%
                man.exec fstm
              ) man flow |>
          OptionExt.return
        (* Control flow *)
        | S_mcs_if (tstm, fstm) ->
          let cond = mk_var (mk_stackpos T_mcs_bool (sp-1)) range in
          let stm = mk_remove cond range in
          switch [
            [cond],
            (fun flow ->
                man.exec (mk_block [stm; tstm] range) flow
            );
            [mk_not cond range],
            (fun flow ->
                man.exec (mk_block [stm; fstm] range) flow
            )
          ] man flow |>
          OptionExt.return
        | S_mcs_loop (stm) ->
          let v0 = mk_var (mk_stackpos T_mcs_bool (sp-1)) range in
          let stm =
            let body =
              mk_block [
                mk_remove v0 range;
                stm;
              ] range
            in
            mk_block [
              mk_while v0 body range;
              mk_remove v0 range
            ] range
          in
          man.exec stm flow |>
          OptionExt.return
        | S_mcs_loop_left (stm) ->
          let ty_u = stack_get_type_spi bef (sp-1) in
          let ty_l, ty_r = match ty_u with
            | T_mcs_union (ty_l, _, ty_r, _) -> ty_l, ty_r
            | _ -> panic "Expected union type"
          in
          let v0 = mk_var (mk_stackpos ty_u (sp-1)) range in
          let vl = mk_var (mk_stackpos ty_l (sp-1)) range in
          let vr = mk_var (mk_stackpos ty_r (sp-1)) range in
          let body =
            mk_block [
              mk_add vl range;
              mk_assign vl (Unstacked_ast.mk_unleft v0 range) range;
              mk_remove v0 range;
              stm;
            ] range
          in
          man.exec (mk_while (Unstacked_ast.mk_is_left v0 range) body range) flow >>%
          man.exec (mk_add vr range) >>%
          man.exec (mk_assign vr (Unstacked_ast.mk_unright v0 range) range) >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        (* Domain specific *)
        | S_mcs_contract (ty, entry) ->
          let v0 = mk_var (mk_stackpos T_mcs_address (sp - 1)) range in
          let vr = mk_var (mk_stackpos (T_mcs_option T_mcs_address) (sp-1)) range in
          let expr = Unstacked_ast.mk_contract ty entry v0 range in
          check_type vr expr;
          man.eval expr flow >>$? fun expr flow ->
          man.exec (mk_add vr range) flow >>$? fun () flow ->
          man.exec (mk_assign vr expr range) flow >>$? fun () flow ->
          man.exec (mk_remove v0 range) flow |>
          OptionExt.return
        | S_mcs_address ->
          let v0 = mk_var (mk_stackpos T_mcs_address (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_address (sp-1)) range in
          let expr = Unstacked_ast.mk_address v0 range in
          man.eval expr flow >>$? fun expr flow ->
          man.exec (mk_assign vr expr range) flow |>
          OptionExt.return
        | S_mcs_source ->
          let vr = mk_var (mk_stackpos T_mcs_address sp) range in
          let expr = Unstacked_ast.mk_source range in
          man.eval expr flow >>$? fun expr flow ->
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) |>
          OptionExt.return
        | S_mcs_sender ->
          let vr = mk_var (mk_stackpos T_mcs_address sp) range in
          let expr = Unstacked_ast.mk_sender range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) |>
          OptionExt.return
        | S_mcs_self entrypoint ->
          let vr = mk_var (mk_stackpos T_mcs_address sp) range in
          let expr = Unstacked_ast.mk_self entrypoint range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) |>
          OptionExt.return
        | S_mcs_check_signature ->
          let v0 = mk_var (mk_stackpos T_mcs_key (sp-1)) range in
          let v1 = mk_var (mk_stackpos T_mcs_signature (sp-2)) range in
          let v2 = mk_var (mk_stackpos T_mcs_bytes (sp-3)) range in
          let vr = mk_var (mk_stackpos T_mcs_bool (sp-3)) range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr (mk_top T_mcs_bool range) range) >>%
          man.exec (mk_remove v0 range) >>%
          man.exec (mk_remove v1 range) >>%
          man.exec (mk_remove v2 range) |>
          OptionExt.return
        | S_mcs_chain_id ->
          let vr = mk_var (mk_stackpos T_mcs_chain_id sp) range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr (mk_top T_mcs_chain_id range) range) |>
          OptionExt.return
        (* Operations *)
        | S_mcs_transfer_tokens ->
          let param_ty = stack_get_type_spi bef (sp - 1) in
          (* let contract_ty = stack_get_type_spi bef (sp-3) in *)
          let v0 = mk_var (mk_stackpos param_ty (sp-1)) range in
          let v1 = mk_var (mk_stackpos T_mcs_mutez (sp-2)) range in
          let v2 = mk_var (mk_stackpos T_mcs_address (sp-3)) range in
          let vr = mk_var (mk_stackpos T_mcs_operation (sp-3)) range in
          man.exec (mk_add vr range) flow >>$? fun () flow ->
          let expr = Unstacked_ast.mk_transfer v2 v0 v1 range in
          man.exec (mk_assign vr expr range) flow >>%
          man.exec (mk_remove v0 range) >>%
          man.exec (mk_remove v1 range) >>%
          man.exec (mk_remove v2 range) |>
          OptionExt.return
        | S_mcs_implicit_account ->
          let tkh = stack_get_type_spi bef (sp - 1) in
          let () = match tkh with
            | T_mcs_key_hash -> ()
            | _ -> panic "Expected type: key_hash"
          in
          let v0 = mk_var (mk_stackpos tkh (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_address (sp-1)) range in
          let expr = Unstacked_ast.mk_implicit_account v0 range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        | S_mcs_hash_key ->
          let v0 = mk_var (mk_stackpos T_mcs_key (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_key_hash (sp-1)) range in
          let expr = mk_top T_mcs_key_hash range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) >>%
          man.exec (mk_remove v0 range) |>
          OptionExt.return
        | S_mcs_lambda stm ->
          let lty = stack_get_type_spi aft sp in
          let vr = mk_var (mk_stackpos lty sp) range in
          let expr = Unstacked_ast.mk_lambda ~etyp:lty stm range in
          man.exec (mk_add vr range) flow >>%
          man.exec (mk_assign vr expr range) |>
          OptionExt.return
        | S_mcs_exec ->
          let param_ty = stack_get_type_spi bef (sp-1) in
          let lambda_ty = stack_get_type_spi bef (sp-2) in
          let ret_ty = stack_get_type_spi aft (sp-2) in
          let () = match lambda_ty with
            | T_mcs_lambda(pty, rty) when compare_typ param_ty pty = 0 &&
                                          compare_typ ret_ty rty = 0 ->
              ()
            | _ -> panic "Unexpected_types for lambda: %a <> (%a, %a)"
                     pp_typ lambda_ty pp_typ param_ty pp_typ ret_ty
          in
          let param = mk_var (mk_stackpos param_ty (sp-1)) range in
          let lambda = mk_var (mk_stackpos lambda_ty (sp-2)) range in
          let ret = mk_var (mk_stackpos ret_ty (sp-2)) range in
          warn "lambda not implemented yet, setting to top";
          man.exec (mk_remove param range) flow >>%
          man.exec (mk_remove lambda range) >>%
          man.exec (mk_add ret range) >>%
          man.exec (mk_assign ret (mk_top ret_ty range) range) |>
          OptionExt.return
        | S_mcs_apply ->
          let param_ty = stack_get_type_spi bef (sp-1) in
          let lambda_ty = stack_get_type_spi bef (sp-2) in
          let rlambda_ty = stack_get_type_spi aft (sp-2) in
          let param = mk_var (mk_stackpos param_ty (sp-1)) range in
          let lambda = mk_var (mk_stackpos lambda_ty (sp-2)) range in
          let rlambda = mk_var (mk_stackpos rlambda_ty (sp-2)) range in
          let expr = Unstacked_ast.mk_lambda_apply lambda param range in
          (if compare_typ expr.etyp rlambda.etyp <> 0 then
             panic "Unexpected type : %a, expecting : %a"
               pp_typ expr.etyp
               pp_typ rlambda.etyp
          );
          man.exec (mk_add rlambda range) flow >>%
          man.exec (mk_assign rlambda expr range) >>%
          man.exec (mk_remove param range) >>%
          man.exec (mk_remove lambda range) |>
          OptionExt.return
        | S_mcs_set_delegate ->
          let t0 = match stack_get_type_spi bef (sp-1) with
            | T_mcs_option (T_mcs_key_hash) as t -> t
            | t -> panic "Expected type : (option key_hash), got %a" pp_typ t
          in
          let v0 = mk_var (mk_stackpos t0 (sp-1)) range in
          let vr = mk_var (mk_stackpos T_mcs_operation (sp-1)) range in
          assume (Unstacked_ast.mk_is_none v0 range)
            ~fthen:(fun flow ->
                let expr = Unstacked_ast.mk_remove_delegate range in
                man.exec (mk_add vr range) flow >>%
                man.exec (mk_assign vr expr range) >>%
                man.exec (mk_remove v0 range)
              )
            ~felse:(fun flow ->
                let key_hash = Unstacked_ast.mk_unsome v0 range in
                let expr = Unstacked_ast.mk_add_delegate key_hash range in
                man.exec (mk_add vr range) flow >>%
                man.exec (mk_assign vr expr range) >>%
                man.exec (mk_remove v0 range)
              )
            man flow |>
          OptionExt.return
        | S_mcs_create_contract_2 { parameter_ty; storage_ty; code } ->
          (* input stack *)
          let delegate = mk_var (mk_stackpos (T_mcs_option T_mcs_key_hash) (sp-1)) range in
          let amount = mk_var (mk_stackpos T_mcs_mutez (sp-2)) range in
          let storage_ty = stack_get_type_spi bef (sp-3) in
          let storage = mk_var (mk_stackpos storage_ty (sp-3)) range in
          (* output stack *)
          let operation = mk_var (mk_stackpos T_mcs_operation (sp-2)) range in
          let address = mk_var (mk_stackpos T_mcs_address (sp-3)) range in
          (* parameters *)
          let expr =
            Unstacked_ast.mk_create_contract
              ~delegate ~amount ~storage ~parameter_ty ~storage_ty ~code range
          in
          man.exec (mk_add operation range) flow >>%
          man.exec (mk_add address range) >>%
          man.exec (mk_assign operation expr range) >>%
          man.exec (mk_assign address (mk_top T_mcs_address range) range) >>%? fun flow ->
          (if compare_typ storage_ty address.etyp = 0 then
             Post.return flow
           else
             man.exec (mk_remove storage range) flow
          ) >>%
          man.exec (mk_remove delegate range) >>%
          man.exec (mk_remove amount range) |>
          OptionExt.return
        (* Other *)
        | S_mcs_nop -> Post.return flow |> OptionExt.return
        | S_mcs_seq (stm1, stm2) ->
          man.exec stm1 flow >>$? fun () flow ->
          man.exec stm2 flow |> OptionExt.return
        | S_mcs_failwith _ ->
          let ty = stack_get_type_spi bef (sp-1) in
          let v = mk_stackpos ty (sp - 1) in
          let var = mk_var v range in
          let flow =
            if !Frontend.opt_failure_alarm then
              Alarms.raise_mcl_failure var range man flow
            else
              flow
          in
          let cur = Flow.get T_cur man.lattice flow in
          let flow' = Flow.add (T_mcs_failwith(range, v)) cur man.lattice flow in
          let flow' = Flow.remove T_cur flow' in
          debug " **** FAILWITH ****";
          debug "%a" (format (Flow.print man.lattice.print)) flow';
          Post.return flow' |> OptionExt.return
        | S_mcs_wrapped_before {assertion; stm} ->
          man.exec assertion flow >>$? fun () flow ->
          man.exec stm flow |>
          OptionExt.return
        | _ -> None
      )
    | _ -> None

  let eval _ _ _ = None

  let ask _ _ _ = None
end

let () =
  Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
