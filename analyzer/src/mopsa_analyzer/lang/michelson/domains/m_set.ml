open Mopsa
open Ast
open Universal.Ast
open Unstacked_ast
open M_common

module Domain = struct

  module Raw_set = struct
    type t =
      | V_bot
      | V_empty
      | V_set
      | V_top

    let print fmt = function
      | V_bot -> pp_string fmt "⊥"
      | V_empty -> pp_string fmt "empty_set"
      | V_set -> pp_string fmt "set"
      | V_top -> pp_string fmt "⊤"

    let bottom = V_bot
    let top = V_top

    let is_bottom = function
      | V_bot -> true
      | _ -> false

    let subset x y =
      match x, y with
      | V_bot, _ -> true
      | _, V_bot -> false
      | V_top, _ -> false
      | _, V_top -> true
      | V_empty, V_empty
      | V_set, V_set -> true
      | V_empty, V_set
      | V_set, V_empty -> false

    let join x y =
      match x, y with
      | v, V_bot
      | V_bot, v -> v
      | V_top, _
      | _, V_top -> V_top
      | V_empty, V_empty -> V_empty
      | V_set, V_set -> V_set
      | V_empty, V_set
      | V_set, V_empty -> V_top

    let meet x y =
      match x, y with
      | _, V_bot
      | V_bot, _ -> V_bot
      | V_top, y' -> y'
      | x', V_top -> x'
      | V_empty, V_empty -> V_empty
      | V_set, V_set -> V_set
      | V_empty, V_set
      | V_set, V_empty-> V_bot

    let widen _ x y = join x y

  end

  module M = Framework.Lattices.Partial_map.Make(Var)(Raw_set)
  include M

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.sets"
    end)

  let numeric = Semantic "U/Numeric"

  let print_state printer a =
    pprint ~path:[Key "sets"] printer (pbox M.print a)

  let print_expr _ _ _ _ = ()

  let init _ man flow =
    set_env T_cur M.empty man flow

  let checks = []

  let set_size_type = T_mcs_int

  let is_type_set t =
    match t with
    | T_mcs_set _ -> true
    | _ -> false

  let item_type t =
    match t with
    | T_mcs_set t -> t
    | _ -> panic "expected set type, got : %a" pp_typ t

  (* Variables *)
  let mk_var_set_item var =
    let ty =
      match var.vtyp with
      | T_mcs_set ty -> ty
      | ty -> panic "Expected set type, got : %a" pp_typ ty
    in
    mk_var_ghost ~mode:WEAK ty var "set_item"

  let mk_var_set_size var =
    let () =
      match var.vtyp with
      | T_mcs_set ty -> ()
      | ty -> panic "Expected set type, got : %a" pp_typ ty
    in
    let mode = vmode var in
    mk_var_ghost ~mode set_size_type var "set_size"


  (****************************************************************************
   * Helpers
   ***************************************************************************)
  let update_aux_vars old_state new_state var range man flow =
    debug "update_aux_vars %a: %a -> %a"
      pp_var var
      (format Raw_set.print) old_state
      (format Raw_set.print) new_state
    ;
    let open Raw_set in
    match old_state, new_state with
    | V_bot, V_set
    | V_bot, V_top
    | V_empty, V_set
    | V_empty, V_top ->
      let item = mk_var (mk_var_set_item var) range in
      let size = mk_var (mk_var_set_size var) range in
      man.exec (mk_add item range) flow >>$ fun () flow ->
      man.exec (mk_add size range) flow
    | V_set, V_empty
    | V_top, V_empty
    | V_set, V_bot
    | V_top, V_bot ->
      let item = mk_var (mk_var_set_item var) range in
      let size = mk_var (mk_var_set_size var) range in
      man.exec (mk_remove item range) flow >>$ fun () flow ->
      man.exec (mk_remove size range) flow
    | (V_empty|V_bot), (V_empty|V_bot)
    | (V_top|V_set), (V_top|V_set) ->
      Post.return flow

  let add2 var mode old_state new_state map =
    match var_mode var mode with
    | STRONG -> new_state, add var new_state map
    | WEAK ->
      let new_state = Raw_set.join old_state new_state in
      new_state, add var new_state map

  (* Determines the mode to use
   * uniquelly for first time assignements *)
  let weak_update_mode old_state var mode =
    if var_mode var mode = STRONG then
      Some STRONG
    else if Raw_set.subset old_state Raw_set.V_empty then
      Some STRONG
    else
      None


  (****************************************************************************
   * Transition functions
   ***************************************************************************)
  let assign_top var mode range man flow =
    let map = get_env T_cur man flow in
    let old_state =
      match find_opt var map with
      | None -> panic "assign_top: Not found: %a" pp_var var
      | Some o -> o
    in
    let new_state = Raw_set.V_top in
    let new_state, map = add2 var mode old_state new_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state new_state var range man flow >>$ fun () flow ->
    let item = mk_var (mk_var_set_item var) range in
    let size = mk_var ~mode (mk_var_set_size var) range in
    man.exec (mk_assign item (mk_top item.etyp range) range) flow >>% fun flow ->
    man.exec (mk_assign size (mk_top set_size_type range) range) flow


  let assign_constant var mode l range man flow =
    let map = get_env T_cur man flow in
    let old_state =
      match find_opt var map with
      | None -> panic "Not found : %a" pp_var var
      | Some o -> o
    in
    let ity = item_type var.vtyp in
    match l with
    | [] ->
      let new_state = Raw_set.V_empty in
      let new_state, map = add2 var mode old_state new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state (find var map) var range man flow
    | hd::tl ->
      let new_state = Raw_set.V_set in
      let new_state, map = add2 var mode old_state new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state (find var map) var range man flow >>% fun flow ->
      (* Update size *)
      let size = mk_var (mk_var_set_size var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign size (mk_int ~typ:set_size_type (List.length l) range) range) flow >>% fun flow ->
      (* Update item *)
      let first_item = mk_var (mk_var_set_item var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign first_item (mk_constant ~etyp:ity hd range) range) flow >>$ fun () flow ->
      (* Assignement of the remaining elements *)
      let item = mk_var (mk_var_set_item var) range in
      List.fold_left
        (fun acc x ->
           acc >>% man.exec (mk_assign item (mk_constant ~etyp:ity x range) range)
        ) (Post.return flow) tl


  let assign_variable var mode src range man flow =
    let map = get_env T_cur man flow in
    let old_state = find var map in
    let src_state = find src map in
    let new_state, map = add2 var mode old_state src_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state new_state var range man flow >>% fun flow ->
    (* if var is V_set or V_top, assign the item and size variables *)
    if Raw_set.subset Raw_set.V_set src_state then
      let src_item = mk_var (mk_var_set_item src) range in
      let src_size = mk_var (mk_var_set_size src) range in
      let dst_item = mk_var (mk_var_set_item var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      let dst_size = mk_var (mk_var_set_size var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign dst_item src_item range) flow >>$ fun () flow ->
      man.exec (mk_assign dst_size src_size range) flow
    else
      Post.return flow


  let assign_add var mode item range man flow =
    let map = get_env T_cur man flow in
    let old_state = find var map in
    let new_state = Raw_set.V_set in
    let new_state, map = add2 var mode old_state new_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state (find var map) var range man flow >>% fun flow ->
    if Raw_set.subset old_state Raw_set.V_empty then
      (* if item and size didn't exist before, they should have been added and be top
       * -> force strong updates *)
      let vsize = mk_var (mk_var_set_size var) ~mode:(Some STRONG) range in
      let vitem = mk_var (mk_var_set_item var) ~mode:(Some STRONG) range in
      let expr = mk_int ~typ:set_size_type 1 range in
      man.exec (mk_assign vitem item range) flow >>$ fun () flow ->
      man.exec (mk_assign vsize expr range) flow
    else
      let vsize = mk_var ~mode (mk_var_set_size var) range in
      let vitem = mk_var (mk_var_set_item var) range in
      let cmp = mk_mcs_compare vitem item range in
      (* Test whether the item is present in the set by comparing to the smashed
       * variable *)
      assume (mk_mcs_binop ~etyp:T_mcs_bool cmp O_ne (mk_int ~typ:T_mcs_int 0 range) range)
        ~fthen:(fun flow ->
            (* if absent *)
            man.exec (mk_assign vitem item range) flow >>% fun flow ->
            man.exec (mk_assign vsize (mk_mcs_incr vsize range) range) flow
          )
        ~felse:(fun flow ->
            Post.return flow
          )
        man flow

  let assign_remove var mode item range man flow =
    let map = get_env T_cur man flow in
    let old_state = find var map in
    if Raw_set.subset old_state Raw_set.V_empty then
      (* If the set is empty, do nothing *)
      Post.return flow
    else
      (
        let vsize = mk_var ~mode (mk_var_set_size var) range in
        let vitem = mk_var (mk_var_set_item var) range in
        let sz i = mk_mcs_compare vsize (mk_int ~typ:set_size_type i range) range in
        let present = mk_mcs_compare vitem item range in
        switch [
          (* If the size is 0, do nothing *)
          [mk_mcs_binop ~etyp:T_mcs_bool (sz 0) O_eq (mk_int ~typ:T_mcs_int 0 range) range],
          (fun flow ->
             Post.return flow
          );
          (* If the size is 1 *)
          [mk_mcs_binop ~etyp:T_mcs_bool (sz 1) O_eq (mk_int ~typ:T_mcs_int 0 range) range],
          (fun flow ->
             assume (mk_mcs_binop ~etyp:T_mcs_bool present O_ne (mk_int ~typ:T_mcs_int 0 range) range)
               (* And the item is not in the set, do nothing *)
               ~fthen:(fun flow ->
                   Post.return flow
                 )
               (* And the item is in the set, return an empty set *)
               ~felse:(fun flow ->
                   let map = get_env T_cur man flow in
                   let old_state = find var map in
                   let new_state = Raw_set.V_empty in
                   let new_state, map = add2 var mode old_state new_state map in
                   let flow = set_env T_cur map man flow in
                   update_aux_vars old_state (find var map) var range man flow
                 )
               man flow
          );
          (* If the size is > 1 *)
          [mk_mcs_binop ~etyp:T_mcs_bool (sz 1) O_gt (mk_int ~typ:T_mcs_int 0 range) range],
          (fun flow ->
             assume (mk_mcs_binop ~etyp:T_mcs_bool present O_ne (mk_int ~typ:T_mcs_int 0 range) range)
               (* And the item is not in the set, do nothing *)
               ~fthen:(fun flow ->
                   Post.return flow
                 )
               (* And the item is in the set, remove from the set, ie
                * we juste decrement size *)
               ~felse:(fun flow ->
                   let expr = mk_mcs_binop
                       ~etyp:set_size_type
                       vsize O_minus (mk_int ~typ:set_size_type 1 range)
                       range
                   in
                   man.exec (mk_assign vsize expr range) flow
                 )
               man flow
          )
        ] man flow
      )


  (****************************************************************************
   * Loops
   ***************************************************************************)
  (* XXX: stack manipulation here, pretty ugly *)
  (* assumes the set is non-empty *)
  let set_iter set stm sp bef range man flow =
    let vnset = mk_range_attr_var range "set" set.etyp in
    let nset = mk_var vnset range in
    let counter = mk_var (mk_range_attr_var range "counter" set_size_type) range in
    let iter = mk_var (mk_stackpos (item_type set.etyp) (sp-1)) range in
    let size = mk_var (mk_range_attr_var range "size" set_size_type) range in
    man.exec (mk_add size range) flow >>%
    man.exec (mk_add counter range) >>%
    man.exec (mk_assign size (Unstacked_ast.mk_set_size set range) range) >>%
    man.exec (mk_assign counter (mk_int ~typ:set_size_type 0 range) range) >>%
    man.exec (mk_rename set nset range) >>% fun flow ->
    debug "set_iter: initialization";
    let loop =
      mk_while
        (lt counter size range)
        (mk_block
           [
             mk_add iter range;
             mk_assign iter (mk_var (mk_var_set_item vnset) range) range;
             stm;
             mk_assign counter
               (mk_mcs_binop ~etyp:set_size_type
                  counter
                  O_plus
                  (mk_int ~typ:set_size_type 1 range)
                  range
               )
               range;
           ]
           range
        )
        range
    in
    debug "set_iter: loop";
    man.exec loop flow >>% fun flow ->
    debug "set_iter: finalization";
    man.exec (mk_remove nset range) flow >>%
    man.exec (mk_remove counter range) >>%
    man.exec (mk_remove size range)


  (****************************************************************************
   * Main
   ***************************************************************************)
  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    (* add the variable *)
    | S_add ({ekind=E_var(var, mode); etyp}) when is_type_set var.vtyp ->
      check_types var.vtyp etyp;
      (* adds item and size variables to environmnent *)
      debug "m_set.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> Raw_set.V_bot
        | Some o -> panic "m_set: adding variable already set: %a" pp_var var
      in
      let new_state = Raw_set.top in
      let new_state, map = add2 var mode old_state new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow >>$? fun () flow ->
      Post.return flow |>
      OptionExt.return

    | S_remove({ekind=E_var(var, _); etyp; _}) when is_type_set var.vtyp ->
      check_types var.vtyp etyp;
      (* remove size from environment, and remove item if necessary *)
      debug "m_set.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> panic "m_set: removing non-existing variable: %a" pp_var var
        | Some o -> o
      in
      let new_state = Raw_set.V_bot in
      let map = remove var map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    | S_rename({ekind=E_var(src_var, _); _}, {ekind=E_var(dst_var, _); _})
      when is_type_set src_var.vtyp ->
      check_types src_var.vtyp dst_var.vtyp;
      (if compare_var src_var dst_var = 0 then
         Post.return flow
       else
         (
           let map = get_env T_cur man flow in
           (match M.find_opt src_var map with
            | None -> Post.return flow
            | Some v ->
              (if Raw_set.subset V_set v then
                 let src_item = mk_var (mk_var_set_item src_var) range in
                 let dst_item = mk_var (mk_var_set_item dst_var) range in
                 let src_size = mk_var (mk_var_set_size src_var) range in
                 let dst_size = mk_var (mk_var_set_size dst_var) range in
                 man.exec (mk_rename src_item dst_item range) flow >>$ fun () flow ->
                 man.exec (mk_rename src_size dst_size range) flow
               else
                 Post.return flow
              ) >>% fun flow ->
              let map = M.add dst_var v map in
              let map = M.remove src_var map in
              set_env T_cur map man flow |>
              Post.return
           )
         )
      ) |>
      OptionExt.return

    | S_assign({ekind=E_var(var, mode); _}, expr)
      when is_type_set var.vtyp ->
      check_types var.vtyp expr.etyp;
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_constant (C_top (T_mcs_set _)) ->
         assign_top var mode range man flow
       | E_constant (C_mcs_set l) ->
         assign_constant var mode l range man flow
       | E_var(src, _) when compare_var var src = 0 ->
         Post.return flow
       | E_var(src, _) ->
         assign_variable var mode src range man flow
       | E_mcs_set_add { item; set } ->
         man.exec (mk_assign (mk_var ~mode var range) set range) flow >>% fun flow ->
         assign_add var mode item range man flow
       | E_mcs_set_remove { item; set } ->
         man.exec (mk_assign (mk_var ~mode var range) set range) flow >>% fun flow ->
         assign_remove var mode item range man flow
       | _ -> panic "TODO"
      ) |>
      OptionExt.return

    | S_michelson_instr { instr = S_mcs_set_iter stm; sp; bef; _ } ->
      let t_set = stack_get_type_spi bef (sp-1) in
      let () =
        match t_set with
        | T_mcs_set t -> ()
        | _ -> panic "Expected set type, got %a" pp_typ t_set
      in
      let vset = mk_stackpos t_set (sp-1) in
      let cur = get_env T_cur man flow in
      (match find_opt vset cur with
       | None -> Flow.bottom_from flow |> Post.return
       | Some v ->
         (match v with
          | V_bot
          | V_empty ->
            man.exec (mk_remove (mk_var vset range) range) flow
          | V_set
          | V_top ->
            set_iter (mk_var vset range) stm sp bef range man flow
         )
      ) |>
      OptionExt.return

    | _ -> None


  let exec stmt man flow =
    let map = get_env T_cur man flow in
    (* XXX: checking for bottom seems to be necessary in order to pass empty env
     * which are due to FAILURES *)
    if is_bottom map then
      Post.return flow |> OptionExt.return
    else
      exec stmt man flow


  let eval expr man flow =
    let range = erange expr in
    match ekind expr with
    | E_mcs_set_mem { set; item } ->
      (match ekind set with
       | E_constant (C_mcs_set []) ->
         man.eval (mk_mcs_false range) flow |>
         OptionExt.return
       | E_constant (C_mcs_set l) ->
         let t_item = item_type (etyp expr) in
         (List.fold_left (fun acc it ->
              let it = mk_constant ~etyp:t_item it range in
              Eval.join acc (man.eval (mk_mcs_binop ~etyp:T_mcs_bool it O_eq item range) flow)
            ) (man.eval (mk_mcs_false range) flow) l
         ) |>
         OptionExt.return
       | E_var(var, _) ->
         let size = Unstacked_ast.mk_set_size set range in
         switch [
           [mk_mcs_binop ~etyp:T_mcs_bool size O_eq (mk_zero ~typ:set_size_type range) range],
           (fun flow -> man.eval (mk_mcs_false range) flow);
           [mk_mcs_binop ~etyp:T_mcs_bool size O_gt (mk_zero ~typ:set_size_type range) range],
           (fun flow ->
              let oitem = mk_var (mk_var_set_item var) range in
              let is_item_there =
                let ecompare = Unstacked_ast.mk_mcs_compare item oitem range in
                mk_mcs_binop ~etyp:T_mcs_bool ecompare O_eq (mk_int ~typ:T_mcs_int 0 range) range
              in
              assume is_item_there
                ~fthen:
                  (fun flow -> man.eval (mk_mcs_true range) flow)
                ~felse:
                  (fun flow -> man.eval (mk_mcs_false range) flow)
                man flow
           )
         ] man flow |>
         OptionExt.return
       | _ ->
         warn_at range "Unhandled: set_mem(%a, %a)" pp_expr set pp_expr item;
         man.eval (mk_top T_mcs_bool range) flow |>
         OptionExt.return
      )

    | E_mcs_set_size set ->
      (match set with
       | { ekind=E_var(var, _); _} ->
         (let map = get_env T_cur man flow in
          match find_opt var map with
          | None -> panic "Not found : %a" pp_var var
          | Some v ->
            (match v with
             | V_bot -> Eval.empty flow
             | V_empty -> man.eval (mk_mcs_int 0 range) flow
             | V_set ->
               let size = mk_var (mk_var_set_size var) range in
               man.eval size flow
             | V_top ->
               let size = mk_var (mk_var_set_size var) range in
               Eval.join
                 (man.eval size flow)
                 (man.eval (mk_mcs_int 0 range) flow)
            )
         )
       | _ ->
         warn_at range "Unhandled: set_size(%a)" pp_expr set;
         man.eval (mk_top T_mcs_int range) flow
      ) |>
      OptionExt.return

    | _ -> None


  let ask _ _ _ = None
  let refine _ _ _ = assert false
  let merge _ _ = assert false
  let widen = widen

end

let () =
  Framework.Sig.Abstraction.Domain.register_standard_domain (module Domain)
