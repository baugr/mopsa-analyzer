open Mopsa
open Universal.Ast
open Ast
open Unstacked_ast

module Domain = struct
  open M_common

  include GenStatelessDomainId (struct
    let name = "michelson.types.booleans"
  end)

  let universal = Semantic "Universal"

  let checks = []

  let is_type_bool t =
    match t with
    | T_mcs_bool -> true
    | _ -> false

  let to_num_type t =
    match t with
    | T_mcs_bool -> T_bool
    | _ -> panic "expected type : m_bool, got : %a" pp_typ t

  let mk_num_var v =
    let vname = Format.asprintf "num(%s)" v.vname in
    mkv vname (V_mcs_num v) (to_num_type v.vtyp)
      ~mode:v.vmode ~semantic:"Universal"

  let mk_num_var_expr e =
    match ekind e with
    | E_var (v, mode) -> mk_var (mk_num_var v) ~mode e.erange
    | _ -> assert false

  let print_expr _man _flow _printer _expr = ()

  let init _ _ flow = flow

  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_add({ekind=E_var _; _} as v) when is_type_bool v.etyp ->
      let v = mk_num_var_expr v in
      man.exec (mk_add v range) flow |>
      OptionExt.return

    | S_remove({ekind=E_var _; _} as v) when is_type_bool v.etyp ->
      let v = mk_num_var_expr v in
      man.exec (mk_remove v range) flow |>
      OptionExt.return

    | S_assign (({ekind=E_var _; _} as lval), rval) when is_type_bool lval.etyp ->
      man.eval ~translate:"Universal" lval flow >>$? fun lval flow ->
      man.eval ~translate:"Universal" rval flow >>$? fun rval flow ->
      man.exec (mk_assign lval rval range) flow |>
      OptionExt.return

    | S_rename (({ekind=E_var _; _} as src),
                ({ekind=E_var _; _} as dst)) when is_type_bool src.etyp &&
                                                  is_type_bool dst.etyp ->
      assert (compare_typ src.etyp dst.etyp = 0);
      let src = mk_num_var_expr src in
      let dst = mk_num_var_expr dst in
      man.exec (mk_rename src dst range) flow |>
      OptionExt.return

    | S_assume (({ekind = E_mcs_binop(op,
                                      {ekind = E_mcs_compare(e1, e2); _},
                                      {ekind = E_constant(C_int i); _}); _}) as expr)
      when is_type_bool expr.etyp &&
           Z.compare i Z.zero = 0 ->
      debug "%s.%a" name pp_stmt stmt;
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      (match op with
       | O_eq -> man.exec (mk_assume (eq e1 e2 range) range) flow
       | O_ne -> man.exec (mk_assume (ne e1 e2 range) range) flow
       | O_lt -> man.exec (mk_assume (lt e1 e2 range) range) flow
       | O_le -> man.exec (mk_assume (le e1 e2 range) range) flow
       | O_gt -> man.exec (mk_assume (gt e1 e2 range) range) flow
       | O_ge -> man.exec (mk_assume (ge e1 e2 range) range) flow
       | _ ->
         warn "Unable to infer guarded environment from %a" pp_stmt stmt;
         Post.return flow
      ) |>
      OptionExt.return
    | S_assume ({ekind =
                   E_unop(O_log_not,
                          {ekind = E_mcs_binop(op,
                                               {ekind = E_mcs_compare(e1, e2); _},
                                               {ekind = E_constant(C_int i); _})
                          ; _})
                ; _} as expr)
      when is_type_bool expr.etyp &&
           Z.compare i Z.zero = 0 ->
      debug "%s.%a" name pp_stmt stmt;
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      (match op with
       | O_eq -> man.exec (mk_assume (mk_not (eq e1 e2 range) range) range) flow
       | O_ne -> man.exec (mk_assume (mk_not (ne e1 e2 range) range) range) flow
       | O_lt -> man.exec (mk_assume (mk_not (lt e1 e2 range) range) range) flow
       | O_le -> man.exec (mk_assume (mk_not (le e1 e2 range) range) range) flow
       | O_gt -> man.exec (mk_assume (mk_not (gt e1 e2 range) range) range) flow
       | O_ge -> man.exec (mk_assume (mk_not (ge e1 e2 range) range) range) flow
       | _ ->
         warn "Unable to infer guarded environment from %a" pp_stmt stmt;
         Post.return flow
      ) |>
      OptionExt.return

    | S_assume (expr) when is_type_bool expr.etyp ->
      debug "m_bool.S_assume(%a)" pp_expr expr;
      man.eval ~translate:"Universal" expr flow >>$? fun expr flow ->
      man.exec ~route:universal (mk_assume expr range) flow |>
      OptionExt.return

    | S_assume ({ekind=E_unop(O_log_not, expr); _}) when is_type_bool expr.etyp ->
      debug "%s.%a" name pp_stmt stmt;
      man.eval ~translate:"Universal" expr flow >>$? fun expr flow ->
      man.exec ~route:universal (mk_assume (mk_not expr range) range) flow |>
      OptionExt.return

    | _ -> None

  let eval expr man flow =
    let range = erange expr in
    match expr with

    | {ekind = E_constant(C_top t); _ } when is_type_bool t ->
      let nexpr = mk_top (to_num_type expr.etyp) range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | {ekind = E_constant(C_bool b); _ } when is_type_bool expr.etyp ->
      let nexpr = mk_bool b range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | {ekind = E_var(_, _); _} when is_type_bool expr.etyp ->
      let v = mk_num_var_expr expr in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" v |>
      OptionExt.return

    | {ekind = E_mcs_unop (O_log_not as op, e); _} when is_type_bool expr.etyp ->
      assert (compare_typ e.etyp T_mcs_bool = 0);
      man.eval e flow >>$? fun e flow ->
      (* michelson expr *)
      let mexpr = { expr with ekind = E_mcs_unop (op, e) } in
      (* numeric expr *)
      let nexpr = { expr with ekind = E_unop(op, e) } in
      (* rebuild numeric expression from translations *)
      let nexpr = michelson2num nexpr in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | { ekind = E_mcs_binop (((O_log_and|O_log_or|O_log_xor) as op), e1, e2); _ }
      when is_type_bool e1.etyp &&
           is_type_bool e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      (* michelson expr *)
      let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
      (* numeric expr *)
      let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
      let nexpr = michelson2num nexpr in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | { ekind = E_mcs_binop(((O_eq|O_ne|O_gt|O_ge|O_lt|O_le) as op), e1, e2); _ }
      when is_type_bool expr.etyp &&
           is_type_bool e1.etyp &&
           is_type_bool e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
      let nexpr = michelson2num nexpr in
      let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | { ekind = E_mcs_compare(({ etyp = T_mcs_bool; _} as e1),
                              ({ etyp = T_mcs_bool; _} as e2)); erange; _ } ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      switch [
        [lt ~etyp:T_mcs_bool e1 e2 erange],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int (-1) range in
           let nexpr = { mexpr with etyp = T_int } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        );
        [eq ~etyp:T_mcs_bool e1 e2 erange],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int 0 range in
           let nexpr = { mexpr with etyp = T_int } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        );
        [gt ~etyp:T_mcs_bool e1 e2 erange],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int 1 range in
           let nexpr = { mexpr with etyp = T_int } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        )
      ] man flow |> OptionExt.return

    | _ -> None

  let ask _ _ _ = None

  let refine _ _ _ = assert false

  let merge _ _ = assert false
end

let () = Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
