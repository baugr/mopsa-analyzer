open Mopsa
open Universal.Ast
open Ast
open Unstacked_ast
open M_common

module Domain = struct
  include GenStatelessDomainId (struct
    let name = "michelson.types.strings"
  end)

  let universal = Semantic "Universal"

  let init _ _ flow = flow

  let alarms = []

  let is_type_string t =
    match t with
    | T_mcs_string -> true
    | _ -> false

  let to_string_type t =
    match t with
    | T_mcs_string -> T_string
    | _ -> panic "Unexpected type : %a" pp_typ t

  let mk_string_var v =
    let vname = Format.asprintf "num(%s)" v.vname in
    mkv vname (V_mcs_num v) (to_string_type v.vtyp)
      ~mode:v.vmode ~semantic:"Universal"

  let mk_string_var_expr e =
    match ekind e with
    | E_var (v, mode) -> mk_var (mk_string_var v) ~mode e.erange
    | _ -> assert false

  let print_expr _man _flow _printer _expr = ()
  let checks = []

  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_add({ekind=E_var(v, _); _} as expr) when is_type_string v.vtyp ->
      let expr = mk_string_var_expr expr in
      man.exec ~route:universal (mk_add expr range) flow |>
      OptionExt.return
    | S_remove({ekind=E_var(v, _); _} as expr) when is_type_string v.vtyp ->
      let expr = mk_string_var_expr expr in
      man.exec ~route:universal (mk_remove expr range) flow |>
      OptionExt.return
    | S_rename(({ekind=E_var _; _} as src),
               ({ekind=E_var _; _} as dst)) when is_type_string src.etyp &&
                                                 is_type_string dst.etyp ->
      let src = mk_string_var_expr src in
      let dst = mk_string_var_expr dst in
      man.exec ~route:universal (mk_rename src dst range) flow |>
      OptionExt.return
    | S_assign(({ekind=E_var (v, _); _} as lval), rval) when is_type_string v.vtyp ->
      let lval = mk_string_var_expr lval in
      man.eval ~translate:"Universal" rval flow >>$? fun rval flow ->
      man.exec ~route:universal (mk_assign lval rval range) flow |>
      OptionExt.return
    | _ -> None

  let eval expr man flow =
    let range = erange expr in
    match ekind expr with
    | E_constant( C_string s ) when is_type_string expr.etyp ->
      let nexpr = mk_string s range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return
    | E_constant( C_top ty ) when is_type_string expr.etyp ->
      let nexpr = mk_top (to_string_type ty) range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return
    | E_var(v, _) when is_type_string v.vtyp ->
      let nexpr = mk_string_var_expr expr in
      debug "eval E_var(%a : %a)" pp_var v pp_typ v.vtyp;
      debug "eval E_var... translated to %a" pp_expr_typ nexpr;
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return
    | E_mcs_string_concat_pair (e1, e2) ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      let mexpr = { expr with ekind = E_mcs_string_concat_pair(e1, e2) } in
      let nexpr = mk_binop ~etyp:expr.etyp e1 O_plus e2 range in
      let nexpr = michelson2num nexpr in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return
    | E_mcs_string_concat _ ->
      warn "string_concat: not implemented, set to top";
      Eval.singleton (mk_top T_mcs_string range) flow |>
      Eval.add_translation "Universal" (mk_top T_string range) |>
      OptionExt.return
    | E_mcs_string_size e ->
      debug "m_string.%a" pp_expr_typ expr;
      man.eval e flow >>$? fun e flow ->
      let mexpr = { expr with ekind = E_mcs_string_size e } in
      let ne = get_expr_translation "Universal" e in
      let nexpr = mk_expr ~etyp:T_int (E_len ne) range in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return
    | E_mcs_string_slice _ ->
      warn "string_slice: not implemented, set to top";
      man.eval (mk_top (T_mcs_option T_mcs_string) range) flow |>
      OptionExt.return

    | E_mcs_compare(e1, e2) when is_type_string e1.etyp &&
                                 is_type_string e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      switch [
        [lt e1 e2 range],
        (fun flow ->
           man.eval (mk_constant ~etyp:T_mcs_int (C_int Z.minus_one) range) flow);
        [eq e1 e2 range],
        (fun flow ->
           man.eval (mk_constant ~etyp:T_mcs_int (C_int Z.zero) range) flow);
        [gt e1 e2 range],
        (fun flow ->
           man.eval (mk_constant ~etyp:T_mcs_int (C_int Z.one) range) flow)
      ] man flow |> OptionExt.return

    | _ -> None

  let ask _ _ _ = None

  let refine _ _ _ = assert false

  let merge _ _ = assert false
end

let () = Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
