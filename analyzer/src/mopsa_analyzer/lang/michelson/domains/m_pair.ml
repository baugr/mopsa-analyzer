open Mopsa
open Unstacked_ast
open Universal.Ast
open Ast
open M_common

module Domain = struct

  include GenStatelessDomainId(struct
      let name = "michelson.types.pairs"
    end)

  let print_expr _ _ _ _ = ()

  let var_fst var =
    let name = "fst" in
    let typ =
      match var.vtyp with
      | T_mcs_pair(t, _) -> t
      | _ -> assert false
    in
    let mode = vmode var in
    mk_var_ghost ~mode typ var name

  let var_snd var =
    let name = "snd" in
    let typ =
      match var.vtyp with
      | T_mcs_pair(_, t) -> t
      | _ -> assert false
    in
    let mode = vmode var in
    mk_var_ghost ~mode typ var name

  let init _ _ flow =
    flow

  let checks = []

  let is_type_pair t =
    match t with
    | T_mcs_pair _ -> true
    | _ -> false

  let fst_type t =
    match t with
    | T_mcs_pair(t1, _) -> t1
    | _ -> panic "Expecting a pair type, got: %a" pp_typ t

  let snd_type t =
    match t with
    | T_mcs_pair(_, t2) -> t2
    | _ -> panic "Expecting a pair type, got: %a" pp_typ t

  let exec stmt man flow =
    let range = srange stmt in
    match skind stmt with
    (* Adding a pair to environment *)
    | S_add ({ ekind=E_var(v, _); _ } as e) when is_type_pair e.etyp ->
      let fst = mk_var (var_fst v) range in
      let snd = mk_var (var_snd v) range in
      man.exec (mk_add fst range) flow >>$? fun () flow ->
      man.exec (mk_add snd range) flow |>
      OptionExt.return

    (* Remove a pair from environment *)
    | S_remove ({ ekind=E_var(v, _); _ } as e) when is_type_pair e.etyp ->
      let fst = mk_var (var_fst v) range in
      let snd = mk_var (var_snd v) range in
      man.exec (mk_remove fst range) flow >>$? fun () flow ->
      man.exec (mk_remove snd range) flow |>
      OptionExt.return

    (* Assignement *)
    | S_assign({ekind=E_var(v, mode); _ } as e, expr) when is_type_pair e.etyp ->
      let fst = mk_var ~mode (var_fst v) range in
      let snd = mk_var ~mode (var_snd v) range in
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_constant (C_top (T_mcs_pair(t1, t2))) ->
         man.exec (mk_assign fst (mk_top t1 range) range) flow >>%
         man.exec (mk_assign snd (mk_top t2 range) range)
       | E_constant (C_mcs_pair(c1, c2)) ->
         man.exec (mk_assign fst (mk_constant ~etyp:(fst.etyp) c1 range) range) flow >>%
         man.exec (mk_assign snd (mk_constant ~etyp:(snd.etyp) c2 range) range)
       | E_var(src, _) ->
         let src_fst = mk_var (var_fst src) range in
         let src_snd = mk_var (var_snd src) range in
         man.exec (mk_assign fst src_fst range) flow >>%
         man.exec (mk_assign snd src_snd range)
       | E_mcs_pair(l, r) ->
         man.exec (mk_assign fst l range) flow >>%
         man.exec (mk_assign snd r range)
       | _ -> panic "unhandled expression: %a" pp_expr expr
      )|>
      OptionExt.return

    | S_rename(({ekind=E_var(v_src, _); _ } as src),
               ({ekind=E_var(v_dst, _); _ } as dst)) when
        is_type_pair src.etyp ->
      assert (compare_typ src.etyp dst.etyp = 0);
      let fst_src = mk_var (var_fst v_src) range in
      let snd_src = mk_var (var_snd v_src) range in
      let fst_dst = mk_var (var_fst v_dst) range in
      let snd_dst = mk_var (var_snd v_dst) range in
      man.exec (mk_rename fst_src fst_dst range) flow >>$? fun () flow ->
      man.exec (mk_rename snd_src snd_dst range) flow |>
      OptionExt.return

    | _ -> None

  let eval expr man flow =
    let range = erange expr in
    match ekind expr with
    | E_constant (C_top (T_mcs_pair (t1, t2))) ->
      debug "m_pair.%a" pp_expr_typ expr;
      let expr = Unstacked_ast.mk_pair (mk_top t1 range) (mk_top t2 range) range in
      man.eval expr flow |>
      OptionExt.return

    | E_mcs_car e ->
      debug "m_pair.%a" pp_expr_typ expr;
      let () = match e.etyp with
        | T_mcs_pair(t1, _) when compare_typ expr.etyp t1 = 0 -> ()
        | _ -> panic "Unexpected type for car(%a) : %a" pp_expr_typ e pp_typ expr.etyp
      in
      man.eval e flow >>$? fun e flow ->
      (match ekind e with
       | E_constant (C_mcs_pair(c, _)) ->
         man.eval (mk_constant ~etyp:expr.etyp c range) flow |>
         OptionExt.return
       | E_constant (C_top (T_mcs_pair(t1, _))) ->
         assert (compare_typ expr.etyp t1 = 0);
         man.eval (mk_top t1 range) flow |>
         OptionExt.return
       | E_mcs_pair(e1, _) ->
         man.eval e1 flow |>
         OptionExt.return
       | E_var(v, _) ->
         let fst = mk_var (var_fst v) range in
         man.eval fst flow |>
         OptionExt.return
       | _ -> panic "car(%a)" pp_expr_typ e
      )

    | E_mcs_cdr e ->
      debug "m_pair.%a" pp_expr_typ expr;
      let () = match e.etyp with
        | T_mcs_pair(_, t2) when compare_typ expr.etyp t2 = 0 -> ()
        | _ -> panic "Unexpected type for cdr(%a) : %a" pp_expr_typ e pp_typ expr.etyp
      in
      man.eval e flow >>$? fun e flow ->
      (match ekind e with
       | E_constant (C_mcs_pair(_, c)) ->
         man.eval (mk_constant ~etyp:expr.etyp c range) flow |>
         OptionExt.return
       | E_constant (C_top (T_mcs_pair(_, t2))) ->
         man.eval (mk_top t2 range) flow |>
         OptionExt.return
       | E_mcs_pair(_, e) ->
         man.eval e flow |>
         OptionExt.return
       | E_var(v, _) ->
         let snd = mk_var (var_snd v) range in
         man.eval snd flow |>
         OptionExt.return
       | _ -> panic "cdr(%a)" pp_expr_typ e
      )

    | E_mcs_compare(e1, e2) when is_type_pair e1.etyp && is_type_pair e2.etyp ->
      debug "m_pair.%a" pp_expr_typ expr;
      let cmpl = mk_mcs_compare (mk_car e1 range) (mk_car e2 range) range in
      let cmpr = mk_mcs_compare (mk_cdr e1 range) (mk_cdr e2 range) range in
      switch [
        [eq ~etyp:T_mcs_bool cmpl (mk_int ~typ:T_mcs_int (-1) range) range],
        (fun flow -> man.eval (mk_int ~typ:T_mcs_int (-1) range) flow);
        [eq ~etyp:T_mcs_bool cmpl (mk_int ~typ:T_mcs_int 1 range) range],
        (fun flow -> man.eval (mk_int ~typ:T_mcs_int 1 range) flow);
        [eq ~etyp:T_mcs_bool cmpl (mk_int ~typ:T_mcs_int 0 range) range],
        (fun flow -> man.eval cmpr flow)
      ] man flow |>
      OptionExt.return

    | _ -> None

  let ask _ _ _ = None
  let refine _ _ _ = assert false
  let merge _ _ = assert false

end

let () =
  Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
