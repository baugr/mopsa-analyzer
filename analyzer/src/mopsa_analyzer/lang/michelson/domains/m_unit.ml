open Mopsa
open Universal.Ast
open Ast

module Domain = struct
  open M_common

  include GenStatelessDomainId (struct
      let name = "michelson.types.units"
    end)

  let checks = []

  let print_expr _man _flow _printer _expr = ()

  let is_type_unit t =
    match t with
    | T_mcs_unit -> true
    | _ -> false

  let init _ _ flow = flow

  let exec stmt man (flow : 'a flow) : 'a post option =
    match skind stmt with
    | S_add({ekind=E_var _; etyp=T_mcs_unit; _}) ->
      Post.return flow |>
      OptionExt.return
    | S_remove({ekind=E_var(_, _); etyp=T_mcs_unit; _}) ->
      Post.return flow |>
      OptionExt.return
    | S_assign (({ekind=E_var _; etyp=T_mcs_unit; _}), rval) ->
      man.eval rval flow >>$? fun _ flow ->
      Post.return flow |>
      OptionExt.return
    | S_rename (({ekind=E_var _; _} as src),
                ({ekind=E_var _; _} as dst)) when is_type_unit src.etyp &&
                                                  is_type_unit dst.etyp ->
      Post.return flow |>
      OptionExt.return
    | _ -> None

  let eval expr _ (flow: 'a flow) : 'a eval option =
    match ekind expr with
    | E_constant(C_top t) when is_type_unit t ->
      Eval.singleton expr flow |>
      OptionExt.return

    (* Constant *)
    | E_constant(C_unit) ->
      Eval.singleton expr flow |>
      OptionExt.return

    (* Variable *)
    | E_var(var, _) when is_type_unit var.vtyp ->
      Eval.singleton expr flow |>
      OptionExt.return

    | _ -> None

  let ask _ _ _ = None

end

let () =
  Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
