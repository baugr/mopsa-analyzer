open Mopsa
open Universal.Ast
open Ast
open Unstacked_ast

module Domain = struct
  open M_common

  include GenStatelessDomainId(struct
      let name = "michelson.types.timestamps"
    end)

  let universal = Semantic "Universal"

  let checks = []

  let is_type_timestamp = function
    | T_mcs_timestamp -> true
    | _ -> false

  let to_num_type t =
    match t with
    | T_mcs_timestamp -> T_int
    | _ -> panic "non timestamp data type: %a" pp_typ t

  let mk_num_var v =
    let vname = Format.asprintf "num(%s)" v.vname in
    mkv vname (V_mcs_num v) (to_num_type v.vtyp)
      ~mode:v.vmode ~semantic:"Universal"

  let mk_num_var_expr e =
    match ekind e with
    | E_var (v, mode) -> mk_var (mk_num_var v) ~mode e.erange
    | _ -> assert false


  let init _ _ flow = flow


  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_add({ekind=E_var(v, _); _} as e) when is_type_timestamp e.etyp ->
      check_types T_mcs_timestamp v.vtyp;
      let expr = mk_num_var_expr e in
      man.exec (mk_add expr range) flow |>
      OptionExt.return

    | S_remove({ekind=E_var(v, _); _ } as e) when is_type_timestamp e.etyp ->
      check_types T_mcs_timestamp v.vtyp;
      let v = mk_num_var_expr e in
      man.exec (mk_remove v range) flow |>
      OptionExt.return

    | S_assign(({ekind=E_var _; _} as lval), rval) when is_type_timestamp lval.etyp ->
      check_types T_mcs_timestamp lval.etyp;
      check_types T_mcs_timestamp rval.etyp;
      man.eval ~translate:"Universal" lval flow >>$? fun lval flow ->
      man.eval ~translate:"Universal" rval flow >>$? fun rval flow ->
      man.exec (mk_assign lval rval range) flow |>
      OptionExt.return

    | S_rename (({ekind=E_var _; _} as src),
                ({ekind=E_var _; _} as dst)) when is_type_timestamp src.etyp &&
                                                  is_type_timestamp dst.etyp ->
      man.eval ~translate:"Universal" src flow >>$? fun src flow ->
      man.eval ~translate:"Universal" dst flow >>$? fun dst flow ->
      man.exec (mk_rename src dst range) flow |>
      OptionExt.return

    | S_expand({ekind = E_var _} as e, el)
      when is_type_timestamp e.etyp &&
           List.for_all (fun ee -> is_type_timestamp ee.etyp) el
      ->
      let v = mk_num_var_expr e in
      let vl = List.map mk_num_var_expr el in
      man.exec (mk_expand v vl range) flow |>
      OptionExt.return

    | S_fold(({ekind = E_var _} as e), el)
      when is_type_timestamp e.etyp &&
           List.for_all (fun ee -> is_type_timestamp ee.etyp) el
      ->
      let v = mk_num_var_expr e in
      let vl = List.map mk_num_var_expr el in
      man.exec (mk_fold v vl range) flow |>
      OptionExt.return

    | S_forget ({ekind = E_var _} as v) when is_type_timestamp v.etyp ->
      let vv = mk_num_var_expr v in
      let top = mk_top vv.etyp range in
      man.exec (mk_assign vv top range) flow |>
      OptionExt.return

    | _ -> None

  let eval expr man flow : 'a eval option =
    let range = erange expr in
    match expr with
    | { ekind = E_constant (C_top t); _ } when is_type_timestamp expr.etyp ->
      let nexpr = mk_top (to_num_type expr.etyp) range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | { ekind = E_constant ((C_int z) as c); _ } when is_type_timestamp expr.etyp ->
      let nexpr = mk_constant ~etyp:(to_num_type expr.etyp) c range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | { ekind = E_var (v, _); _} when is_type_timestamp expr.etyp ->
      check_types T_mcs_timestamp v.vtyp;
      let nexpr = mk_num_var_expr expr in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | { ekind = E_mcs_now; _ } ->
      Eval.singleton (mk_top T_int range) flow |>
      OptionExt.return

    | { ekind = E_mcs_binop((O_plus as op),
                            ({etyp=T_mcs_timestamp; _} as e1),
                            ({etyp=T_mcs_int; _} as e2))|
                E_mcs_binop((O_plus as op),
                            ({etyp=T_mcs_int; _} as e1),
                            ({etyp=T_mcs_timestamp; _} as e2))|
                E_mcs_binop((O_minus as op),
                            ({etyp=T_mcs_timestamp; _} as e1),
                            ({etyp=T_mcs_int; _} as e2))|
                E_mcs_binop((O_minus as op),
                            ({etyp=T_mcs_timestamp; _} as e1),
                            ({etyp=T_mcs_timestamp; _} as e2)); _} ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
      let nexpr = michelson2num nexpr in
      let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    (* Comparison operators *)
    | { ekind = E_mcs_binop(((O_eq|O_ne|O_gt|O_ge|O_lt|O_le) as op), e1, e2); _ }
      when is_type_bool expr.etyp &&
           is_type_timestamp e1.etyp &&
           is_type_timestamp e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
      let nexpr = michelson2num nexpr in
      let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | {ekind=E_mcs_compare(e1, e2); _} when is_type_int expr.etyp &&
                                            is_type_timestamp e1.etyp &&
                                            is_type_timestamp e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      switch [
        [lt ~etyp:T_mcs_bool e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int (-1) range in
           let nexpr = { mexpr with etyp = T_mcs_int } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        );
        [eq ~etyp:T_mcs_bool e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int 0 range in
           let nexpr = { mexpr with etyp = T_mcs_int } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        );
        [gt ~etyp:T_mcs_bool e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int 1 range in
           let nexpr = { mexpr with etyp = T_mcs_int } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        )
      ] man flow |> OptionExt.return

    | _ -> None


  let print_expr _man _flow _printer _expr = ()


  let ask _ _ _ = None
  let refine _ _ _ = assert false
  let merge _ _ = assert false

end


let () =
  Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
