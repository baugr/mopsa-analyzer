open Mopsa
open Framework.Sig.Abstraction.Stateless
open Universal.Ast
open Ast

module Domain = struct
  open M_common

  include GenStatelessDomainId (struct
      let name = "michelson.test-store"
    end)

  let universal = Semantic "Universal"

  let checks = []

  let print_expr _man _flow _printer _expr = ()

  let init _ _ flow =
    init_state_store flow


  (* Transfer functions *)
  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_michelson_instr { instr; sp; loc; bef; aft; _ } ->
      (match instr with
       | S_mcs_seq _ -> man.exec ~route:(Below name) stmt flow
       | _ ->
         man.exec ~route:(Below name) stmt flow >>% fun flow ->
         add_state_store range flow |>
         Post.return
      ) |>
      OptionExt.return

    | S_program ({prog_kind = P_michelson
                      { contract;
                        context; _}
                 ; _ }, _) ->

      let init_flow = flow in

      let module Tezos_ast = (val contract.tezos_ast) in

      let ast = Tezos_ast.tezos_ast in

      Format.eprintf "Entering m_test_store with %s@." Tezos_ast.filename;

      let step_constants = Tezos_ast.step_constants
          "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv"
          "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv"
          "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv"
          500L
          "NetXaFDF7xZQCpR"
      in

      Crowbar.add_test ~name:Tezos_ast.filename
        [Tezos_ast.interpret_crowbar step_constants ast]
        (fun logs ->
           (* abstract analysis *)
           (* Format.eprintf "IN CROWBAR: RUNNING with %s@." Tezos_ast.filename; *)
           let abstract =
             man.exec ~route:(Below name) stmt init_flow |> post_to_flow man
           in

           (* Get results *)
           let range_map = get_state_store abstract in

           List.iter (fun (Tezos_ast.Log(range, stack)) ->
               let stack = Mast_to_preast.translate_stack stack in
               (* Format.eprintf "STACK: %a@;" Pre_ast.pp_stack stack; *)
               let concrete, _ =
                 List.fold_left (fun (flow, i) stack_item ->
                     let ty, cst = Frontend.of_const stack_item in
                     let v = mk_var (mk_stackpos ty i) range in
                     let flow =
                       (man.exec (mk_add v range) flow >>%
                        man.exec (mk_assign
                                    v
                                    (mk_constant ~etyp:ty cst range)
                                    range
                                 )
                       ) |> post_to_flow man
                     in
                     flow, (i-1)
                 ) (init_flow, (List.length stack - 1)) stack
               in
               let abstract = RangeMap.find range range_map in
               let result = Flow.subset man.lattice concrete abstract in
               (* if not result then *)
               (*   ( *)
                   Format.eprintf "concrete: %a - %a@." pp_range range (format (Flow.print man.lattice.print)) concrete;
                   Format.eprintf "abstract: %a - %a@." pp_range range (format (Flow.print man.lattice.print)) abstract;
                 (* ); *)
               Crowbar.check result
             ) logs
        );

      Post.return flow |>
      OptionExt.return

    | _ -> None

  let eval expr man flow : 'a eval option = None

  let ask _ _ _ = None

end

let () =
  register_stateless_domain (module Domain)
