open Mopsa
open Universal.Ast
open Ast
open Bot
open Unstacked_ast
open M_common

module Parameter =
struct
  type t =
    | V_top
    | V_parameter of typ
    | V_bot

  let top = V_top
  let bottom = V_bot

  let compare x y =
    match x, y with
    | V_parameter t1, V_parameter t2 -> compare_typ t1 t2
    | _ -> Stdlib.compare x y

  let join x y =
    match x, y with
    | V_top, _ -> V_top
    | _, V_top -> V_top
    | V_bot, x
    | x, V_bot -> x
    | V_parameter t1, V_parameter t2 when compare_typ t1 t2 = 0 ->
      V_parameter t1
    | _ -> V_top

  let meet x y =
    match x, y with
    | V_top, x
    | x, V_top -> x
    | V_parameter t1, V_parameter t2 when compare_typ t1 t2 = 0 ->
      V_parameter t1
    | _ -> V_bot

  let subset x y =
    match x, y with
    | V_bot, _ -> true
    | V_top, _ -> false
    | _, V_bot -> false
    | _, V_top -> true
    | V_parameter t1, V_parameter t2 when compare_typ t1 t2 = 0 -> true
    | _ -> false

  let is_bottom = function
    | V_bot -> true
    | _ -> false

  let widen _ = join

  let print = unformat (fun fmt x ->
      match x with
      | V_top -> Format.pp_print_string fmt "⊤"
      | V_bot -> Format.pp_print_string fmt "⊥"
      | V_parameter t -> pp_typ fmt t
    )
end

module Domain =
struct

  module M = Framework.Lattices.Partial_map.Make(Var)(Parameter)
  include M

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.parameter_type"
    end)

  let init _ man flow =
    set_env T_cur M.empty man flow

  let checks = []

  let universal = Semantic "Universal"

  let print_expr _ _ = assert false

  let print_state printer a =
    pprint ~path:[Key "parameter_type"] printer (pbox M.print a)

  let is_type_alpha t =
    match t with
    | T_mcs_alpha -> true
    | _ -> false

  let mk_var_content typ var =
    assert ((compare_typ typ T_mcs_alpha) <> 0);
    let name = "content" in
    let mode = vmode var in
    mk_var_ghost ~mode typ var name

  let add2 var mode old_state new_state map =
    match var_mode var mode with
    | STRONG -> new_state, add var new_state map
    | WEAK ->
      let new_state = Parameter.join old_state new_state in
      new_state, add var new_state map

  let update_aux_vars old_state new_state var range man flow =
    debug "update_aux_vars %a: %a -> %a"
      pp_var var
      (format Parameter.print) old_state
      (format Parameter.print) new_state
    ;
    (match new_state with
     | V_parameter T_mcs_alpha -> panic "should not happen"
     | _ -> ()
    );
    let open Parameter in
    match old_state, new_state with
    | (V_top|V_bot), V_parameter typ ->
      let item = mk_var (mk_var_content typ var) range in
      man.exec (mk_add item range) flow
    | V_parameter typ, (V_top|V_bot) ->
      let item = mk_var (mk_var_content typ var) range in
      man.exec (mk_remove item range) flow
    | V_parameter t1, V_parameter t2  when compare_typ t1 t2 = 0 ->
      Post.return flow
    | V_parameter _, V_parameter _ ->
      assert false
    | (V_top|V_bot), (V_top|V_bot) ->
      Post.return flow


  let exec stmt (man : ('a, 'b) Framework.Core.Manager.man) flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_add({ekind=E_var(var, _); _}) when is_type_alpha var.vtyp ->
      debug "m_parameter_type.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
       | None -> Parameter.bottom
       | Some _ -> panic "m_parameter_type: adding an already existing variable: %a" pp_var var
      in
      let new_state = Parameter.top in
      let map = M.add var new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow >>%? fun flow ->
      Post.return flow |>
      OptionExt.return

    | S_remove({ekind=E_var(var, _); _}) when is_type_alpha var.vtyp ->
      debug "m_paramter_type.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state =
        match M.find_opt var map with
        | None -> panic "m_parameter_type: removing an inexisting variable: %a" pp_var var
        | Some v -> v
      in
      let new_state = Parameter.bottom in
      let map = M.remove var map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow >>%? fun flow ->
      Post.return flow |>
      OptionExt.return

    | S_rename({ekind=E_var(src_var, _); _}, {ekind=E_var(dst_var, _); _})
      when is_type_alpha src_var.vtyp &&
           is_type_alpha dst_var.vtyp ->
      debug "m_parameter_type.%a" pp_stmt stmt;

      (if compare_var src_var dst_var = 0 then
         Post.return flow
       else
         let map = get_env T_cur man flow in
         let () = match M.find_opt dst_var map with
           | None -> ()
           | Some _ -> panic "m_parameter_type: renaming to an already existing variable: %a" pp_var dst_var;
         in
         let src_state = match M.find_opt src_var map with
           | None -> panic "m_parameter_type: renaming an inexisting variable: %a" pp_var src_var
           | Some ps -> ps
         in
         let map = M.remove src_var map in
         let map = M.add dst_var src_state map in
         let flow = set_env T_cur map man flow in
         (match src_state with
          | Parameter.V_parameter typ ->
            let old_content = mk_var (mk_var_content typ src_var) range in
            let new_content = mk_var (mk_var_content typ dst_var) range in
            man.exec (mk_rename old_content new_content range) flow
          | _ -> Post.return flow
         )
      ) |>
      OptionExt.return

    | S_assign(({ekind=E_var(var, mode); _} as lval), rval) when
        is_type_alpha var.vtyp ->
      debug "m_parameter_type.%a" pp_stmt stmt;
      check_types var.vtyp (etyp lval);
      man.eval rval flow >>$? fun rval flow ->
      (match ekind rval with
       | E_constant (C_top _) ->
         let map = get_env T_cur man flow in
         let old_state = match find_opt var map with
           | None -> panic "Assignment to non-existing variable: %a" pp_var var
           | Some v -> v
         in
         let new_state = Parameter.V_top in
         let map = add var new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow |>
         OptionExt.return
       | _ ->
         let typ = etyp rval in
         let map = get_env T_cur man flow in
         let old_state = match find_opt var map with
           | None -> panic "Assignment to non-existing variable: %a" pp_var var
           | Some v -> v
         in
         let new_state = Parameter.V_parameter typ in
         let new_state, map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         (match new_state with
          | Parameter.V_parameter t when compare_typ t typ = 0 ->
            let v_content = mk_var ~mode (mk_var_content typ var) range in
            man.exec (mk_assign v_content rval range) flow
          | Parameter.V_parameter _ ->
            assert false
          | _ -> Post.return flow
         ) |>
         OptionExt.return
      )
    | _ -> None

  let eval expr man flow =
    let range = erange expr in
    match ekind expr with
    | E_var(var, _) when
        var.vtyp = T_mcs_alpha &&
        expr.etyp = T_mcs_alpha ->
      let map = get_env T_cur man flow in
      (let open Parameter in
       match find var map with
       | V_parameter typ ->
         man.eval (mk_var (mk_var_content typ var) range) flow
       | _ ->
         Eval.singleton (mk_top T_mcs_alpha range) flow
      ) |>
      OptionExt.return
    | _ -> None

  let merge _ _ _ = assert false
  let ask _ _ _ = None

end

let () =
  Sig.Abstraction.Domain.register_standard_domain (module Domain)
