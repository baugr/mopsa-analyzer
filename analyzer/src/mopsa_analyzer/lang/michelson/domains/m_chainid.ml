open Mopsa
open Universal.Ast
open Ast
open Unstacked_ast
open M_common

module Domain = struct
  include GenStatelessDomainId (struct
    let name = "michelson.types.chainids"
  end)

  let universal = Semantic "Universal"

  let init _ _ flow = flow

  let alarms = []

  let is_type_chainid t =
    match t with
    | T_mcs_chain_id -> true
    | _ -> false

  let to_string_type t =
    match t with
    | T_mcs_chain_id -> T_string
    | _ -> panic "Unexpected type : %a" pp_typ t

  let mk_chainid_var v =
    let vname = Format.asprintf "num(%s)" v.vname in
    mkv vname (V_mcs_num v) (to_string_type v.vtyp)
      ~mode:v.vmode ~semantic:"Universal"

  let mk_chainid_var_expr e =
    match ekind e with
    | E_var (v, mode) -> mk_var (mk_chainid_var v) ~mode e.erange
    | _ -> assert false

  let print_expr _man _flow _printer _expr = ()
  let checks = []

  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_add({ekind=E_var(v, _); _} as expr) when is_type_chainid v.vtyp ->
      let expr = mk_chainid_var_expr expr in
      man.exec (mk_add expr range) flow |>
      OptionExt.return
    | S_remove({ekind=E_var(v, _); _} as expr) when is_type_chainid v.vtyp ->
      let expr = mk_chainid_var_expr expr in
      man.exec (mk_remove expr range) flow |>
      OptionExt.return
    | S_rename(({ekind=E_var _; _} as src),
               ({ekind=E_var _; _} as dst)) when is_type_chainid src.etyp &&
                                                 is_type_chainid dst.etyp ->
      let src = mk_chainid_var_expr src in
      let dst = mk_chainid_var_expr dst in
      man.exec (mk_rename src dst range) flow |>
      OptionExt.return
    | S_assign(({ekind=E_var (v, _); _} as lval), rval) when is_type_chainid v.vtyp ->
      let lval = mk_chainid_var_expr lval in
      man.eval ~translate:"Universal" rval flow >>$? fun rval flow ->
      man.exec (mk_assign lval rval range) flow |>
      OptionExt.return
    | _ -> None

  let eval expr man flow =
    let range = erange expr in
    match ekind expr with
    | E_constant( C_string s ) when is_type_chainid expr.etyp ->
      let nexpr = mk_string s range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return
    | E_constant( C_top ty ) when is_type_chainid expr.etyp ->
      let nexpr = mk_top (to_string_type ty) range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return
    | E_var(v, _) when is_type_chainid v.vtyp ->
      let nexpr = mk_chainid_var_expr expr in
      debug "eval E_var(%a : %a)" pp_var v pp_typ v.vtyp;
      debug "eval E_var... translated to %a" pp_expr_typ nexpr;
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | E_mcs_compare(e1, e2) when is_type_chainid e1.etyp &&
                                 is_type_chainid e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      switch [
        [lt e1 e2 range],
        (fun flow ->
           man.eval (mk_constant ~etyp:T_mcs_int (C_int Z.minus_one) range) flow);
        [eq e1 e2 range],
        (fun flow ->
           man.eval (mk_constant ~etyp:T_mcs_int (C_int Z.zero) range) flow);
        [gt e1 e2 range],
        (fun flow ->
           man.eval (mk_constant ~etyp:T_mcs_int (C_int Z.one) range) flow)
      ] man flow |> OptionExt.return

    | _ -> None

  let ask _ _ _ = None

  let refine _ _ _ = assert false

  let merge _ _ = assert false
end

let () = Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
