open Mopsa
open Universal.Ast
open Ast
open Unstacked_ast
open M_common

module Domain = struct

  module Raw_map = struct
    type t =
      | V_bot
      | V_empty
      | V_map
      | V_top

    let print fmt = function
      | V_bot -> pp_string fmt "⊥"
      | V_empty -> pp_string fmt "empty_map"
      | V_map -> pp_string fmt "map"
      | V_top -> pp_string fmt "⊤"

    let bottom = V_bot
    let top = V_top

    let is_bottom = function
      | V_bot -> true
      | _ -> false

    let subset x y =
      match x, y with
      | V_bot, _ -> true
      | _, V_bot -> false
      | V_top, _ -> false
      | _, V_top -> true
      | V_empty, V_empty
      | V_map, V_map -> true
      | V_empty, V_map
      | V_map, V_empty -> false

    let join x y =
      match x, y with
      | v, V_bot
      | V_bot, v -> v
      | V_top, _
      | _, V_top -> V_top
      | V_empty, V_empty -> V_empty
      | V_map, V_map -> V_map
      | V_empty, V_map
      | V_map, V_empty -> V_top

    let meet x y =
      match x, y with
      | _, V_bot
      | V_bot, _ -> V_bot
      | V_top, y' -> y'
      | x', V_top -> x'
      | V_empty, V_empty -> V_empty
      | V_map, V_map -> V_map
      | V_empty, V_map
      | V_map, V_empty-> V_bot

    let widen _ x y = join x y

  end

  module M = Framework.Lattices.Partial_map.Make(Var)(Raw_map)
  include M

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.maps"
    end)

  let print_state printer a =
    pprint ~path:[Key "maps"] printer (pbox M.print a)

  let print_expr _ _ _ _ = ()

  let init _ man flow =
    set_env T_cur M.empty man flow

  let checks = []


  (****************************************************************************
   * Helpers
   ***************************************************************************)
  let map_size_type = T_mcs_int

  let is_type_map t =
    match t with
    | T_mcs_map _ -> true
    | _ -> false

  let key_type ty =
    match ty with
    | T_mcs_map(kty, _) -> kty
    | _ -> panic "Expected map type, got : %a" pp_typ ty

  let value_type ty =
    match ty with
    | T_mcs_map(_, vty) -> vty
    | _ -> panic "Expected map type, got : %a" pp_typ ty

  let mk_var_map_key var =
    let ty =
      match var.vtyp with
      | T_mcs_map (kty, _) -> kty
      | ty -> panic "Expected map type, got : %a" pp_typ ty
    in
    mk_var_ghost ~mode:WEAK ty var "map_key"

  let mk_var_map_value var =
    let ty =
      match var.vtyp with
      | T_mcs_map (_, vty) -> vty
      | ty -> panic "Expected map type, got : %a" pp_typ ty
    in
    mk_var_ghost ~mode:WEAK ty var "map_value"

  let mk_var_map_size ?(mode=STRONG) var =
    let () =
      match var.vtyp with
      | T_mcs_map _ -> ()
      | ty -> panic "Expected map type, got : %a" pp_typ ty
    in
    mk_var_ghost ~mode map_size_type var "map_size"

  let update_aux_vars old_state new_state var range man flow =
    debug "update_aux_vars %a: %a -> %a"
      pp_var var
      (format Raw_map.print) old_state
      (format Raw_map.print) new_state
    ;
    let open Raw_map in
    match old_state, new_state with
    | V_bot, V_map
    | V_bot, V_top
    | V_empty, V_map
    | V_empty, V_top ->
      let key = mk_var (mk_var_map_key var) range in
      let value = mk_var (mk_var_map_value var) range in
      let size = mk_var (mk_var_map_size var) range in
      man.exec (mk_add key range) flow >>$ fun () flow ->
      man.exec (mk_add value range) flow >>$ fun () flow ->
      man.exec (mk_add size range) flow
    | V_map, V_empty
    | V_top, V_empty
    | V_map, V_bot
    | V_top, V_bot ->
      let key = mk_var (mk_var_map_key var) range in
      let value = mk_var (mk_var_map_value var) range in
      let size = mk_var (mk_var_map_size var) range in
      man.exec (mk_remove key range) flow >>$ fun () flow ->
      man.exec (mk_remove value range) flow >>$ fun () flow ->
      man.exec (mk_remove size range) flow
    | (V_empty|V_bot), (V_empty|V_bot)
    | (V_top|V_map), (V_top|V_map) ->
      Post.return flow

  let add2 var mode old_state new_state map =
    match var_mode var mode with
    | STRONG -> new_state, add var new_state map
    | WEAK ->
      let new_state = Raw_map.join old_state new_state in
      new_state, add var new_state map

  (* Determines the mode to use
   * uniquelly for first time assignements *)
  let weak_update_mode old_state var mode =
    if var_mode var mode = STRONG then
      Some STRONG
    else if Raw_map.subset old_state Raw_map.V_empty then
      Some STRONG
    else
      None

  (****************************************************************************
   * Transition functions
   ***************************************************************************)
  let assign_top var mode range man flow =
    let map = get_env T_cur man flow in
    let old_state =
      match find_opt var map with
      | None -> panic "assign_top: Not found: %a" pp_var var
      | Some o -> o
    in
    let new_state = Raw_map.V_top in
    let new_state, map = add2 var mode old_state new_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state new_state var range man flow >>$ fun () flow ->
    let key = mk_var (mk_var_map_key var) range in
    let value = mk_var (mk_var_map_value var) range in
    let size = mk_var ~mode (mk_var_map_size var) range in
    man.exec (mk_assign key (mk_top key.etyp range) range) flow >>% fun flow ->
    man.exec (mk_assign value (mk_top value.etyp range) range) flow >>% fun flow ->
    man.exec (mk_assign size (mk_top map_size_type range) range) flow

  let assign_constant var mode l range man flow =
    let map = get_env T_cur man flow in
    let old_state =
      match find_opt var map with
      | None -> panic "Not found: %a" pp_var var
      | Some o -> o
    in
    let kty = key_type var.vtyp in
    let vty = value_type var.vtyp in
    match l with
    | [] ->
      let new_state = Raw_map.V_empty in
      let new_state, map = add2 var mode old_state new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state (find var map) var range man flow
    | (k, v)::tl ->
      let new_state = Raw_map.V_map in
      let new_state, map = add2 var mode old_state new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state (find var map) var range man flow >>% fun flow ->
      (* update size *)
      let size = mk_var (mk_var_map_size var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign size (mk_int ~typ:map_size_type (List.length l) range) range) flow >>% fun flow ->
      (* update keys and values *)
      let first_key = mk_var (mk_var_map_key var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      let first_value = mk_var (mk_var_map_value var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign first_key (mk_constant ~etyp:kty k range) range) flow >>%
      man.exec (mk_assign first_value (mk_constant ~etyp:vty v range) range) >>% fun flow ->
      (* Assignment of the tail elements *)
      let key = mk_var (mk_var_map_key var) range in
      let value = mk_var (mk_var_map_value var) range in
      List.fold_left
        (fun acc (k, v) ->
           acc >>%
           man.exec (mk_assign key (mk_constant ~etyp:kty k range) range) >>%
           man.exec (mk_assign value (mk_constant ~etyp:vty v range) range)
        ) (Post.return flow) tl


  let assign_variable var mode src range man flow =
    let map = get_env T_cur man flow in
    let old_state = find var map in
    let src_state = find src map in
    let new_state, map = add2 var mode old_state src_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state new_state var range man flow >>% fun flow ->
    (* if var is V_map or V_top, assign the key, value and size variables *)
    if Raw_map.subset Raw_map.V_map src_state then
      let src_key = mk_var (mk_var_map_key src) range in
      let src_value = mk_var (mk_var_map_value src) range in
      let src_size = mk_var (mk_var_map_size src) range in
      (* Strong update key, value and size if variables were not defined *)
      let dst_key = mk_var (mk_var_map_key var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      let dst_value = mk_var (mk_var_map_value var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      let dst_size = mk_var (mk_var_map_size var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign dst_key src_key range) flow >>%
      man.exec (mk_assign dst_value src_value range) >>%
      man.exec (mk_assign dst_size src_size range)
    else
      Post.return flow


  let assign_add var mode key value range man flow =
    let map = get_env T_cur man flow in
    let old_state = find var map in
    let new_state = Raw_map.V_map in
    let new_state, map = add2 var mode old_state new_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state new_state var range man flow >>% fun flow ->
    if Raw_map.subset old_state Raw_map.V_empty then
      (* if the map was empty, the variables were not present, but have been
       * added by update_aux_vars, so strong-update them *)
      let vsize = mk_var (mk_var_map_size var) range in
      let vkey = mk_var (mk_var_map_key var) ~mode:(Some STRONG) range in
      let vvalue = mk_var (mk_var_map_value var) ~mode:(Some STRONG) range in
      let size = mk_mcs_int ~typ:map_size_type 1 range in
      man.exec (mk_assign vsize size range) flow >>%
      man.exec (mk_assign vkey key range) >>%
      man.exec (mk_assign vvalue value range)
    else
      let vsize = mk_var ~mode (mk_var_map_size var) range in
      let vkey = mk_var (mk_var_map_key var) range in
      let vvalue = mk_var (mk_var_map_value var) range in
      let cmp = mk_mcs_compare vkey key range in
      (* Test whether key is present in the map by comparing to the smashed
       * variable *)
      assume (mk_mcs_binop ~etyp:T_mcs_bool cmp O_ne (mk_mcs_int 0 range) range)
        ~fthen:(fun flow ->
            (* if absent *)
            man.exec (mk_assign vkey key range) flow >>%
            man.exec (mk_assign vvalue value range) >>%
            man.exec (mk_assign vsize (mk_mcs_incr vsize range) range)
          )
        ~felse:(fun flow ->
            man.exec (mk_assign vvalue value range) flow
          ) man flow


  let assign_remove var mode key range man flow =
    let map = get_env T_cur man flow in
    let old_state = find var map in
    if Raw_map.subset old_state Raw_map.V_empty then
      (* If the map is empty, do nothing *)
      Post.return flow
    else
      (
        let vsize = mk_var ~mode (mk_var_map_size var) range in
        let vkey = mk_var (mk_var_map_key var) range in
        let sz i = mk_mcs_compare vsize (mk_int ~typ:map_size_type i range) range in
        let present = mk_mcs_compare vkey key range in
        switch [
          (* if the size is 0, do nothing *)
          [mk_mcs_binop ~etyp:T_mcs_bool (sz 0) O_eq (mk_mcs_int 0 range) range],
          (fun flow ->
             Post.return flow
          );
          (* if the size is 1 *)
          [mk_mcs_binop ~etyp:T_mcs_bool (sz 1) O_eq (mk_mcs_int 0 range) range],
          (fun flow ->
             assume (mk_mcs_binop ~etyp:T_mcs_bool present O_ne (mk_mcs_int 0 range) range)
               (* and the key is not present in the keys, do nothing *)
               ~fthen:(fun flow ->
                   Post.return flow
                 )
               (* and the key is present in the keys, return an empty map *)
               ~felse:(fun flow ->
                   let map = get_env T_cur man flow in
                   let old_state = find var map in
                   let new_state = Raw_map.V_empty in
                   let new_state, map = add2 var mode old_state new_state map in
                   let flow = set_env T_cur map man flow in
                   update_aux_vars old_state (find var map) var range man flow
                 )
               man flow
          );
          (* if size > 1 *)
          [mk_mcs_binop ~etyp:T_mcs_bool (sz 1) O_gt (mk_mcs_int 0 range) range],
          (fun flow ->
             assume (mk_mcs_binop ~etyp:T_mcs_bool present O_ne (mk_mcs_int 0 range) range)
               (* and the key is not present in the keys, do nothing *)
               ~fthen:(fun flow ->
                   Post.return flow
                 )
               (* and the key is present in the keys, remove from the map, ie
                * we just decrement size *)
               ~felse:(fun flow ->
                   let expr = mk_mcs_binop
                     ~etyp:map_size_type
                     vsize O_minus (mk_int ~typ:map_size_type 1 range)
                     range
                   in
                   man.exec (mk_assign vsize expr range) flow
                 ) man flow
          )
        ] man flow
      )

  (****************************************************************************
   * Loops
   ***************************************************************************)
  let map_iter omap stm sp range man flow =
    let vnmap = mk_range_attr_var range "map" omap.etyp in
    let nmap = mk_var vnmap range in
    let kiter = mk_var (mk_var_map_key vnmap) range in
    let viter = mk_var (mk_var_map_value vnmap) range in
    let iter = mk_var (mk_stackpos
                         (T_mcs_pair(key_type omap.etyp, value_type omap.etyp))
                         (sp-1)) range
    in
    man.exec (mk_rename omap nmap range) flow >>% fun flow ->
    let loop =
      mk_while
        (mk_top T_mcs_bool range)
        (mk_block
           [
             (* XXX: should we add a E_map_nth expression ? *)
             mk_add iter range;
             mk_assign iter
               (Unstacked_ast.mk_pair kiter viter range) range;
             stm;
           ]
           range
        )
        range
    in
    man.exec loop flow >>% fun flow ->
    man.exec (mk_remove nmap range) flow

  let map_map map t_omap stm sp range man flow =
    (* use the input map renamed outside the stack namespace *)
    let vtmp_map = mk_range_attr_var range "imap" map.etyp in
    let tmp_map = mk_var vtmp_map range in
    (* get the smashed key and values from the map *)
    let kiter = mk_var (mk_var_map_key vtmp_map) range in
    let viter = mk_var (mk_var_map_value vtmp_map) range in
    (* builds a pair as argument to the body stm *)
    let iter_in = mk_var (mk_stackpos
                         (T_mcs_pair(key_type map.etyp, value_type map.etyp))
                         (sp-1)) range
    in
    (* the variables tracks the body stm output *)
    let iter_out = mk_var (mk_stackpos (value_type t_omap) (sp-1)) range in
    let map_tmp_out = mk_var (mk_range_attr_var range "omap" t_omap) range in
    (* rename the input map outside the stack namespace *)
    man.exec (mk_rename map tmp_map range) flow >>%
    man.exec (mk_add map_tmp_out range) >>%
    man.exec (mk_assign map_tmp_out (Unstacked_ast.mk_map_empty ~etyp:t_omap range) range) >>% fun flow ->
    let loop =
      mk_while
        (mk_top T_mcs_bool range)
        (mk_block
           [
             (* add and assign the input pair before executing the body *)
             mk_add iter_in range;
             mk_assign iter_in (Unstacked_ast.mk_pair kiter viter range) range;
             (* execute the body *)
             stm;
             (* add the result to the result map *)
             mk_assign map_tmp_out
               (Unstacked_ast.mk_map_add ~map:map_tmp_out ~key:kiter ~value:iter_out range)
               range;
             mk_remove iter_out range;
           ]
           range
        )
        range
    in
    let map_out = mk_var (mk_stackpos t_omap (sp-1)) range in
    man.exec loop flow >>%
    man.exec (mk_remove tmp_map range) >>%
    man.exec (mk_rename map_tmp_out map_out range)

  (****************************************************************************
   * Main
   ***************************************************************************)
  let exec stmt man flow =
    let range = srange stmt in
    match skind stmt with
    | S_add ({ekind=E_var(var, mode); _}) when is_type_map var.vtyp ->
      (* adds key, value, and size variables to environmnent *)
      debug "m_map.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> Raw_map.V_bot
        | Some _ -> panic "m_map: adding an already existing variable: %a" pp_var var
      in
      let new_state = Raw_map.top in
      let new_state, map = add2 var mode old_state new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow >>$? fun () flow ->
      Post.return flow |>
      OptionExt.return

    | S_remove({ekind=E_var(var, _); etyp=T_mcs_map(kty, vty); _}) ->
      (* remove size from environment, and remove item if necessary *)
      debug "m_map.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> panic "m_map: removing an non-existing variable: %a" pp_var var
        | Some o -> o
      in
      let new_state = Raw_map.V_bot in
      let map = remove var map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    | S_rename({ekind=E_var(src_var, _); _}, {ekind=E_var(dst_var, _); _})
      when is_type_map src_var.vtyp &&
           is_type_map dst_var.vtyp ->
      check_types src_var.vtyp dst_var.vtyp;
      (if compare_var src_var dst_var = 0 then
         Post.return flow
       else
         (
           let map = get_env T_cur man flow in
           let value = M.find src_var map in
           let map = M.add dst_var value map in
           let map = M.remove src_var map in
           let flow = set_env T_cur map man flow in
           (if Raw_map.subset V_map value then
              let src_key = mk_var (mk_var_map_key src_var) range in
              let dst_key = mk_var (mk_var_map_key dst_var) range in
              let src_value = mk_var (mk_var_map_value src_var) range in
              let dst_value = mk_var (mk_var_map_value dst_var) range in
              let src_size = mk_var (mk_var_map_size src_var) range in
              let dst_size = mk_var (mk_var_map_size dst_var) range in
              man.exec (mk_rename src_key dst_key range) flow >>% fun flow ->
              man.exec (mk_rename src_value dst_value range) flow >>% fun flow ->
              man.exec (mk_rename src_size dst_size range) flow
            else
              Post.return flow
           )
         )
      ) |>
      OptionExt.return

    | S_assign({ekind=E_var(var, mode); _}, expr)
      when is_type_map var.vtyp ->
      check_types var.vtyp expr.etyp;
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_constant (C_top (T_mcs_map _)) ->
         assign_top var mode range man flow
       | E_constant (C_mcs_map l) ->
         assign_constant var mode l range man flow
       | E_var(src, _) when compare_var var src = 0 ->
         Post.return flow
       | E_var(src, _) ->
         assign_variable var mode src range man flow
       | E_mcs_map_add { map; key; value } ->
         man.exec (mk_assign (mk_var ~mode var range) map range) flow >>%
         assign_add var mode key value range man
       | E_mcs_map_remove { map; key } ->
         man.exec (mk_assign (mk_var ~mode var range) map range) flow >>%
         assign_remove var mode key range man
       | _ -> panic "Unexpected map.s_assign(_, %a)" pp_expr expr
      ) |>
      OptionExt.return

    | S_michelson_instr { instr = S_mcs_map_iter stm; sp; bef; _} ->
      let t_map = stack_get_type_spi bef (sp-1) in
      let () =
        match t_map with
        | T_mcs_map _ -> ()
        | _ -> panic "Expected map type, got %a" pp_typ t_map
      in
      let vmap = mk_stackpos t_map (sp-1) in
      let cur = get_env T_cur man flow in
      (match find_opt vmap cur with
       | None -> Flow.bottom_from flow |> Post.return
       | Some v ->
         (match v with
          | V_bot ->
            Flow.bottom_from flow |> Post.return
          | V_empty ->
            man.exec (mk_remove (mk_var vmap range) range) flow
          | V_map
          | V_top ->
            map_iter (mk_var vmap range) stm sp range man flow
         )
      ) |>
      OptionExt.return

    | S_michelson_instr { instr = S_mcs_map_map stm; sp; bef; aft; _} ->
      let t_imap = stack_get_type_spi bef (sp-1) in
      let t_omap = stack_get_type_spi aft (sp-1) in
      let () =
        match t_imap with
        | T_mcs_map _ -> ()
        | _ -> panic "Expected map type, got %a" pp_typ t_imap
      in
      let v_imap = mk_stackpos t_imap (sp-1) in
      let cur = get_env T_cur man flow in
      (match find_opt v_imap cur with
       | None -> Flow.bottom_from flow |> Post.return
       | Some v ->
         (match v with
          | V_bot ->
            Flow.bottom_from flow |> Post.return
          | V_empty ->
            Post.return flow
          | V_map
          | V_top ->
            map_map (mk_var v_imap range) t_omap stm sp range man flow
         )
      ) |>
      OptionExt.return

    | _ -> None


  let exec stmt man flow =
    let map = get_env T_cur man flow in
    if is_bottom map then
      Post.return flow |> OptionExt.return
    else
      exec stmt man flow


  let eval expr man flow =
    let range = erange expr in
    match ekind expr with
    | E_mcs_map_mem { map; key } ->
      begin match map with
        | { ekind = E_var(var, _); etyp=T_mcs_map(kty, _); _} ->
          let size = mk_map_size (mk_var var range) range in
          let okey = mk_var (mk_var_map_key var) range in
          let is_key_there =
            let ecompare = Unstacked_ast.mk_mcs_compare key okey range in
            mk_binop ~etyp:T_mcs_bool ecompare O_eq (mk_mcs_int 0 range) range
          in
          switch [
            [eq size (mk_zero range) range],
            (fun flow -> man.eval (mk_mcs_false range) flow);
            [gt size (mk_zero range) range],
            (fun flow ->
               assume is_key_there
                 ~fthen:(fun flow -> man.eval (mk_mcs_true range) flow)
                 ~felse:(fun flow -> man.eval (mk_mcs_false range) flow)
                 man flow
            )
          ] man flow |> OptionExt.return
        | _ -> man.eval (mk_top T_mcs_bool range) flow |> OptionExt.return
      end

    | E_mcs_map_get { map; key } ->
      man.eval map flow >>$? fun map flow ->
      man.eval key flow >>$? fun key flow ->
      begin match map with
        | { ekind = E_var(var, _); etyp=T_mcs_map(kty, vty); _} ->
          (* call size() for this map *)
          let size = mk_map_size (mk_var var range) range in
          let okey = mk_var (mk_var_map_key var) range in
          let ovalue = mk_var (mk_var_map_value var) range in
          assert(compare_typ kty okey.etyp = 0);
          assert(compare_typ vty ovalue.etyp = 0);
          let is_key_there =
            let ecompare = Unstacked_ast.mk_mcs_compare key okey range in
            mk_binop ~etyp:T_mcs_bool ecompare O_eq (mk_mcs_int 0 range) range
          in
          switch [
            (* Case empty map *)
            [eq size (mk_zero range) range],
            (fun flow ->
               Eval.singleton (Unstacked_ast.mk_none (T_mcs_option vty) range) flow);
            (* Case non empty map *)
            [gt size (mk_zero range) range],
            (fun flow ->
               assume is_key_there
                 ~fthen:(fun flow ->
                     man.eval (Unstacked_ast.mk_some ovalue range) flow
                   )
                 ~felse:(fun flow ->
                     man.eval (Unstacked_ast.mk_none (T_mcs_option vty) range) flow
                   ) man flow
            )
          ] man flow |> OptionExt.return
        | { etyp=T_mcs_map(_, vty); _ } ->
          warn "map_get(%a, %a): return ⊤" pp_expr_typ map pp_expr_typ key;
          man.eval (mk_top (T_mcs_option vty) range) flow |>
          OptionExt.return
        | _ -> panic "Unexpected type for %a" pp_expr_typ expr
      end

    | E_mcs_map_size { map } ->
      begin match map with
        | { ekind = E_var(var, _); etyp = T_mcs_map _; _} ->
          let cur = get_env T_cur man flow in
          (let open Raw_map in
           match M.find_opt var cur with
           | None -> Eval.empty flow |> OptionExt.return
           | Some v ->
             (match v with
              | V_bot -> Eval.empty flow
              | V_empty -> man.eval (mk_mcs_int 0 range) flow
              | V_map ->
                let size = mk_var (mk_var_map_size var) range in
                man.eval size flow
              | V_top ->
                let size = mk_var (mk_var_map_size var) range in
                Eval.join
                  (man.eval size flow)
                  (man.eval (mk_mcs_int 0 range) flow)
             ) |> OptionExt.return
          )
        | _ ->
          warn_at range "Unhandled: map_size(%a)" pp_expr map;
          Eval.singleton (mk_top T_mcs_int range) flow |> OptionExt.return
      end

    | _ -> None

  let ask _ _ _ = None
  let refine _ _ _ = assert false

  let merge _pre (a1, e1) (a2, e2) =
    debug "merge:";
    debug "  a1:@\n%a@\n e1:@\n%a" (format M.print) a1 pp_effect e1;
    debug "  a2:@\n%a@\n e2:@\n%a" (format M.print) a2 pp_effect e2;
    let a1, a2 = generic_merge
        ~add:M.add
        ~find:M.find
        ~remove:M.remove
        (a1, e1) (a2, e2)
    in
    let result = M.meet a1 a2 in
    debug "=> %a" (format M.print) result;
    result

end

let () =
  Framework.Sig.Abstraction.Domain.register_standard_domain (module Domain)
