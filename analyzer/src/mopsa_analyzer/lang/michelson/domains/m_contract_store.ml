open Mopsa
open Ast

module Contract = struct
  open Bot_top

  type t = contract with_bot_top

  let top = TOP
  let bottom = BOT

  let is_bottom v =
    match v with
    | BOT -> true
    | _ -> false

  let join x y =
    match x, y with
    | TOP, _
    | _, TOP -> TOP
    | BOT, v
    | v, BOT -> v
    | Nbt c1, Nbt c2 ->
      if compare_contract c1 c2 = 0 then
        Nbt c1
      else
        TOP

  let meet x y =
    match x, y with
    | TOP, v
    | v, TOP -> v
    | BOT, _
    | _, BOT -> BOT
    | Nbt c1, Nbt c2 ->
      if compare_contract c1 c2 = 0 then
        Nbt c1
      else
        TOP

  let widen _ = join

  let subset =
    bot_top_included (fun c1 c2 ->
        compare_contract c1 c2 = 0
      )

  let print printer v =
    unformat (bot_top_fprint pp_contract_short) printer v

end

module Domain = struct
  include GenStatelessDomainId (struct
    let name = "michelson.contract_store"
  end)
  open Common.Addresses

  module M = Framework.Lattices.Partial_map.Make(Address)(Contract)
  include M

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.contracts_store"
    end)

  let print_expr _ _ _ _ = ()

  let print_state printer a =
    pprint ~path:[Key "store"] printer (pbox M.print a)

  let checks = []

  let init _ man flow =
    set_env T_cur M.empty man flow

  let exec_contract_initialization contract (context:Ast.contract_context) range man flow =
    let address =
      match context.address with
      | Some addr -> addr
      | None ->
        (* FIXME: should not happen because we specificly drop the contracts
         * without address in Mcs_store_add_contract we need to create
         * an arbitrary unique identifier bound to SELF
         *)
        panic "Should not happen"
    in
    (* Use specified initial storage or top *)
    let initial_storage =
      match context.storage with
      | None -> mk_top contract.storage_ty range
      | Some storage -> mk_constant ~etyp:contract.storage_ty storage range
    in
    let initial_balance =
      match context.balance with
      | None -> mk_top T_mcs_mutez range
      | Some balance -> Universal.Ast.mk_z ~typ:T_mcs_mutez balance range
    in
    (* adds a named variable for the storage *)
    let storage = mk_var
        (mk_var_named ~address contract.storage_ty "storage")
        range
    in
    let balance = mk_var
      (mk_var_named ~address T_mcs_mutez "balance")
      range
    in
    (* adds a named variable for operations *)
    man.exec (mk_add storage range) flow >>%
    man.exec (mk_add balance range) >>%
    man.exec (mk_assign storage initial_storage range) >>%
    man.exec (mk_assign balance initial_balance range)

  let exec stmt man flow =
    let range = srange stmt in
    match skind stmt with
    | S_mcs_store_add_contracts contracts ->
      let map = get_env T_cur man flow in
      let map, flow =
        List.fold_left (fun (map, flow) (contract, (context: contract_context)) ->
            match context.address with
            | None ->
              (* FIXME allow symbolic handling of address *)
              warn "No address specified for contract '%s'. Contract won't be called" contract.filename;
              map, flow
            | Some address ->
              let address = Address.Contract (address, "default") in
              let flow = exec_contract_initialization contract context range man flow |> post_to_flow man in
              M.add address (Nbt contract) map, flow
          ) (map, flow) contracts
      in
      set_env T_cur map man flow |>
      Post.return |>
      OptionExt.return

    | S_mcs_store_exec_contract { address; parameter; amount } ->
      let map = get_env T_cur man flow in
      let addr, entry = match address with
        | Address _ -> assert false
        | Contract (addr, entry) -> addr, entry
      in
      (match find_opt address map with
       | None ->
         warn "call to contract %a, which is unknown. Provide the .tz with --extern to get (possibly) better results"
           (format Address.print) address;
         let stm = mk_stmt
             (S_mcs_inter_exec {
                 contract = None;
                 address = None;
                 entry = None;
                 parameter;
                 amount;
               })
             range
         in
         man.exec stm flow
       | Some v ->
         (match v with
          | TOP -> assert false
          | BOT -> assert false
          | Nbt contract ->
            let stm = mk_stmt
                (S_mcs_inter_exec {
                    contract = Some contract;
                    address = Some addr;
                    entry = Some entry;
                    parameter;
                    amount;
                  })
                range
            in
            man.exec stm flow
         )
      ) |>
      OptionExt.return
    | _ -> None


  let eval _ _ _ = None

  let ask _ _ _ = None
  let refine _ _ _ = assert false
  let merge _ _ = assert false

end

let () =
  Framework.Sig.Abstraction.Domain.register_standard_domain (module Domain)
