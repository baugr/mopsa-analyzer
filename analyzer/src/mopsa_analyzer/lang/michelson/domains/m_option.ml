open Mopsa
open Ast
open Universal.Ast
open Unstacked_ast
open M_common

module Domain = struct

  module Raw_option = struct
    type t =
      | V_some
      | V_none
      | V_bot
      | V_top

    let print fmt = function
      | V_some -> pp_string fmt "some"
      | V_none -> pp_string fmt "none"
      | V_bot  -> pp_string fmt "⊥"
      | V_top  -> pp_string fmt "⊤"

    let bottom = V_bot
    let top = V_top

    let is_bottom = function
      | V_bot -> true
      | _ -> false

    let subset x y =
      match x, y with
      | V_bot, _ -> true
      | _, V_bot -> false
      | V_top, _ -> false
      | _, V_top -> true
      | V_some, V_some -> true
      | V_none, V_none -> true
      | V_some, V_none -> false
      | V_none, V_some -> false

    let join x y =
      match x, y with
      | v, V_bot
      | V_bot, v -> v
      | V_top, _
      | _, V_top -> V_top
      | V_some, V_some -> V_some
      | V_none, V_none -> V_none
      | V_some, V_none
      | V_none, V_some -> V_top

    let meet x y =
      match x, y with
      | _, V_bot
      | V_bot, _ -> V_bot
      | V_top, y' -> y'
      | x', V_top -> x'
      | V_some, V_some -> V_some
      | V_none, V_none -> V_none
      | V_some, V_none
      | V_none, V_some -> V_bot

    let widen _ x y = join x y

  end

  let is_type_option t =
    match t with
    | T_mcs_option _ -> true
    | _ -> false

  let type_opt t =
    match t with
    | T_mcs_option t -> t
    | _ -> panic "Unexpected type, expected option, got : %a" pp_typ t

  let mk_vopt var =
    let name = "opt" in
    let mode = vmode var in
    let typ =
      match var.vtyp with
      | T_mcs_option t -> t
      | t -> panic "Expected type option, got %a" pp_typ t
    in
    mk_var_ghost ~mode typ var name

  module M = Framework.Lattices.Partial_map.Make(Var)(Raw_option)
  include M

  let find v map = match find_opt v map with
    | None -> panic "Cannot find : %a" pp_var v
    | Some v -> v

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.options"
    end)

  let print_state printer a =
    pprint ~path:[Key "options"] printer (pbox M.print a)

  let print_expr _ _ _ _ = ()

  let init _ man flow =
    set_env T_cur M.empty man flow

  let checks = []

  (****************************************************************************
   * Helpers
   ***************************************************************************)
  let update_aux_vars old_state new_state var range man flow =
    debug "update_aux_vars %a: %a -> %a"
      pp_var var
      (format Raw_option.print) old_state
      (format Raw_option.print) new_state
    ;
    let open Raw_option in
    match old_state, new_state with
    | (V_bot|V_none),(V_some|V_top) ->
      let opt = mk_var (mk_vopt var) range in
      man.exec (mk_add opt range) flow
    | (V_some|V_top),(V_bot|V_none) ->
      let opt = mk_var (mk_vopt var) range in
      man.exec (mk_remove opt range) flow
    | (V_none|V_bot), (V_none|V_bot)
    | (V_top|V_some), (V_top|V_some) ->
      Post.return flow

  let add2 var mode old_state new_state map =
    match var_mode var mode with
    | STRONG -> new_state, add var new_state map
    | WEAK ->
      let new_state = Raw_option.join old_state new_state in
      new_state, add var new_state map

  let weak_update_mode old_state var mode =
    if var_mode var mode = STRONG then
      Some STRONG
    else if Raw_option.subset old_state Raw_option.V_none then
      Some STRONG
    else
      None

  (****************************************************************************
   * Transfer functions
   ***************************************************************************)
  let exec stmt man flow =
    let range = srange stmt in
    match skind stmt with
    (* Declare the variable *)
    | S_add ({ ekind=E_var(var, _); etyp; _}) when is_type_option var.vtyp ->
      check_types var.vtyp etyp;
      debug "m_option.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | Some _ ->
          panic "m_option: adding variable already existing: %a" pp_var var
        | None -> Raw_option.V_bot
      in
      let new_state = Raw_option.V_top in
      let map = add var new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    (* Remove the variable *)
    | S_remove ({ ekind=E_var(var, _); etyp; _ }) when is_type_option var.vtyp ->
      check_types var.vtyp etyp;
      debug "m_option.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match find_opt var map with
        | Some v -> v
        | None -> panic "m_option: removing variable not added: %a" pp_var var
      in
      let new_state = Raw_option.V_bot in
      let map = remove var map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    | S_rename ({ekind=E_var(src, _); etyp; _ },
                {ekind=E_var(dst, _); etyp=etypd}) when is_type_option dst.vtyp ->
      check_types src.vtyp etyp;
      check_types dst.vtyp src.vtyp;
      debug "m_option.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let new_state = find src map in
      let map = add dst new_state map in
      let map = remove src map in
      let flow = set_env T_cur map man flow in
      (if Raw_option.(subset V_some new_state) then
         let src_opt = mk_var (mk_vopt src) range in
         let dst_opt = mk_var (mk_vopt dst) range in
         man.exec (mk_rename src_opt dst_opt range) flow
       else
         Post.return flow
      ) |>
      OptionExt.return

    (* Assignment of option to variable *)
    | S_assign({ekind=E_var(var, mode); etyp; _}, expr) when is_type_option var.vtyp ->
      check_types var.vtyp etyp;
      check_types var.vtyp expr.etyp;
      man.eval expr flow >>$? fun expr flow ->
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> panic "Variable was not added: %a" pp_var var
        | Some o -> o
      in
      (match ekind expr with
       | E_constant (C_top ty) ->
         check_types var.vtyp ty;
         let new_state = Raw_option.V_top in
         let new_state, map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         let vopt = mk_var ~mode:(weak_update_mode old_state var mode)
             (mk_vopt var) range
         in
         man.exec (mk_assign vopt (mk_top vopt.etyp range) range) flow |>
         OptionExt.return
       | E_constant (C_option c) ->
         (match c with
          | None ->
            let new_state = Raw_option.V_none in
            let new_state, map = add2 var mode old_state new_state map in
            let flow = set_env T_cur map man flow in
            update_aux_vars old_state new_state var range man flow
          | Some v ->
            let new_state = Raw_option.V_some in
            let new_state, map = add2 var mode old_state new_state map in
            let flow = set_env T_cur map man flow in
            update_aux_vars old_state new_state var range man flow >>% fun flow ->
            let vopt = mk_var ~mode:(weak_update_mode old_state var mode)
                (mk_vopt var) range
            in
            let expr = mk_constant ~etyp:vopt.etyp v range in
            man.exec (mk_assign vopt expr range) flow
         ) |>
         OptionExt.return
       | E_mcs_none ->
         let new_state = Raw_option.V_none in
         let new_state, map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow |>
         OptionExt.return
       | E_mcs_some expr ->
         let new_state = Raw_option.V_some in
         let new_state, map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         let vopt = mk_var ~mode:(weak_update_mode old_state var mode) (mk_vopt var) range in
         man.exec (mk_assign vopt expr range) flow |>
         OptionExt.return
       | E_var (src, _) ->
         check_types src.vtyp var.vtyp;
         let src_state = find src map in
         let new_state, map = add2 var mode old_state src_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         (match src_state with
          | Raw_option.V_bot -> Post.return (Flow.bottom_from flow)
          | Raw_option.V_none -> Post.return flow
          | Raw_option.V_some ->
            let src_opt = mk_var (mk_vopt src) range in
            let dst_opt = mk_var
                ~mode:(weak_update_mode old_state var mode)
                (mk_vopt var) range
            in
            man.exec (mk_assign dst_opt src_opt range) flow
          | Raw_option.V_top ->
            let src_opt = mk_var (mk_vopt src) range in
            let dst_opt = mk_var
                ~mode:(weak_update_mode old_state var mode)
                (mk_vopt var) range
            in
            man.exec (mk_assign dst_opt src_opt range) flow
         ) |> OptionExt.return
       | _ -> None
      )

    | _ -> None

  let exec stmt man flow =
    let map = get_env T_cur man flow in
    if is_bottom map then
      Post.return flow |>
      OptionExt.return
    else
      exec stmt man flow

  let eval expr man flow =
    let range = erange expr in
    match expr with
    | { ekind = E_mcs_is_none (e); _ } ->
      check_types expr.etyp T_mcs_bool;
      assert (is_type_option e.etyp);
      man.eval e flow >>$? fun e flow ->
      (match ekind e with
       | E_constant (C_top ty) when is_type_option ty ->
         man.eval (mk_top T_mcs_bool range) flow |>
         OptionExt.return
       | E_constant (C_option c) ->
         (match c with
          | Some _ ->
            man.eval (mk_mcs_false range) flow
          | None ->
            man.eval (mk_mcs_true range) flow
         ) |>
         OptionExt.return
       | E_mcs_none ->
         man.eval (mk_mcs_true range) flow |>
         OptionExt.return
       | E_mcs_some _ ->
         man.eval (mk_mcs_false range) flow |>
         OptionExt.return
       | E_var(var, _) ->
         assert (is_type_option e.etyp);
         check_types var.vtyp e.etyp;
         let map = get_env T_cur man flow in
         (match find var map with
          | V_bot -> Eval.empty flow
          | V_none -> man.eval (mk_mcs_true range) flow
          | V_some -> man.eval (mk_mcs_false range) flow
          | V_top -> man.eval (mk_top T_mcs_bool range) flow
         ) |>
         OptionExt.return
       | _ ->
         warn "Unhandled option expr: is_none(%a)" pp_expr_typ e;
         man.eval (mk_top T_mcs_bool range) flow |>
         OptionExt.return
      )

    | { ekind = E_mcs_unsome (e); _ } ->
      assert (is_type_option e.etyp);
      man.eval e flow >>$? fun e flow ->
      (match ekind e with
       | E_constant (C_top ty) ->
         check_types (type_opt ty) expr.etyp;
         man.eval (mk_top (type_opt ty) range) flow |>
         OptionExt.return
       | E_constant (C_option c) ->
         (match c with
          | None -> Eval.empty flow
          | Some c ->
            man.eval (mk_constant ~etyp:(type_opt expr.etyp) c range) flow
         ) |>
         OptionExt.return
       | E_var(var, _) ->
         let map = get_env T_cur man flow in
         (match find var map with
          | V_bot
          | V_none -> Eval.empty flow
          | V_some ->
            let vopt = mk_var (mk_vopt var) range in
            man.eval vopt flow
          | V_top ->
            let vopt = mk_var (mk_vopt var) range in
            Eval.join
              (man.eval vopt flow)
              (Eval.empty flow)
         ) |>
         OptionExt.return
       | E_mcs_none ->
         Eval.empty flow |>
         OptionExt.return
       | E_mcs_some(expr) ->
         man.eval expr flow |>
         OptionExt.return
       | _ ->
         warn "Unhandled option expr: unsome(%a)" pp_expr_typ expr;
         man.eval (mk_top (type_opt expr.etyp) range) flow |>
         OptionExt.return
      )

    | { ekind = E_mcs_compare(e1, e2); _ } when is_type_option e1.etyp &&
                                                is_type_option e2.etyp ->
      man.eval (mk_is_none e1 range) flow >>$? fun is_none_e1 flow ->
      man.eval (mk_is_none e2 range) flow >>$? fun is_none_e2 flow ->
      switch [
        [mk_log_and
           (eq is_none_e1 (mk_mcs_true range) range)
           (eq is_none_e2 (mk_mcs_true range) range) range],
        (fun flow ->
           man.eval (mk_mcs_int 0 range) flow
        );
        [mk_log_and
           (eq is_none_e1 (mk_true range) range)
           (eq is_none_e2 (mk_false range) range) range],
        (fun flow ->
           man.eval (mk_mcs_int 1 range) flow);
        [mk_log_and
           (eq is_none_e1 (mk_false range) range)
           (eq is_none_e2 (mk_true range) range) range],
        (fun flow ->
           man.eval (mk_mcs_int (-1) range) flow);
        [mk_log_and
           (eq is_none_e1 (mk_false range) range)
           (eq is_none_e2 (mk_false range) range) range],
        (fun flow ->
           man.eval (mk_mcs_compare
                       (mk_unsome e1 range)
                       (mk_unsome e2 range) range) flow)
      ] man flow |>
      OptionExt.return

    | _ -> None

  let ask _ _ _ = None
  let refine _ _ _ = assert false
  let merge _ _ = assert false
  let widen = widen

end

let () =
  Framework.Sig.Abstraction.Domain.register_standard_domain (module Domain)
