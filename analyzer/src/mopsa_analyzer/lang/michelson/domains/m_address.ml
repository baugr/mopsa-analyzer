open Mopsa
open Universal.Ast
open Ast
open Bot
open Unstacked_ast
open M_common
open Common.Addresses

module AddressesPower = Framework.Lattices.Powerset.Make(Address)


module Domain =
struct

  module M = Framework.Lattices.Partial_map.Make(Var)(AddressesPower)
  include M

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.addresses"
    end)

  let init _ man flow =
    set_env T_cur M.empty man flow

  let checks = []

  let universal = Semantic "Universal"

  let print_expr _ _ = assert false

  let print_state printer a =
    pprint ~path:[Key "addresses"] printer (pbox M.print a)

  let is_type_address t =
    match t with
    | T_mcs_address -> true
    | _ -> false


  let exec stmt (man : ('a, 'b) Framework.Core.Manager.man) flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_add({ekind=E_var(var, _); _}) when is_type_address var.vtyp ->
      debug "m_address.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      (match M.find_opt var map with
       | None -> ()
       | Some _ -> panic "m_address: adding an already existing variable: %a" pp_var var
      );
      debug "I'm before top";
      let new_state = AddressesPower.top in
      let map = M.add var new_state map in
      let flow = set_env T_cur map man flow in
      Post.return flow |>
      OptionExt.return

    | S_remove({ekind=E_var(var, _); _}) when is_type_address var.vtyp ->
      debug "m_address.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      (match M.find_opt var map with
       | None -> panic "m_address: removing an non-existing variable: %a" pp_var var
       | Some _ -> ()
      );
      let map = M.remove var map in
      let flow = set_env T_cur map man flow in
      Post.return flow |>
      OptionExt.return

    | S_rename({ekind=E_var(src_var, _); _}, {ekind=E_var(dst_var, _); _})
      when is_type_address src_var.vtyp &&
           is_type_address dst_var.vtyp ->
      debug "m_address.%a" pp_stmt stmt;
      (if compare_var src_var dst_var = 0 then
         Post.return flow
       else
         let map = get_env T_cur man flow in
         let state = match M.find_opt src_var map with
           | None -> panic "m_address: renaming an non-existing variable: %a" pp_var src_var
           | Some ps -> ps
         in
         let map = M.remove src_var map in
         let map = M.add dst_var state map in
         set_env T_cur map man flow |>
         Post.return
      ) |>
      OptionExt.return

    | S_assign(({ekind=E_var(var, mode); _} as lval), rval) when is_type_address var.vtyp ->
      debug "m_address.%a" pp_stmt stmt;
      check_types T_mcs_address (etyp lval);
      check_types T_mcs_address (etyp rval);
      man.eval rval flow >>$? fun rval flow ->
      (match ekind rval with
       | E_constant (C_mcs_address Address_constant (s, e))
       | E_constant (C_mcs_address Address_caller (Some (s, e))) ->
         let map = get_env T_cur man flow in
         let state = match M.find_opt var map with
           | None -> panic "m_address: assigning to non-existing variable: %a" pp_var var
           | Some state ->
             if var_mode var mode = STRONG then
               AddressesPower.(singleton (Address (s, e)))
             else
               AddressesPower.(union (singleton (Address (s, e))) state)
         in
         let map = M.add var state map in
         set_env T_cur map man flow |>
         Post.return
       | E_constant (C_mcs_address Address_caller None) ->
         let map = get_env T_cur man flow in
         let state = match M.find_opt var map with
           | None -> panic "m_address: assigning to non-existing variable: %a" pp_var var
           | Some _ -> AddressesPower.top
         in
         let map = M.add var state map in
         set_env T_cur map man flow |>
         Post.return
       | E_constant (C_mcs_contract(s, e)) ->
         let map = get_env T_cur man flow in
         let state = match M.find_opt var map with
           | None -> panic "m_address: assigning to non-existing variable: %a" pp_var var
           | Some state ->
             if var_mode var mode = STRONG then
               AddressesPower.(singleton (Contract(s, e)))
             else
               AddressesPower.(union (singleton (Contract(s, e))) state)
         in
         let map = M.add var state map in
         set_env T_cur map man flow |>
         Post.return
       | E_var(src, _) ->
         let map = get_env T_cur man flow in
         let state = match M.find_opt var map with
           | None -> panic "m_address: assigning to non-existing variable: %a" pp_var var
           | Some old_state ->
             (match M.find_opt src map with
              | None -> panic "m_address: assigning from an non-existing variable: %a" pp_var src
              | Some new_state ->
                if var_mode var mode = STRONG then
                  new_state
                else
                  AddressesPower.(union new_state old_state)
             )
         in
         let map = M.add var state map in
         set_env T_cur map man flow |>
         Post.return
       | _ -> warn "m_address: Not handled expression in assignement, setting top: %a" pp_expr rval;
         let map = get_env T_cur man flow in
         let state = match M.find_opt var map with
           | None -> panic "m_address: assigning to non-existing variable: %a" pp_var var
           | Some _ -> AddressesPower.top
         in
         let map = M.add var state map in
         set_env T_cur map man flow |>
         Post.return
      ) |>
      OptionExt.return

    | S_mcs_inter_exec_transfer_address { address; parameter; amount } ->
      man.eval address flow >>$? fun address flow ->
      (match ekind address with
       | E_var (var, _) ->
         let map = get_env T_cur man flow in
         let state = find var map in
         if AddressesPower.is_top state then
           (
             warn "Possible transfer to undetermined address. Setting to top";
             man.exec (mk_stmt (S_mcs_inter_exec {
                 contract = None;
                 entry = None;
                 address = None;
                 amount;
                 parameter;
               }) range) flow
           )
         else
           AddressesPower.fold (fun addr flow ->
               (match addr with
                | Address.Contract (c, entry) ->
                  (* FIXME REPLACE WITH GET AND COMPARE FROM ABSTRACT STATE *)
                  let contract =
                    mk_constant
                      ~etyp:T_mcs_address
                      (C_mcs_contract (c, entry)) range
                  in
                  assume (eq contract address range)
                    ~fthen:(fun flow ->
                        let stm = S_mcs_store_exec_contract {
                            address = addr;
                            amount;
                            parameter;
                          }
                        in
                        man.exec (mk_stmt stm range) flow
                      )
                    ~felse:(fun flow ->
                        Post.return flow
                      ) man flow
                  |> post_to_flow man
                  |> Flow.join man.lattice flow
                | Address.Address _ ->
                  Format.eprintf "ENV: %a"
                    (format (Flow.print man.lattice.print)) flow;
                  assert false
               )
             ) state flow |>
           Post.return
       | _ -> assert false
      ) |>
      OptionExt.return

    | S_assume({ekind=E_binop (O_eq, e1, e2); _}) when
        is_type_address e1.etyp &&
        is_type_address e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      (match ekind e1 with
       | E_var(v1, _) ->
         (match ekind e2 with
          | E_var(v2, _) ->
            let map = get_env T_cur man flow in
            let state1 = find v1 map in
            let state2 = find v2 map in
            let state = AddressesPower.meet state1 state2 in
            let map = add v1 state map in
            let map = add v2 state map in
            let flow = set_env T_cur map man flow in
            Post.return flow
          | E_mcs_sender ->
            man.exec (mk_assign e1 (mk_sender range) range) flow
          | _ -> panic "unhandled: assume(%a = %a)" pp_expr e1 pp_expr e2
         )
       | _ -> panic "unhandled: assume(%a = %a)" pp_expr e1 pp_expr e2
      ) |>
      OptionExt.return

    | S_assume({ekind=E_binop (O_ne, e1, e2); _}) when
        is_type_address e1.etyp &&
        is_type_address e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      Post.return flow |>
      OptionExt.return

    | _ -> None


  let eval expr (man: ('a, 'b) Framework.Core.Manager.man) flow =
    let range = erange expr in
    match ekind expr with
    | E_mcs_compare(e1, e2) when is_type_address e1.etyp &&
                                 is_type_address e2.etyp ->
      debug "m_address.%a" pp_expr expr;
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      man.eval (mk_int_interval ~typ:T_mcs_int (-1) (1) range) flow |>
      OptionExt.return

    | E_mcs_contract { address; entry; typ } ->
      debug "m_address.%a" pp_expr expr;
      (match address with
       | { ekind = E_var (var, mode); _ } ->
         let map = get_env T_cur man flow in
         (* Casts all addresses to contract : address%entry *)
         let state = match M.find_opt var map with
           | None -> panic "Calling contract on non-existing variable: %a" pp_var var
           | Some state ->
             if not (AddressesPower.is_top state) then
               AddressesPower.fold (fun address acc ->
                   match address with
                   | Address (s, _) -> AddressesPower.add (Contract(s, entry)) acc
                   | Contract _ -> panic "Unexpected call of CONTRACT on a contract"
                 ) state AddressesPower.empty
             else
               state
         in
         (* replaces the var inplace *)
         let map = M.add var state map in
         let flow = set_env T_cur map man flow in
         (* FIXME: still returns None | Some(contract) as of now *)
         let some = mk_some address range in
         let none = mk_none (T_mcs_option T_mcs_address) range in
         check_types none.etyp some.etyp;
         Eval.join
           (man.eval none flow)
           (man.eval some flow)
         |>
         OptionExt.return
       | _ -> None
      )

    | E_mcs_implicit_account kh ->
      debug "m_address.%a" pp_expr expr;
      warn "verifying contract address not supported. imprecise";
      man.eval (mk_top T_mcs_address range) flow |>
      OptionExt.return

    | E_mcs_address contract ->
      debug "m_address.%a" pp_expr expr;
      man.eval contract flow |>
      OptionExt.return

    | E_mcs_self _ ->
      let var = mk_var (mk_var_named T_mcs_address "self") range in
      Eval.singleton var flow |>
      OptionExt.return

    | E_mcs_sender ->
      let var = mk_var (mk_var_named T_mcs_address "sender") range in
      Eval.singleton var flow |>
      OptionExt.return

    | E_mcs_source ->
      let var = mk_var (mk_var_named T_mcs_address "source") range in
      Eval.singleton var flow |>
      OptionExt.return

    | _ -> None

  let merge _pre (a1, e1) (a2, e2) =
    debug "merge:";
    debug "  a1:@\n%a@\n e1:@\n%a" (format M.print) a1 pp_effect e1;
    debug "  a2:@\n%a@\n e2:@\n%a" (format M.print) a2 pp_effect e2;
    let a1, a2 = generic_merge
        ~add:M.add
        ~find:M.find
        ~remove:M.remove
        (a1, e1) (a2, e2)
    in
    let result = M.meet a1 a2 in
    debug "=> %a" (format M.print) result;
    result

  let ask _ _ _ = None

end

let () =
  Sig.Abstraction.Domain.register_standard_domain (module Domain)
