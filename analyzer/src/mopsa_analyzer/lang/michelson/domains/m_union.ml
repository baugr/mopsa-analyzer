open Mopsa
open Unstacked_ast
open Universal.Ast
open Ast
open M_common

module Domain = struct

  module Raw_union = struct
    type t =
      | V_left
      | V_right
      | V_top
      | V_bot

    let print fmt = function
      | V_left -> pp_string fmt "left"
      | V_right -> pp_string fmt "right"
      | V_bot  -> pp_string fmt "⊥"
      | V_top  -> pp_string fmt "⊤"

    let bottom = V_bot
    let top = V_top

    let is_bottom = function
      | V_bot -> true
      | _ -> false

    let subset x y =
      match x, y with
      | V_bot, _ -> true
      | _, V_bot -> false
      | V_top, _ -> false
      | _, V_top -> true
      | V_left, V_left -> true
      | V_right, V_right -> true
      | V_left, V_right -> false
      | V_right, V_left -> false

    let join x y =
      match x, y with
      | v, V_bot
      | V_bot, v -> v
      | V_top, _
      | _, V_top -> V_top
      | V_left, V_left -> V_left
      | V_right, V_right -> V_right
      | V_left, V_right
      | V_right, V_left -> V_top

    let meet x y =
      match x, y with
      | _, V_bot
      | V_bot, _ -> V_bot
      | V_top, y' -> y'
      | x', V_top -> x'
      | V_left, V_left -> V_left
      | V_right, V_right -> V_right
      | V_left, V_right
      | V_right, V_left -> V_bot

    let widen _ x y = join x y

  end

  let print_expr _man _flow _printer _expr = ()
  let checks = []

  module M = Framework.Lattices.Partial_map.Make(Var)(Raw_union)
  include M

  (* Override find with more explicit message *)
  let find v map = match find_opt v map with
    | None -> panic "Cannot find : %a" pp_var v
    | Some v -> v


  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.unions"
    end)

  let print_state printer a =
    pprint ~path:[Key "unions"] printer (pbox M.print a)

  let init _ man flow =
    set_env T_cur M.empty man flow

  let alarms = []

  (****************************************************************************
   * Helpers
   ***************************************************************************)
  let is_type_union t =
    match t with
    | T_mcs_union _ -> true
    | _ -> false

  let type_left t =
    match t with
    | T_mcs_union(t, _, _, _) -> t
    | _ -> panic "Expected union type, got %a" pp_typ t

  let type_right t =
    match t with
    | T_mcs_union(_, _, t, _) -> t
    | _ -> panic "Expected union type, got %a" pp_typ t

  let mk_var_left var =
    let etyp = match var.vtyp with
      | T_mcs_union (ty_l, _, _, _) -> ty_l
      | _ -> panic "Unexpected type for mk_var_left: %a" pp_typ var.vtyp
    in
    let name = "left" in
    let mode = vmode var in
    mk_var_ghost ~mode etyp var name

  let mk_var_right var =
    let etyp = match var.vtyp with
      | T_mcs_union (_, _, ty_r, _) -> ty_r
      | _ -> panic "Unexpected type for mk_var_right: %a" pp_typ var.vtyp
    in
    let name = "right" in
    let mode = vmode var in
    mk_var_ghost ~mode etyp var name

  let update_aux_vars old_state new_state var range man flow =
    debug "update_aux_vars %a: %a -> %a"
      pp_var var
      (format Raw_union.print) old_state
      (format Raw_union.print) new_state
    ;
    let open Raw_union in
    let left = mk_var (mk_var_left var) range in
    let right = mk_var (mk_var_right var) range in
    match old_state, new_state with
    (* no change *)
    | V_bot, V_bot
    | V_left, V_left
    | V_right, V_right
    | V_top, V_top -> Post.return flow
    (* from bot to new_state *)
    | V_bot, V_left ->
      man.exec (mk_add left range) flow
    | V_bot, V_right ->
      man.exec (mk_add right range) flow
    | V_bot, V_top ->
      man.exec (mk_add left range) flow >>%
      man.exec (mk_add right range)
    (* from left to new_state *)
    | V_left, V_bot ->
      man.exec (mk_remove left range) flow
    | V_left, V_right ->
      man.exec (mk_remove left range) flow >>%
      man.exec (mk_add right range)
    | V_left, V_top ->
      man.exec (mk_add right range) flow
    (* from right to new_state *)
    | V_right, V_bot ->
      man.exec (mk_remove right range) flow
    | V_right, V_left ->
      man.exec (mk_remove right range) flow >>%
      man.exec (mk_add left range)
    | V_right, V_top ->
      man.exec (mk_add left range) flow
    (* from top to new_state *)
    | V_top, V_bot ->
      man.exec (mk_remove left range) flow >>%
      man.exec (mk_remove right range)
    | V_top, V_left ->
      man.exec (mk_remove right range) flow
    | V_top, V_right ->
      man.exec (mk_remove left range) flow


  let add2 var mode old_state new_state map =
    match var_mode var mode with
    | STRONG -> new_state, add var new_state map
    | WEAK ->
      let new_state = Raw_union.join old_state new_state in
      new_state, add var new_state map

  let weak_update_mode_left old_state var mode =
    if var_mode var mode = STRONG then
      Some STRONG
    else if Raw_union.subset old_state Raw_union.V_left then
      Some STRONG
    else
      None

  let weak_update_mode_right old_state var mode =
    if var_mode var mode = STRONG then
      Some STRONG
    else if Raw_union.subset old_state Raw_union.V_right then
      Some STRONG
    else
      None

  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    (* Declare the variable *)
    | S_add {ekind=E_var(var, _); etyp; _} when is_type_union var.vtyp ->
      check_types var.vtyp etyp;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> Raw_union.V_bot
        | Some o -> panic "m_union: adding variable already existing: %a" pp_var var
      in
      let new_state = Raw_union.top in
      let map = add var new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    | S_remove { ekind=E_var(var, _); etyp; _} when is_type_union var.vtyp ->
      check_types var.vtyp etyp;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> panic "m_union: removing variable not existing: %a" pp_var var
        | Some o -> o
      in
      let new_state = Raw_union.bottom in
      let map = remove var map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    | S_rename({ekind=E_var(src, _); etyp; _}, {ekind=E_var(dst, _); _})
      when is_type_union dst.vtyp ->
      check_types dst.vtyp src.vtyp;
      check_types dst.vtyp etyp;
      let map = get_env T_cur man flow in
      let union = find src map in
      let map = add dst union map in
      let map = remove src map in
      let flow = set_env T_cur map man flow in
      (match union with
       | V_bot -> Post.return flow
       | V_left ->
         let vl_src = mk_var (mk_var_left src) range in
         let vl_dst = mk_var (mk_var_left dst) range in
         man.exec (mk_rename vl_src vl_dst range) flow
       | V_right ->
         let vr_src = mk_var (mk_var_right src) range in
         let vr_dst = mk_var (mk_var_right dst) range in
         man.exec (mk_rename vr_src vr_dst range) flow
       | V_top ->
         let vl_src = mk_var (mk_var_left src) range in
         let vr_src = mk_var (mk_var_right src) range in
         let vl_dst = mk_var (mk_var_left dst) range in
         let vr_dst = mk_var (mk_var_right dst) range in
         man.exec (mk_rename vl_src vl_dst range) flow >>$ fun () flow ->
         man.exec (mk_rename vr_src vr_dst range) flow
      ) |>
      OptionExt.return

    | S_assign({ekind=E_var(var, mode); etyp; _}, expr) when
        is_type_union var.vtyp ->
      debug "S_assign(%a, %a)" pp_var var pp_expr_typ expr;
      check_types var.vtyp etyp;
      check_types var.vtyp expr.etyp;
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_mcs_cons_left e ->
         check_types (type_left var.vtyp) e.etyp;
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state = Raw_union.V_left in
         let new_state, map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         let left = mk_var ~mode:(weak_update_mode_left old_state var mode)
             (mk_var_left var) range
         in
         check_types left.etyp e.etyp;
         man.exec (mk_assign left e range) flow |>
         OptionExt.return
       | E_mcs_cons_right e ->
         check_types (type_right var.vtyp) e.etyp;
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state = Raw_union.V_right in
         let new_state, map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         let right = mk_var ~mode:(weak_update_mode_right old_state var mode)
             (mk_var_right var) range
         in
         check_types right.etyp e.etyp;
         man.exec (mk_assign right e range) flow |>
         OptionExt.return
       | E_var (src, _) ->
         check_types src.vtyp var.vtyp;
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let src_state = find src map in
         let new_state, map = add2 var mode old_state src_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         (match src_state with
          | V_bot -> Post.return (Flow.bottom_from flow)
          | V_left ->
            let left_dst = mk_var (mk_var_left var)
                ~mode:(weak_update_mode_left old_state var mode)
                 range
            in
            let left_src = mk_var (mk_var_left src) range in
            man.exec (mk_assign left_dst left_src range) flow
          | V_right ->
            let right_dst = mk_var (mk_var_right var)
                ~mode:(weak_update_mode_right old_state var mode)
                range
            in
            let right_src = mk_var (mk_var_right src) range in
            man.exec (mk_assign right_dst right_src range) flow
          | V_top ->
            let left_dst = mk_var (mk_var_left var)
                ~mode:(weak_update_mode_left old_state var mode) range
            in
            let left_src = mk_var (mk_var_left src) range in
            let right_dst = mk_var (mk_var_right var)
                ~mode:(weak_update_mode_right old_state var mode)  range
            in
            let right_src = mk_var (mk_var_right src) range in
            man.exec (mk_assign left_dst left_src range) flow >>%
            man.exec (mk_assign right_dst right_src range)
         ) |>
         OptionExt.return

       | _ ->
         panic "Unexpected: S_assign(%a, %a)" pp_var var pp_expr_typ expr
      )
    | _ -> None

  let exec stmt man flow =
    let map = get_env T_cur man flow in
    if is_bottom map then
      Post.return flow |> OptionExt.return
    else
      exec stmt man flow


  let eval expr man flow =
    let range = erange expr in
    match expr with
    | { ekind=E_constant (C_top etyp); _ } when is_type_union expr.etyp ->
      check_types expr.etyp etyp;
      let topl = mk_top (type_left etyp) range in
      let topr = mk_top (type_right etyp) range in
      let e1 = mk_mcs_cons_left ~etyp topl range in
      let e2 = mk_mcs_cons_right ~etyp topr range in
      check_types expr.etyp e1.etyp;
      check_types expr.etyp e2.etyp;
      Eval.join (man.eval e1 flow) (man.eval e2 flow) |>
      OptionExt.return

    | {ekind=E_constant (C_mcs_union (C_mcs_left c)); _} when
        is_type_union expr.etyp ->
      let c = mk_constant ~etyp:(type_left expr.etyp) c range in
      man.eval (mk_mcs_cons_left expr.etyp c range) flow |>
      OptionExt.return

    | {ekind=E_constant (C_mcs_union (C_mcs_right c)); _} when
        is_type_union expr.etyp ->
      let c = mk_constant ~etyp:(type_right expr.etyp) c range in
      man.eval (mk_mcs_cons_right expr.etyp c range) flow |>
      OptionExt.return

    | {ekind=E_mcs_compare(e1, e2); _} when
        is_type_union e1.etyp &&
        is_type_union e2.etyp ->
      check_types T_mcs_int expr.etyp;
      man.eval (mk_is_left e1 range) flow >>$? fun is_left_e1 flow ->
      man.eval (mk_is_left e2 range) flow >>$? fun is_left_e2 flow ->
      switch [
        [mk_log_and
           (eq is_left_e1 (mk_true range) range)
           (eq is_left_e2 (mk_true range) range) range],
        (fun flow ->
           man.eval
             (mk_mcs_compare (mk_unleft e1 range) (mk_unleft e2 range) range) flow
        );
        [mk_log_and
           (eq is_left_e1 (mk_false range) range)
           (eq is_left_e2 (mk_false range) range) range],
        (fun flow ->
           man.eval
             (mk_mcs_compare (mk_unright e1 range) (mk_unright e2 range) range)
             flow
        );
        [mk_log_and
          (eq is_left_e1 (mk_true range) range)
          (eq is_left_e2 (mk_false range) range) range],
        (fun flow ->
           man.eval (mk_mcs_int 1 range) flow);
        [mk_log_and
          (eq is_left_e1 (mk_false range) range)
          (eq is_left_e2 (mk_true range) range) range],
        (fun flow ->
           man.eval (mk_mcs_int (-1) range) flow);
      ] man flow |>
      OptionExt.return

    | { ekind=E_mcs_is_left(expr); _} ->
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_mcs_cons_left(expr) ->
         man.eval (mk_mcs_true range) flow
       | E_mcs_cons_right(expr) ->
         man.eval (mk_mcs_false range) flow
       | E_var(var, _) ->
         let map = get_env T_cur man flow in
         (match find var map with
          | V_bot -> Eval.empty flow
          | V_left -> man.eval (mk_mcs_true range) flow
          | V_right -> man.eval (mk_mcs_false range) flow
          | V_top -> man.eval (mk_top (T_mcs_bool) range) flow
         )
       | _ ->
         warn "not handled : is_left(%a)" pp_expr expr;
         man.eval (mk_top T_mcs_bool range) flow
      ) |> OptionExt.return

    | {ekind=E_mcs_unleft(expr); etyp; _} ->
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_mcs_cons_left (expr) ->
         man.eval expr flow |>
         OptionExt.return
       | E_mcs_cons_right _ ->
         Eval.empty flow |>
         OptionExt.return
       | E_var(var, _) ->
         let map = get_env T_cur man flow in
         (match find var map with
          | V_bot
          | V_right -> Eval.empty flow
          | V_left
          | V_top ->
            let left = mk_var (mk_var_left var) range in
            man.eval left flow
         ) |>
         OptionExt.return
       | _ ->
         warn "not handled: unleft(%a)" pp_expr expr;
         man.eval (mk_top etyp range) flow |>
         OptionExt.return
      )
    | {ekind=E_mcs_unright(expr); etyp; _} ->
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_mcs_cons_right (expr) ->
         man.eval expr flow |>
         OptionExt.return
       | E_mcs_cons_left _ ->
         Eval.empty flow |>
         OptionExt.return
       | E_var(var, _) ->
         let map = get_env T_cur man flow in
         (match find var map with
          | V_bot
          | V_left -> Eval.empty flow
          | V_right
          | V_top ->
            let right = mk_var (mk_var_right var) range in
            man.eval right flow
         ) |>
         OptionExt.return
       | _ ->
         warn "not handled: unright(%a)" pp_expr expr;
         man.eval (mk_top etyp range) flow |>
         OptionExt.return
      )
    | _ -> None

  let ask _ _ _ = None
  let refine _ _ _ = assert false
  let merge _ _ = assert false

end

let () =
  Framework.Sig.Abstraction.Domain.register_standard_domain (module Domain)
