open Mopsa
open Ast
open Universal.Ast
open Unstacked_ast
open M_common

module Domain = struct

  module Raw_lambda = struct
    type t =
      | V_lambda of stmt
      | V_bot
      | V_top

    let print printer = function
      | V_lambda stm ->
        Print.pprint printer (fbox "lambda {%a}" pp_stmt stm)
      | V_bot  -> pp_string printer "⊥"
      | V_top  -> pp_string printer "⊤"

    let bottom = V_bot
    let top = V_top

    let is_bottom = function
      | V_bot -> true
      | _ -> false

    let subset x y =
      match x, y with
      | V_bot, _ -> true
      | _, V_bot -> false
      | V_top, _ -> false
      | _, V_top -> true
      | V_lambda stm1, V_lambda stm2 -> compare_stmt stm1 stm2 = 0

    let join x y =
      match x, y with
      | v, V_bot
      | V_bot, v -> v
      | V_top, _
      | _, V_top -> V_top
      | V_lambda stm1, V_lambda stm2 ->
        if compare_stmt stm1 stm2 = 0 then
          V_lambda stm1
        else
          V_top

    let meet x y =
      match x, y with
      | _, V_bot
      | V_bot, _ -> V_bot
      | V_top, y' -> y'
      | x', V_top -> x'
      | V_lambda stm1, V_lambda stm2 ->
        if compare_stmt stm1 stm2 = 0 then
          V_lambda stm1
        else
          V_bot

    let widen _ x y = join x y

  end

  let is_type_lambda t =
    match t with
    | T_mcs_lambda _ -> true
    | _ -> false

  module M = Framework.Lattices.Partial_map.Make(Var)(Raw_lambda)
  include M

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.lambdas"
    end)

  let print_state printer a =
    pprint ~path:[Key "lambda"] printer (pbox M.print a)

  let print_expr _ _ _ _ = ()

  let init _ man flow =
    set_env T_cur M.empty man flow

  let checks = []

  (****************************************************************************
   * Helpers
   ***************************************************************************)
  let add2 var mode old_state new_state map =
    match var_mode var mode with
    | STRONG -> add var new_state map
    | WEAK -> add var (Raw_lambda.join old_state new_state) map

  let exec stmt man flow =
    match skind stmt with
    (* Declare the variable *)
    | S_add ({ ekind=E_var(var, mode); etyp; _}) when is_type_lambda var.vtyp ->
      debug "m_lambda.%a" pp_stmt stmt;
      check_types var.vtyp etyp;
      let map = get_env T_cur man flow in
      let new_state = Raw_lambda.V_top in
      let map = add var new_state map in
      let flow = set_env T_cur map man flow in
      Post.return flow |>
      OptionExt.return

    (* Remove the variable *)
    | S_remove ({ ekind=E_var(var, _); etyp; _ }) when is_type_lambda var.vtyp ->
      debug "m_lambda.%a" pp_stmt stmt;
      check_types var.vtyp etyp;
      let map = get_env T_cur man flow in
      let map = remove var map in
      let flow = set_env T_cur map man flow in
      Post.return flow |>
      OptionExt.return

    | S_rename ({ekind=E_var(v_src, _); _ },
                {ekind=E_var(v_dst, _); _}) when is_type_lambda v_src.vtyp ->
      debug "m_lambda.%a" pp_stmt stmt;
      check_types v_src.vtyp v_dst.vtyp;
      let cur = get_env T_cur man flow in
      (match M.find_opt v_src cur with
       | None -> Post.return flow |> OptionExt.return
       | Some value ->
         let cur = M.add v_dst value cur in
         let cur = M.remove v_src cur in
         let flow = set_env T_cur cur man flow in
         Post.return flow |>
         OptionExt.return
      )

    (* Assignment of lambda to variable *)
    | S_assign({ekind=E_var(var, mode); _}, expr) when is_type_lambda var.vtyp ->
      check_types var.vtyp expr.etyp;
      man.eval expr flow >>$? fun expr flow ->
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> panic "Variable was not added: %a" pp_var var
        | Some o -> o
      in
      (match ekind expr with
       | E_constant (C_top (T_mcs_lambda _)) ->
         let map = get_env T_cur man flow in
         let new_state = Raw_lambda.V_top in
         let map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         Post.return flow |>
         OptionExt.return
       | E_constant (C_mcs_lambda lambda) ->
         let map = get_env T_cur man flow in
         let new_state = (Raw_lambda.V_lambda lambda) in
         let map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         Post.return flow |>
         OptionExt.return
       | E_var (src, _) ->
         check_types src.vtyp var.vtyp;
         let map = get_env T_cur man flow in
         let new_state = find src map in
         let map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         Post.return flow |>
         OptionExt.return
       | _ ->
         warn "Unhandled expression: %a" pp_expr expr;
         let map = get_env T_cur man flow in
         let new_state = Raw_lambda.V_top in
         let map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         Post.return flow |>
         OptionExt.return
      )

    | _ -> None

  let eval expr man flow = None

  let ask _ _ _ = None
  let refine _ _ _ = assert false
  let merge _ _ = assert false

end

let () =
  Framework.Sig.Abstraction.Domain.register_standard_domain (module Domain)
