open Mopsa
open Ast
open Unstacked_ast
open Universal.Ast

module Domain = struct
  open M_common

  include GenStatelessDomainId(struct
      let name = "michelson.types.mutez"
    end)

  let universal = Semantic "Universal"

  let checks = [ Alarms.CHK_MCL_SHIFT_OVERFLOW ]

  let print_expr _man _flow _printer _expr = ()

  (* XXX remove when confident *)
  let lbound, rbound =
      Z.(zero, (one lsl 63) - one)

  let eval_fail_on_overflow mexpr nexpr range man flow =
    (* FIXME: assume tries to get translation but fails *)
    assume (mk_log_and ~etyp:T_bool
              (mk_ge ~etyp:T_bool nexpr (mk_z ~typ:T_int lbound range) range)
              (mk_le ~etyp:T_bool nexpr (mk_z ~typ:T_int rbound range) range)
              range)
      ~fthen:(fun flow ->
          let flow = Alarms.safe_mcl_mutez_overflow range man flow in
          Eval.singleton mexpr flow |>
          Eval.add_translation "Universal" nexpr
        )
      ~felse:(fun flow ->
          let flow = Alarms.raise_mcl_mutez_overflow nexpr range man flow in
          Eval.empty (Flow.bottom_from flow)
        )
      man flow

  let is_type_mutez t =
    match t with
    | T_mcs_mutez -> true
    | _ -> false

  let is_type_int t =
    match t with
    | T_mcs_int -> true
    | _ -> false

  let is_type_ediv_result t =
    match t with
    | T_mcs_option (T_mcs_pair (T_mcs_int, T_mcs_mutez))
    | T_mcs_option (T_mcs_pair (T_mcs_mutez, T_mcs_int)) -> true
    | _ -> false

  let to_num_type t =
    match t with
    | T_mcs_mutez -> T_int
    | _ -> assert false

  let mk_num_var v =
    let vname = Format.asprintf "num(%s)" v.vname in
    mkv vname (V_mcs_num v) (to_num_type v.vtyp)
      ~mode:v.vmode ~semantic:"Universal"

  let mk_num_var_expr e =
    match ekind e with
    | E_var (v, mode) -> mk_var (mk_num_var v) ~mode e.erange
    | _ -> assert false


  let init _ _ flow = flow


  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_add({ekind=E_var (v, _); _} as expr) when is_type_mutez v.vtyp ->
      debug "%a" pp_stmt stmt;
      let expr = mk_num_var_expr expr in
      man.exec (mk_add expr range) flow >>$? fun () flow ->
      let rval = mk_z_interval lbound rbound range in
      man.exec (mk_assign expr rval range) flow |>
      OptionExt.return

    | S_remove({ekind=E_var(v, _); _} as expr) when is_type_mutez v.vtyp ->
      let expr = mk_num_var_expr expr in
      man.exec (mk_remove expr range) flow |>
      OptionExt.return

    | S_assign(({ekind=E_var(v, _); _} as lval), rval) when is_type_mutez v.vtyp ->
      check_types T_mcs_mutez (etyp lval);
      check_types T_mcs_mutez (etyp rval);
      man.eval ~translate:"Universal" lval flow >>$? fun lval flow ->
      man.eval ~translate:"Universal" rval flow >>$? fun rval flow ->
      man.exec (mk_assign lval rval range) flow |>
      OptionExt.return

    | S_rename(({ekind=E_var _; _} as src),
               ({ekind=E_var _; _} as dst)) when is_type_mutez src.etyp &&
                                                 is_type_mutez dst.etyp ->
      let src = mk_num_var_expr src in
      let dst = mk_num_var_expr dst in
      man.exec (mk_rename src dst range) flow |>
      OptionExt.return

    | S_expand({ekind = E_var _; _} as e, el)
      when is_type_mutez e.etyp &&
           List.for_all (fun ee -> is_type_mutez ee.etyp) el
      ->
      debug "%a" pp_stmt stmt;
      let v = mk_num_var_expr e in
      let vl = List.map mk_num_var_expr el in
      man.exec (mk_expand v vl range) flow |>
      OptionExt.return

    | S_fold(({ekind = E_var _; _} as e), el)
      when is_type_mutez e.etyp &&
           List.for_all (fun ee -> is_type_mutez ee.etyp) el
      ->
      debug "%a" pp_stmt stmt;
      let v = mk_num_var_expr e in
      let vl = List.map mk_num_var_expr el in
      man.exec (mk_fold v vl range) flow |>
      OptionExt.return

    | S_forget ({ekind = E_var _; _} as v) when is_type_mutez v.etyp ->
      let vv = mk_num_var_expr v in
      let top = mk_top vv.etyp range in
      man.exec (mk_assign vv top range) flow |>
      OptionExt.return

    | _ -> None


  let eval_ediv ~etyp e1 e2 range man flow =
    let comp_typ t = match t with
      | T_mcs_int -> T_mcs_mutez
      | T_mcs_mutez -> T_mcs_int
      | _ -> assert false
    in
    let t_div = comp_typ e2.etyp in
    let t_rem = e1.etyp in
    man.eval e1 flow >>$ fun e1 flow ->
    man.eval e2 flow >>$ fun e2 flow ->
    let cond = mk_ne ~etyp:T_mcs_bool e2 (mk_zero ~typ:e2.etyp range) range in
    assume cond
      ~fthen:(fun flow ->
          let div =
            let e1 = get_expr_translation "Universal" e1 in
            let e2 = get_expr_translation "Universal" e2 in
            let expr = mk_binop ~etyp:T_int e1 O_ediv e2 range in
            mk_universal_expression ~etyp:t_div expr range
          in
          let rem =
            let e1 = get_expr_translation "Universal" e1 in
            let e2 = get_expr_translation "Universal" e2 in
            let expr = mk_binop ~etyp:T_int e1 O_erem e2 range in
            mk_universal_expression ~etyp:t_rem expr range
          in
          let expr = mk_some (mk_pair div rem range) range in
          (* call eval because we leave the m_int domain *)
          man.eval expr flow
        )
      ~felse:(fun flow ->
          debug "building none to return";
          (* call eval because we leave the m_int domain *)
          man.eval (mk_none etyp range) flow
        )
      man flow


  let eval expr man flow : 'a eval option =
    let range = erange expr in
    match expr with
    | {ekind=E_constant(C_top cty); _} when is_type_mutez expr.etyp ->
      assert (compare_typ cty T_mcs_mutez = 0);
      let nexpr = mk_z_interval ~typ:(to_num_type expr.etyp) lbound rbound range in
      Eval.singleton (mk_z_interval ~typ:T_mcs_mutez lbound rbound range) flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | {ekind=E_constant((C_int _|C_int_interval _) as c); _} when is_type_mutez expr.etyp ->
      let nexpr = mk_constant ~etyp:(to_num_type expr.etyp) c range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

      (* rewrite to evaluate embedded universal expression *)
    | {ekind=E_mcs_universal_expression e; _} when is_type_mutez expr.etyp ->
      let expr' = add_expr_translation "Universal" e expr in
      Eval.singleton expr' flow |>
      OptionExt.return

    | {ekind = E_var(v, _); _} when is_type_mutez expr.etyp ->
      assert (compare_typ v.vtyp T_mcs_mutez = 0);
      let nexpr = mk_num_var_expr expr in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | {ekind=E_mcs_binop(((O_plus|O_minus) as op), e1, e2); _ }
      when is_type_mutez expr.etyp ->
      assert (compare_typ e1.etyp T_mcs_mutez = 0);
      assert (compare_typ e2.etyp T_mcs_mutez = 0);
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
      let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
      let nexpr = michelson2num nexpr in
      eval_fail_on_overflow mexpr nexpr range man flow |>
      OptionExt.return

    | { ekind = E_mcs_binop((O_mult as op), e1, e2); _ } when
        is_type_mutez expr.etyp ->
      assert((is_type_mutez e1.etyp && is_type_int e2.etyp) ||
             (is_type_int e1.etyp && is_type_mutez e2.etyp));
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
      let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
      let nexpr = michelson2num nexpr in
      eval_fail_on_overflow mexpr nexpr range man flow |>
      OptionExt.return

    | { ekind = E_mcs_binop(O_div,
                            ({etyp=T_mcs_mutez; _} as e1),
                            ({etyp=T_mcs_int; _} as e2)); _ } ->
      check_types (T_mcs_option(T_mcs_pair(T_mcs_mutez, T_mcs_mutez))) expr.etyp;
      eval_ediv ~etyp:expr.etyp e1 e2 range man flow |>
      OptionExt.return

    | { ekind = E_mcs_binop(O_div,
                            ({etyp=T_mcs_mutez; _} as e1),
                            ({etyp=T_mcs_mutez; _} as e2)); _ } ->
      check_types (T_mcs_option(T_mcs_pair(T_mcs_int, T_mcs_mutez))) expr.etyp;
      eval_ediv ~etyp:expr.etyp e1 e2 range man flow |>
      OptionExt.return

    | { ekind = E_mcs_amount; etyp; _ } ->
      check_types etyp T_mcs_mutez;
      Eval.singleton (mk_var (mk_var_named T_mcs_mutez "amount") range) flow |>
      OptionExt.return

    | { ekind = E_mcs_balance; etyp; _ } ->
      check_types etyp T_mcs_mutez;
      Eval.singleton (mk_var (mk_var_named T_mcs_mutez "balance") range) flow |>
      OptionExt.return

    | { ekind = E_mcs_compare(e1, e2); _ } when is_type_mutez e1.etyp &&
                                                is_type_mutez e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      switch [
        [lt e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int (-1) range in
           let nexpr = { mexpr with etyp = T_int } in
           debug "ASSUME LT: flow after:@\n";
           debug "%a" (format (Flow.print man.lattice.print)) flow;
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        );
        [eq e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int 0 range in
           let nexpr = { mexpr with etyp = T_int } in
           debug "ASSUME EQ : flow after:@\n";
           debug "%a" (format (Flow.print man.lattice.print)) flow;
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        );
        [gt e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int 1 range in
           let nexpr = { mexpr with etyp = T_int } in
           debug "ASSUME GT : flow after:@\n";
           debug "%a" (format (Flow.print man.lattice.print)) flow;
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        )
      ] man flow |> OptionExt.return

    |  _ -> None

  let ask _ _ _ = None

  let refine _ _ _ = assert false
  let merge _ _ = assert false

end

let () =
  Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
