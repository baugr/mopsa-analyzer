open Mopsa
open Universal.Ast
open Ast
open Unstacked_ast

type var_kind +=
  | V_mcs_num of var

let () =
  register_var {
    print = (fun next fmt v ->
        match v.vkind with
        | V_mcs_num vv -> Format.fprintf fmt "num⦅%a⦆ : %a"
                            (pp_var_untyped pp_var) vv
                            pp_typ v.vtyp
        | _ -> next fmt v
      );
    compare = (fun next v1 v2 ->
        match v1.vkind, v2.vkind with
        | V_mcs_num vv1, V_mcs_num vv2 -> compare_var vv1 vv2
        | _ -> next v1 v2
      );
  }

let is_type_bool t =
  match t with
  | T_mcs_bool -> true
  | _ -> false

let is_type_int t =
  match t with
  | T_mcs_int -> true
  | _ -> false

let is_type_address t =
  match t with
  | T_mcs_address -> true
  | _ -> false

let mk_mcs_bool b range =
  mk_constant ~etyp:T_mcs_bool (C_bool b) range

let mk_mcs_true range =
  mk_constant ~etyp:T_mcs_bool (C_bool true) range

let mk_mcs_false range =
  mk_constant ~etyp:T_mcs_bool (C_bool false) range

let mk_mcs_int ?(typ=T_mcs_int) i range =
  mk_int ~typ i range

let mk_mcs_int_interval ?(typ=T_mcs_int) i j range =
  mk_int_interval ~typ i j range

let mk_mcs_incr e range =
  let etyp = match e.etyp with
    | T_mcs_int -> T_mcs_int
    | ty -> panic "Unexpected type for incr: %a" pp_typ ty
  in
  mk_mcs_binop ~etyp e O_plus (mk_int ~typ:etyp 1 range) range

(** Rebuild a michelson expression from its parts *)
let rebuild_michelson_expr exp parts =
  let _, builder = structure_of_expr exp in
  builder {exprs = parts; stmts = []}

let michelson2num exp =
  let to_num_type t =
    match t with
    | T_mcs_unit -> T_int
    | T_mcs_bool -> T_bool
    | T_mcs_int -> T_int
    | T_mcs_mutez -> T_int
    | T_mcs_timestamp -> T_int
    | T_mcs_string -> T_string
    | T_mcs_bytes -> T_string
    | T_mcs_key -> T_string
    | T_mcs_key_hash -> T_string
    | T_mcs_signature -> T_string
    | _ -> panic "Unexpected type : %a" pp_typ t
  in
  let parts,builder = structure_of_expr exp in
  let nparts = { exprs = List.map (get_expr_translation "Universal") parts.exprs; stmts = [] } in
  let e = builder nparts in
  { e with etyp = to_num_type e.etyp }


(* XXX for debugging *)
let rec pp_expr_typ fmt expr =
  match ekind expr with
  | E_var (v, _) -> Format.fprintf fmt "(%a{vtyp:%a} : %a)" pp_var v pp_typ v.vtyp pp_typ expr.etyp
  | E_mcs_binop(op, e1, e2) ->
    Format.fprintf fmt "(%a %a_m %a):%a" pp_expr_typ e1 pp_operator op pp_expr_typ e2 pp_typ expr.etyp
  | E_binop(op, e1, e2) ->
    Format.fprintf fmt "(%a %a %a):%a" pp_expr_typ e1 pp_operator op pp_expr_typ e2 pp_typ expr.etyp
  | E_mcs_unop(op, e) ->
    Format.fprintf fmt "(%a_m %a):%a" pp_operator op pp_expr_typ e pp_typ expr.etyp
  | E_unop(op, e) ->
    Format.fprintf fmt "(%a %a):%a" pp_operator op pp_expr_typ e pp_typ expr.etyp
  |  _ -> Format.fprintf fmt "%a : %a" pp_expr expr pp_typ expr.etyp


let check_types t1 t2 =
  if compare_typ t1 t2 <> 0 then
    panic "Expected type %a, got: %a" pp_typ t1 pp_typ t2


(*****************************************************************************
 * For Property testing
 *****************************************************************************)
let property_is_michelson_type t =
  match t with
  | T_mcs_unit
  | T_mcs_bool
  | T_mcs_int
  | T_mcs_mutez -> true
  | _ -> false

let property_is_universal_type t =
  match t with
  | T_unit
  | T_bool
  | T_int -> true
  | t when property_is_michelson_type t -> false
  | _ -> panic "%s: Not yet handled: %a" __LOC__ pp_typ t

let rec check_property_has_correct_universal_translation expr =
  let expr' =
    try SemanticMap.find "Universal" expr.etrans
    with Not_found -> panic "%a: cannot find translation" pp_expr_typ expr;
  in
  if not (property_is_universal_type expr'.etyp) then
    panic "%a: expected universal expression" pp_expr_typ expr'
  ;
  match ekind expr' with
  | E_var(v, _) ->
    if not (property_is_universal_type v.vtyp)
    then
      panic "%a: expected universal expression" pp_expr_typ expr'
  | E_constant _ -> ()
  | E_binop(op, e1, e2) ->
    check_property_has_correct_universal_translation e1;
    check_property_has_correct_universal_translation e2
  | _ -> panic "not yet handled: %a" pp_expr_typ expr'

let rec check_property_is_valid_michelson_semantic expr =
  if not (property_is_michelson_type expr.etyp) then
    panic "%a does not have a michelson type" pp_expr_typ expr
  ;
  check_property_has_correct_universal_translation expr;
  match ekind expr with
  | E_var(v, _) ->
    if not (property_is_michelson_type v.vtyp) then
      panic "%a does not have a michelson type" pp_expr_typ expr
  | E_constant _ -> ()
  | E_mcs_binop(op, e1, e2) ->
    check_property_has_correct_universal_translation e1;
    check_property_has_correct_universal_translation e2
  | _ -> panic "Not yet handled: %a" pp_expr_typ expr

let checked_eval_return eval =
  Cases.map_result
    (
      fun e -> check_property_is_valid_michelson_semantic e; e
    ) eval
