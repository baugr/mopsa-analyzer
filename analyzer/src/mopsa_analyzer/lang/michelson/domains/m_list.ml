open Mopsa
open Unstacked_ast
open Universal.Ast
open Ast
open M_common

module Domain = struct

  module Raw_list = struct
    type t =
      | V_bot
      | V_empty (* an empty list *)
      | V_list  (* a non-empty list *)
      | V_top

    let print fmt = function
      | V_bot -> pp_string fmt "⊥"
      | V_empty -> pp_string fmt "empty_list"
      | V_list -> pp_string fmt "list"
      | V_top -> pp_string fmt "⊤"

    let bottom = V_bot
    let top = V_top

    let is_bottom = function
      | V_bot -> true
      | _ -> false

    let subset x y =
      match x, y with
      | V_bot, _ -> true
      | _, V_bot -> false
      | V_top, _ -> false
      | _, V_top -> true
      | V_empty, V_empty
      | V_list, V_list -> true
      | V_empty, V_list
      | V_list, V_empty -> false

    let join x y =
      match x, y with
      | v, V_bot
      | V_bot, v -> v
      | V_top, _
      | _, V_top -> V_top
      | V_empty, V_empty -> V_empty
      | V_list, V_list -> V_list
      | V_empty, V_list
      | V_list, V_empty -> V_top

    let meet x y =
      match x, y with
      | _, V_bot
      | V_bot, _ -> V_bot
      | V_top, y' -> y'
      | x', V_top -> x'
      | V_empty, V_empty -> V_empty
      | V_list, V_list -> V_list
      | V_empty, V_list
      | V_list, V_empty-> V_bot

    let widen _ x y = join x y

  end

  module M = Framework.Lattices.Partial_map.Make(Var)(Raw_list)
  include M

  (* Override find with more explicit message *)
  let find v map = match find_opt v map with
    | None -> panic "Cannot find : %a" pp_var v
    | Some v -> v

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.lists"
    end)

  let checks = []


  (* XXX: it is useful to set as nat instead of int ? *)
  let list_size_type = T_mcs_int

  let is_type_list t =
    match t with
    | T_mcs_list _ -> true
    | _ -> false

  let item_type t =
    match t with
    | T_mcs_list t -> t
    | _ -> panic "expected list type, got : %a" pp_typ t

  let mk_var_list_item var =
    let ty =
      match var.vtyp with
      | T_mcs_list ty -> ty
      | ty -> panic "Expected list type, got : %a" pp_typ ty
    in
    mk_var_ghost ~mode:WEAK ty var "list_item"

  let mk_var_list_size var =
    let mode = vmode var in
    mk_var_ghost ~mode list_size_type var "list_size"


  let print_state printer a =
    pprint ~path:[Key "lists"] printer (pbox M.print a)

  let print_expr man flow printer expr =
    ()

  let init _ man flow =
    set_env T_cur M.empty man flow



  (****************************************************************************
   * Helpers
   ***************************************************************************)
  let update_aux_vars old_state new_state var range man flow =
    debug "update_aux_vars %a: %a -> %a"
      pp_var var
      (format Raw_list.print) old_state
      (format Raw_list.print) new_state
    ;
    let open Raw_list in
    match old_state, new_state with
    | V_bot, V_list
    | V_bot, V_top
    | V_empty, V_list
    | V_empty, V_top ->
      let item = mk_var (mk_var_list_item var) range in
      let size = mk_var (mk_var_list_size var) range in
      man.exec (mk_add item range) flow >>$ fun () flow ->
      man.exec (mk_add size range) flow
    | V_list, V_empty
    | V_top, V_empty
    | V_list, V_bot
    | V_top, V_bot ->
      let item = mk_var (mk_var_list_item var) range in
      let size = mk_var (mk_var_list_size var) range in
      man.exec (mk_remove item range) flow >>$ fun () flow ->
      man.exec (mk_remove size range) flow
    | (V_empty|V_bot), (V_empty|V_bot)
    | (V_top|V_list), (V_top|V_list) ->
      Post.return flow

  let add2 var mode old_state new_state map =
    match var_mode var mode with
    | STRONG -> new_state, add var new_state map
    | WEAK ->
      let new_state = Raw_list.join old_state new_state in
      new_state, add var new_state map

  (* Determines the mode to use
   * uniquelly for first time assignements *)
  let weak_update_mode old_state var mode =
    if var_mode var mode = STRONG then
      Some STRONG
    else if Raw_list.subset old_state Raw_list.V_empty then
      Some STRONG
    else
      None


  (****************************************************************************
   * Transfer functions
   ***************************************************************************)
  let assign_top var mode range man flow =
    let map = get_env T_cur man flow in
    let old_state =
      match find_opt var map with
      | None -> panic "assign_top: Not found: %a" pp_var var
      | Some o -> o
    in
    let new_state = Raw_list.V_top in
    let new_state, map = add2 var mode old_state new_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state new_state var range man flow >>% fun flow ->
    let item = mk_var (mk_var_list_item var) range in
    let size = mk_var ~mode (mk_var_list_size var) range in
    man.exec (mk_assign item (mk_top item.etyp range) range) flow >>%
    man.exec (mk_assign size (mk_top list_size_type range) range) >>%
    man.exec (mk_assume (ge ~etyp:T_mcs_bool size (mk_mcs_int 0 range) range) range)

  let assign_constant var mode l range man flow =
    let map = get_env T_cur man flow in
    let old_state =
      match find_opt var map with
      | None -> panic "Not found : %a" pp_var var
      | Some o -> o
    in
    let ity = item_type var.vtyp in
    match l with
    | [] ->
      let new_state = Raw_list.V_empty in
      let new_state, map = add2 var mode old_state new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state (find var map) var range man flow
    | hd::tl ->
      let new_state = Raw_list.V_list in
      let new_state, map = add2 var mode old_state new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow >>% fun flow ->
      (* Update size *)
      let size = mk_var (mk_var_list_size var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign size (mk_int ~typ:list_size_type (List.length l) range) range) flow >>% fun flow ->
      (* Update item *)
      let first_item = mk_var (mk_var_list_item var) ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign first_item (mk_constant ~etyp:ity hd range) range) flow >>$ fun () flow ->
      (* Assignement of the remaining elements *)
      let item = mk_var (mk_var_list_item var) range in
      List.fold_left
        (fun acc x ->
           acc >>% man.exec (mk_assign item (mk_constant ~etyp:ity x range) range)
        ) (Post.return flow) tl


  let assign_variable var mode src range man flow =
    let map = get_env T_cur man flow in
    let old_state = find var map in
    let src_state = find src map in
    let new_state, map = add2 var mode old_state src_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state new_state var range man flow >>% fun flow ->
    (* if var is V_list or V_top, assign the item and size variables *)
    if Raw_list.subset Raw_list.V_list src_state then
      let src_item = mk_var (mk_var_list_item src) range in
      let src_size = mk_var (mk_var_list_size src) range in
      let dst_item = mk_var (mk_var_list_item var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      let dst_size = mk_var (mk_var_list_size var)
          ~mode:(weak_update_mode old_state var mode) range
      in
      man.exec (mk_assign dst_item src_item range) flow >>$ fun () flow ->
      man.exec (mk_assign dst_size src_size range) flow
    else
      Post.return flow


  let assign_add var mode item range man flow =
    let map = get_env T_cur man flow in
    let old_state = find var map in
    let new_state = Raw_list.V_list in
    let new_state, map = add2 var mode old_state new_state map in
    let flow = set_env T_cur map man flow in
    update_aux_vars old_state new_state var range man flow >>% fun flow ->
    if Raw_list.subset old_state Raw_list.V_empty then
      (* if item and size didn't exist before, they should have been added and be top
       * if we are strong-updating, force strong update of item *)
      let vsize = mk_var (mk_var_list_size var) ~mode:(Some STRONG) range in
      let vitem = mk_var (mk_var_list_item var) ~mode:(Some STRONG) range in
      let expr = mk_int ~typ:list_size_type 1 range in
      man.exec (mk_assign vitem item range) flow >>$ fun () flow ->
      man.exec (mk_assign vsize expr range) flow
    else
      (* if item and size existed before, we assign item, and increment size *)
      let vitem = mk_var (mk_var_list_item var) range in
      let vsize = mk_var ~mode (mk_var_list_size var) range in
      let expr = mk_mcs_binop ~etyp:list_size_type
          vsize O_plus (mk_int ~typ:list_size_type 1 range)
          range
      in
      man.exec (mk_assign vitem item range) flow >>$ fun () flow ->
      man.exec (mk_assign vsize expr range) flow


  let assign_tail var mode range man flow =
    let size = mk_list_size (mk_var var range) range in
    switch [
      [eq ~etyp:T_mcs_bool size (mk_int ~typ:list_size_type 0 range) range],
      (fun flow -> Flow.bottom_from flow |> Post.return);
      [eq ~etyp:T_mcs_bool size (mk_int ~typ:list_size_type 1 range) range],
      (fun flow ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state = Raw_list.V_empty in
         let new_state, map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow
      );
      [gt ~etyp:T_mcs_bool size (mk_int ~typ:list_size_type 1 range) range],
      (fun flow ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state = Raw_list.V_list in
         let new_state, map = add2 var mode old_state new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>% fun flow ->
         let vsize = mk_var ~mode (mk_var_list_size var) range in
         let nsize = mk_mcs_binop ~etyp:list_size_type
             vsize
             O_minus (mk_int ~typ:list_size_type 1 range)
             range
         in
         man.exec (mk_assign vsize nsize range) flow
      )
    ] man flow

  (****************************************************************************
   * Loops
   ***************************************************************************)
  (* XXX: stack manipulation here, that is pretty ugly *)
  (* assumes the list is non-empty *)
  let list_iter_fast olist stm sp bef range man flow =
    let vnlist = mk_range_attr_var range "list" olist.etyp in
    let nlist = mk_var vnlist range in
    let iter = mk_var (mk_stackpos (item_type olist.etyp) (sp-1)) range in
    man.exec (mk_rename olist nlist range) flow >>% fun flow ->
    let loop =
      mk_while
        (mk_top T_mcs_bool range)
        (mk_block
           [
             (* XXX: should we add a E_list_nth expression ? *)
             mk_assign iter (mk_var (mk_var_list_item vnlist) range) range;
             stm;
           ]
           range
        )
        range
    in
    man.exec loop flow >>% fun flow ->
    man.exec (mk_remove nlist range) flow >>% fun flow ->
    man.exec (mk_remove iter range) flow


  (* XXX: stack manipulation here, that is pretty ugly *)
  (* assumes the list is non-empty *)
  let list_iter olist stm sp bef range man flow =
    let vnlist = mk_range_attr_var range "list" olist.etyp in
    let nlist = mk_var vnlist range in
    let counter = mk_var (mk_range_attr_var range "counter" list_size_type) range in
    let iter = mk_var (mk_stackpos (item_type olist.etyp) (sp-1)) range in
    let size = mk_var (mk_range_attr_var range "size" list_size_type) range in
    man.exec (mk_add size range) flow >>%
    man.exec (mk_add counter range) >>%
    man.exec (mk_assign size (Unstacked_ast.mk_list_size olist range) range) >>%
    man.exec (mk_assign counter (mk_int ~typ:list_size_type 0 range) range) >>%
    man.exec (mk_rename olist nlist range) >>% fun flow ->
    let loop =
      mk_while
        (lt counter size range)
        (mk_block
           [
             mk_add iter range;
             mk_assign iter (mk_var (mk_var_list_item vnlist) range) range;
             stm;
             mk_assign counter
               (mk_mcs_binop ~etyp:list_size_type
                  counter
                  O_plus
                  (mk_int ~typ:list_size_type 1 range)
                  range
               )
               range;
           ]
           range
        )
        range
    in
    man.exec loop flow >>%
    man.exec (mk_remove nlist range) >>%
    man.exec (mk_remove counter range) >>%
    man.exec (mk_remove size range)


  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with

    (* Declare the variable *)
    | S_add ({ekind=E_var(var, mode); etyp; _}) when is_type_list var.vtyp ->
      check_types var.vtyp etyp;
      debug "m_list.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> Raw_list.V_bot
        | Some _ -> panic "m_list: adding variable already existing: %a" pp_var var
      in
      let new_state = Raw_list.top in
      let map = add var new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow >>%? fun flow ->
      Post.return flow |>
      OptionExt.return

    (* Remove the variable *)
    | S_remove ({ekind=E_var(var, _); etyp=T_mcs_list t; _}) ->
      debug "m_list.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match M.find_opt var map with
        | None -> panic "m_list: removing variable not existing: %a" pp_var var
        | Some o -> o
      in
      let new_state = Raw_list.V_bot in
      let map = remove var map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    (* Rename of a variable *)
    | S_rename({ekind=E_var(src, _); etyp=T_mcs_list typ; _},
               {ekind=E_var(dst, _); etyp=T_mcs_list typ'; _}) ->
      assert (compare_typ typ typ' = 0);
      debug "m_list.%a" pp_stmt stmt;
      (if compare_var src dst = 0 then
         Post.return flow
       else
         (
           let map = get_env T_cur man flow in
           let new_state = find src map in
           let map = add dst new_state map in
           let map = remove src map in
           let flow = set_env T_cur map man flow in
           (match new_state with
            | V_bot
            | V_empty -> Post.return flow
            | V_top
            | V_list ->
              let src_item = mk_var (mk_var_list_item src) range in
              let dst_item = mk_var (mk_var_list_item dst) range in
              let src_size = mk_var (mk_var_list_size src) range in
              let dst_size = mk_var (mk_var_list_size dst) range in
              man.exec (mk_rename src_item dst_item range) flow >>$ fun () flow ->
              man.exec (mk_rename src_size dst_size range) flow
           )
         )
      ) |> OptionExt.return

    (* Assignment of an expression to a variable *)
    | S_assign({ekind=E_var(var, mode); _}, expr)
      when is_type_list var.vtyp ->
      check_types var.vtyp expr.etyp;
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_constant (C_top (T_mcs_list _)) ->
         assign_top var mode range man flow
       | E_constant (C_mcs_list l) ->
         assign_constant var mode l range man flow
       | E_var(src, _) when compare_var var src = 0 ->
         Post.return flow
       | E_var(src, _) ->
         assign_variable var mode src range man flow
       | E_mcs_list_cons(item, l) ->
         man.exec (mk_assign (mk_var ~mode var range) l range) flow >>%
         assign_add var mode item range man
       | E_mcs_list_tail(l) ->
         man.exec (mk_assign (mk_var ~mode var range) l range) flow >>%
         assign_tail var mode range man
       | _ ->
         warn "Not handled: assign(%a, %a)" pp_var var pp_expr expr;
         assign_top var mode range man flow
      ) |>
      OptionExt.return

    | S_michelson_instr { instr = S_mcs_list_iter stm; sp; bef; _} ->
      let t_list = stack_get_type_spi bef (sp-1) in
      let () =
        match t_list with
        | T_mcs_list t -> ()
        | _ -> panic "Expected list type, got %a" pp_typ t_list
      in
      let vlist = mk_stackpos t_list (sp-1) in
      let cur = get_env T_cur man flow in
      (match find_opt vlist cur with
       | None -> Flow.bottom_from flow |> Post.return
       | Some v ->
         (match v with
          | V_bot
          | V_empty ->
            man.exec (mk_remove (mk_var vlist range) range) flow
          | V_list
          | V_top ->
            list_iter (mk_var vlist range) stm sp bef range man flow
         )
      ) |>
      OptionExt.return

    (* XXX: stack manipulation here, that is pretty ugly *)
    | S_michelson_instr { instr = S_mcs_list_map stm; sp; bef; aft; _ } ->
      let t_olist = stack_get_type_spi bef (sp-1) in
      let t_nlist = stack_get_type_spi aft (sp-1) in
      let t_oitem, t_nitem =
        match t_olist, t_nlist with
        | T_mcs_list t1, T_mcs_list t2 -> t1, t2
        | _ -> panic "Expected list types, got %a and %a" pp_typ t_olist pp_typ t_nlist
      in
      (* the old list *)
      let olist = mk_var (mk_stackpos t_olist (sp-1)) range in
      (* the new list *)
      let vnlist = mk_stackpos t_nlist (sp-1) in
      let nlist = mk_var vnlist range in
      (* aux variables for storing list *)
      let tmp_olist = mk_var (mk_range_attr_var range "oril" t_olist) range in
      let tmp_nlist = mk_var (mk_range_attr_var range "resl" t_nlist) range in
      (* item variable for exec stm *)
      let pre_item = mk_var (mk_stackpos t_oitem (sp-1)) range in
      let post_item = mk_var (mk_stackpos t_nitem (sp-1)) range in
      (* ghost variables *)
      let size = mk_var (mk_range_attr_var range "size" list_size_type) range in
      let counter = mk_var (mk_range_attr_var range "counter" list_size_type) range in
      (* adds ghost variables for size and counter *)
      man.exec (mk_add size range) flow >>%
      man.exec (mk_add counter range) >>%? fun flow ->
      man.exec (mk_assign size (Unstacked_ast.mk_list_size olist range) range) flow >>%? fun flow ->
      man.exec (mk_assign counter (mk_int ~typ:list_size_type 0 range) range) flow >>%? fun flow ->
      (* rename the old list to aux variable *)
      man.exec (mk_rename olist tmp_olist range) flow >>%? fun flow ->
      (* adds a new list for storing the result, sets as empty list *)
      man.exec (mk_add tmp_nlist range) flow >>%? fun flow ->
      man.exec (mk_assign tmp_nlist (Unstacked_ast.mk_list_nil t_nlist range) range) flow >>%? fun flow ->
      (* prepare the stack before exec stm *)
      let pre_block =
        [
          mk_add pre_item range;
          mk_assign pre_item (Unstacked_ast.mk_list_head tmp_olist range) range;
        ]
      in
      let post_block =
        [
          mk_assign tmp_nlist (Unstacked_ast.mk_list_cons ~item:post_item ~list:tmp_nlist range) range;
          mk_remove post_item range;
        ]
      in
      let counter_block =
        [
          mk_assign counter
            (mk_mcs_binop ~etyp:list_size_type
               counter
               O_plus
               (mk_int ~typ:list_size_type 1 range)
               range
            )
            range;
        ]
      in
      let loop =
        mk_while
          (lt counter size range)
          (mk_block
             [
               mk_block pre_block range;
               stm;
               mk_block post_block range;
               mk_block counter_block range;
             ]
             range
          )
          range
      in
      man.exec loop flow >>%
      man.exec (mk_rename tmp_nlist nlist range) >>%? fun flow ->
      let list_size = mk_var (mk_var_list_size vnlist) range in
      man.exec (mk_assign list_size size range) flow >>%
      man.exec (mk_remove tmp_olist range) >>%
      man.exec (mk_remove counter range) >>%? fun flow ->
      man.exec (mk_remove size range) flow |>
      OptionExt.return

    | S_mcs_inter_operation_list_iter { operations } ->
      assume (Unstacked_ast.mk_list_is_nil operations range)
        ~fthen:(fun flow -> Post.return flow)
        ~felse:(fun flow ->
            (* FIXME: list_head works, but would like to access item ideally *)
            let transfer = mk_list_head operations range in
            assume (Unstacked_ast.mk_is_transfer transfer range)
              ~fthen:(fun flow ->
                  man.exec (mk_stmt (S_mcs_inter_exec_transfer { transfer }) range) flow
                )
              ~felse:(fun flow ->
                  Post.return flow
                )
              man flow
          )
        man flow |>
      OptionExt.return

    | S_assume({ekind=E_mcs_list_is_nil(expr); _}) ->
      debug "%a" pp_stmt stmt;
      (match ekind expr with
       | E_constant (C_mcs_list []) ->
         Post.return flow
       | E_constant (C_mcs_list _) ->
         Post.return (Flow.bottom_from flow)
       | E_mcs_list_cons _ ->
         Post.return (Flow.bottom_from flow)
       | E_mcs_list_tail expr ->
         switch [
           [gt (mk_list_size expr range) (mk_mcs_int 0 range) range],
           (fun flow -> Flow.bottom_from flow |> Post.return);
           [eq (mk_list_size expr range) (mk_mcs_int 0 range) range],
           (fun flow -> Post.return flow);
         ] man flow
       | E_var(var, _) ->
         let cur = get_env T_cur man flow in
         (match M.find_opt var cur with
          | None -> panic "assume on non-existing variable: %a" pp_var var
          | Some v ->
            (let open Raw_list in
             match v with
             | V_bot
             | V_list ->
               Post.return (Flow.bottom_from flow)
             | V_empty ->
               Post.return flow
             | V_top ->
               let expr = mk_binop expr O_eq (mk_list_nil expr.etyp range) range in
               man.exec (mk_assume expr range) flow
            )
         )
       | _ ->
         warn "Not handled: assume(%a)" pp_expr expr;
         Post.return flow
      ) |>
      OptionExt.return

    | S_assume({ekind=E_unop(O_log_not, {ekind=E_mcs_list_is_nil(expr); _}); _}) ->
      (match ekind expr with
       | E_constant (C_mcs_list []) ->
         Post.return (Flow.bottom_from flow)
       | E_constant (C_mcs_list _) ->
         Post.return flow
       | E_mcs_list_cons _ ->
         Post.return flow
       | E_mcs_list_tail expr ->
         switch [
           [gt (mk_list_size expr range) (mk_mcs_int 0 range) range],
           (fun flow -> Post.return flow);
           [eq (mk_list_size expr range) (mk_mcs_int 0 range) range],
           (fun flow -> Flow.bottom_from flow |> Post.return);
         ] man flow
       | E_var(var, _) ->
         let cur = get_env T_cur man flow in
         (match M.find_opt var cur with
          | None -> panic "assume on non-existing variable: %a" pp_var var
          | Some v ->
            (let open Raw_list in
             match v with
             | V_bot
             | V_empty ->
               Post.return (Flow.bottom_from flow)
             | V_list ->
               Post.return flow
             | V_top ->
               let expr = mk_binop expr O_ne (mk_list_nil expr.etyp range) range in
               man.exec (mk_assume expr range) flow
            )
         )
       | _ ->
         warn "Not handled: assume(%a)" pp_expr expr;
         Post.return flow
      ) |>
      OptionExt.return

    | S_assume({ekind=E_binop(O_eq, e1, e2); _}) when
        is_type_list e1.etyp &&
        is_type_list e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      (match ekind e1, ekind e2 with
       | E_var(v1, _), E_var(v2, _) ->
         let map = get_env T_cur man flow in
         let old_state1 = find v1 map in
         let old_state2 = find v2 map in
         let new_state = Raw_list.meet old_state1 old_state2 in
         let map = add v1 new_state map in
         let map = add v2 new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state1 new_state v1 range man flow >>%
         update_aux_vars old_state2 new_state v2 range man >>% fun flow ->
         (if Raw_list.subset Raw_list.V_list new_state then
           let item1 = mk_var (mk_var_list_item v1) range in
           let item2 = mk_var (mk_var_list_item v2) range in
           let size1 = mk_var (mk_var_list_size v1) range in
           let size2 = mk_var (mk_var_list_size v2) range in
           man.exec (mk_assume (eq item1 item2 range) range) flow >>%
           man.exec (mk_assume (eq size1 size2 range) range)
         else
           Post.return flow
        )
       | E_var(var, _), E_constant (C_mcs_list [])
       | E_constant (C_mcs_list []), E_var(var, _) ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state = Raw_list.meet old_state Raw_list.V_empty in
         let map = add var new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow
       | _ ->
         warn "Not handled: %a" pp_stmt stmt;
         Post.return flow
      ) |>
      OptionExt.return

    | S_assume({ekind=E_binop(O_ne, e1, e2); _}) when
        is_type_list e1.etyp &&
        is_type_list e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      (match ekind e1, ekind e2 with
       | E_var(v1, _), E_var(v2, _) ->
         let map = get_env T_cur man flow in
         (let open Raw_list in
          match find v1 map, find v2 map with
          | V_bot, _
          | _, V_bot ->
            Post.return (Flow.bottom_from flow)
          | V_empty, V_empty ->
            Post.return (Flow.bottom_from flow)
          | V_top, V_top
          | V_list, V_list ->
            Post.return flow
          | V_empty, V_list
          | V_list, V_empty ->
            Post.return flow
          | V_list, V_top
          | V_top, V_list ->
            Post.return flow
          | V_empty, V_top ->
            let map = add v2 V_list map in
            let flow = set_env T_cur map man flow in
            Post.return flow
          | V_top, V_empty ->
            let map = add v1 V_list map in
            let flow = set_env T_cur map man flow in
            Post.return flow
         )
       | E_var(var, _), E_constant (C_mcs_list [])
       | E_constant (C_mcs_list []), E_var(var, _) ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state =
           let open Raw_list in
           match old_state with
           | V_bot -> V_bot
           | V_empty -> V_bot
           | V_list -> V_list
           | V_top -> V_list
         in
         let map = add var new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow
       | _ ->
         warn "Not handled: %a" pp_stmt stmt;
         Post.return flow
      ) |>
      OptionExt.return

    | _ -> None

  let eval expr man flow =
    let range = erange expr in
    match ekind expr with

    | E_mcs_list_is_nil(expr) ->
      (match ekind expr with
       | E_constant (C_top (T_mcs_list _)) ->
         Eval.singleton (mk_top T_mcs_bool range) flow |> OptionExt.return
       | E_constant (C_mcs_list []) ->
         Eval.singleton (mk_mcs_true range) flow |> OptionExt.return
       | E_constant (C_mcs_list _) ->
         Eval.singleton (mk_mcs_false range) flow |> OptionExt.return
       | E_mcs_list_cons _ ->
         Eval.singleton (mk_mcs_false range) flow |> OptionExt.return
       | E_mcs_list_tail _ ->
         (* XXX: could check size *)
         Eval.singleton (mk_top T_mcs_bool range) flow |> OptionExt.return
       | E_var(var, _) ->
         let cur = get_env T_cur man flow in
         (match M.find_opt var cur with
          | None -> Eval.empty flow |> OptionExt.return
          | Some v ->
            (let open Raw_list in
             match v with
             | V_bot ->
               Eval.empty flow |> OptionExt.return
             | V_empty ->
               Eval.singleton (mk_mcs_true range) flow |> OptionExt.return
             | V_list ->
               Eval.singleton (mk_mcs_false range) flow |> OptionExt.return
             | V_top ->
               Eval.singleton (mk_top T_mcs_bool range) flow |>
               OptionExt.return
            )
         )
       | _ ->
         warn "Not handled: list_is_nil(%a)" pp_expr expr;
         Eval.singleton (mk_top T_mcs_bool range) flow |>
         OptionExt.return
      )

    | E_mcs_list_size (expr) ->
      debug "E_mcs_list_size(%a)" pp_expr expr;
      warn "%s:FIXME: introduces a potential loss of precision" __LOC__;
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_constant (C_top (T_mcs_list _)) ->
         man.eval (mk_top list_size_type range) flow
       | E_constant (C_mcs_list l) ->
         man.eval (mk_int ~typ:list_size_type (List.length l) range) flow
       | E_mcs_list_cons(_, e) ->
         (* FIXME: could be slow, maybe try to unroll cases *)
         let sz = mk_binop ~etyp:list_size_type
             (mk_list_size e range)
             O_plus
             (mk_int ~typ:list_size_type 1 range)
             range
         in
         man.eval sz flow
       | E_mcs_list_tail e ->
         (* FIXME: could be slow, maybe try to unroll cases *)
         let sz = mk_binop ~etyp:list_size_type
             (mk_list_size e range)
             O_minus
             (mk_int ~typ:list_size_type 1 range)
             range
         in
         man.eval sz flow
       | E_var (var, _) ->
         let cur = get_env T_cur man flow in
         (match M.find_opt var cur with
          | None -> Eval.empty flow
          | Some v ->
            (match v with
             | V_bot -> Eval.empty flow
             | V_empty -> man.eval (mk_int ~typ:list_size_type 0 range) flow
             | V_list ->
               let size = mk_var (mk_var_list_size var) range in
               man.eval size flow
             | V_top ->
               let size = mk_var (mk_var_list_size var) range in
               Eval.join
                 (man.eval size flow)
                 (man.eval (mk_int ~typ:list_size_type 0 range) flow)
            )
         )
       | _ ->
         warn "Not handled: list_size(%a)" pp_expr expr;
         Eval.singleton (mk_top list_size_type range) flow
      ) |> OptionExt.return

    | E_mcs_list_head e ->
      (match ekind e with
       | E_constant(C_top (T_mcs_list ty)) ->
         man.eval (mk_top ty range) flow
       | E_constant(C_mcs_list (hd::tl))  ->
         man.eval (mk_constant ~etyp:(etyp expr) hd range) flow
       | E_mcs_list_cons(e_item, _) ->
         (* XXX: is man.eval needed here ? *)
         man.eval e_item flow
       | E_var(var, _) ->
         let map = get_env T_cur man flow in
         (match find_opt var map with
          | None | Some (V_bot|V_empty) -> Eval.empty flow
          | _  ->
            let item = mk_var (mk_var_list_item var) range in
            man.eval item flow
         )
       | _ ->
         warn "Not handled: list_head(%a)" pp_expr expr;
         Eval.singleton (mk_top (etyp expr) range) flow
      ) |> OptionExt.return

    | _ -> None

  let ask _ _ _ = None
  let refine _ _ _ = assert false
  let merge _ _ = assert false
  let widen = widen

end

let () =
  Framework.Sig.Abstraction.Domain.register_standard_domain (module Domain)
