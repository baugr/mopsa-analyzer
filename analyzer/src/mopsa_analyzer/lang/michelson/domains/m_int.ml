open Mopsa
open Universal.Ast
open Ast
open Unstacked_ast

module Domain = struct
  open M_common

  include GenStatelessDomainId(struct
      let name = "michelson.types.integers"
    end)

  let universal = Semantic "Universal"

  let checks = [ Alarms.CHK_MCL_SHIFT_OVERFLOW ]

  let print_expr _man _flow _printer _expr = ()

  let assume_num = Universal.Numeric.Common.assume_num


  let is_type_bool t =
    match t with
    | T_mcs_bool -> true
    | _ -> false

  let is_type_int t =
    match t with
    | T_mcs_int -> true
    | _ -> false

  let to_num_type t =
    match t with
    | T_mcs_int -> T_int
    | _ -> panic "Unexpected type : %a" pp_typ t

  let mk_num_var v = { v with vtyp = to_num_type v.vtyp }

  let mk_num_var v =
    let vname = Format.asprintf "num(%s)" v.vname in
    mkv vname (V_mcs_num v) (to_num_type v.vtyp)
      ~mode:v.vmode ~semantic:"Universal"

  let mk_num_var_expr e =
    match ekind e with
    | E_var (v, mode) -> mk_var (mk_num_var v) ~mode e.erange
    | _ -> assert false


  let init _ _ flow = flow

  let exec stmt man flow : 'a post option =
    let range = srange stmt in
    match skind stmt with
    | S_add({ekind=E_var (v, _); _} as expr) when is_type_int v.vtyp ->
      let expr = mk_num_var_expr expr in
      man.exec (mk_add expr range) flow |>
      OptionExt.return

    | S_remove({ekind=E_var(v, _); _} as expr) when is_type_int v.vtyp ->
      let expr = mk_num_var_expr expr in
      man.exec (mk_remove expr range) flow |>
      OptionExt.return

    | S_assign(({ekind=E_var (v, _); _} as lval), rval) when is_type_int v.vtyp ->
      check_types T_mcs_int lval.etyp;
      check_types T_mcs_int rval.etyp;
      man.eval ~translate:"Universal" lval flow >>$? fun lval flow ->
      man.eval ~translate:"Universal" rval flow >>$? fun rval flow ->
      man.exec (mk_assign lval rval range) flow |>
      OptionExt.return

    | S_rename(({ekind=E_var _; _} as src),
               ({ekind=E_var _; _} as dst)) when is_type_int src.etyp &&
                                                 is_type_int dst.etyp ->
      let src = mk_num_var_expr src in
      let dst = mk_num_var_expr dst in
      man.exec (mk_rename src dst range) flow |>
      OptionExt.return

    | S_expand({ekind = E_var _; _} as e, el)
      when is_type_int e.etyp &&
           List.for_all (fun ee -> is_type_int ee.etyp) el
      ->
      let v = mk_num_var_expr e in
      let vl = List.map mk_num_var_expr el in
      man.exec (mk_expand v vl range) flow |>
      OptionExt.return

    | S_fold(({ekind = E_var _; _} as e), el)
      when is_type_int e.etyp &&
           List.for_all (fun ee -> is_type_int ee.etyp) el
      ->
      let v = mk_num_var_expr e in
      let vl = List.map mk_num_var_expr el in
      man.exec (mk_fold v vl range) flow |>
      OptionExt.return

    | S_forget ({ekind = E_var _; _} as v) when is_type_int v.etyp ->
      let vv = mk_num_var_expr v in
      let top = mk_top vv.etyp range in
      man.exec (mk_assign vv top range) flow |>
      OptionExt.return

    | S_assume ({ekind=E_mcs_binop(op, e1, e2); _}) when
        is_type_int e1.etyp &&
        is_type_int e2.etyp ->
      warn "HERE, S_assume (%a)" pp_stmt stmt;
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      man.exec (mk_assume (mk_binop ~etyp:T_bool e1 op e2 range) range) flow |>
      OptionExt.return

    | S_assume({ekind=E_unop(O_log_not, ({ekind=E_mcs_binop(op, e1, e2); _})); _}) ->
      warn "HERE, S_assume (%a)" pp_stmt stmt;
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      man.exec (mk_assume (mk_not (mk_binop ~etyp:T_bool e1 op e2 range) range) range) flow |>
      OptionExt.return


    | _ -> None


  let eval_ediv ~etyp e1 e2 range man flow =
    man.eval e1 flow >>$ fun e1 flow ->
    man.eval e2 flow >>$ fun e2 flow ->
    let cond = mk_ne ~etyp:T_mcs_bool e2 (mk_zero ~typ:T_mcs_int range) range in
    assume cond
      ~fthen:(fun flow ->
          let div =
            let e1 = get_expr_translation "Universal" e1 in
            let e2 = get_expr_translation "Universal" e2 in
            let expr = mk_binop ~etyp:T_int e1 O_ediv e2 range in
            mk_universal_expression ~etyp:T_mcs_int expr range
          in
          let rem =
            let e1 = get_expr_translation "Universal" e1 in
            let e2 = get_expr_translation "Universal" e2 in
            let expr = mk_binop ~etyp:T_int e1 O_erem e2 range in
            mk_universal_expression ~etyp:T_mcs_int expr range
          in
          (* call eval because we leave the m_int domain *)
          man.eval (mk_some (mk_pair div rem range) range) flow
        )
      ~felse:(fun flow ->
          (* call eval because we leave the m_int domain *)
          man.eval (mk_none etyp range) flow
        )
      man flow

  let eval expr man flow : 'a eval option =
    let range = erange expr in
    match expr with

    | {ekind=E_constant (C_top _); _} when is_type_int expr.etyp ->
      let nexpr = mk_top (to_num_type expr.etyp) range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | {ekind=E_constant ((C_int _ | C_int_interval _) as c); _} when is_type_int expr.etyp ->
      let nexpr = mk_constant ~etyp:(to_num_type expr.etyp) c range in
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

      (* rewrite to evaluate embedded universal expression *)
    | {ekind=E_mcs_universal_expression e; _} when is_type_int expr.etyp ->
      let expr' = add_expr_translation "Universal" e expr in
      Eval.singleton expr' flow |>
      OptionExt.return

    | {ekind=E_var(v, _); _} when is_type_int expr.etyp ->
      let nexpr = mk_num_var_expr expr in
      debug "eval E_var(%a : %a)" pp_var v pp_typ v.vtyp;
      debug "eval E_var... translated to %a" pp_expr_typ nexpr;
      Eval.singleton expr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    (* Unary operators translated to numeric *)
    | { ekind = E_mcs_unop((O_minus|O_abs|O_bit_invert) as op, e); _ }
      when is_type_int e.etyp ->
      man.eval e flow >>$? fun e flow ->
      (* michelson expr *)
      let mexpr = { expr with ekind = E_mcs_unop(op, e) } in
      (* numeric expr *)
      let nexpr = { expr with ekind = E_unop(op, e) } in
      (* rebuild numeric expression from translations *)
      let nexpr = michelson2num nexpr in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    | { ekind = E_mcs_unop(O_isnat, e); etyp=T_mcs_option _; _ }
      when is_type_int e.etyp ->
      let cond = mk_mcs_binop ~etyp:T_mcs_bool
          e O_ge (mk_int ~typ:T_mcs_int 0 range)
          range
      in
      assume cond man flow
        ~fthen:(fun flow -> man.eval (mk_some e range) flow)
        ~felse:(fun flow -> man.eval (mk_none (T_mcs_option T_mcs_int) range) flow) |>
      OptionExt.return

    (* Guarded binary operators : raise error if right operand > 256 *)
    | { ekind = E_mcs_binop(((O_bit_lshift|O_bit_rshift) as op), e1, e2); _ } ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      (* test e2 in numeric for > 256 *)
      let ne2 = get_expr_translation "Universal" e2 in
      let test = mk_le ne2 (mk_int ~typ:T_int 256 range) range in
      assume_num ~route:universal test
        ~fthen:(fun flow ->
            let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
            let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
            let nexpr = michelson2num nexpr in
            let flow = Alarms.safe_mcl_overflow_check range man flow in
            Eval.singleton mexpr flow |>
            Eval.add_translation "Universal" nexpr
          )
        ~felse:(fun flow ->
            let flow = Alarms.raise_mcl_shift_overflow_alarm expr range man flow in
            Eval.empty (Flow.bottom_from flow))
        man flow |>
      OptionExt.return

    (* Standard binary operators *)
    | { ekind = E_mcs_binop((O_plus|O_minus|O_mult|O_bit_and|O_bit_or|O_bit_xor) as op, e1, e2); _ }
      when is_type_int expr.etyp &&
           is_type_int e1.etyp &&
           is_type_int e2.etyp ->
      debug "eval (E_mcs_binop(%a %a %a))" pp_expr e1 pp_operator op pp_expr e2;
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
      let nexpr = michelson2num nexpr in
      let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
      debug "calling michelson2num of mexpr";
      debug "==> michelson  expr = %a" pp_expr_typ mexpr;
      debug "==> translated expr = %a" pp_expr_typ nexpr;
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    (* Comparison operators *)
    | { ekind = E_mcs_binop(((O_eq|O_ne|O_gt|O_ge|O_lt|O_le) as op), e1, e2); _ }
      when is_type_bool expr.etyp &&
           is_type_int e1.etyp &&
           is_type_int e2.etyp ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      let nexpr = { expr with ekind = E_binop(op, e1, e2) } in
      let nexpr = michelson2num nexpr in
      let mexpr = { expr with ekind = E_mcs_binop(op, e1, e2) } in
      Eval.singleton mexpr flow |>
      Eval.add_translation "Universal" nexpr |>
      OptionExt.return

    (* Euclidian division *)
    | { ekind = E_mcs_binop(O_div, e1, e2);
        etyp=T_mcs_option(T_mcs_pair(t_num, t_den)); _}
      when is_type_int e1.etyp &&
           is_type_int e2.etyp &&
           is_type_int t_num &&
           is_type_int t_den ->
      eval_ediv ~etyp:expr.etyp e1 e2 range man flow |>
      OptionExt.return

    (* Michelson' COMPARE *)
    | { ekind = E_mcs_compare(({ etyp = T_mcs_int; _} as e1),
                              ({ etyp = T_mcs_int; _} as e2)); _ } ->
      man.eval e1 flow >>$? fun e1 flow ->
      man.eval e2 flow >>$? fun e2 flow ->
      switch [
        [lt e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int (-1) range in
           let nexpr = { mexpr with etyp = to_num_type mexpr.etyp } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        );
        [eq e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int 0 range in
           let nexpr = { mexpr with etyp = to_num_type mexpr.etyp } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        );
        [gt e1 e2 range],
        (fun flow ->
           let mexpr = mk_int ~typ:T_mcs_int 1 range in
           let nexpr = { mexpr with etyp = to_num_type mexpr.etyp } in
           Eval.singleton mexpr flow |>
           Eval.add_translation "Universal" nexpr
        )
      ] man flow |> OptionExt.return

    | _ -> None

  let ask _ _ _ = None

  let refine _ _ _ = assert false
  let merge _ _ = assert false

end

let () =
  Framework.Sig.Abstraction.Stateless.register_stateless_domain (module Domain)
