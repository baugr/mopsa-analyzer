open Mopsa
open Ast
open Universal.Ast
open Unstacked_ast
open M_common

module Domain = struct

  module Raw_operation = struct
    type t =
      | V_top
      | V_bot
      | V_transfer
      | V_delegate_set
      | V_delegate_remove
      | V_delegate_top
      | V_contract_creation

    let print fmt = function
      | V_bot  -> pp_string fmt "⊥"
      | V_top  -> pp_string fmt "⊤"
      | V_transfer -> pp_string fmt "transfer"
      | V_delegate_set -> pp_string fmt "set_delegate"
      | V_delegate_remove -> pp_string fmt "remove_delegate"
      | V_delegate_top -> pp_string fmt "delegation"
      | V_contract_creation -> pp_string fmt "contract_creation"

    let bottom = V_bot
    let top = V_top

    let is_bottom = function
      | V_bot -> true
      | _ -> false

    let subset x y =
      let result =
        match x, y with
        | V_bot, _ -> true
        | _, V_bot -> false
        | V_top, _ -> false
        | _, V_top -> true
        (* delegates *)
        | V_delegate_set, V_delegate_set -> true
        | V_delegate_remove, V_delegate_remove -> true
        | (V_delegate_set|V_delegate_remove), (V_delegate_set|V_delegate_remove) -> false
        | (V_delegate_set|V_delegate_remove|V_delegate_top), V_delegate_top -> true
        | V_delegate_top, (V_delegate_set|V_delegate_remove) -> false
        (* transfers *)
        | V_transfer, V_transfer -> true
        (* contract creation *)
        | V_contract_creation, V_contract_creation -> true
        (* mixed *)
        | (V_delegate_set|V_delegate_remove|V_delegate_top),
          (V_transfer|V_contract_creation)
        | V_transfer, (V_delegate_set|V_delegate_remove|V_delegate_top|V_contract_creation)
        | V_contract_creation, (V_delegate_set|V_delegate_remove|V_delegate_top|V_transfer) -> false
      in
      debug "subset (%a) (%a) = (%b)"
        (format print) x
        (format print) y
        result;
      result

    let join x y =
      let result = match x, y with
        | v, V_bot
        | V_bot, v -> v
        | V_top, _
        | _, V_top -> V_top
        (* transfers *)
        | V_transfer, V_transfer -> V_transfer
        (* delegations *)
        | V_delegate_set, V_delegate_set -> V_delegate_set
        | V_delegate_remove, V_delegate_remove -> V_delegate_remove
        | (V_delegate_set|V_delegate_remove|V_delegate_top),
          (V_delegate_set|V_delegate_remove|V_delegate_top) -> V_delegate_top
        (* contracts creations *)
        | V_contract_creation, V_contract_creation -> V_contract_creation
        (* mixed *)
        | V_transfer, (V_delegate_set|V_delegate_remove|V_delegate_top|V_contract_creation)
        | V_contract_creation, (V_transfer|V_delegate_set|V_delegate_remove|V_delegate_top) -> V_top
        | (V_delegate_set|V_delegate_remove|V_delegate_top), (V_transfer|V_contract_creation) -> V_top
      in
      debug "join (%a) (%a) = (%a)"
        (format print) x
        (format print) y
        (format print) result;
      result

    let meet x y =
      let result = match x, y with
      | _, V_bot
      | V_bot, _ -> V_bot
      | V_top, y' -> y'
      | x', V_top -> x'
      (* transfers *)
      | V_transfer, V_transfer -> V_transfer
      (* contracts creation *)
      | V_contract_creation, V_contract_creation -> V_contract_creation
      (* delegate set *)
      | V_delegate_set, (V_delegate_set|V_delegate_top) -> V_delegate_set
      | V_delegate_top, V_delegate_set -> V_delegate_set
      | V_delegate_set, V_delegate_remove -> V_bot
      (* delegate remove *)
      | V_delegate_remove, (V_delegate_remove|V_delegate_top)
      | V_delegate_top, V_delegate_remove -> V_delegate_remove
      | V_delegate_remove, V_delegate_set -> V_bot
      | V_delegate_top, V_delegate_top -> V_delegate_top
      (* mixed *)
      | V_transfer, (V_delegate_set|V_delegate_remove|V_delegate_top|V_contract_creation)
      | (V_delegate_set|V_delegate_remove|V_delegate_top), (V_transfer|V_contract_creation)
      | V_contract_creation, (V_transfer|V_delegate_set|V_delegate_remove|V_delegate_top) -> V_bot
      in
      debug "meet (%a) (%a) = (%a)"
        (format print) x
        (format print) y
        (format print) result;
      result

    let widen _ x y = join x y

  end

  module M = Framework.Lattices.Partial_map.Make(Var)(Raw_operation)
  include M

  (* Override find with more explicit message *)
  let find v map = match find_opt v map with
    | None -> panic "Cannot find : %a" pp_var v
    | Some v -> v

  include Framework.Core.Id.GenDomainId(struct
      type nonrec t = t
      let name = "michelson.types.operations"
    end)

  let print_state printer a =
    pprint ~path:[Key "operations"] printer (pbox M.print a)

  let print_expr _ _ _ _ = ()

  let init _ man flow =
    set_env T_cur M.empty man flow

  let checks = []

  (****************************************************************************
   * Helpers
   ***************************************************************************)
  let is_type_operation t =
    match t with
    | T_mcs_operation -> true
    | _ -> false

  let mk_var_contract var =
    let name = "contract" in
    let mode = var.vmode in
    mk_var_ghost ~mode T_mcs_address var name

  let mk_var_mutez var =
    let name = "amount" in
    let mode = var.vmode in
    mk_var_ghost ~mode T_mcs_mutez var name

  let mk_var_delegate var =
    let name = "delegate" in
    let mode = var.vmode in
    mk_var_ghost ~mode T_mcs_key_hash var name

  let mk_var_parameter var =
    let name = "parameter" in
    let mode = var.vmode in
    mk_var_ghost ~mode T_mcs_alpha var name

  let update_aux_vars old_state new_state var range man flow =
    debug "update_aux_vars %a: %a -> %a"
      pp_var var
      (format Raw_operation.print) old_state
      (format Raw_operation.print) new_state
    ;
    let open Raw_operation in
    if old_state = new_state then
      Post.return flow
    else
      match new_state with
      (* cases in which we need to remove all variables *)
      | (V_delegate_remove|V_contract_creation|V_bot) ->
        (if subset V_delegate_set old_state then
           man.exec (mk_remove (mk_var (mk_var_delegate var) range) range) flow
         else
           Post.return flow
        ) >>% fun flow ->
        (if subset V_transfer old_state then
           man.exec (mk_remove (mk_var (mk_var_contract var) range) range) flow >>%
           man.exec (mk_remove (mk_var (mk_var_mutez var) range) range) >>%
           man.exec (mk_remove (mk_var (mk_var_parameter var) range) range)
         else
           Post.return flow
        )
      (* case new_state is V_transfer: remove all variables but contract & mutez *)
      | V_transfer ->
        (if subset V_delegate_set old_state then
           man.exec (mk_remove (mk_var (mk_var_delegate var) range) range) flow
         else
           Post.return flow
        ) >>% fun flow ->
        (if subset V_transfer old_state then
           Post.return flow
         else
           man.exec (mk_add (mk_var (mk_var_contract var) range) range) flow >>%
           man.exec (mk_add (mk_var (mk_var_mutez var) range) range) >>%
           man.exec (mk_add (mk_var (mk_var_parameter var) range) range)
        )
      (* case in which we need the delegate variable *)
      | (V_delegate_set|V_delegate_top) ->
        (if subset V_transfer old_state then
           man.exec (mk_remove (mk_var (mk_var_contract var) range) range) flow >>%
           man.exec (mk_remove (mk_var (mk_var_mutez var) range) range) >>%
           man.exec (mk_remove (mk_var (mk_var_parameter var) range) range)
         else
           Post.return flow
        ) >>% fun flow ->
        (if subset V_delegate_set old_state then
           Post.return flow
         else
           man.exec (mk_add (mk_var (mk_var_delegate var) range) range) flow
        )
      (* case in which we need all variables *)
      | V_top ->
        debug "I'm here";
        (if subset V_delegate_set old_state then
           Post.return flow
         else
           man.exec (mk_add (mk_var (mk_var_delegate var) range) range) flow
        ) >>% fun flow ->
        (if subset V_transfer old_state then
           Post.return flow
         else
           man.exec (mk_add (mk_var (mk_var_contract var) range) range) flow >>%
           man.exec (mk_add (mk_var (mk_var_mutez var) range) range) >>%
           man.exec (mk_add (mk_var (mk_var_parameter var) range) range)
        )

  let add2 var mode old_state new_state map =
    match var_mode var mode with
    | STRONG -> new_state, add var new_state map
    | WEAK ->
      let new_state = Raw_operation.join old_state new_state in
      new_state, add var new_state map


  (****************************************************************************
   * Transfer functions
   ***************************************************************************)
  let exec stmt man flow =
    let range = srange stmt in
    match skind stmt with

    (* Declare the variable *)
    | S_add ({ ekind=E_var(var, _); etyp; _}) when is_type_operation var.vtyp ->
      check_types var.vtyp etyp;
      debug "m_operation.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match find_opt var map with
        | Some _ -> panic "m_operation: adding variable already existing: %a" pp_var var
        | None -> Raw_operation.V_bot
      in
      let new_state = Raw_operation.V_top in
      let map = add var new_state map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    (* Remove the variable *)
    | S_remove ({ ekind=E_var(var, _); etyp; _ }) when is_type_operation var.vtyp ->
      check_types var.vtyp etyp;
      debug "m_operation.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let old_state = match find_opt var map with
        | Some v -> v
        | None -> panic "m_operation: removing variable not added: %a" pp_var var
      in
      let new_state = Raw_operation.V_bot in
      let map = remove var map in
      let flow = set_env T_cur map man flow in
      update_aux_vars old_state new_state var range man flow |>
      OptionExt.return

    (* rename the variable *)
    | S_rename ({ekind=E_var(src, _); etyp; _ },
                {ekind=E_var(dst, _); _}) when is_type_operation src.vtyp ->
      check_types src.vtyp etyp;
      check_types dst.vtyp etyp;
      debug "m_operation.%a" pp_stmt stmt;
      let map = get_env T_cur man flow in
      let new_state = find src map in
      let map = add dst new_state map in
      let map = remove src map in
      let flow = set_env T_cur map man flow in
      (if Raw_operation.(subset V_transfer new_state) then
         let src_mutez = mk_var (mk_var_mutez src) range in
         let dst_mutez = mk_var (mk_var_mutez dst) range in
         let src_contract = mk_var (mk_var_contract src) range in
         let dst_contract = mk_var (mk_var_contract dst) range in
         let src_parameter = mk_var (mk_var_parameter src) range in
         let dst_parameter = mk_var (mk_var_parameter dst) range in
         man.exec (mk_rename src_mutez dst_mutez range) flow >>%
         man.exec (mk_rename src_contract dst_contract range) >>%
         man.exec (mk_rename src_parameter dst_parameter range)
       else
         Post.return flow
      ) >>%? fun flow ->
      (if Raw_operation.(subset V_delegate_set new_state) then
         let src_delegate = mk_var (mk_var_delegate src) range in
         let dst_delegate = mk_var (mk_var_delegate dst) range in
         man.exec (mk_rename src_delegate dst_delegate range) flow
       else
         Post.return flow
      ) |>
      OptionExt.return

    | S_assign({ekind=E_var(var, mode); etyp; _}, expr) when
        is_type_operation var.vtyp ->
      check_types var.vtyp etyp;
      check_types expr.etyp etyp;
      debug "m_operation.%a" pp_stmt stmt;
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_constant (C_top T_mcs_operation) ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state = Raw_operation.V_top in
         let map = add var new_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow |>
         OptionExt.return
       | E_var (src, _) ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let src_state = find src map in
         let new_state, map = add2 var mode old_state src_state map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         (if Raw_operation.(subset V_transfer src_state) then
            let dst_contract = mk_var ~mode (mk_var_contract var) range in
            let src_contract = mk_var ~mode (mk_var_contract src) range in
            let dst_amount = mk_var ~mode (mk_var_mutez var) range in
            let src_amount = mk_var ~mode (mk_var_mutez src) range in
            let dst_parameter = mk_var ~mode (mk_var_parameter var) range in
            let src_parameter = mk_var ~mode (mk_var_parameter src) range in
            man.exec (mk_assign dst_contract src_contract range) flow >>%
            man.exec (mk_assign dst_amount src_amount range) >>%
            man.exec (mk_assign dst_parameter src_parameter range)
          else
            Post.return flow
         ) >>%? fun flow ->
         (if Raw_operation.(subset V_delegate_set src_state) then
            let dst_delegate = mk_var ~mode (mk_var_delegate var) range in
            let src_delegate = mk_var ~mode (mk_var_delegate src) range in
            man.exec (mk_assign dst_delegate src_delegate range) flow
          else
            Post.return flow
         ) |>
         OptionExt.return
       | E_mcs_transfer { parameter; amount; contract } ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state, map = add2 var mode old_state Raw_operation.V_transfer map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         let v_contract = mk_var ~mode (mk_var_contract var) range in
         let v_amount = mk_var ~mode (mk_var_mutez var) range in
         let v_parameter = mk_var ~mode (mk_var_parameter var) range in
         man.exec (mk_assign v_contract contract range) flow >>%
         man.exec (mk_assign v_amount amount range) >>%
         man.exec (mk_assign v_parameter parameter range) |>
         OptionExt.return
       | E_mcs_add_delegate key_hash ->
         man.eval key_hash flow >>$? fun key_hash flow ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state, map = add2 var mode old_state Raw_operation.V_delegate_set map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow >>%? fun flow ->
         let delegate = mk_var ~mode (mk_var_delegate var) range in
         man.exec (mk_assign delegate key_hash range) flow |>
         OptionExt.return
       | E_mcs_remove_delegate ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state, map = add2 var mode old_state Raw_operation.V_delegate_remove  map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow |>
         OptionExt.return
       | E_mcs_create_contract _ ->
         let map = get_env T_cur man flow in
         let old_state = find var map in
         let new_state, map = add2 var mode old_state Raw_operation.V_contract_creation  map in
         let flow = set_env T_cur map man flow in
         update_aux_vars old_state new_state var range man flow |>
         OptionExt.return
       | _ -> None
      )

    | S_mcs_inter_exec_transfer { transfer } ->
      man.eval transfer flow >>$? fun transfer flow ->
      (match ekind transfer with
       | E_var (var, mode) ->
         let map = get_env T_cur man flow in
         let state = find var map in
         (match state with
          | Raw_operation.V_top
          | Raw_operation.V_transfer ->
            let address = mk_var ~mode (mk_var_contract var) range in
            let amount = mk_var ~mode (mk_var_mutez var) range in
            let parameter = mk_var ~mode (mk_var_parameter var) range in
            let stm = mk_stmt
              (S_mcs_inter_exec_transfer_address
                 { address; amount; parameter }) range
            in
            man.exec stm flow
          | _ ->
            warn "Unhandled operation. Undefined results !";
            Post.return flow
         )
       | _ -> assert false
      ) |>
      OptionExt.return

    | _ -> None

  let exec stmt man flow =
    let map = get_env T_cur man flow in
    (* XXX: checking for bottom seems to be necessary in order to pass empty env
     * which are due to FAILURES *)
    if is_bottom map then
      Post.return flow |> OptionExt.return
    else
      exec stmt man flow

  let eval expr man flow =
    let range = erange expr in
    match ekind expr with
    | E_mcs_is_transfer expr ->
      (* NOTE: expr must be evaluated, could be a list_head *)
      man.eval expr flow >>$? fun expr flow ->
      (match ekind expr with
       | E_mcs_transfer _ -> man.eval (mk_mcs_bool true range) flow
       | E_var (var, _) ->
         let map = get_env T_cur man flow in
         let value = find var map in
         (match value with
          | Raw_operation.V_bot -> assert false
          | Raw_operation.V_transfer
          | Raw_operation.V_top -> man.eval (mk_mcs_bool true range) flow
          | _ -> man.eval (mk_mcs_bool false range) flow
         )
       | _ ->
         warn "Unhandled: is_transfer(%a)" pp_expr expr;
         man.eval (mk_top T_mcs_bool range) flow
      ) |>
      OptionExt.return
    | _ -> None

  let refine _ _ _ = assert false
  let merge _ _ = assert false

  let subset _man _sman _ctx (a, s) (a', s') =
    (M.subset a a', s, s')

  let add_delegate var range man flow =
    man.exec (mk_add (mk_var (mk_var_delegate var) range) range) flow

  let add_transfer var range man flow =
    man.exec (mk_add (mk_var (mk_var_mutez var) range) range) flow >>%
    man.exec (mk_add (mk_var (mk_var_contract var) range) range) >>%
    man.exec (mk_add (mk_var (mk_var_parameter var) range) range)

  (* FIXME: is it safe to use fresh_range here ? *)
  let unify_range = tag_range (mk_fresh_range ()) "operation-unification"

  let join man sman ctx (a, s) (a', s') =
    let add_delegate man sman var a op acc =
      let acc =
        (if not Raw_operation.(subset V_delegate_set op) then
           sub_env_exec (add_delegate var unify_range man) ctx man sman a acc |>
           snd
         else
           acc
        )
      in
      acc
    in
    let add_transfer man sman var a op acc =
      let acc =
        (if not Raw_operation.(subset V_transfer op) then
           sub_env_exec (add_transfer var unify_range man) ctx man sman a acc |>
           snd
         else
           acc
        )
      in
      acc
    in
    let s, s' =
      fold2zo
        (fun var op1 (acc1, acc2) ->
           debug "folding on var: %a: op1=%a" pp_var var
             (format Raw_operation.print) op1;
           acc1, acc2
        )
        (fun var op2 (acc1, acc2) ->
           debug "folding on var: %a: op2=%a" pp_var var
             (format Raw_operation.print) op2;
           acc1, acc2
        )
        (fun var (op1:Raw_operation.t) (op2:Raw_operation.t) (acc1, acc2) ->
           debug "folding on var: %a: op1=%a; op2=%a" pp_var var
             (format Raw_operation.print) op1
             (format Raw_operation.print) op2;
           let open Raw_operation in
           let op = join op1 op2 in
           match op with
           | V_top ->
             let acc1 = add_delegate man sman var a op1 acc1 in
             let acc1 = add_transfer man sman var a op1 acc1 in
             let acc2 = add_delegate man sman var a' op2 acc2 in
             let acc2 = add_transfer man sman var a' op2 acc2 in
             acc1, acc2
           | V_delegate_top ->
             let acc1 = add_delegate man sman var a op1 acc1 in
             let acc2 = add_delegate man sman var a' op2 acc2 in
             acc1, acc2
           | V_transfer ->
             let acc1 = add_transfer man sman var a op1 acc1 in
             let acc2 = add_transfer man sman var a' op2 acc2 in
             acc1, acc2
           | _ -> acc1, acc2
        )
        a a' (s, s')
    in
    let result = join a a' in
    debug "m_operation.join (%a) (%a) -> %a"
      (format print) a (format print) a' (format print) result;
    (result, s, s')

  let meet _man _sman _ctx (_a, _s) (_a', _s') =
    assert false

  let widen man sman ctx (a,s) (a',s') =
    let (a, s, s') = join man sman ctx (a,s) (a',s') in
    (a, s, s', true)

  let ask _ _ _ = None

end

let () =
  Framework.Sig.Abstraction.Stacked.register_stacked_domain(module Domain)
