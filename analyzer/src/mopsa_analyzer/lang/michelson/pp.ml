open Mopsa
open Ast

let pp_instr fmt (sp, instr) =
  let pp_block fmt lambda =
    Format.fprintf fmt "@[<v 0>{@[<v 2>@;";
    Format.fprintf fmt "%a" pp_stmt lambda;
    Format.fprintf fmt "@]@;}@]"
  in
  match instr with
    | S_mcs_drop _ -> Format.fprintf fmt "[%d] DROP" sp
    | S_mcs_dropn n -> Format.fprintf fmt "[%d] DROP %d" sp n
    | S_mcs_dup _ -> Format.fprintf fmt "[%d] DUP" sp
    | S_mcs_swap _ -> Format.fprintf fmt "[%d] SWAP" sp
    | S_mcs_dig n -> Format.fprintf fmt "[%d] DIG %d" sp n
    | S_mcs_dug n -> Format.fprintf fmt "[%d] DUG %d" sp n
    | S_mcs_dip stmt -> Format.fprintf fmt "[%d] DIP %a" sp pp_block stmt
    | S_mcs_dipn (n, stmt) ->
      Format.fprintf fmt "[%d] DIP %d %a" sp n pp_block stmt
    (* Pairs *)
    | S_mcs_cons_pair _ -> Format.fprintf fmt "[%d] CONS_PAIR" sp
    | S_mcs_cdr _ -> Format.fprintf fmt "[%d] CDR" sp
    | S_mcs_car _ -> Format.fprintf fmt "[%d] CAR" sp
    (* Options *)
    | S_mcs_cons_some _ -> Format.fprintf fmt "[%d] SOME" sp
    | S_mcs_cons_none _ -> Format.fprintf fmt "[%d] NONE" sp
    | S_mcs_if_none (_, tstm, fstm) ->
      Format.fprintf fmt "[%d] IF_NONE @[<v 0>" sp;
      Format.fprintf fmt "@[<v 2>{@;%a@]@;}@;" pp_stmt tstm;
      Format.fprintf fmt "@[<v 2>{@;%a@]@;}@]" pp_stmt fstm
    (* Lists *)
    | S_mcs_nil _ -> Format.fprintf fmt "[%d] NIL" sp
    | S_mcs_cons_list _ -> Format.fprintf fmt "[%d] CONS" sp
    | S_mcs_list_size _ -> Format.fprintf fmt "[%d] LIST_SIZE" sp
    | S_mcs_if_cons (_, tstm, fstm) ->
      Format.fprintf fmt "[%d] IF_CONS @[<v 0>" sp;
      Format.fprintf fmt "@[<v 2>{@;%a@]@;}@;" pp_stmt tstm;
      Format.fprintf fmt "@[<v 2>{@;%a@]@;}@]" pp_stmt fstm
    | S_mcs_list_iter stm ->
      Format.fprintf fmt "[%d] LIST_ITER %a" sp pp_block stm
    | S_mcs_list_map stm ->
      Format.fprintf fmt "[%d] LIST_MAP %a" sp pp_block stm
    (* Sets *)
    | S_mcs_empty_set ty -> Format.fprintf fmt "[%d] EMPTY_SET %a" sp pp_typ ty
    | S_mcs_set_update _ -> Format.fprintf fmt "[%d] SET_UPDATE" sp
    | S_mcs_set_mem _ -> Format.fprintf fmt "[%d] SET_MEM" sp
    | S_mcs_set_size -> Format.fprintf fmt "[%d] SET_SIZE" sp
    | S_mcs_set_iter lambda ->
      Format.fprintf fmt "[%d] SET_ITER %a" sp pp_block lambda
    (* Maps *)
    | S_mcs_empty_map { kty; vty } ->
      Format.fprintf fmt "[%d] EMPTY_MAP %a %a" sp pp_typ kty pp_typ vty
    | S_mcs_map_update _ ->
      Format.fprintf fmt "[%d] MAP_UPDATE" sp
    | S_mcs_map_get _ ->
      Format.fprintf fmt "[%d] MAP_GET" sp
    | S_mcs_map_mem _ ->
      Format.fprintf fmt "[%d] MAP_MEM" sp
    | S_mcs_map_size _ ->
      Format.fprintf fmt "[%d] MAP_SIZE" sp
    | S_mcs_map_iter stm ->
      Format.fprintf fmt "[%d] MAP_ITER %a" sp pp_block stm
    | S_mcs_map_map stm ->
      Format.fprintf fmt "[%d] MAP_MAP %a" sp pp_block stm
    (* Big maps *)
    | S_mcs_empty_big_map ->
      Format.fprintf fmt "[%d] EMPTY_BIG_MAP" sp
    | S_mcs_big_map_get ->
      Format.fprintf fmt "[%d] BIG_MAP_GET" sp
    | S_mcs_big_map_mem ->
      Format.fprintf fmt "[%d] BIG_MAP_MEM" sp
    | S_mcs_big_map_update ->
      Format.fprintf fmt "[%d] BIG_MAP_UPDATE" sp
    (* Unions *)
    | S_mcs_cons_left (T_mcs_union(_, _, ty, _)) ->
      Format.fprintf fmt "[%d] LEFT %a" sp pp_typ ty
    | S_mcs_cons_right (T_mcs_union(ty, _, _, _)) ->
      Format.fprintf fmt "[%d] RIGHT %a" sp pp_typ ty
    | S_mcs_cons_left _
    | S_mcs_cons_right _ -> assert false
    | S_mcs_if_left(_, tstm, fstm) ->
      Format.fprintf fmt "[%d] IF_LEFT @[<v 0>" sp;
      Format.fprintf fmt "@[<v 2>{@;%a@]@;}@;" pp_stmt tstm;
      Format.fprintf fmt "@[<v 2>{@;%a@]@;}@]" pp_stmt fstm
    (* Control flow *)
    | S_mcs_if(tstm, fstm) ->
      Format.fprintf fmt "[%d] IF @[<v 0>" sp;
      Format.fprintf fmt "@[<v 2>{@;%a@]@;}@;" pp_stmt tstm;
      Format.fprintf fmt "@[<v 2>{@;%a@]@;}@]" pp_stmt fstm
    | S_mcs_loop(stm) ->
      Format.fprintf fmt "[%d] LOOP %a" sp pp_block stm
    | S_mcs_loop_left(stm) ->
      Format.fprintf fmt "[%d] LOOP_LEFT %a" sp pp_block stm
    (* Constants *)
    | S_mcs_nop -> Format.fprintf fmt "[%d] NOP" sp
    | S_mcs_const (ty, v) ->
      Format.fprintf fmt "[%d] PUSH %a %a" sp pp_typ ty pp_constant v
    (* Integers *)
    | S_mcs_abs_int -> Format.fprintf fmt "[%d] ABS_INT" sp
    | S_mcs_neg_int -> Format.fprintf fmt "[%d] NEG_INT" sp
    | S_mcs_neg_nat -> Format.fprintf fmt "[%d] NEG_NAT" sp
    | S_mcs_add_intint -> Format.fprintf fmt "[%d] ADD_INT_INT" sp
    | S_mcs_add_intnat -> Format.fprintf fmt "[%d] ADD_INT_NAT" sp
    | S_mcs_add_natint -> Format.fprintf fmt "[%d] ADD_NAT_INT" sp
    | S_mcs_add_natnat -> Format.fprintf fmt "[%d] ADD_NAT_NAT" sp
    | S_mcs_sub_int    -> Format.fprintf fmt "[%d] SUB_INT" sp
    | S_mcs_mul_intint -> Format.fprintf fmt "[%d] MUL_INT_INT" sp
    | S_mcs_mul_intnat -> Format.fprintf fmt "[%d] MUL_INT_NAT" sp
    | S_mcs_mul_natint -> Format.fprintf fmt "[%d] MUL_NAT_INT" sp
    | S_mcs_mul_natnat  -> Format.fprintf fmt "[%d] MUL_NAT_NAT" sp
    | S_mcs_ediv_intint -> Format.fprintf fmt "[%d] EDIV_INT_INT" sp
    | S_mcs_ediv_intnat -> Format.fprintf fmt "[%d] EDIV_INT_NAT" sp
    | S_mcs_ediv_natint -> Format.fprintf fmt "[%d] EDIV_NAT_INT" sp
    | S_mcs_ediv_natnat  -> Format.fprintf fmt "[%d] EDIV_NAT_NAT" sp
    (* Integer comparisons *)
    | S_mcs_eq -> Format.fprintf fmt "[%d] EQ" sp
    | S_mcs_neq -> Format.fprintf fmt "[%d] NEQ" sp
    | S_mcs_gt -> Format.fprintf fmt "[%d] GT" sp
    | S_mcs_ge -> Format.fprintf fmt "[%d] GE" sp
    | S_mcs_lt -> Format.fprintf fmt "[%d] LT" sp
    | S_mcs_le -> Format.fprintf fmt "[%d] LE" sp
    (* Integer casts *)
    | S_mcs_is_nat -> Format.fprintf fmt "[%d] ISNAT" sp
    | S_mcs_int_nat -> Format.fprintf fmt "[%d] INT" sp
    (* Bitwise and Shift *)
    | S_mcs_not_int -> Format.fprintf fmt "[%d] NOT_INT" sp
    | S_mcs_not_nat -> Format.fprintf fmt "[%d] NOT_NAT" sp
    | S_mcs_and_nat -> Format.fprintf fmt "[%d] AND_NAT" sp
    | S_mcs_and_int_nat -> Format.fprintf fmt "[%d] AND_INT_NAT" sp
    | S_mcs_or_nat -> Format.fprintf fmt "[%d OR_NAT" sp
    | S_mcs_lsl_nat -> Format.fprintf fmt "[%d] LSL" sp
    | S_mcs_lsr_nat -> Format.fprintf fmt "[%d] LSR" sp
    | S_mcs_xor_nat -> Format.fprintf fmt "[%d] XOR" sp
    (* Booleans *)
    | S_mcs_not -> Format.fprintf fmt "[%d] NOT" sp
    | S_mcs_and -> Format.fprintf fmt "[%d] AND" sp
    | S_mcs_or  -> Format.fprintf fmt "[%d] OR" sp
    | S_mcs_xor -> Format.fprintf fmt "[%d] XOR" sp
    (* Mutez *)
    | S_mcs_add_tez -> Format.fprintf fmt "[%d] ADD_TEZ" sp
    | S_mcs_sub_tez -> Format.fprintf fmt "[%d] SUB_TEZ" sp
    | S_mcs_mul_nattez -> Format.fprintf fmt "[%d] MUL_NAT_TEZ" sp
    | S_mcs_mul_teznat -> Format.fprintf fmt "[%d] MUL_TEZ_NAT" sp
    | S_mcs_ediv_tez -> Format.fprintf fmt "[%d] DIV_TEZ" sp
    | S_mcs_ediv_teznat -> Format.fprintf fmt "[%d] DIV_TEZNAT" sp
    | S_mcs_amount -> Format.fprintf fmt "[%d] AMOUNT" sp
    | S_mcs_balance -> Format.fprintf fmt "[%d] BALANCE" sp
    (* Timestamps *)
    | S_mcs_add_seconds_to_timestamp -> Format.fprintf fmt "[%d] ADD_INT_TS" sp
    | S_mcs_add_timestamp_to_seconds -> Format.fprintf fmt "[%d] ADD_TS_INT" sp
    | S_mcs_sub_timestamp_seconds -> Format.fprintf fmt "[%d] SUB_TS_INT" sp
    | S_mcs_diff_timestamps -> Format.fprintf fmt "[%d] SUB_TS" sp
    | S_mcs_now -> Format.fprintf fmt "[%d] NOW" sp
    (* Strings *)
    | S_mcs_concat_string -> Format.fprintf fmt "[%d] CONCAT_STRING" sp
    | S_mcs_concat_string_pair -> Format.fprintf fmt "[%d] CONCAT_STRING_PAIR" sp
    | S_mcs_string_size -> Format.fprintf fmt "[%d] STRING_SIZE" sp
    | S_mcs_slice_string -> Format.fprintf fmt "[%d] STRING_SLICE" sp
    (* Bytes *)
    | S_mcs_concat_bytes -> Format.fprintf fmt "[%d] CONCAT_BYTES" sp
    | S_mcs_concat_bytes_pair -> Format.fprintf fmt "[%d] CONCAT_BYTES_PAIR" sp
    | S_mcs_bytes_size -> Format.fprintf fmt "[%d] BYTES_SIZE" sp
    | S_mcs_slice_bytes -> Format.fprintf fmt "[%d] BYTES_SLICE" sp
    | S_mcs_blake2b -> Format.fprintf fmt "[%d] BLAKE2B" sp
    | S_mcs_sha256 -> Format.fprintf fmt "[%d] SHA256" sp
    | S_mcs_sha512 -> Format.fprintf fmt "[%d] SHA512" sp
    | S_mcs_sha3 -> Format.fprintf fmt "[%d] SHA3" sp
    | S_mcs_keccak -> Format.fprintf fmt "[%d] KECCAK" sp
    (* Polymorphic comparison *)
    | S_mcs_compare _ -> Format.fprintf fmt "[%d] COMPARE" sp
    (* Specials *)
    | S_mcs_seq (stm1, stm2) ->
      Format.fprintf fmt "@[<v 0>%a@;%a@]" pp_stmt stm1 pp_stmt stm2
    | S_mcs_failwith _ ->
      Format.fprintf fmt "[%d] FAILWITH" sp
    | S_mcs_wrapped_before { stm; assertion } ->
      Format.fprintf fmt "@[<v 0>%a@;%a@]" pp_stmt assertion pp_stmt stm
    | S_mcs_wrapped_after { stm; assertion } ->
      Format.fprintf fmt "@[<v 0>%a@;%a@]" pp_stmt stm pp_stmt assertion
    | S_mcs_unpair _ -> Format.fprintf fmt "[%d] UNPAIR" sp
    (* Domain specific *)
    | S_mcs_contract (ty, entrypoint) ->
      Format.fprintf fmt "[%d] CONTRACT %%%s %a" sp entrypoint pp_typ ty
    | S_mcs_address ->
      Format.fprintf fmt "[%d] ADDRESS" sp
    | S_mcs_self entrypoint ->
      Format.fprintf fmt "[%d] SELF%s" sp
        (match entrypoint with
         | "default" -> ""
         | _ -> " %" ^ entrypoint
        )
    | S_mcs_source ->
      Format.fprintf fmt "[%d] SOURCE" sp
    | S_mcs_sender ->
      Format.fprintf fmt "[%d] SENDER" sp
    | S_mcs_transfer_tokens ->
      Format.fprintf fmt "[%d] TRANSFER_TOKENS" sp
    | S_mcs_create_account ->
      Format.fprintf fmt "[%d] CREATE_ACCOUNT" sp
    | S_mcs_create_contract _ ->
      Format.fprintf fmt "[%d] CREATE_CONTRACT ..." sp
    | S_mcs_create_contract_2 _ ->
      Format.fprintf fmt "[%d] CREATE_CONTRACT2 ..." sp
    | S_mcs_exec ->
      Format.fprintf fmt "[%d] EXEC" sp
    | S_mcs_apply ->
      Format.fprintf fmt "[%d] APPLY" sp
    | S_mcs_implicit_account ->
      Format.fprintf fmt "[%d] IMPLICIT_ACCOUNT" sp
    | S_mcs_set_delegate ->
      Format.fprintf fmt "[%d] SET_DELEGATE" sp
    | S_mcs_check_signature ->
      Format.fprintf fmt "[%d] CHECK_SIGNATURE" sp
    | S_mcs_steps_to_quota ->
      Format.fprintf fmt "[%d] STEPS_TO_QUOTA" sp
    | S_mcs_lambda stm ->
      Format.fprintf fmt "[%d] LAMBDA %a" sp pp_block stm
    | S_mcs_hash_key ->
      Format.fprintf fmt "[%d] HASH_KEY" sp
    | S_mcs_pack ->
      Format.fprintf fmt "[%d] PACK" sp
    | S_mcs_unpack ->
      Format.fprintf fmt "[%d] UNPACK" sp
    | S_mcs_chain_id ->
      Format.fprintf fmt "[%d] CHAIN_ID" sp
    | S_mcs_level ->
      Format.fprintf fmt "[%d] LEVEL" sp
    | S_mcs_self_address ->
      Format.fprintf fmt "[%d] SELF_ADDRESS" sp
    | S_mcs_voting_power ->
      Format.fprintf fmt "[%d] VOTING_POWER" sp
    | S_mcs_total_voting_power ->
      Format.fprintf fmt "[%d] TOTAL_VOTING_POWER" sp


let () =
  register_program_pp (fun default fmt prg ->
      match prg.prog_kind with
      | Ast.P_michelson { contract; _ } ->
        Ast.pp_contract fmt contract;
      | _ -> default fmt prg
    );
  register_stmt_pp (fun default fmt stmt ->
      match stmt.skind with
      | S_michelson_instr { instr; bef; _ } ->
        pp_instr fmt (List.length bef,instr)
      (* Inter-contracts *)
      | S_mcs_inter_exec { contract = None; _ } ->
        Format.fprintf fmt "IC_CALL ?"
      | S_mcs_inter_exec { contract = Some _; address; _ } ->
        Format.fprintf fmt "IC_CALL %a"
          (Format.pp_print_option
             Format.pp_print_string) address;
      | S_mcs_inter_operation_list_iter { operations } ->
        Format.fprintf fmt "IC_ITER_OPERATION (%a)"
          pp_expr operations
      | S_mcs_inter_exec_transfer { transfer } ->
        Format.fprintf fmt "IC_EXEC_TRANSFER (%a)" pp_expr transfer
      | S_mcs_inter_exec_transfer_address {
          address; parameter; amount
        } ->
        Format.fprintf fmt "IC_EXEC_TRANSFER_ADDRESS { %a; %a; %a }"
          pp_expr address
          pp_expr parameter
          pp_expr amount
      | S_mcs_store_add_contracts contracts ->
        Format.fprintf fmt "@[<v 0>STORE_ADD_CONTRACTS@;";
        List.iter (fun (contract, _context) ->
            Format.fprintf fmt "\t%a@;" pp_contract contract
          ) contracts;
        Format.fprintf fmt "@]"
      | S_mcs_store_exec_contract
          { address: Common.Addresses.Address.t;
            parameter: expr; _ } ->
        Format.fprintf fmt "STORE_EXEC_CONTRACT(%a, %a)"
          (format Common.Addresses.Address.print)
          address
          pp_expr parameter
      | _ -> default fmt stmt
    )
