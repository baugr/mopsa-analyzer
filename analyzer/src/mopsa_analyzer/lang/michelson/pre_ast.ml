open Mopsa
open Mopsa_michelson_parser

module Al_ast = Mopsa_michelson_al_parser.Al_ast

type typ = Al_ast.typ
let pp_typ = Al_ast.pp_typ

type annot = Var_annot of string
type stack = (annot option * typ) list

type constant_kind =
  | Unit
  | Bool of bool
  | Mutez of Z.t
  | Nat of Z.t
  | Int of Z.t
  | String of string
  | Bytes of string
  | Timestamp of Z.t
  (* TODO *)
  | Signature of string
  | Key of string
  | Key_hash of string
  | Address of string * string
  (* Containers *)
  | Option of constant option
  | Pair of constant * constant
  | Union of union
  | List of constant list
  | Set of constant list
  | Map of (constant * constant) list
  | Lambda of statement

and union =
  | L of constant
  | R of constant

and constant =
  {
    ctyp: typ;
    ckind : constant_kind;
  }

and statement_kind =
  | S_drop of typ
  | S_dup of typ
  | S_swap of typ * typ
  | S_const of constant
  | S_cons_pair of typ * typ
  | S_car of typ
  | S_cdr of typ
  | S_unpair of typ
  | S_cons_some of typ
  | S_cons_none of typ
  | S_if_none of typ * statement * statement
  | S_cons_left of typ
  | S_cons_right of typ
  | S_if_left of typ * statement * statement
  | S_cons_list of typ
  | S_nil of typ
  | S_if_cons of typ * statement * statement
  | S_list_map of statement
  | S_list_iter of statement
  | S_list_size of typ
  | S_empty_set of typ
  | S_set_iter of statement
  | S_set_mem of typ
  | S_set_update of typ
  | S_set_size
  | S_empty_map of typ * typ
  | S_map_map of statement
  | S_map_iter of statement
  | S_map_mem of typ * typ
  | S_map_get of typ * typ
  | S_map_update of typ * typ
  | S_map_size of typ * typ
  | S_empty_big_map
  | S_big_map_mem
  | S_big_map_get
  | S_big_map_update
  | S_concat_string
  | S_concat_string_pair
  | S_slice_string
  | S_string_size
  | S_concat_bytes
  | S_concat_bytes_pair
  | S_slice_bytes
  | S_bytes_size
  | S_add_seconds_to_timestamp
  | S_add_timestamp_to_seconds
  | S_sub_timestamp_seconds
  | S_diff_timestamps
  | S_add_tez
  | S_sub_tez
  | S_mul_teznat
  | S_mul_nattez
  | S_ediv_teznat
  | S_ediv_tez
  | S_or
  | S_and
  | S_xor
  | S_not
  | S_is_nat
  | S_neg_nat
  | S_neg_int
  | S_abs_int
  | S_int_nat
  | S_add_intint
  | S_add_intnat
  | S_add_natint
  | S_add_natnat
  | S_sub_int
  | S_mul_intint
  | S_mul_intnat
  | S_mul_natint
  | S_mul_natnat
  | S_ediv_intint
  | S_ediv_intnat
  | S_ediv_natint
  | S_ediv_natnat
  | S_lsl_nat
  | S_lsr_nat
  | S_or_nat
  | S_and_nat
  | S_and_int_nat
  | S_xor_nat
  | S_not_nat
  | S_not_int
  | S_seq of statement * statement
  | S_if of statement * statement
  | S_loop of statement
  | S_loop_left of statement
  | S_dip of statement
  | S_exec
  | S_apply
  | S_lambda of statement
  | S_failwith of typ
  | S_nop
  | S_compare of typ
  | S_eq
  | S_neq
  | S_lt
  | S_gt
  | S_le
  | S_ge
  | S_address
  | S_contract of typ * string
  | S_transfer_tokens
  | S_create_account
  | S_implicit_account
  | S_create_contract of statement
  | S_create_contract_2 of { parameter_ty: typ;
                             storage_ty: typ;
                             code : statement }
  | S_set_delegate
  | S_now
  | S_balance
  | S_check_signature
  | S_hash_key
  | S_pack
  | S_unpack
  | S_blake2b
  | S_sha256
  | S_sha512
  | S_steps_to_quota
  | S_source
  | S_sender
  | S_self of string
  | S_amount
  | S_dig of int
  | S_dug of int
  | S_dipn of int * statement
  | S_dropn of int
  | S_chain_id
  | S_level
  | S_self_address
  | S_voting_power
  | S_total_voting_power
  | S_keccak
  | S_sha3
  | S_wrapped_before of { stmt: statement; assertion: Al_ast.statement }
  | S_wrapped_after of { stmt: statement; assertion: Al_ast.statement }

and statement =
  {
    skind : statement_kind;
    bef : stack;
    aft: stack;
    srange: range
  }

let pp_stack_item fmt v =
  match v with
  | Some (Var_annot s), typ -> Format.fprintf fmt "%a %%%s" Al_ast.pp_typ typ s
  | None, typ -> Format.fprintf fmt "%a" Al_ast.pp_typ typ

let pp_stack_ty fmt v = (Format.pp_print_list
                        ~pp_sep:(fun fmt () -> Format.fprintf fmt "; ")
                        pp_stack_item) fmt v

let stack_get_type stack index =
  let len = List.length stack in
  if index < 0 || index >= len then
    Mopsa.panic "invalid access to item %d of stack: %a" index pp_stack_ty stack
  else
    let (_, typ) = List.nth stack index in
    typ

(* get stack type from index relative to stack pointer *)
let stack_get_type_spi stack spi =
  let index = (List.length stack - 1) - spi in
  stack_get_type stack index

let rec pp_constant fmt constant =
  match constant.ckind with
  | Unit -> Format.fprintf fmt "unit"
  | Bool b -> Format.fprintf fmt "%b" b
  | Mutez z -> Format.fprintf fmt "(%a:mutez)" Z.pp_print z
  | Int i -> Format.fprintf fmt "(%a:int)" Z.pp_print i
  | Nat i -> Format.fprintf fmt "(%a:nat)" Z.pp_print i
  | Pair (x, y) -> Format.fprintf fmt "(%a, %a)" pp_constant x pp_constant y
  | Option (Some x) -> Format.fprintf fmt "Some (%a)" pp_constant x
  | Option None -> Format.fprintf fmt "None"
  | List (l) ->
    Format.fprintf fmt "{ %a }"
      (Format.pp_print_list pp_constant
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ";"))
      l
  | Set l ->
    Format.fprintf fmt "{ %a }"
      (Format.pp_print_list
         pp_constant ~pp_sep:(fun fmt () -> Format.fprintf fmt ";"))
      l
  | Map l ->
    Format.fprintf fmt "{ %a }"
      (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ";")
         (fun fmt (k, v) -> Format.fprintf fmt "Elt %a %a"
             pp_constant k
             pp_constant v
         ))
      l
  | String s -> Format.fprintf fmt "\"%s\"" s
  | Bytes s -> Format.fprintf fmt "0x%s" s
  | Address (s, e) -> Format.fprintf fmt "\"%s%%%s\"" s e
  | Timestamp t -> Format.fprintf fmt "%a" Z.pp_print t
  | Union (L v) -> Format.fprintf fmt "(Left %a)" pp_constant v
  | Union (R v) -> Format.fprintf fmt "(Right %a)" pp_constant v
  | Key_hash kh -> Format.fprintf fmt "\"%s\"" kh
  | Lambda stm -> Format.fprintf fmt "{ %a }" pp_statement stm
  | _ -> Exceptions.panic "TODO"

and pp_statement fmt stmt =
  match stmt.skind with
  | S_drop _ -> Format.fprintf fmt "DROP"
  | S_dup _ -> Format.fprintf fmt "DUP"
  | S_swap _ -> Format.fprintf fmt "SWAP"
  | S_const(v) -> Format.fprintf fmt "PUSH %a" pp_constant v
  | S_cons_pair _ -> Format.fprintf fmt "CONS_PAIR"
  | S_car _ -> Format.fprintf fmt "CAR"
  | S_cdr _ -> Format.fprintf fmt "CDR"
  | S_unpair _ -> Format.fprintf fmt "UNPAIR"
  | S_cons_some _ -> Format.fprintf fmt "CONS_SOME"
  | S_cons_none _ -> Format.fprintf fmt "CONS_NONE"
  | S_if_none(ty, tstm, fstm) ->
    Format.fprintf fmt "IF_NONE { %a } { %a }" pp_statement tstm pp_statement fstm
  | S_cons_left ty -> Format.fprintf fmt "LEFT %a" pp_typ ty
  | S_cons_right ty -> Format.fprintf fmt "RIGHT %a" pp_typ ty
  | S_if_left (_, tstm, fstm) ->
    Format.fprintf fmt "IF_LEFT { %a } { %a }" pp_statement tstm pp_statement fstm
  | S_cons_list _ -> Format.fprintf fmt "CONS_LIST"
  | S_nil(ty) -> Format.fprintf fmt "NIL"
  | S_if_cons (ty, tstm, fstm) ->
    Format.fprintf fmt "IF_CONS { %a } { %a }" pp_statement tstm pp_statement fstm
  | S_list_map (stm) ->
    Format.fprintf fmt "LIST_MAP { %a }" pp_statement stm
  | S_list_iter (stm) ->
    Format.fprintf fmt "LIST_ITER { %a }" pp_statement stm
  | S_list_size _ -> Format.fprintf fmt "LIST_SIZE"
  | S_empty_set _ -> Format.fprintf fmt "EMPTY_SET"
  | S_set_iter (stm) -> Format.fprintf fmt "SET_ITER { %a }" pp_statement stm
  | S_set_mem _ -> Format.fprintf fmt "SET_MEM"
  | S_set_update _ -> Format.fprintf fmt "SET_UPDATE"
  | S_set_size -> Format.fprintf fmt "SET_SIZE"
  | S_empty_map _ -> Format.fprintf fmt "EMPTY_MAP"
  | S_map_map (stm) -> Format.fprintf fmt "MAP_MAP { %a }" pp_statement stm
  | S_map_iter (stm) -> Format.fprintf fmt "MAP_ITER { %a } " pp_statement stm
  | S_map_mem _ -> Format.fprintf fmt "MAP_MEM"
  | S_map_get _ -> Format.fprintf fmt "MAP_GET"
  | S_map_update _ -> Format.fprintf fmt "MAP_UPDATE"
  | S_map_size _ -> Format.fprintf fmt "MAP_SIZE"
  | S_empty_big_map -> Format.fprintf fmt "EMPTY_BIG_MAP"
  | S_big_map_mem -> Format.fprintf fmt "BIG_MAP_MEM"
  | S_big_map_get -> Format.fprintf fmt "BIG_MAP_GET"
  | S_big_map_update -> Format.fprintf fmt "BIG_MAP_UPDATE"
  | S_concat_string -> Format.fprintf fmt "CONCAT_STRING"
  | S_concat_string_pair -> Format.fprintf fmt "CONCAT_STRING_PAIR"
  | S_slice_string -> Format.fprintf fmt "SLICE_STRING"
  | S_string_size -> Format.fprintf fmt "STRING_SIZE"
  | S_concat_bytes -> Format.fprintf fmt "CONCAT_BYTES"
  | S_concat_bytes_pair -> Format.fprintf fmt "CONCAT_BYTES_PAIR"
  | S_slice_bytes -> Format.fprintf fmt "SLICE_BYTES"
  | S_bytes_size -> Format.fprintf fmt "BYTES_SIZE"
  | S_add_seconds_to_timestamp -> Format.fprintf fmt "ADD_SECONDS_TO_TIMESTAMP"
  | S_add_timestamp_to_seconds -> Format.fprintf fmt "ADD_TIMESTAMP_TO_SECONDS"
  | S_sub_timestamp_seconds -> Format.fprintf fmt "SUB_TIMESTAMP_SECONDS"
  | S_diff_timestamps -> Format.fprintf fmt "DIFF_TIMESTAMPS"
  | S_add_tez -> Format.fprintf fmt "ADD_TEZ"
  | S_sub_tez -> Format.fprintf fmt "SUB_TEZ"
  | S_mul_teznat -> Format.fprintf fmt "MUL_TEZNAT"
  | S_mul_nattez -> Format.fprintf fmt "MUL_NATTEZ"
  | S_ediv_teznat -> Format.fprintf fmt "EDIV_TEZNAT"
  | S_ediv_tez -> Format.fprintf fmt "EDIV_TEZ"
  | S_or -> Format.fprintf fmt "OR"
  | S_and -> Format.fprintf fmt "AND"
  | S_xor -> Format.fprintf fmt "XOR"
  | S_not -> Format.fprintf fmt "NOT"
  | S_is_nat -> Format.fprintf fmt "IS_NAT"
  | S_neg_nat -> Format.fprintf fmt "NEG_NAT"
  | S_neg_int -> Format.fprintf fmt "NEG_INT"
  | S_abs_int -> Format.fprintf fmt "ABS_INT"
  | S_int_nat -> Format.fprintf fmt "INT_NAT"
  | S_add_intint -> Format.fprintf fmt "ADD_INTINT"
  | S_add_intnat -> Format.fprintf fmt "ADD_INTNAT"
  | S_add_natint -> Format.fprintf fmt "ADD_NATINT"
  | S_add_natnat -> Format.fprintf fmt "ADD_NATNAT"
  | S_sub_int -> Format.fprintf fmt "SUB_INT"
  | S_mul_intint -> Format.fprintf fmt "MUL_INTINT"
  | S_mul_intnat -> Format.fprintf fmt "MUL_INTNAT"
  | S_mul_natint -> Format.fprintf fmt "MUL_NATINT"
  | S_mul_natnat -> Format.fprintf fmt "MUL_NATNAT"
  | S_ediv_intint -> Format.fprintf fmt "EDIV_INTINT"
  | S_ediv_intnat -> Format.fprintf fmt "EDIV_INTNAT"
  | S_ediv_natint -> Format.fprintf fmt "EDIV_NATINT"
  | S_ediv_natnat -> Format.fprintf fmt "EDIV_NATNAT"
  | S_lsl_nat -> Format.fprintf fmt "LSL_NAT"
  | S_lsr_nat -> Format.fprintf fmt "LSR_NAT"
  | S_or_nat -> Format.fprintf fmt "OR_NAT"
  | S_and_nat -> Format.fprintf fmt "AND_NAT"
  | S_and_int_nat -> Format.fprintf fmt "AND_INT_NAT"
  | S_xor_nat -> Format.fprintf fmt "XOR_NAT"
  | S_not_nat -> Format.fprintf fmt "NOT_NAT"
  | S_not_int -> Format.fprintf fmt "NOT_INT"
  | S_seq(stm1, stm2) -> Format.fprintf fmt "%a@;%a" pp_statement stm1
                         pp_statement stm2
  | S_if(stm1, stm2) -> Format.fprintf fmt "IF { %a } { %a }" pp_statement stm1
                        pp_statement
                        stm2
  | S_loop (stm) -> Format.fprintf fmt "LOOP { %a } " pp_statement stm
  | S_loop_left (stm) -> Format.fprintf fmt "LOOP_LEFT { %a } " pp_statement stm
  | S_dip (stm) -> Format.fprintf fmt "DIP { %a }" pp_statement stm
  | S_exec -> Format.fprintf fmt "EXEC"
  | S_apply -> Format.fprintf fmt "APPLY"
  | S_lambda stm -> Format.fprintf fmt "LAMBDA { %a }" pp_statement stm
  | S_failwith typ -> Format.fprintf fmt "FAILWITH"
  | S_nop -> Format.fprintf fmt "NOP"
  | S_compare _ -> Format.fprintf fmt "COMPARE"
  | S_eq -> Format.fprintf fmt "EQ"
  | S_neq -> Format.fprintf fmt "NEQ"
  | S_lt -> Format.fprintf fmt "LT"
  | S_gt -> Format.fprintf fmt "GT"
  | S_le -> Format.fprintf fmt "LE"
  | S_ge -> Format.fprintf fmt "GE"
  | S_address -> Format.fprintf fmt "ADDRESS"
  | S_contract (typ, entrypoint) -> Format.fprintf fmt "CONTRACT %s %a" entrypoint pp_typ typ
  | S_transfer_tokens -> Format.fprintf fmt "TRANSFER_TOKENS"
  | S_create_account -> Format.fprintf fmt "CREATE_ACCOUNT"
  | S_implicit_account -> Format.fprintf fmt "IMPLICIT_ACCOUNT"
  | S_create_contract(stm) -> Format.fprintf fmt "CREATE_CONTRACT { %a }"
                              pp_statement stm
  | S_create_contract_2 { code; _ } ->
    Format.fprintf fmt "CREATE_CONTRACT { %a } " pp_statement code
  | S_set_delegate -> Format.fprintf fmt "SET_DELEGATE"
  | S_now -> Format.fprintf fmt "NOW"
  | S_balance -> Format.fprintf fmt "BALANCE"
  | S_check_signature -> Format.fprintf fmt "CHECK_SIGNATURE"
  | S_hash_key -> Format.fprintf fmt "HASH_KEY"
  | S_pack -> Format.fprintf fmt "PACK"
  | S_unpack -> Format.fprintf fmt "UNPACK"
  | S_blake2b -> Format.fprintf fmt "BLAKE2B"
  | S_sha256 -> Format.fprintf fmt "SHA256"
  | S_sha512 -> Format.fprintf fmt "SHA512"
  | S_steps_to_quota -> Format.fprintf fmt "STEPS_TO_QUOTA"
  | S_source -> Format.fprintf fmt "SOURCE"
  | S_sender -> Format.fprintf fmt "SENDER"
  | S_self entrypoint -> Format.fprintf fmt "SELF(%S)" entrypoint
  | S_amount -> Format.fprintf fmt "AMOUNT"
  | S_dig(n) -> Format.fprintf fmt "DIG(%d)" n
  | S_dug(n) -> Format.fprintf fmt "DUG(%d)" n
  | S_dipn(n, stm) -> Format.fprintf fmt "DIP(%d) { %a} " n pp_statement stm
  | S_dropn(n) -> Format.fprintf fmt "DROP(%d)" n
  | S_chain_id -> Format.fprintf fmt "CHAIN_ID"
  | S_level -> Format.fprintf fmt "LEVEL"
  | S_self_address -> Format.fprintf fmt "SELF_ADDRESS"
  | S_voting_power -> Format.fprintf fmt "VOTING_POWER"
  | S_total_voting_power -> Format.fprintf fmt "TOTAL_VOTING_POWER"
  | S_keccak -> Format.fprintf fmt "KECCAK"
  | S_sha3 -> Format.fprintf fmt "SHA3"
  | S_wrapped_before { stmt; assertion } ->
    Format.fprintf fmt "%a@;"
      (Al_ast.pp_stmt ~typed:false ~range:false)
      assertion;
    Format.fprintf fmt "%a" pp_statement stmt
  | S_wrapped_after { stmt; assertion } ->
    Format.fprintf fmt "%a@;"
      (Al_ast.pp_stmt ~typed:false ~range:false)
      assertion;
    Format.fprintf fmt "%a" pp_statement stmt

and pp_stack fmt stack =
  Format.fprintf fmt "@[<hv 2>";
  List.iter (pp_constant fmt) stack;
  Format.fprintf fmt "@]"

