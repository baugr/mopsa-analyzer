# @configure_input@

##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################


.PHONY: all tests clean distclean

all:
	$(MAKE) -C utils
ifneq (@disable_c@,yes)
	$(MAKE) -C parsers/c
	$(MAKE) -C parsers/c_stubs
endif
ifneq (@disable_python@,yes)
	$(MAKE) -C parsers/python
endif
ifneq (@disable_michelson@,yes)
	$(MAKE) -C libs/mopsa_tezos
	$(MAKE) -C parsers/michelson
	$(MAKE) -C parsers/michelson_al
endif
	$(MAKE) -C parsers/universal
	$(MAKE) -C analyzer


tests:
	$(MAKE) -C analyzer tests
ifneq (@disable_michelson@,yes)
	$(MAKE) -C tests/michelson
	$(MAKE) -C tests/michelson tests
endif


clean:
	$(MAKE) -C utils clean
ifneq (@disable_c@,yes)
	$(MAKE) -C parsers/c clean
	$(MAKE) -C parsers/c_stubs clean
endif
ifneq (@disable_python@,yes)
	$(MAKE) -C parsers/python clean
endif
ifneq (@disable_michelson@,yes)
	$(MAKE) -C parsers/michelson clean
	$(MAKE) -C parsers/michelson_al clean
	$(MAKE) -C libs/mopsa_tezos clean
	$(MAKE) -C tests/michelson clean
endif
	$(MAKE) -C parsers/universal clean
	$(MAKE) -C analyzer clean


MLFILES_TO_INSTALL = $(foreach dir,@libs_to_install@,$(wildcard $(dir)/lib/*.cm* $(dir)/lib/*.a $(dir)/lib/*.so))

install:
	$(MAKE) -C analyzer install
ifneq (@disable_c@,yes)
	$(MAKE) -C parsers/c install
endif
	@echo -e "$(INSTALLMSG)     mopsa library"
	$(QUIET) $(OCAMLFIND) install mopsa META $(MLFILES_TO_INSTALL)

uninstall:
	$(MAKE) -C analyzer uninstall
ifneq (@disable_c@,yes)
	$(MAKE) -C parsers/c uninstall
endif
	@echo -e "$(UINSTALLMSG)     mopsa library"
	$(QUIET) $(OCAMLFIND) remove mopsa


distclean: clean
	rm -f 	Makefile			\
		make/constants.mk               \
		make/targets.mk			\
		make/version.mk			\
		utils/Makefile			\
		utils/src/version.ml		\
		libs/mopsa_tezos/Makefile       \
		parsers/universal/Makefile	\
		parsers/c/Makefile	 	\
		parsers/c_stubs/Makefile 	\
		parsers/python/Makefile		\
		parsers/michelson/Makefile      \
		parsers/michelson_al/Makefile 	\
		analyzer/Makefile		\
		META

include make/constants.mk
